# README #

This repository contains the code discussed in the paper *Programming with Classical Types*.

* The folder `parigot` contains the Isabelle formalisation of Parigot's lambda-mu-calculus.

* The folder `muml` contains the formalisation of the extended calculus and the prototype programming language: *muML*.

* The folder `muml/ocaml` contains the OCaml code for the *muML* interpreter. 


To use the interpreter:

  - install rlwrap
  - install opam (the interpreter was implemented using OCaml version 4.02.3)
  - run `opam install ocamlbuild` and `opam install menhir`
  - run `muml/ocaml/make` to build the interpreter
  - to start the repl, from the folder `muml/ocaml/`, type `./main`

The syntax of the *muML* language can be found in `muml/ocaml/syntax.txt`. Example programs can be found in `muml/ocaml/tests/peirceslaw.lmt`
  

### Authors ###

* Cristina Matache		cm780@cam.ac.uk
* Victor B. F. Gomes		victorborgesfg@gmail.com
* Dominic P. Mulligan		dominic.p.mulligan@gmail.com