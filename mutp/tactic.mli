exception Tactic of string

(** Tactics *)

type tactic

val assm_tac : int -> tactic
val zero_nat_intro_tac : tactic
val true_bool_intro_tac : tactic
val false_bool_intro_tac : tactic

val succ_nat_intro_tac : tactic

val imp_intro_tac : tactic
val imp_elim_tac : Kernel.t -> tactic

val conj_intro_tac : tactic
val conj_left_elim_tac : Kernel.t -> tactic
val conj_right_elim_tac : Kernel.t -> tactic

val disj_left_intro_tac : tactic
val disj_right_intro_tac : tactic
val disj_elim_tac : Kernel.t -> Kernel.t -> tactic

val bool_if_elim_tac : tactic

val nat_nrec_elim_tac : tactic

val mu_label_intro_tac : int -> tactic
val mu_top_intro_tac : tactic

val all_elim_tac : Kernel.t -> Kernel.t -> tactic
val all_intro_tac : tactic

(** Proof states *)

val conjecture : Kernel.t -> unit
val apply : int -> tactic -> unit
val qed : unit -> Kernel.tthm
val print_proof_state : unit -> unit