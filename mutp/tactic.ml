open Util

exception Tactic of string

(** Proof states *)

type proof_state =
  | Assm of Kernel.tctxt * Kernel.cctxt * int
  | FalseI of Kernel.tctxt * Kernel.cctxt
  | TrueI of Kernel.tctxt * Kernel.cctxt
  | ZeroI of Kernel.tctxt * Kernel.cctxt
  | SuccI of proof_state * (Kernel.tthm -> Kernel.tthm)
  | ImpI of proof_state * (Kernel.tthm -> Kernel.tthm)
  | ImpE of proof_state * proof_state * (Kernel.tthm -> Kernel.tthm -> Kernel.tthm)
  | ConjI of proof_state * proof_state * (Kernel.tthm -> Kernel.tthm -> Kernel.tthm)
  | ConjE1 of proof_state * (Kernel.tthm -> Kernel.tthm)
  | ConjE2 of proof_state * (Kernel.tthm -> Kernel.tthm)
  | DisjI1 of proof_state * (Kernel.tthm -> Kernel.tthm)
  | DisjI2 of proof_state * (Kernel.tthm -> Kernel.tthm)
  | DisjE of proof_state * proof_state * proof_state * (Kernel.tthm -> Kernel.tthm -> Kernel.tthm -> Kernel.tthm)
  | IfE of proof_state * proof_state * proof_state * (Kernel.tthm -> Kernel.tthm -> Kernel.tthm -> Kernel.tthm)
  | NRecE of proof_state * proof_state * proof_state * (Kernel.tthm -> Kernel.tthm -> Kernel.tthm -> Kernel.tthm)
  | MuTopI of proof_state * (Kernel.tthm -> Kernel.tthm)
  | MuLabelI of proof_state * (Kernel.tthm -> Kernel.tthm)
  | AllI of string * proof_state * (Kernel.tthm -> Kernel.tthm)
  | AllE of proof_state * (Kernel.tthm -> Kernel.tthm)
  | ToProve of Kernel.tctxt * Kernel.cctxt * Kernel.t

let open_proof : proof_state option ref = ref None

(** Pretty printing *)

let rec depth_type (t : Kernel.t) : int =
  if Kernel.is_bool_t t || Kernel.is_nat_t t || Kernel.is_bot_t t || Kernel.is_var_t t then
    1
  else if Kernel.is_all_t t then
    let t = Kernel.dest_all_t t in
      1 + depth_type t
  else if Kernel.is_sum_t t then
    let (l, r) = Kernel.dest_sum_t t in
      1 + depth_type l + depth_type r
  else if Kernel.is_arrow_t t then
    let (l, r) = Kernel.dest_arrow_t t in
      1 + depth_type l + depth_type r
  else if Kernel.is_prod_t t then
    let (l, r) = Kernel.dest_prod_t t in
      1 + depth_type l + depth_type r
  else assert false (* nothing else *)

let rec pretty_type (ctxt : string list) (t : Kernel.t) : string =
  if Kernel.is_bool_t t then
    "bool"
  else if Kernel.is_nat_t t then
    "nat"
  else if Kernel.is_bot_t t then
    "bot"
  else if Kernel.is_var_t t then
    let v = Kernel.dest_var_t t in
      if v < List.length ctxt then
        List.nth ctxt v
      else "?"
  else if Kernel.is_arrow_t t then
    let (dom, rng) = Kernel.dest_arrow_t t in
      pretty_bracket_type ctxt dom ^ " --> " ^ pretty_bracket_type ctxt rng
  else if Kernel.is_sum_t t then
    let (dom, rng) = Kernel.dest_sum_t t in
      pretty_bracket_type ctxt dom ^ " \\/ " ^ pretty_bracket_type ctxt rng
  else if Kernel.is_prod_t t then
    let (dom, rng) = Kernel.dest_prod_t t in
      pretty_bracket_type ctxt dom ^ " /\\ " ^ pretty_bracket_type ctxt rng
  else if Kernel.is_all_t t then
    let b = Kernel.dest_all_t t in
    let f = gen_fresh_forall () in
      "forall " ^ f ^ ". " ^ pretty_bracket_type (f::ctxt) b
  else assert false (* nothing else *)
and pretty_bracket_type (ctxt : string list) (t : Kernel.t) : string =
  if depth_type t > 1 then
    "(" ^ pretty_type ctxt t ^ ")"
  else pretty_type ctxt t

let pretty_tctxt (ctxt : string list) (t : Kernel.tctxt) : string =
  String.concat "\n" ((List.mapi (fun i -> fun t -> string_of_int i ^ ": " ^ pretty_bracket_type ctxt t)) t)

let pretty_cctxt (ctxt : string list) (t : Kernel.cctxt) : string =
  String.concat "\n" ((List.mapi (fun i -> fun t -> string_of_int i ^ ": " ^ pretty_bracket_type ctxt t)) t)

let pretty_proof_state (state : proof_state) : string =
  let rec go counter ctxt state =
    match state with
      | ToProve (gamma, delta, t) ->
        (("=======================================\n" ^
          "Lambda assms:\n" ^ pretty_tctxt ctxt gamma ^ "\n\n" ^
          "Mu assms:\n" ^ pretty_cctxt ctxt delta ^ "\n" ^
          "----------[In (term) goal: " ^ string_of_int counter ^ "]----------\n") ^
          "|- " ^ pretty_type ctxt t, 1 + counter)
      | SuccI (d, t) -> go counter ctxt d
      | ImpI (d, t) -> go counter ctxt d
      | ImpE (d1, d2, t) ->
          let (pp, cnt) = go counter ctxt d1 in
          let (qq, cnt) = go cnt ctxt d2 in
            (pp ^ "\n\n" ^ qq, cnt)
      | ConjI (d1, d2, t) ->
          let (pp, cnt) = go counter ctxt d1 in
          let (qq, cnt) = go cnt ctxt d2 in
            (pp ^ "\n\n" ^ qq, cnt)
      | ConjE1 (d, t) -> go counter ctxt d
      | ConjE2 (d, t) -> go counter ctxt d
      | DisjI1 (d, t) -> go counter ctxt d
      | DisjI2 (d, t) -> go counter ctxt d
      | DisjE (d1, d2, d3, t) ->
          let (pp, cnt) = go counter ctxt d1 in
          let (qq, cnt) = go cnt ctxt d2 in
          let (rr, cnt) = go cnt ctxt d3 in
            (pp ^ "\n\n" ^ qq ^ "\n\n" ^ rr, cnt)
      | IfE (d1, d2, d3, t) ->
          let (pp, cnt) = go counter ctxt d1 in
          let (qq, cnt) = go cnt ctxt d2 in
          let (rr, cnt) = go cnt ctxt d3 in
            (pp ^ "\n\n" ^ qq ^ "\n\n" ^ rr, cnt)
      | NRecE (d1, d2, d3, t) ->
          let (pp, cnt) = go counter ctxt d1 in
          let (qq, cnt) = go cnt ctxt d2 in
          let (rr, cnt) = go cnt ctxt d3 in
            (pp ^ "\n\n" ^ qq ^ "\n\n" ^ rr, cnt)
      | MuTopI (d, t) -> go counter ctxt d
      | MuLabelI (d, t) -> go counter ctxt d
      | AllI (n, d, t) -> go counter (n::ctxt) d
      | AllE (d, t) -> go counter (List.tl ctxt) d
      | _ -> ("", counter)
  in let (fst, snd) = go 0 [] state in fst

let open_subgoals (state : proof_state) : int =
  let rec go counter state =
    match state with
      | ToProve (gamma, delta, t) -> 1 + counter
      | SuccI (d, t) -> go counter d
      | ImpI (d, t) -> go counter d
      | ImpE (d1, d2, t) ->
          let cnt = go counter d1 in
          let cnt = go cnt d2 in
            cnt
      | ConjI (d1, d2, t) ->
          let cnt = go counter d1 in
          let cnt = go cnt d2 in
            cnt
      | ConjE1 (d, t) -> go counter d
      | ConjE2 (d, t) -> go counter d
      | DisjI1 (d, t) -> go counter d
      | DisjI2 (d, t) -> go counter d
      | DisjE (d1, d2, d3, t) ->
          let cnt = go counter d1 in
          let cnt = go cnt d2 in
          let cnt = go cnt d3 in
            cnt
      | IfE (d1, d2, d3, t) ->
          let cnt = go counter d1 in
          let cnt = go cnt d2 in
          let cnt = go cnt d3 in
            cnt
      | NRecE (d1, d2, d3, t) ->
          let cnt = go counter d1 in
          let cnt = go cnt d2 in
          let cnt = go cnt d3 in
            cnt
      | MuTopI (d, t) -> go counter d
      | MuLabelI (d, t) -> go counter d
      | AllI (n, d, t) -> go counter d
      | AllE (d, t) -> go counter d
      | _ -> counter
  in go 0 state

let conjecture t =
  match !open_proof with
    | None ->
      if Kernel.closed t then
        open_proof := Some (ToProve ([], [], t))
      else raise (Tactic "conjecture: cannot conjecture open formula")
    | _ -> raise (Tactic "conjecture: cannot conjecture whilst proof in progress")

let rec qed' state =
  match state with
    | Assm (gamma, delta, i) -> Kernel.assm gamma delta i
    | FalseI (gamma, delta) -> Kernel.false_bool_intro gamma delta
    | TrueI (gamma, delta) -> Kernel.true_bool_intro gamma delta
    | ZeroI (gamma, delta) -> Kernel.zero_nat_intro gamma delta
    | SuccI (d, t) -> t (qed' d)
    | ImpI (d, t) -> t (qed' d)
    | ImpE (l, r, t) -> t (qed' l) (qed' r)
    | ConjI (l, r, t) -> t (qed' l) (qed' r)
    | ConjE1 (d, t) -> t (qed' d)
    | ConjE2 (d, t) -> t (qed' d)
    | DisjI1 (d, t) -> t (qed' d)
    | DisjI2 (d, t) -> t (qed' d)
    | DisjE (d1, d2, d3, t) -> t (qed' d1) (qed' d2) (qed' d3)
    | IfE (d1, d2, d3, t) -> t (qed' d1) (qed' d2) (qed' d3)
    | NRecE (d1, d2, d3, t) -> t (qed' d1) (qed' d2) (qed' d3)
    | MuTopI (d, t) -> t (qed' d)
    | MuLabelI (d, t) -> t (qed' d)
    | AllI (n, d, t) -> t (qed' d)
    | AllE (d, t) -> t (qed' d)
    | ToProve (gamma, delta, t) -> raise (Tactic "qed': cannot qed incomplete proof")

let qed () =
  match !open_proof with
    | None -> raise (Tactic "qed: no open proof")
    | Some prf ->
      let thm  = qed' prf in
      let trm  = Kernel.term_of thm in
      let _    = open_proof := None in
      let typ  = R.Types.type_term (fun x -> assert false)
                  (fun x -> assert false) (Kernel.term_of thm) in
      let _    = Printf.printf ("Finished proof of conjecture:\n\n%s\n") (Util.pretty_type [] typ) in
        thm

(** Tactics *)

type tactic = Kernel.tctxt -> Kernel.cctxt -> Kernel.t -> proof_state

let apply' (goal : int) (tac : tactic) (s : proof_state) =
  let rec go (counter : int) (tac : tactic) (s : proof_state) =
    match s with
      | ToProve (gamma, delta, t) ->
          if counter = goal then (tac gamma delta t, 1+counter) else (ToProve (gamma, delta, t), 1+counter)
      | SuccI (d, t) ->
          let (d, cnt) = go counter tac d in
            (SuccI (d, t), cnt)
      | ImpI (d, t) ->
          let (d, cnt) = go counter tac d in
            (ImpI (d, t), cnt)
      | ImpE (d1, d2, t) ->
          let (d1, cnt) = go counter tac d1 in
          let (d2, cnt) = go cnt tac d2 in
            (ImpE (d1, d2, t), cnt)
      | ConjI (d1, d2, t) ->
          let (d1, cnt) = go counter tac d1 in
          let (d2, cnt) = go cnt tac d2 in
            (ConjI (d1, d2, t), cnt)
      | ConjE1 (d, t) ->
          let (d, cnt) = go counter tac d in
            (ConjE1 (d, t), cnt)
      | ConjE2 (d, t) ->
          let (d, cnt) = go counter tac d in
            (ConjE2 (d, t), cnt)
      | DisjI1 (d, t) ->
          let (d, cnt) = go counter tac d in
            (DisjI1 (d, t), cnt)
      | DisjI2 (d, t) ->
          let (d, cnt) = go counter tac d in
            (DisjI2 (d, t), cnt)
      | DisjE (d1, d2, d3, t) ->
          let (d1, cnt) = go counter tac d1 in
          let (d2, cnt) = go cnt tac d2 in
          let (d3, cnt) = go cnt tac d3 in
            (DisjE (d1, d2, d3, t), cnt)
      | IfE (d1, d2, d3, t) ->
          let (d1, cnt) = go counter tac d1 in
          let (d2, cnt) = go cnt tac d2 in
          let (d3, cnt) = go cnt tac d3 in
            (IfE (d1, d2, d3, t), cnt)
      | NRecE (d1, d2, d3, t) ->
          let (d1, cnt) = go counter tac d1 in
          let (d2, cnt) = go cnt tac d2 in
          let (d3, cnt) = go cnt tac d3 in
            (NRecE (d1, d2, d3, t), cnt)
      | AllI (n, d, t) ->
          let (d, cnt) = go counter tac d in
            (AllI (n, d, t), cnt)
      | AllE (d, t) ->
          let (d, cnt) = go counter tac d in
            (AllE (d, t), cnt)
      | MuTopI (d, t) ->
          let (d, cnt) = go counter tac d in
            (MuTopI (d, t), cnt)
      | MuLabelI (d, t) ->
          let (d, cnt) = go counter tac d in
            (MuLabelI (d, t), cnt)
      | s -> (s, counter)
  in go 0 tac s

let apply (goal : int) (tac : tactic) =
  match !open_proof with
    | None -> raise (Tactic "apply: invalid proof context")
    | Some ps ->
      let (qs, counter) = apply' goal tac ps in
        (open_proof := Some qs;
         if open_subgoals qs = 0 then
           Printf.printf "\nProof complete!\n"
         else ())

let print_proof_state () =
  match !open_proof with
    | None -> raise (Tactic "print_proof_state: no open proof")
    | Some p -> Printf.printf "%s\n" (pretty_proof_state p)

let assm_tac i gamma delta t =
  if i < List.length gamma then
    if List.nth gamma i = t then
      Assm (gamma, delta, i)
    else raise (Tactic "assm_tac: cannot match assumption")
  else raise (Tactic "assm_tac: no assumption")

let false_bool_intro_tac gamma delta t =
  if t = Kernel.mk_bool_t then
    FalseI (gamma, delta)
  else raise (Tactic "false_bool_intro_tac: not a boolean goal")

let true_bool_intro_tac gamma delta t =
  if t = Kernel.mk_bool_t then
    TrueI (gamma, delta)
  else raise (Tactic "true_bool_intro_tac: not a boolean goal")

let zero_nat_intro_tac gamma delta t =
  if t = Kernel.mk_nat_t then
    ZeroI (gamma, delta)
  else raise (Tactic "zero_nat_intro_tac: not a natural goal")

let succ_nat_intro_tac gamma delta t =
  if t = Kernel.mk_nat_t then
    SuccI (ToProve (gamma, delta, Kernel.mk_nat_t), Kernel.succ_nat_intro)
  else raise (Tactic "succ_nat_intro_tac: not a natural goal")

let conj_intro_tac gamma delta t =
  try
    let (left, right) = Kernel.dest_prod_t t in
      ConjI (ToProve (gamma, delta, left), ToProve (gamma, delta, right), Kernel.conj_intro)
  with Kernel.Kernel _ -> raise (Tactic "conj_intro_tac: goal not a conjunction")

let conj_left_elim_tac right gamma delta t =
  ConjE1 (ToProve (gamma, delta, Kernel.mk_prod_t t right), Kernel.conj_left_elim)

let conj_right_elim_tac left gamma delta t =
  ConjE2 (ToProve (gamma, delta, Kernel.mk_prod_t left t), Kernel.conj_right_elim)

let disj_left_intro_tac gamma delta t =
  try
    let (left, right) = Kernel.dest_sum_t t in
      DisjI1 (ToProve (gamma, delta, left), Kernel.disj_left_intro right)
  with Kernel.Kernel _ -> raise (Tactic "disj_left_intro_tac: goal not a disjunction")

let disj_right_intro_tac gamma delta t =
  try
    let (left, right) = Kernel.dest_sum_t t in
      DisjI2 (ToProve (gamma, delta, left), Kernel.disj_right_intro right)
  with Kernel.Kernel _ -> raise (Tactic "disj_right_intro_tac: goal not a disjunction")

let imp_elim_tac dom gamma delta t =
    ImpE (ToProve (gamma, delta, Kernel.mk_arrow_t dom t),
      ToProve (gamma, delta, dom), Kernel.imp_elim)

let bool_if_elim_tac gamma delta t =
  IfE (ToProve (gamma, delta, Kernel.mk_bool_t),
    ToProve (gamma, delta, t),
    ToProve (gamma, delta, t),
    Kernel.bool_if_elim)

let disj_elim_tac left right gamma delta t =
  DisjE (ToProve (gamma, delta, Kernel.mk_sum_t left right),
    ToProve (Kernel.shift_tctxt gamma left, delta, t),
    ToProve (Kernel.shift_tctxt gamma right, delta, t),
    Kernel.disj_elim t)

let imp_intro_tac gamma delta t =
  try
    let (dom, rng) = Kernel.dest_arrow_t t in
      ImpI (ToProve (Kernel.shift_tctxt gamma dom, delta, rng), Kernel.imp_intro)
  with Kernel.Kernel _ -> raise (Tactic "imp_intro_tac: goal not a function")

let nat_nrec_elim_tac gamma delta t =
  NRecE (ToProve (gamma, delta, t),
      ToProve (gamma, delta, Kernel.mk_arrow_t Kernel.mk_nat_t (Kernel.mk_arrow_t t t)),
      ToProve (gamma, delta, Kernel.mk_nat_t),
      Kernel.nat_nrec_elim)

let mu_label_intro_tac i gamma delta t =
  let delta = Kernel.shift_cctxt delta t in
    if i < List.length delta then
      let s = List.nth delta i in
        MuLabelI (ToProve (gamma, delta, s),
          fun d -> Kernel.mu_intro (Kernel.label_intro i d))
    else raise (Tactic "mu_label_intro_tac: nth")

let mu_top_intro_tac gamma delta t =
  MuTopI (ToProve (gamma, Kernel.shift_cctxt delta t, Kernel.mk_bot_t),
    fun d -> Kernel.mu_intro (Kernel.top_intro d))

let all_intro_tac gamma delta t =
  try
    let t = Kernel.dest_all_t t in
    let gamma = Kernel.type_shift_tctxt gamma 0 in
    let delta = Kernel.type_shift_cctxt delta 0 in
    let f = gen_fresh_forall () in
      AllI (f, ToProve (gamma, delta, t), Kernel.all_intro)
  with Kernel.Kernel _ -> raise (Tactic "all_intro_tac: goal not a universal")

let all_elim_tac repl spec gamma delta t =
  let t' = Kernel.subst repl 0 spec in
    if t' = t then
      AllE (ToProve (gamma, delta, repl), Kernel.all_elim spec)
    else raise (Tactic "all_elim_tac: cannot change type")
    