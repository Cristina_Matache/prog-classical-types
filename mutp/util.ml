module RS = Reduction.Syntax
module R = Reduction

let fresh_count : int ref = ref 0

let gen_fresh_forall () =
  let t = "a" ^ string_of_int (!fresh_count) in
  let _ = fresh_count := !fresh_count + 1 in
    t

let gen_fresh_mu () =
  let t = "m" ^ string_of_int (!fresh_count) in
  let _ = fresh_count := !fresh_count + 1 in
    t

let gen_fresh_lambda () =
  let t = "x" ^ string_of_int (!fresh_count) in
  let _ = fresh_count := !fresh_count + 1 in
    t

let rec int_of_arith_nat (a : R.Arith.nat) : int =
  match a with
    | R.Arith.Zero_nat -> 0
    | R.Arith.Suc m -> 1 + int_of_arith_nat m

let rec arith_nat_of_int (i : int) : R.Arith.nat =
  if i < 0 then
    raise (Failure "arith_nat_of_int: negative argument")
  else if i = 0 then
    R.Arith.Zero_nat
  else R.Arith.Suc (arith_nat_of_int (i - 1))

let rec depth_term (t : RS.trm) : int =
  match t with
    | RS.LVar a -> 1
    | RS.Lbd (ty, b) -> 1 + depth_term b
    | RS.App (l, r) -> 1 + depth_term l + depth_term r
    | RS.Mu (ty, c) -> 1 + depth_cmd c
    | RS.TAbs t -> 1 + depth_term t
    | RS.TApp (t, ty) -> 1 + depth_term t
    | RS.Zero -> 1
    | RS.S t -> 1 + depth_term t
    | RS.Nrec (ty, z, s, t) -> 1 + depth_term z + depth_term s + depth_term t
    | RS.TTrue -> 1
    | RS.TFalse -> 1
    | RS.If (ty, c, t, f) -> 1 + depth_term c + depth_term t + depth_term f
    | RS.Pair (l, r, ty) -> 1 + depth_term l + depth_term r
    | RS.Proj1 t -> 1 + depth_term t
    | RS.Proj2 t -> 1 + depth_term t
    | RS.Inl (ty, t) -> 1 + depth_term t
    | RS.Inr (ty, t) -> 1 + depth_term t
    | RS.Case (ty, c, l, r) -> 1 + depth_term c + depth_term l + depth_term r
and depth_cmd (c : RS.cmd) : int =
  match c with
    | RS.MVar (a, t) -> 1 + depth_term t
    | RS.Top t -> 1 + depth_term t

let rec depth_type (t : RS.typea) : int =
  match t with
    | RS.TVar a -> 1
    | RS.TAll t -> 1 + depth_type t
    | RS.Nat -> 1
    | RS.Bool -> 1
    | RS.Bottom -> 1
    | RS.Prod (l, r) -> 1 + depth_type l + depth_type r
    | RS.Sum (l, r) -> 1 + depth_type l + depth_type r
    | RS.Fun (l, r) -> 1 + depth_type l + depth_type r

let rec pretty_type (ctxt : string list) (t : RS.typea) : string =
  match t with
    | RS.TVar a ->
      let v = int_of_arith_nat a in
        if v < List.length ctxt then
          List.nth ctxt v
        else raise (Failure "pretty_type: dangling pointer")
    | RS.TAll t ->
      let f = gen_fresh_forall () in
        "forall " ^ f ^ ". " ^ pretty_bracket_type (f::ctxt) t
    | RS.Nat -> "nat"
    | RS.Bool -> "bool"
    | RS.Bottom -> "bot"
    | RS.Prod (l, r) ->
      pretty_bracket_type ctxt l ^ " * " ^ pretty_bracket_type ctxt r
    | RS.Sum (l, r) ->
      pretty_bracket_type ctxt l ^ " + " ^ pretty_bracket_type ctxt r
    | RS.Fun (l, r) ->
      pretty_bracket_type ctxt l ^ " -> " ^ pretty_bracket_type ctxt r
and pretty_bracket_type (ctxt : string list) (t : RS.typea) : string =
  if depth_type t > 1 then
    "(" ^ pretty_type ctxt t ^ ")"
  else pretty_type ctxt t

let rec pretty_term (t_ctxt : string list) (m_ctxt : string list) (l_ctxt : string list) (t : RS.trm) : string =
  match t with
    | RS.LVar a ->
      let i = int_of_arith_nat a in
        if i < List.length l_ctxt then
          List.nth l_ctxt i
        else raise (Failure "pretty_term: dangling pointer")
    | RS.Lbd (ty, b) ->
      let f = gen_fresh_lambda () in
      let b = pretty_bracket_term t_ctxt m_ctxt (f::l_ctxt) b in
        "fun (" ^ f ^ " : " ^ pretty_type t_ctxt ty ^ ") -> \n  " ^ b
    | RS.App (l, r) ->
      let l = pretty_bracket_term t_ctxt m_ctxt l_ctxt l in
      let r = pretty_bracket_term t_ctxt m_ctxt l_ctxt r in
        l ^ " " ^ r
    | RS.Mu (ty, c) ->
      let f = gen_fresh_mu () in
      let c = pretty_bracket_cmd t_ctxt (f::m_ctxt) l_ctxt c in
        "bind (" ^ f ^ " : " ^ pretty_type t_ctxt ty ^ ") -> \n  " ^ c
    | RS.TAbs t ->
      let f = gen_fresh_forall () in
        "tabs (" ^ f ^ ") -> \n  " ^ pretty_bracket_term (f::t_ctxt) m_ctxt l_ctxt t
    | RS.TApp (t, ty) ->
        pretty_bracket_term t_ctxt m_ctxt l_ctxt t ^ "[" ^ pretty_type t_ctxt ty ^ "]"
    | RS.Zero -> "Zero"
    | RS.S t -> "S " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t
    | RS.Nrec (ty, z, s, t) -> assert false
    | RS.TTrue -> "True"
    | RS.TFalse -> "False"
    | RS.If (ty, c, t, f) ->
      let ty = pretty_type t_ctxt ty in
      let c  = pretty_term t_ctxt m_ctxt l_ctxt c in
      let t  = pretty_term t_ctxt m_ctxt l_ctxt t in
      let f  = pretty_term t_ctxt m_ctxt l_ctxt f in
        "(if " ^ c ^ " then " ^ t ^ " else " ^ f ^ " end : " ^ ty ^ ")"
    | RS.Pair (l, r, ty) ->
        "{" ^ pretty_term t_ctxt m_ctxt l_ctxt l ^ ", " ^ pretty_term t_ctxt m_ctxt l_ctxt r ^ "} : " ^ pretty_type t_ctxt ty
    | RS.Proj1 t -> "proj1 " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t
    | RS.Proj2 t -> "proj2 " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t
    | RS.Inl (ty, t) -> "(inl " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t ^ " : " ^ pretty_type t_ctxt ty ^ ")"
    | RS.Inr (ty, t) -> "(inr " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t ^ " : " ^ pretty_type t_ctxt ty ^ ")"
    | RS.Case (ty, c, l, r) ->
      let f = gen_fresh_lambda () in
      let g = gen_fresh_lambda () in
        "case : " ^ pretty_bracket_type t_ctxt ty ^ " " ^ pretty_term t_ctxt m_ctxt l_ctxt c ^ " of inl(" ^ f ^ ") -> "
          ^ pretty_term t_ctxt m_ctxt (f::l_ctxt) l ^ " | inr(" ^ g ^ ") -> " ^ pretty_term t_ctxt m_ctxt (g::l_ctxt) r ^ " end"
and pretty_cmd (t_ctxt : string list) (m_ctxt : string list) (l_ctxt : string list) (c : RS.cmd) : string =
  match c with
    | RS.MVar (a, t) ->
      let i = int_of_arith_nat a in
        if i < List.length m_ctxt then
          "[" ^ List.nth m_ctxt i ^ "]. " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t
        else raise (Failure "pretty_cmd: dangling pointer")
    | RS.Top t ->
        "[abort]. " ^ pretty_bracket_term t_ctxt m_ctxt l_ctxt t
and pretty_bracket_term (t_ctxt : string list) (m_ctxt : string list) (l_ctxt : string list) (t : RS.trm) : string =
  if depth_term t > 1 then
    "(" ^ pretty_term t_ctxt m_ctxt l_ctxt t ^ ")"
  else pretty_term t_ctxt m_ctxt l_ctxt t
and pretty_bracket_cmd (t_ctxt : string list) (m_ctxt : string list) (l_ctxt : string list) (c : RS.cmd) : string =
  if depth_cmd c > 1 then
    "(" ^ pretty_cmd t_ctxt m_ctxt l_ctxt c ^ ")"
  else pretty_cmd t_ctxt m_ctxt l_ctxt c