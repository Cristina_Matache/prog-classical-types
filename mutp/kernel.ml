(** Utilities *)

open Util

module R = Reduction

let subst t d r = R.DeBruijn.type_subst t (arith_nat_of_int d) r

exception Kernel of string

(** Formulae *)

type t = R.Syntax.typea

let mk_nat_t = R.Syntax.Nat
let mk_bool_t = R.Syntax.Bool
let mk_bot_t = R.Syntax.Bottom
let mk_prod_t l r = R.Syntax.Prod (l, r)
let mk_sum_t l r = R.Syntax.Sum (l, r)
let mk_arrow_t l r = R.Syntax.Fun (l, r)
let mk_var_t i = R.Syntax.TVar (arith_nat_of_int i)
let mk_all_t t = R.Syntax.TAll t

let dest_arrow_t t =
  match t with
    | R.Syntax.Fun (l, r) -> (l, r)
    | _ -> raise (Kernel "dest_arrow_t: not an arrow")

let dest_sum_t t =
  match t with
    | R.Syntax.Sum (l, r) -> (l, r)
    | _ -> raise (Kernel "dest_sum_t: not a sum")

let dest_prod_t t =
  match t with
    | R.Syntax.Prod (l, r) -> (l, r)
    | _ -> raise (Kernel "dest_prod_t: not a product")

let dest_all_t t =
  match t with
    | R.Syntax.TAll t -> t
    | _ -> raise (Kernel "dest_all_t: not a universal quantifier")

let dest_var_t t =
  match t with
    | R.Syntax.TVar i -> int_of_arith_nat i
    | _ -> raise (Kernel "dest_var_t: not a variable")

let is_nat_t t =
  match t with
    | R.Syntax.Nat -> true
    | _ -> false

let is_bool_t t =
  match t with
    | R.Syntax.Bool -> true
    | _ -> false

let is_bot_t t =
  match t with
    | R.Syntax.Bottom -> true
    | _ -> false

let is_arrow_t t =
  match t with
    | R.Syntax.Fun (l, r) -> true
    | _ -> false

let is_sum_t t =
  match t with
    | R.Syntax.Sum (l, r) -> true
    | _ -> false

let is_prod_t t =
  match t with
    | R.Syntax.Prod (l, r) -> true
    | _ -> false

let is_all_t t =
  match t with
    | R.Syntax.TAll t -> true
    | _ -> false

let is_var_t t =
  match t with
    | R.Syntax.TVar i -> true
    | _ -> false  

let closed t =
  let rec go binder_count t =
    match t with
      | R.Syntax.Prod (l, r) -> go binder_count l && go binder_count r
      | R.Syntax.Sum (l, r) -> go binder_count l && go binder_count r
      | R.Syntax.Fun (l, r) -> go binder_count l && go binder_count r
      | R.Syntax.TAll d -> go (1+binder_count) d
      | R.Syntax.TVar a -> int_of_arith_nat a <= binder_count
      | _ -> true
  in go 0 t

(** Theorems *)

type tctxt = t list
type cctxt = t list

let rec arithnat_to_int (n : R.Arith.nat) : int = match n with
    | R.Arith.Zero_nat -> 0
    | R.Arith.Suc x -> 1 + (arithnat_to_int x)

let rec liftT_type (i : int) (ty : t) : t = match ty with
    | R.Syntax.TVar n -> if (arithnat_to_int n) >= i 
                         then R.Syntax.TVar (R.Arith.plus_nat n (R.Arith.Suc R.Arith.Zero_nat))
                         else R.Syntax.TVar n
    | R.Syntax.TAll x -> R.Syntax.TAll (liftT_type (i+1) x)
    | R.Syntax.Prod (x, y) -> R.Syntax.Prod (liftT_type i x, liftT_type i y)
    | R.Syntax.Sum (x, y) -> R.Syntax.Sum (liftT_type i x, liftT_type i y)
    | R.Syntax.Fun (x, y) -> R.Syntax.Fun (liftT_type i x, liftT_type i y)
    | x -> x

let rec dropT_type (i : int) (ty : t) : t = match ty with
    | R.Syntax.TVar n -> if (arithnat_to_int n) > i 
                         then R.Syntax.TVar (R.Arith.minus_nat n (R.Arith.Suc R.Arith.Zero_nat))
                         else R.Syntax.TVar n
    | R.Syntax.TAll x -> R.Syntax.TAll (dropT_type (i+1) x)
    | R.Syntax.Prod (x, y) -> R.Syntax.Prod (dropT_type i x, dropT_type i y)
    | R.Syntax.Sum (x, y) -> R.Syntax.Sum (dropT_type i x, dropT_type i y)
    | R.Syntax.Fun (x, y) -> R.Syntax.Fun (dropT_type i x, dropT_type i y)
    | x -> x

let shift_tctxt (t : tctxt) (e : t) : tctxt = e::t
let drop_tctxt (t : tctxt) : tctxt * t =
  match t with
    | x::xs -> (xs,x)
    | _ -> raise (Kernel "drop_tctxt: empty context")
let type_drop_tctxt (t : tctxt) (i : int) : tctxt =
     List.map (dropT_type i) t
let type_shift_tctxt (t : tctxt) (i : int) : tctxt =
     List.map (liftT_type i) t


let shift_cctxt (c : cctxt) (e : t) : cctxt = e::c
let drop_cctxt (t : cctxt) : cctxt * t =
  match t with
    | x::xs -> (xs,x)
    | _ -> raise (Kernel "drop_cctxt: empty context")
let type_drop_cctxt (t : cctxt) (i : int) : cctxt =
     List.map (dropT_type i) t
let type_shift_cctxt (t : cctxt) (i : int) : cctxt =
     List.map (liftT_type i) t

type trm = R.Syntax.trm
type cmd = R.Syntax.cmd

type tthm = TTheorem of tctxt * cctxt * trm * t
type cthm = CTheorem of tctxt * cctxt * cmd

let term_of t =
  match t with
    | TTheorem (gamma, delta, trm, ty) -> trm

let command_of t =
  match t with
    | CTheorem (gamma, delta, cmd) -> cmd

(*** Axioms *)

let assm gamma delta v =
  if v < List.length gamma then
    TTheorem (gamma, delta, R.Syntax.LVar (arith_nat_of_int v), List.nth gamma v)
  else raise (Kernel "assm: unbound variable")

let true_bool_intro gamma delta =
  TTheorem (gamma, delta, R.Syntax.TTrue, mk_bool_t)

let false_bool_intro gamma delta =
  TTheorem (gamma, delta, R.Syntax.TFalse, mk_bool_t)

let zero_nat_intro gamma delta =
  TTheorem (gamma, delta, R.Syntax.Zero, mk_nat_t)

(*** Inference rules *)

let succ_nat_intro t =
  match t with
    | TTheorem (gamma, delta, trm, t) ->
      if t = mk_nat_t then
        TTheorem (gamma, delta, R.Syntax.S trm, t)
      else raise (Kernel "succ_nat_intro: assumption not of nat type")

let bool_if_elim c t f =
  match (c, t, f) with
    | (TTheorem (gamma1, delta1, trm1, t1), TTheorem (gamma2, delta2, trm2, t2), TTheorem (gamma3, delta3, trm3, t3)) ->
        if t1 = mk_bool_t then
          if t2 = t3 then
            if gamma1 = gamma2 && gamma2 = gamma3 &&
                delta1 = delta2 && delta2 = delta3 then
              TTheorem (gamma1, delta1, R.Syntax.If (t2, trm1, trm2, trm3), t2)
            else raise (Kernel "bool_if_elim: differing contexts")
          else raise (Kernel "bool_if_elim: branches of differing types")
        else raise (Kernel "bool_if_elim: conditional not of boolean type")

let nat_nrec_elim z s e =
  match (e, z, s) with
    | (TTheorem (gamma1, delta1, trm1, t1), TTheorem (gamma2, delta2, trm2, t2), TTheorem (gamma3, delta3, trm3, t3)) ->
      if t1 = mk_nat_t then
        raise (Kernel "nat_nrec_elim: TODO")
      else raise (Kernel "nat_nrec_elim: scrutinee not of natural type")

let imp_elim l r =
  match (l, r) with
    | (TTheorem (gamma1, delta1, trm1, t1), TTheorem (gamma2, delta2, trm2, t2)) ->
       if gamma1 = gamma2 && delta1 = delta2 then
         try
           let (dom, rng) = dest_arrow_t t1 in
             if dom = t2 then
               TTheorem (gamma1, delta1, R.Syntax.App (trm1, trm2), rng)
             else raise (Kernel "imp_elim: mismatch in domain type")
         with Kernel _ -> raise (Kernel "imp_elim: not an arrow")
       else raise (Kernel "imp_elim: differing contexts")

let imp_intro t =
  match t with
    | TTheorem (gamma, delta, trm, rng) ->
      try
        let (gamma', dom) = drop_tctxt gamma in
          TTheorem (gamma', delta, R.Syntax.Lbd (dom, trm), mk_arrow_t dom rng)
      with Kernel _ -> raise (Kernel "imp_intro: context empty")

let conj_left_elim t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      try
        let (l, r) = dest_prod_t ty in
          TTheorem (gamma, delta, R.Syntax.Proj1 trm, l)
      with Kernel _ -> raise (Kernel "conj_left_elim: not a product")

let conj_right_elim t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      try
        let (l, r) = dest_prod_t ty in
          TTheorem (gamma, delta, R.Syntax.Proj2 trm, r)
      with Kernel _ -> raise (Kernel "conj_right_elim: not a product")

let conj_intro l r =
  match (l, r) with
    | (TTheorem (gamma1, delta1, trm1, ty1), TTheorem (gamma2, delta2, trm2, ty2)) ->
      if gamma1 = gamma2 && delta1 = delta2 then
        TTheorem (gamma1, delta1, R.Syntax.Pair (trm1, trm2, mk_prod_t ty1 ty2), mk_prod_t ty1 ty2)
      else raise (Kernel "conj_intro: differing contexts")

let disj_left_intro o t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
        TTheorem (gamma, delta, R.Syntax.Inl ((mk_sum_t ty o), trm), mk_sum_t ty o)

let disj_right_intro o t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
        TTheorem (gamma, delta, R.Syntax.Inr ((mk_sum_t o ty), trm), mk_sum_t o ty)

let disj_elim t c l r =
  match (c, l, r) with
    | (TTheorem (gamma1, delta1, trm1, ty1), TTheorem (gamma2, delta2, trm2, ty2), TTheorem (gamma3, delta3, trm3, ty3)) ->
      try
        let (left, right) = dest_sum_t ty1 in
          if delta1 = delta2 && delta2 = delta3 then
            if gamma2 = shift_tctxt gamma1 left && gamma3 = shift_tctxt gamma1 right then
              if ty2 = t && ty3 = t then
                TTheorem (gamma1, delta1, R.Syntax.Case (t, trm1, trm2, trm3), t)
              else raise (Kernel "disj_elim: incorrect types in branches")
            else raise (Kernel "disj_elim: incorrect context")
          else raise (Kernel "disj_elim: contexts do not match")
      with Kernel _ -> raise (Kernel "disj_elim: scrutinee not a sum")

let top_intro t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      if ty = mk_bot_t then
        CTheorem (gamma, delta, R.Syntax.Top trm)
      else raise (Kernel "top_intro: type is not bottom")

let mu_intro t =
  match t with
    | CTheorem (gamma, delta, trm) ->
        try
          let (delta', t) = drop_cctxt delta in
            TTheorem (gamma, delta', R.Syntax.Mu (t, trm), t)
        with Kernel _ -> raise (Kernel "mu_intro: context empty")

let label_intro i t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      if i < List.length delta then
        let t = List.nth delta i in
          if ty = t then
            CTheorem (gamma, delta, R.Syntax.MVar (arith_nat_of_int i, trm))
          else raise (Kernel "label_intro: types do not match")
      else raise (Kernel "label_intro: index larger than context")

let all_elim spec t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      try
        let ty = dest_all_t ty in
          TTheorem (gamma, delta, R.Syntax.TApp (trm, spec), R.DeBruijn.type_subst ty R.Arith.Zero_nat spec)
      with Kernel _ -> raise (Kernel "all_elim: not a universal")

let all_intro t =
  match t with
    | TTheorem (gamma, delta, trm, ty) ->
      let gamma = type_drop_tctxt gamma 0 in
      let delta = type_drop_cctxt delta 0 in
        TTheorem (gamma, delta, R.Syntax.TAbs trm, mk_all_t ty)
