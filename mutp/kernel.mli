type t

val subst : t -> int -> t -> t

exception Kernel of string

val mk_nat_t : t
val mk_bot_t : t
val mk_arrow_t : t -> t -> t
val mk_bool_t : t
val mk_prod_t : t -> t -> t
val mk_sum_t : t -> t -> t
val mk_var_t : int -> t
val mk_all_t : t -> t

val dest_arrow_t : t -> t * t
val dest_prod_t : t -> t * t
val dest_sum_t : t -> t * t
val dest_var_t : t -> int
val dest_all_t : t -> t

val is_nat_t : t -> bool
val is_bot_t : t -> bool
val is_arrow_t : t -> bool
val is_bool_t : t -> bool
val is_prod_t : t -> bool
val is_sum_t : t -> bool
val is_var_t : t -> bool
val is_all_t : t -> bool

val closed : t -> bool

type tctxt = t list
type cctxt = t list

val shift_tctxt : tctxt -> t -> tctxt
val drop_tctxt : tctxt -> (tctxt * t)
val type_drop_tctxt : tctxt -> int -> tctxt
val type_shift_tctxt : tctxt -> int -> tctxt

val shift_cctxt : cctxt -> t -> cctxt
val drop_cctxt : cctxt -> (cctxt * t)
val type_drop_cctxt : cctxt -> int -> cctxt
val type_shift_cctxt : cctxt -> int -> cctxt

type tthm
type cthm

type trm = Reduction.Syntax.trm
type cmd = Reduction.Syntax.cmd

val term_of : tthm -> trm
val command_of : cthm -> cmd

(** Axioms *)

val assm : tctxt -> cctxt -> int -> tthm

val zero_nat_intro  : tctxt -> cctxt -> tthm
val true_bool_intro  : tctxt -> cctxt -> tthm
val false_bool_intro : tctxt -> cctxt -> tthm

(** Inference rules *)

val succ_nat_intro : tthm -> tthm

val imp_intro : tthm -> tthm
val imp_elim : tthm -> tthm -> tthm

val conj_intro : tthm -> tthm -> tthm
val conj_left_elim : tthm -> tthm
val conj_right_elim : tthm -> tthm

val disj_left_intro : t -> tthm -> tthm
val disj_right_intro : t -> tthm -> tthm
val disj_elim : t -> tthm -> tthm -> tthm -> tthm

val bool_if_elim : tthm -> tthm -> tthm -> tthm

val nat_nrec_elim : tthm -> tthm -> tthm -> tthm

val mu_intro : cthm -> tthm
val label_intro : int -> tthm -> cthm
val top_intro : tthm -> cthm

val all_intro : tthm -> tthm
val all_elim : t -> tthm -> tthm
