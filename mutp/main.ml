let mk_neg_t t = Kernel.mk_arrow_t t Kernel.mk_bot_t
let mk_iff_t t u = Kernel.mk_prod_t (Kernel.mk_arrow_t t u) (Kernel.mk_arrow_t u t)

(** A proof that nat --> nat, testing implication and assumption *)
let id_nat_arrow_test () =
  let trm = Kernel.mk_arrow_t Kernel.mk_nat_t Kernel.mk_nat_t in
  let thm =
    Tactic.conjecture trm;
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 (Tactic.assm_tac 0);
    Tactic.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that bool --> (nat --> nat), testing if-then-else, and successor *)
let if_then_else_test () =
  let trm = Kernel.mk_arrow_t Kernel.mk_bool_t (Kernel.mk_arrow_t Kernel.mk_nat_t Kernel.mk_nat_t) in
  let thm =
    Tactic.conjecture trm;
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 (Tactic.bool_if_elim_tac);
      Tactic.apply 0 (Tactic.assm_tac 1);
      Tactic.apply 0 (Tactic.assm_tac 0);
      Tactic.apply 0 (Tactic.succ_nat_intro_tac);
      Tactic.apply 0 (Tactic.succ_nat_intro_tac);
      Tactic.apply 0 (Tactic.succ_nat_intro_tac);
      Tactic.apply 0 (Tactic.assm_tac 0);
    Tactic.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that !!nat --> nat, testing implication, and mu *)
let mu_test () =
  let trm = Kernel.mk_arrow_t (Kernel.mk_arrow_t (Kernel.mk_arrow_t Kernel.mk_nat_t Kernel.mk_bot_t) Kernel.mk_bot_t) Kernel.mk_nat_t in
  let thm =
    Tactic.conjecture trm;
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 Tactic.mu_top_intro_tac;
      Tactic.apply 0 (Tactic.imp_elim_tac (Kernel.mk_arrow_t Kernel.mk_nat_t Kernel.mk_bot_t));
      Tactic.apply 0 (Tactic.assm_tac 0);
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 (Tactic.mu_label_intro_tac 1);
      Tactic.apply 0 (Tactic.assm_tac 0);
    Tactic.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof of forall A. A -> A *)
let id_test () =
  let module K = Kernel in
  let module T = Tactic in
  let trm      = K.mk_all_t (K.mk_arrow_t (K.mk_var_t 0) (K.mk_var_t 0)) in
  let thm      =
    T.conjecture trm;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.imp_intro_tac;
      T.apply 0 (T.assm_tac 0);
    T.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that forall A. !!A --> A *)
let double_neg_elim_test () =
  let module K = Kernel in
  let trm      = K.mk_all_t (K.mk_arrow_t (K.mk_arrow_t (K.mk_arrow_t (K.mk_var_t 0) K.mk_bot_t) K.mk_bot_t) (K.mk_var_t 0)) in
  let thm      =
    Tactic.conjecture trm;
      Tactic.apply 0 Tactic.all_intro_tac;
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 Tactic.mu_top_intro_tac;
      Tactic.apply 0 (Tactic.imp_elim_tac (Kernel.mk_arrow_t (Kernel.mk_var_t 0) Kernel.mk_bot_t));
      Tactic.apply 0 (Tactic.assm_tac 0);
      Tactic.apply 0 Tactic.imp_intro_tac;
      Tactic.apply 0 (Tactic.mu_label_intro_tac 1);
      Tactic.apply 0 (Tactic.assm_tac 0);
    Tactic.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that forall A B. !(A --> !B) --> (A * B) *)
let impl_prod_test () =
  let module K = Kernel in
  let module T = Tactic in
  let trm      = K.mk_all_t (K.mk_all_t (K.mk_arrow_t (mk_neg_t (K.mk_arrow_t (K.mk_var_t 1) (mk_neg_t (K.mk_var_t 0)))) (K.mk_prod_t (K.mk_var_t 1) (K.mk_var_t 0)))) in
  let thm      =
    T.conjecture trm;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.imp_intro_tac;
      T.apply 0 T.mu_top_intro_tac;
      T.apply 0 (T.imp_elim_tac (K.mk_arrow_t (K.mk_var_t 1) (mk_neg_t (K.mk_var_t 0))));
      T.apply 0 (T.assm_tac 0);
      T.apply 0 T.imp_intro_tac;
      T.apply 0 T.imp_intro_tac;
      T.apply 0 (T.mu_label_intro_tac 1);
      T.apply 0 T.conj_intro_tac;
      T.apply 0 (T.assm_tac 1);
      T.apply 0 (T.assm_tac 0);
    T.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that forall A B C. ((A --> B --> C) --> (A * B --> C)) /\ ((A * B --> C) --> A --> B --> C) *)
let curry_uncurry_iff_test () =
  let module K = Kernel in
  let module T = Tactic in
  let trm      =
    K.mk_all_t (K.mk_all_t (K.mk_all_t (
      K.mk_prod_t
        (K.mk_arrow_t
          (K.mk_arrow_t
            (K.mk_var_t 2)
            (K.mk_arrow_t (K.mk_var_t 1) (K.mk_var_t 0)))
          (K.mk_arrow_t
            (K.mk_prod_t (K.mk_var_t 2) (K.mk_var_t 1))
              (K.mk_var_t 0)))
        (K.mk_arrow_t
          (K.mk_arrow_t
            (K.mk_prod_t (K.mk_var_t 2) (K.mk_var_t 1))
              (K.mk_var_t 0))
          (K.mk_arrow_t (K.mk_var_t 2) (K.mk_arrow_t (K.mk_var_t 1) (K.mk_var_t 0))))
    ))) in
  let thm =
    T.conjecture trm;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.all_intro_tac;
      T.apply 0 T.conj_intro_tac;
      T.apply 1 T.imp_intro_tac;
      T.apply 1 T.imp_intro_tac;
      T.apply 1 T.imp_intro_tac;
      T.apply 1 (T.imp_elim_tac (K.mk_prod_t (K.mk_var_t 2) (K.mk_var_t 1)));
      T.apply 2 T.conj_intro_tac;
      T.apply 3 (T.assm_tac 0);
      T.apply 2 (T.assm_tac 1);
      (* uncomment the following line to view active proof state at this point *)
      (* T.print_proof_state (); *)
      T.apply 1 (T.assm_tac 2);
      T.apply 0 T.imp_intro_tac;
      T.apply 0 T.imp_intro_tac;
      T.apply 0 (T.imp_elim_tac (K.mk_var_t 1));
      T.apply 1 (T.conj_right_elim_tac (K.mk_var_t 2));
      T.apply 1 (T.assm_tac 0);
      T.apply 0 (T.imp_elim_tac (K.mk_var_t 2));
      T.apply 0 (T.assm_tac 1);
      T.apply 0 (T.conj_left_elim_tac (K.mk_var_t 1));
      T.apply 0 (T.assm_tac 0);
    T.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that forall A B. ((A --> B) --> A) --> A *)
let peirces_law_test () =
  let module K = Kernel in
  let module T = Tactic in
  let trm      = K.mk_all_t (K.mk_all_t (K.mk_arrow_t (K.mk_arrow_t (K.mk_arrow_t (K.mk_var_t 1) (K.mk_var_t 0)) (K.mk_var_t 1)) (K.mk_var_t 1))) in
  let thm      =
    T.conjecture trm;
    T.apply 0 T.all_intro_tac;
    T.apply 0 T.all_intro_tac;
    T.apply 0 T.imp_intro_tac;
    T.apply 0 (T.mu_label_intro_tac 0);
    T.apply 0 (T.imp_elim_tac (K.mk_arrow_t (K.mk_var_t 1) (K.mk_var_t 0)));
    T.apply 0 (T.assm_tac 0);
    T.apply 0 T.imp_intro_tac;
    T.apply 0 (T.mu_label_intro_tac 1);
    T.apply 0 (T.assm_tac 0);
    T.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

(** A proof that forall A. A --> !A --> forall B. B *)
let contr_explosion_test () =
  let module K = Kernel in
  let module T = Tactic in
  let trm      = K.mk_all_t (K.mk_arrow_t (K.mk_var_t 0) (K.mk_arrow_t (mk_neg_t (K.mk_var_t 0)) (K.mk_all_t (K.mk_var_t 0)))) in
  let thm      =
    T.conjecture trm;
    T.apply 0 T.all_intro_tac;
    T.apply 0 T.imp_intro_tac;
    T.apply 0 T.imp_intro_tac;
    T.apply 0 T.all_intro_tac;
    T.apply 0 T.mu_top_intro_tac;
    T.apply 0 (T.imp_elim_tac (K.mk_var_t 1));
    T.apply 1 (T.assm_tac 1);
    T.apply 0 (T.assm_tac 0);
    T.qed ()
  in Printf.printf "Extracted MuML program:\n\n%s\n"
    (Util.pretty_term [] [] [] (Kernel.term_of thm))

let _ =
  id_nat_arrow_test ();
  if_then_else_test ();
  id_test ();
  mu_test ();
  double_neg_elim_test ();
  impl_prod_test ();
  curry_uncurry_iff_test ();
  peirces_law_test ();
  contr_explosion_test ()
