section \<open>Reduction relation\<close>

theory Reduction
  imports Substitution
begin
  
subsection \<open>Normal forms\<close>
  
fun is_nf :: "trm \<Rightarrow> bool" and
  is_nfC :: "cmd \<Rightarrow> bool" where
  "is_nfC (<c> v) = (is_val v \<and> (c = 0 \<longrightarrow> 0 \<in> fmv_trm v 0))" |
  "is_nfC (<\<top>> v) = is_val v" |
  "is_nf (\<mu> T : c) = is_nfC c" |
  "is_nf v = is_val v"

lemma is_nfD:
  "is_nf t \<Longrightarrow> is_val t \<or> (\<exists>U \<beta> v. t = (\<mu> U: (<\<beta>> v)) \<and> is_val v) \<or> (\<exists>U v. t = (\<mu> U:(<\<top>> v)) \<and> is_val v)"
  "is_nfC c \<Longrightarrow> (\<exists>U \<beta> v. c = (<\<beta>> v) \<and> (is_val v \<and> (\<beta> = 0 \<longrightarrow> 0 \<in> fmv_trm v 0))) \<or> (\<exists>U v. c = <\<top>> v \<and> is_val v)"
   by (induct rule: is_nf_is_nfC.induct) auto

lemma is_nfD2:
  "is_nf t \<Longrightarrow> is_val t \<or> (\<exists>U c. t = \<mu> U: c \<and> is_nfC c)"
  "is_nfC c \<Longrightarrow> (\<exists>U \<beta> v. c = (<\<beta>> v) \<and> is_val v) \<or> (\<exists>U v. c = <\<top>> v \<and> is_val v)"
   by (induct rule: is_nf_is_nfC.induct) auto

lemma is_nfI: "is_val t \<or> (\<exists>U \<beta> v. \<beta> \<noteq> 0 \<and> t = (\<mu> U: (<\<beta>> v)) \<and> is_val v) \<or> (\<exists>U v. t = (\<mu> U:(<\<top>> v)) \<and> is_val v) \<Longrightarrow> is_nf t"
  by (induct t; auto)
    
lemma is_nf_valI [intro]: "is_val t \<Longrightarrow> is_nf t"
  by (induct t; auto)
  
subsection \<open>Single-step reduction\<close>

inductive red_trm :: "[trm, trm] \<Rightarrow> bool"  (infixl "\<rightarrow>\<^sub>\<beta>" 50) 
      and red_cmd :: "[cmd, cmd] \<Rightarrow> bool"  (infixl "\<^sub>C\<rightarrow>\<^sub>\<beta>" 50)
where
  beta   [intro]: "is_nf v \<Longrightarrow> (\<lambda> T : t)\<degree>v \<rightarrow>\<^sub>\<beta> t[v/0]\<^sup>T" |
  struct [intro]: "\<lbrakk> is_nf v; is_nf (\<mu> (T1\<rightarrow>T2) : c)\<rbrakk> \<Longrightarrow> 
            (\<mu> (T1\<rightarrow>T2) : c)\<degree>v \<rightarrow>\<^sub>\<beta> \<mu> T2 : (c[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet> (liftM_trm v 0))]\<^sup>C)" |
  rename [intro]: "\<lbrakk> is_val t; 0 \<notin> (fmv_trm t 0) \<rbrakk> \<Longrightarrow> (\<mu> T : (<0> t)) \<rightarrow>\<^sub>\<beta> dropM_trm t 0" |
  type_beta [intro]: "in_nf t \<Longrightarrow> TApp (TAbs t) \<tau> \<rightarrow>\<^sub>\<beta> subst_type_trm t 0 \<tau>" |
  
  [intro]: "is_nfC c \<Longrightarrow> <i> (\<mu> T : c) \<^sub>C\<rightarrow>\<^sub>\<beta> (dropM_cmd (c[(Some 0) = (Some i) \<diamond>]\<^sup>C) i)" |
  [intro]: "is_nfC c \<Longrightarrow> <\<top>> (\<mu> T : c) \<^sub>C\<rightarrow>\<^sub>\<beta> (dropM_cmd (c[(Some 0) = None \<diamond>]\<^sup>C) 0)" |
  
  [intro]: "s \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (s\<degree>t) \<rightarrow>\<^sub>\<beta> (u\<degree>t)" |
  [intro]: "is_nf v \<Longrightarrow> t \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (v\<degree>t) \<rightarrow>\<^sub>\<beta> (v\<degree>u)" |
  [intro]: "c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<Longrightarrow> (\<mu> T : c) \<rightarrow>\<^sub>\<beta> (\<mu> T : d)" |
  [intro]: "t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> (<i> t) \<^sub>C\<rightarrow>\<^sub>\<beta> (<i> s)" |
  [intro]: "t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> (<\<top>> t) \<^sub>C\<rightarrow>\<^sub>\<beta> (<\<top>> s)" |
  
  [intro]: "\<lbrakk> is_nf r; is_nf s \<rbrakk> \<Longrightarrow> Nrec T r s Zero \<rightarrow>\<^sub>\<beta> r" |
  [intro]: "\<lbrakk> is_nf r; is_nf s; is_natval n \<rbrakk> \<Longrightarrow> Nrec T r s (S n) \<rightarrow>\<^sub>\<beta> s\<degree>n\<degree>(Nrec T r s n)" |
  [intro]: "is_nfC c \<Longrightarrow> S (\<mu> T : c) \<rightarrow>\<^sub>\<beta> \<mu> T : (c[(Some 0) = (Some 0) (CSuc \<diamond>)]\<^sup>C)" |
  [intro]: "\<lbrakk> is_nf r; is_nf s; is_nfC c \<rbrakk> \<Longrightarrow> Nrec T r s (\<mu> T1 : c)  \<rightarrow>\<^sub>\<beta> \<mu> T : (c[(Some 0) =
                    (Some 0) (CNrec T (liftM_trm r 0) (liftM_trm s 0) \<diamond>)]\<^sup>C)" |
  
  [intro]: "s \<rightarrow>\<^sub>\<beta> t \<Longrightarrow> (S s) \<rightarrow>\<^sub>\<beta> (S t)" |
  [intro]: "r \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta> (Nrec T u s t)" |
  [intro]: "\<lbrakk> is_nf r; s \<rightarrow>\<^sub>\<beta> u \<rbrakk> \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta> (Nrec T r u t)" |
  [intro]: "\<lbrakk> is_nf r; is_nf s; t \<rightarrow>\<^sub>\<beta> u \<rbrakk> \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta> (Nrec T r s u)" |

  [intro]: "(If T true Then t2 Else t3) \<rightarrow>\<^sub>\<beta> t2" |
  [intro]: "(If T false Then t2 Else t3) \<rightarrow>\<^sub>\<beta> t3" |
  [intro]: "t1 \<rightarrow>\<^sub>\<beta> s1 \<Longrightarrow> (If T t1 Then t2 Else t3) \<rightarrow>\<^sub>\<beta> (If T s1 Then t2 Else t3)" |

  [intro]: "\<lbrakk> is_val t1; is_val t2 \<rbrakk> \<Longrightarrow> (\<pi>\<^sub>1 \<lparr>t1, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> t1" |
  [intro]: "\<lbrakk> is_val t1; is_val t2 \<rbrakk> \<Longrightarrow> (\<pi>\<^sub>2 \<lparr>t1, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> t2" |
  [intro]: "t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> (\<pi>\<^sub>1 t) \<rightarrow>\<^sub>\<beta> (\<pi>\<^sub>1 s)" |
  [intro]: "t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> (\<pi>\<^sub>2 t) \<rightarrow>\<^sub>\<beta> (\<pi>\<^sub>2 s)" |
  [intro]: "\<lbrakk>t \<rightarrow>\<^sub>\<beta> t'\<rbrakk> \<Longrightarrow> TApp t \<tau> \<rightarrow>\<^sub>\<beta> TApp t' \<tau>" |
  [intro]: "t1 \<rightarrow>\<^sub>\<beta> s1 \<Longrightarrow> (\<lparr>t1, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> (\<lparr>s1, t2\<rparr>:T)" |
  [intro]: "\<lbrakk> is_val t1; t2 \<rightarrow>\<^sub>\<beta> s2 \<rbrakk> \<Longrightarrow> (\<lparr>t1, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> (\<lparr>t1, s2\<rparr>:T)" |

  [intro]: "is_nfC c \<Longrightarrow> TApp (\<mu> (TAll T) : c) \<tau> \<rightarrow>\<^sub>\<beta> \<mu> (type_subst T 0 \<tau>) : c[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau>)]\<^sup>C" |
  
  [intro]: "is_nfC c \<Longrightarrow> (If U (\<mu> T: c) Then t2 Else t3) \<rightarrow>\<^sub>\<beta>
                      \<mu> U:(c[(Some 0) = (Some 0) (CIf U \<diamond> (liftM_trm t2 0) (liftM_trm t3 0))]\<^sup>C)" |
  [intro]: "is_nfC c \<Longrightarrow> (\<lparr>\<mu> T1: c, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> \<mu> T:(c[(Some 0) = (Some 0) \<lparr>\<diamond>, (liftM_trm t2 0)\<rparr>\<^sub>l:T]\<^sup>C)" |
  [intro]: "is_val t1 \<Longrightarrow> is_nfC c \<Longrightarrow> (\<lparr>t1, \<mu> T2: c\<rparr>:T) \<rightarrow>\<^sub>\<beta> \<mu> T:(c[(Some 0) = (Some 0) \<lparr>(liftM_trm t1 0), \<diamond>\<rparr>\<^sub>r:T]\<^sup>C)" |
  [intro]: "is_nfC c \<Longrightarrow> (\<pi>\<^sub>1 (\<mu> T1\<times>\<^sub>tT2: c)) \<rightarrow>\<^sub>\<beta> \<mu> T1:(c[(Some 0) = (Some 0) (\<Pi>\<^sub>1 \<diamond>)]\<^sup>C)" |
  [intro]: "is_nfC c \<Longrightarrow> (\<pi>\<^sub>2 (\<mu> T1\<times>\<^sub>tT2: c)) \<rightarrow>\<^sub>\<beta> \<mu> T2:(c[(Some 0) = (Some 0) (\<Pi>\<^sub>2 \<diamond>)]\<^sup>C)" |

  [intro]: "is_val v \<Longrightarrow> (Case U (Inl T v) Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta> t1[v/0]\<^sup>T" |
  [intro]: "is_val v \<Longrightarrow> (Case U (Inr T v) Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta> t2[v/0]\<^sup>T" |
  [intro]: "s \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (Case U s Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta> (Case U u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2)" |
  [intro]: "s \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (Inl T s) \<rightarrow>\<^sub>\<beta> (Inl T u)" |
  [intro]: "s \<rightarrow>\<^sub>\<beta> u \<Longrightarrow> (Inr T s) \<rightarrow>\<^sub>\<beta> (Inr T u)" |
  [intro]: "is_nfC c \<Longrightarrow> (Inl T (\<mu> U: c)) \<rightarrow>\<^sub>\<beta> \<mu> T:(c[(Some 0) = (Some 0) (CInl T \<diamond>)]\<^sup>C)" |
  [intro]: "is_nfC c \<Longrightarrow> (Inr T (\<mu> U: c)) \<rightarrow>\<^sub>\<beta> \<mu> T:(c[(Some 0) = (Some 0) (CInr T \<diamond>)]\<^sup>C)" |
  [intro]: "is_nfC c \<Longrightarrow> (Case U (\<mu> T: c) Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta>
      \<mu> U:(c[(Some 0) = (Some 0) (CCase U \<diamond> Of CInl\<Rightarrow> (liftM_trm t1 0)|CInr\<Rightarrow> (liftM_trm t2 0))]\<^sup>C)"

inductive_cases redE [elim!]:
  "`i \<rightarrow>\<^sub>\<beta> s"
  "(\<lambda> T : t) \<rightarrow>\<^sub>\<beta> s"
  "S t \<rightarrow>\<^sub>\<beta> s"
  "Zero \<rightarrow>\<^sub>\<beta> s"
  "s\<degree>t \<rightarrow>\<^sub>\<beta> u"
  "(\<mu> T : c) \<rightarrow>\<^sub>\<beta> t"
  "<i> t \<^sub>C\<rightarrow>\<^sub>\<beta> c"
  "Nrec T r s t \<rightarrow>\<^sub>\<beta> u"
  "<\<top>> (\<mu> T : c) \<^sub>C\<rightarrow>\<^sub>\<beta> c"
  "(<\<top>> t) \<^sub>C\<rightarrow>\<^sub>\<beta> c"
  
  "TApp t \<tau> \<rightarrow>\<^sub>\<beta> u"
  "TAbs t \<rightarrow>\<^sub>\<beta> u"

  "true \<rightarrow>\<^sub>\<beta> s"
  "false \<rightarrow>\<^sub>\<beta> s"
  "(If T t1 Then t2 Else t3) \<rightarrow>\<^sub>\<beta> t"

  "(\<pi>\<^sub>1 t) \<rightarrow>\<^sub>\<beta> s"
  "(\<pi>\<^sub>2 t) \<rightarrow>\<^sub>\<beta> s"
  "(\<lparr>t1, t2\<rparr>:T) \<rightarrow>\<^sub>\<beta> s"

  "(Inl T s) \<rightarrow>\<^sub>\<beta> t"
  "(Inr T s) \<rightarrow>\<^sub>\<beta> t"
  "(Case U t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta> s"

subsection \<open>Many-step reduction\<close>

inductive red_trm_rtc :: "[trm, trm] \<Rightarrow> bool"  (infixl "\<rightarrow>\<^sub>\<beta>\<^sup>*" 50) 
where
    refl_term [iff]: "s \<rightarrow>\<^sub>\<beta>\<^sup>* s"
  | step_term: "\<lbrakk>s \<rightarrow>\<^sub>\<beta> t; t \<rightarrow>\<^sub>\<beta>\<^sup>* u\<rbrakk> \<Longrightarrow> s \<rightarrow>\<^sub>\<beta>\<^sup>* u"

inductive red_cmd_rtc :: "[cmd, cmd] \<Rightarrow> bool"  (infixl "\<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>*" 50)
where
    refl_command [iff]: "c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* c"
  | step_command: "c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<Longrightarrow> d \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* e \<Longrightarrow> c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* e"

lemma step_term2: "\<lbrakk>s \<rightarrow>\<^sub>\<beta>\<^sup>* t; t \<rightarrow>\<^sub>\<beta> u\<rbrakk> \<Longrightarrow> s \<rightarrow>\<^sub>\<beta>\<^sup>* u"
  apply (induct rule: red_trm_rtc.induct)
  using step_term apply force+
  done

text\<open>Proof that the beta reduction relation is included in the reflexive transitive closure\<close>

lemma rtc_term_incl [intro]: "s \<rightarrow>\<^sub>\<beta> t \<Longrightarrow> s \<rightarrow>\<^sub>\<beta>\<^sup>* t"
  by(auto intro: step_term)

lemma [intro]: "c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<Longrightarrow> c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* d"
  by(auto intro: step_command)

text\<open>Proof that the reflexive transitive closure as defined above is transitive\<close>

lemma rtc_term_trans [intro]: "s \<rightarrow>\<^sub>\<beta>\<^sup>* t \<Longrightarrow> t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> s \<rightarrow>\<^sub>\<beta>\<^sup>* u"
  by(induction rule: red_trm_rtc.induct) (auto simp add: step_term)

lemma rtc_command_trans[intro]: "c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* d \<Longrightarrow> d \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* e \<Longrightarrow> c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* e"
  by(induction rule: red_cmd_rtc.induct) (auto simp add: step_command)

subsection\<open>Congruence rules for reflexive transitive closure\<close>

lemma rtc_appL: "s \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (s\<degree>t) \<rightarrow>\<^sub>\<beta>\<^sup>* (u\<degree>t)"
  by(induction rule: red_trm_rtc.induct; blast) 

lemma rtc_appR: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> is_nf s \<Longrightarrow> (s\<degree>t) \<rightarrow>\<^sub>\<beta>\<^sup>* (s\<degree>u)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_mu: "c \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* d \<Longrightarrow> (\<mu> T : c) \<rightarrow>\<^sub>\<beta>\<^sup>* (\<mu> T : d)"
  by(induction rule: red_cmd_rtc.induct) auto

lemma rtc_suc: "s \<rightarrow>\<^sub>\<beta>\<^sup>* t \<Longrightarrow> (S s) \<rightarrow>\<^sub>\<beta>\<^sup>* (S t)"
  by(induction rule: red_trm_rtc.induct) auto

lemma rtc_nrecL: "r \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta>\<^sup>*(Nrec T u s t)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_nrecM: "s \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> is_nf r \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta>\<^sup>* (Nrec T r u t)"
  by(induction rule: red_trm_rtc.induct; blast)
    
lemma rtc_nrecR: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> is_nf r \<Longrightarrow> is_nf s \<Longrightarrow> (Nrec T r s t) \<rightarrow>\<^sub>\<beta>\<^sup>* (Nrec T r s u)"
  by(induction rule: red_trm_rtc.induct; blast)
    
lemma rtc_mVar2: "t \<rightarrow>\<^sub>\<beta>\<^sup>* s \<Longrightarrow> (<i> t) \<^sub>C\<rightarrow>\<^sub>\<beta>\<^sup>* (<i> s)"
  by(induction rule: red_trm_rtc.induct) auto

lemma rtc_if: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (If T t Then t2 Else t3)  \<rightarrow>\<^sub>\<beta>\<^sup>* (If T u Then t2 Else t3)"
  by(induction rule: red_trm_rtc.induct) blast+
    
lemma rtc_pair1: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (\<lparr>t, s\<rparr>:T) \<rightarrow>\<^sub>\<beta>\<^sup>* (\<lparr>u, s\<rparr>:T)"
  by(induction rule: red_trm_rtc.induct) blast+

lemma rtc_pair2: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> is_val s \<Longrightarrow> (\<lparr>s, t\<rparr>:T) \<rightarrow>\<^sub>\<beta>\<^sup>* (\<lparr>s, u\<rparr>:T)"
  by(induction rule: red_trm_rtc.induct) blast+

lemma rtc_proj1: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (\<pi>\<^sub>1 t) \<rightarrow>\<^sub>\<beta>\<^sup>* (\<pi>\<^sub>1 u)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_proj2: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (\<pi>\<^sub>2 t) \<rightarrow>\<^sub>\<beta>\<^sup>* (\<pi>\<^sub>2 u)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_inl: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (Inl T t) \<rightarrow>\<^sub>\<beta>\<^sup>* (Inl T u)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_inr: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (Inr T t) \<rightarrow>\<^sub>\<beta>\<^sup>* (Inr T u)"
  by(induction rule: red_trm_rtc.induct; blast)

lemma rtc_case: "t \<rightarrow>\<^sub>\<beta>\<^sup>* u \<Longrightarrow> (Case T t Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta>\<^sup>* (Case T u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2)"
  by(induction rule: red_trm_rtc.induct; blast)

end