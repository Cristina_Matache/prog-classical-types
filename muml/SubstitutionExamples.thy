subsection\<open>Examples\<close>

theory SubstitutionExamples
  imports Substitution
begin

lemma "liftL_trm (\<lambda> T1 : (\<lambda> T2 : ((`1)\<degree>(`0)\<degree>(`2)))) 0
    = (\<lambda> T1 : (\<lambda> T2 : ((`1)\<degree>(`0)\<degree>(`3))))"
  by simp

lemma "liftL_trm (\<mu> T1 : (<0> (\<lambda> T2 : (`2)))) 0
    = (\<mu> T1 : (<0> (\<lambda> T2 : (`3))))"
  by simp

lemma "liftM_trm (\<lambda> T2 : (`2)) 0
    = (\<lambda> T2 : (`2))"
  by simp

lemma "liftM_trm (\<mu> T1 : (<1> (\<mu> T2 : (<2> Zero)))) 0
    = (\<mu> T1 : (<2> (\<mu> T2 : (<3> Zero))))"
  by simp

lemma "liftM_trm (\<lambda> T1 : ((`0)\<degree>(\<mu> T2 : (<2> Zero)))) 0
    = (\<lambda> T1 : ((`0)\<degree>(\<mu> T2 : (<3> Zero))))"
  by simp

lemma "(`2)[(\<lambda> T : (`0))/2]\<^sup>T = (\<lambda> T : (`0))"
  by simp

lemma "(\<lambda> T1 : (\<lambda> T2 : ((`0)\<degree>(`1)\<degree>(`2))))[(\<lambda> T : (`0))/0]\<^sup>T
    = (\<lambda> T1 : (\<lambda> T2 : ((`0)\<degree>(`1)\<degree>(\<lambda> T : (`0)))))"
  by simp

lemma "(\<lambda> T1 : (\<lambda> T2 : ((`0)\<degree>(`1)\<degree>(`2))))[(`5)/0]\<^sup>T
    = (\<lambda> T1 : (\<lambda> T2 : ((`0)\<degree>(`1)\<degree>(`7))))"
  by simp

lemma "(\<mu> T1 : (<0> (`1)))[(\<mu> T2 : (<1> Zero))/1]\<^sup>T
    = (\<mu> T1 : (<0> (\<mu> T2 : (<2> Zero))))"
  by simp

lemma "(\<mu> T1 : (<0> (`1)))[(\<mu> T2 : (<0> Zero))/1]\<^sup>T
    = (\<mu> T1 : (<0> (\<mu> T2 : (<0> Zero))))"
  by simp

lemma subst_eq: "(`k)[s/k]\<^sup>T = s"
  by simp

lemma subst_sm: "i < k \<Longrightarrow> (`i)[s/k]\<^sup>T = (`i)"
  by simp

lemma subst_gt: "i > k \<Longrightarrow> (`i)[s/k]\<^sup>T = (`(i-1))"
  by simp

lemma "liftL_ctxt ((CSuc \<diamond>) \<^sup>\<bullet> (\<lambda> T : (`1))) 0
    = ((CSuc \<diamond>) \<^sup>\<bullet> (\<lambda> T : (`2)))"
  by simp

lemma "liftM_ctxt ((CSuc \<diamond>) \<^sup>\<bullet> (\<mu> T : (<1> Zero))) 0
    = ((CSuc \<diamond>) \<^sup>\<bullet> (\<mu> T : (<2> Zero)))"
  by simp


lemma "(<i> t)[(Some i)= (Some k) E]\<^sup>C = (<k> (ctxt_subst E (t[(Some i)=(Some k) E]\<^sup>T)))"
  by simp

lemma "(\<mu> T : (<0> Zero))[(Some 0)=(Some 1) (CSuc \<diamond>)]\<^sup>T = (\<mu> T : (<0> Zero))"
  by simp

lemma "(\<lambda> T1 : (\<mu> T2 : (<2> (`2))))[(Some 1)=(Some 2) (CSuc \<diamond>)]\<^sup>T
    = (\<lambda> T1 : (\<mu> T2 : (<3> (S (`2)))))"
  by simp

lemma "(\<lambda> T1 : (\<mu> T2 : (<2> (`2))))[(Some 1)=(Some 2) (\<diamond> \<^sup>\<bullet> (`0))]\<^sup>T
    = (\<lambda> T1 : (\<mu> T2 : (<3> ((`2)\<degree>(`1)))))"
  by simp

lemma "(\<mu> T1 : (<1> (`1)))[(Some 0)=(Some 2) (\<diamond> \<^sup>\<bullet> (\<mu> T2 : (<1> (`2))))]\<^sup>T
    = Mu T1 (MVar 3 ((LVar 1)\<degree>(Mu T2 (MVar 2 (LVar 2)))))"
  by simp

lemma "(\<mu> T : ((<0> Zero)[(Some 0)=(Some 0) (CSuc \<diamond>)]\<^sup>C)) = \<mu> T : (<0> (S Zero))"
  by simp

lemma "(\<mu> T : ((<0> (\<mu> T1 : (<1> Zero)))[(Some 0)=(Some 0) (CSuc \<diamond>)]\<^sup>C))
     = \<mu> T : (<0> (S (\<mu> T1 : (<1> (S Zero)))))"
 by simp

end