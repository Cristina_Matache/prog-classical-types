section \<open>Classical Logic Examples\<close>
  
theory ClassicalExamples
  imports Types
begin
  
lemma
  assumes "\<Delta> 0 = Command"
  shows   "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lambda> T1\<rightarrow>Command: (\<lambda> T1: (\<mu> Command:(<1>((`1) \<degree> (`0))))) : (T1\<rightarrow>Command)\<rightarrow>(T1\<rightarrow>Command)"
  using assms by fastforce
    
lemma double_negation:
  assumes "\<Delta> 0 = Command"
  shows   "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lambda> (T1\<rightarrow>Command)\<rightarrow>Command: (\<mu> T1:(<1>((`0) \<degree> (\<lambda> T1: (\<mu> Command:(<1> (`0))))))) :  ((T1\<rightarrow>Command)\<rightarrow>Command)\<rightarrow>T1"
  using assms by fastforce
    
lemma pierce_law:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lambda> (T1\<rightarrow>Command)\<rightarrow>T1: (\<mu> T1:(<0>((`0) \<degree> (\<lambda> T1: (\<mu> Command:(<1> (`0))))))) : ((T1\<rightarrow>Command)\<rightarrow>T1)\<rightarrow>T1"
  by fastforce

lemma
  assumes "\<Delta> 0 = Command"
  shows   "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lambda> (T1\<rightarrow>T2)\<rightarrow>Command: (\<mu> T1:(<1>((`0) \<degree> (\<lambda> T1: (\<mu> T2:(<1> (`0))))))) : ((T1\<rightarrow>T2)\<rightarrow>Command)\<rightarrow>T1"
using assms by fastforce

end