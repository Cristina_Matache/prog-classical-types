section\<open>Type Preservation\<close>

theory TypePreservation
  imports ContextFacts
begin

subsection\<open>Lifting preserves well-typedness\<close>

lemma liftL_type: 
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma>\<langle>k:U\<rangle>, \<Delta> \<turnstile>\<^sub>T (liftL_trm t k) : T"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma>\<langle>k:U\<rangle>, \<Delta> \<turnstile>\<^sub>C (liftL_cmd c k)"
by(induction arbitrary: k U and k U rule: typing_trm_typing_cmd.inducts) auto

lemma liftM_type: 
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma>, \<Delta>\<langle>k:U\<rangle> \<turnstile>\<^sub>T (liftM_trm t k) : T"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma>, \<Delta>\<langle>k:U\<rangle> \<turnstile>\<^sub>C (liftM_cmd c k)"
by(induction arbitrary: k U and k U rule: typing_trm_typing_cmd.inducts) auto

lemma dropM_type:
  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>T t : T \<Longrightarrow> \<forall> k \<Delta>. (k \<notin> fmv_trm t 0 \<longrightarrow> (\<forall>x. x<k \<longrightarrow> \<Delta>1 x = \<Delta> x) 
    \<longrightarrow> (\<forall>x. x>k \<longrightarrow> \<Delta>1 x = \<Delta> (x-1)) \<longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T dropM_trm t k : T)"
  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>C c \<Longrightarrow> \<forall> k \<Delta>. (k \<notin> fmv_cmd c 0 \<longrightarrow> (\<forall>x. x<k \<longrightarrow> \<Delta>1 x = \<Delta> x) 
    \<longrightarrow> (\<forall>x. x>k \<longrightarrow> \<Delta>1 x = \<Delta> (x-1)) \<longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C dropM_cmd c k)"
   apply(induction rule: typing_trm_typing_cmd.inducts)
                     prefer 19
                     apply clarsimp
  using env_type_shift_def apply force
                    apply(fastforce simp: shift_def)+
  done

lemma liftL_ctxt_type:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T \<Leftarrow> U"
  shows   "\<Gamma>\<langle>k:T1\<rangle>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (liftL_ctxt E k) : T \<Leftarrow> U"
using assms by(induct rule: typing_ctxt.induct) (auto simp add: liftL_type)

lemma liftM_ctxt_type:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T \<Leftarrow> U"
  shows   "\<Gamma>, \<Delta>\<langle>k:T1\<rangle> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (liftM_ctxt E k) : T \<Leftarrow> U"
using assms by(induct rule: typing_ctxt.induct) (auto simp add: liftM_type)

subsection\<open>Typing is preserved under substitution\<close>

lemma liftT_type_subst_commute:
  shows "m \<le> k \<Longrightarrow> liftT_type (type_subst T m \<tau>) k = type_subst (liftT_type T (Suc k)) m (liftT_type \<tau> k)"
  apply(induction T arbitrary: \<tau> m k, auto)
  apply(erule_tac x="liftT_type \<tau> k" in meta_allE)
  apply(erule_tac x="Suc m" in meta_allE)
  apply(erule_tac x="Suc k" in meta_allE)
  apply clarsimp
  apply(simp add: liftT_type_commute)
  done
  
lemma liftT_kind_ctxt_shift:
  shows "\<Gamma>, \<Delta> \<turnstile>\<^sub>T r : T \<Longrightarrow> env_type_shift \<Gamma> k , env_type_shift \<Delta> k \<turnstile>\<^sub>T liftT_trm r k : liftT_type T k"
    and "\<Gamma>, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> env_type_shift \<Gamma> k , env_type_shift \<Delta> k \<turnstile>\<^sub>C liftT_cmd c k"
   apply(induction arbitrary: k and k rule: typing_trm_typing_cmd.inducts)
    apply auto
     apply(simp add: env_type_shift_def)
    apply(simp add: env_type_shift_def)
   apply(erule_tac x="Suc k" in meta_allE)
   apply(simp add: env_type_shift_commute)
  apply(erule_tac x="k" in meta_allE)
  apply(subst liftT_type_subst_commute, force, force)
  done
  
theorem subst_type:
  "\<Gamma>1, \<Delta> \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T r : T1 \<Longrightarrow> \<Gamma>1 = \<Gamma>\<langle>k:T1\<rangle>
    \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t[r/k]\<^sup>T : T"
  "\<Gamma>1, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> \<forall>\<Gamma>.\<forall>r.\<forall>T1. (\<Gamma>, \<Delta> \<turnstile>\<^sub>T r : T1 \<longrightarrow> (\<forall>k. (\<Gamma>1 = \<Gamma>\<langle>k:T1\<rangle>
    \<longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C c[r/k]\<^sup>C)))"
  apply(induct arbitrary: \<Gamma> k T1 r rule: typing_trm_typing_cmd.inducts)
  apply(auto)
  apply(rotate_tac 2)
  apply(drule liftL_type(1))
  apply(fastforce)
  apply(rotate_tac 2)
     apply(drule liftM_type(1))
     apply(fastforce)
    apply (metis liftL_type(1))
   apply (metis liftL_type(1))
  using liftT_kind_ctxt_shift(1) by blast

lemma liftM_liftT_commute:
  shows "liftM_trm (liftT_trm T m) n = liftT_trm (liftM_trm T n) m"
    and "liftM_cmd (liftT_cmd C m) n = liftT_cmd (liftM_cmd C n) m"
by(induction T and C arbitrary: m n and m n) auto
    
lemma liftT_liftM_commute:
  shows "liftT_ctxt (liftM_ctxt E m) n = liftM_ctxt (liftT_ctxt E n) m"
  apply(induction E arbitrary: m n)
              apply(auto simp add: liftM_liftT_commute)
  done
    
lemma typing_ctxt_env_type_shift [intro]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T"
  shows "env_type_shift \<Gamma> 0 , env_type_shift \<Delta> 0 \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t liftT_ctxt E 0 : liftT_type U 0 \<Leftarrow> liftT_type T 0"
  using assms
  apply(induction rule: typing_ctxt.induct)
              apply auto
        apply(simp add: liftT_kind_ctxt_shift(1) typing_ctxt.intros(2))
       apply (metis liftT_kind_ctxt_shift(1) liftT_type.simps(5) liftT_type.simps(6) typing_ctxt.intros(4))
      apply (simp add: liftT_type_subst_commute typing_ctxt.intros(5))
  using liftT_kind_ctxt_shift(1) apply blast
    apply (simp add: liftT_kind_ctxt_shift(1) typing_ctxt.intros(7))
   apply (simp add: liftT_kind_ctxt_shift(1) typing_ctxt.intros(8))
  apply(metis env_type_shift_eq liftT_kind_ctxt_shift(1) typing_ctxt.intros(13))
  done
    
theorem struct_subst_type:
  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta>1 = \<Delta>\<langle>\<alpha>:T1\<rangle>
     \<Longrightarrow> \<Gamma>, \<Delta>\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
  "(\<Gamma>, \<Delta>1 \<turnstile>\<^sub>C c \<Longrightarrow>  (\<forall>\<Delta> E U T1. (\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 
    \<longrightarrow> (\<forall>\<alpha>. (\<Delta>1 = \<Delta>\<langle>\<alpha>:T1\<rangle>
    \<longrightarrow> (\<forall>\<beta>. \<Gamma>, \<Delta>\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>C c[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>C))))))"
proof (induct arbitrary: \<Delta> T1 E U \<beta> \<alpha> rule: typing_trm_typing_cmd.inducts)
  fix \<Gamma> x T \<Delta> \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> x = T" 
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"  
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (`x)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> t \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
  thus " \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T Zero[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Nat"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t \<Delta>' T1 E U \<beta> \<alpha>
  assume "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> 
           \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Nat)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T S t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Nat"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t T1 T2 s \<Delta>' T1a E U \<beta> \<alpha>
  assume "(\<And>\<Delta>a T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>a \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>a\<langle>\<alpha>:T1a\<rangle> 
           \<Longrightarrow> \<Gamma> , \<Delta>a\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 \<rightarrow> T2)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1" 
         "(\<And>\<Delta>a T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>a \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>a\<langle>\<alpha>:T1a\<rangle> 
          \<Longrightarrow> \<Gamma> , \<Delta>a\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T s[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a" 
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (t \<degree> s)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T2"
    by(fastforce)
next
  fix \<Gamma> T1 \<Delta> t T2 \<Delta>' T1a E U \<beta> \<alpha> 
  assume "(\<And>\<Delta>a T1a E U \<beta> \<alpha>. \<Gamma>\<langle>0:T1\<rangle> , \<Delta>a \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>a\<langle>\<alpha>:T1a\<rangle> 
          \<Longrightarrow> (\<Gamma>\<langle>0:T1\<rangle> , \<Delta>a\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T2))"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a" 
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (\<lambda> T1 : t)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 \<rightarrow> T2"
    apply(clarsimp)
    apply(drule liftL_ctxt_type)
    apply(fastforce simp: liftLM_comm_ctxt fmv_liftL_ctxt)
  done
next
  fix \<Gamma> \<Delta> r T s t \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T r : T"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> 
           \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T r[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat \<rightarrow> T \<rightarrow> T"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> 
          \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T s[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Nat \<rightarrow> T \<rightarrow> T)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : Nat"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> 
          \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Nat)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T Nrec T r s t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
    by(fastforce)
next
  fix \<Gamma> \<Delta> T c \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta>\<langle>0:T\<rangle> \<turnstile>\<^sub>C c"
         "\<forall>\<Delta>a E U T1. \<Gamma> , \<Delta>a \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<longrightarrow> (\<forall>\<alpha>. \<Delta>\<langle>0:T\<rangle> = \<Delta>a\<langle>\<alpha>:T1\<rangle> \<longrightarrow> 
            (\<forall>\<beta>. \<Gamma> , \<Delta>a\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>C c[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>C))"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1" 
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus   "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (\<mu> T : c)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
    apply(clarsimp)
    apply(drule liftM_ctxt_type)
    apply(clarsimp simp add: liftMM_comm_ctxt)
    apply(fastforce)
  done
next
  fix \<Gamma> \<Delta> t T x
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> 
           \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
         "\<Delta> x = T"
  thus "\<forall>\<Delta>' E U T1. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<longrightarrow> 
        (\<forall>\<alpha>. \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<longrightarrow> (\<forall>\<beta>. \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>C <x> t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>C))"
    apply(clarsimp)
    apply(rule conjI, fastforce, clarsimp)
    apply(rule conjI, fastforce, clarsimp)
    apply(rule conjI, clarsimp)
    apply(rule_tac T = "(\<Delta>''\<langle>\<beta>:U\<rangle>) \<beta>" in typing_trm_typing_cmd.intros(5))
    apply(rule ctxt_to_term_typing [where ?U = T])
    apply(simp add: liftM_ctxt_type)
    apply(fastforce)
    apply(simp)
    apply(clarsimp)
    apply(rule_tac T = "(\<Delta>''\<langle>\<beta>:U\<rangle>) x" in typing_trm_typing_cmd.intros(5))
    apply(fastforce)+
  done
next
  fix \<Gamma> \<Delta> t
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : \<bottom>"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : \<bottom>)"
  thus "\<forall>\<Delta>' E U T1. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<longrightarrow> (\<forall>\<alpha>. \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<longrightarrow> 
         (\<forall>\<beta>. \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>C <\<top>> t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>C))"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T true[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Bool"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T false[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Bool"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> t1 t2 T t3 \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t1 : Bool"
        "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t1[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : Bool)"
        "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t2 : T"
        "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t2[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
        "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t3 : T"
        "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t3[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
        "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
        "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (If T t1 Then t2 Else t3)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t1 T1 t2 T2 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t1 : T1"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t1[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t2 : T2"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t2[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T2)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (\<lparr>t1,t2\<rparr>:(T1\<times>\<^sub>tT2))[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 \<times>\<^sub>t T2"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t T1 T2 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1 \<times>\<^sub>t T2"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 \<times>\<^sub>t T2)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (\<pi>\<^sub>1 t)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t T1 T2 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1 \<times>\<^sub>t T2"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 \<times>\<^sub>t T2)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (\<pi>\<^sub>2 t)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T2"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t T1 T2 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus " \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T Inl (T1 +\<^sub>t T2) t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 +\<^sub>t T2"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t T2 T1 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T2"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T2)"
         " \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         " \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T Inr (T1 +\<^sub>t T2) t[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 +\<^sub>t T2"
    by(fastforce)
next
  fix \<Gamma> \<Delta> t0 T1 T2 t1 T t2 \<Delta>' T1a E U \<beta> \<alpha>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t0 : T1 +\<^sub>t T2"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> 
            \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t0[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T1 +\<^sub>t T2)"
         "\<Gamma>\<langle>0:T1\<rangle> , \<Delta> \<turnstile>\<^sub>T t1 : T"
         "(\<And>\<Delta>' T1a E U \<beta> \<alpha>. \<Gamma>\<langle>0:T1\<rangle> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle> 
            \<Longrightarrow> \<Gamma>\<langle>0:T1\<rangle> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t1[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
         "\<Gamma>\<langle>0:T2\<rangle> , \<Delta> \<turnstile>\<^sub>T t2 : T"
         "(\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma>\<langle>0:T2\<rangle> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> 
            \<Longrightarrow> \<Gamma>\<langle>0:T2\<rangle> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t2[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T)"
         "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1a"
         "\<Delta> = \<Delta>'\<langle>\<alpha>:T1a\<rangle>"
  thus " \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T (Case T  t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2)[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>T : T"
    apply(clarsimp)
    apply(rule typing_trm_typing_cmd.intros(18))
    apply(fastforce)
    apply(drule liftL_ctxt_type(1) [where ?T1.0 = T1])
    apply(fastforce simp add: liftLM_comm_ctxt)
    apply(drule liftL_ctxt_type(1) [where ?T1.0 = T2])
    apply(fastforce simp add: liftLM_comm_ctxt)
    done
next
  fix \<Gamma> \<Delta> t T \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<And>\<Delta>' T1 E U \<beta> \<alpha>. env_type_shift \<Gamma> 0 , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> env_type_shift \<Delta> 0 = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> env_type_shift \<Gamma> 0 , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[Some \<alpha>=Some \<beta> liftM_ctxt E \<beta>]\<^sup>T : T"
    and "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
    and "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
    and "env_type_shift \<Gamma> 0 , env_type_shift \<Delta> 0 \<turnstile>\<^sub>T t : T"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T TAbs t[Some \<alpha>=Some \<beta> liftM_ctxt E \<beta>]\<^sup>T : TAll T"
    by(clarsimp simp add: liftT_liftM_commute) blast
next
  fix \<Gamma> \<Delta> t T \<tau> \<Delta>' T1 E U \<beta> \<alpha>
  assume "\<And>\<Delta>' T1 E U \<beta> \<alpha>. \<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 \<Longrightarrow> \<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle> \<Longrightarrow> \<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T t[Some \<alpha>=Some \<beta> liftM_ctxt E \<beta>]\<^sup>T : TAll T"
    and "\<Gamma> , \<Delta>' \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1"
    and "\<Delta> = \<Delta>'\<langle>\<alpha>:T1\<rangle>"
  thus "\<Gamma> , \<Delta>'\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>T TApp t \<tau>[Some \<alpha>=Some \<beta> liftM_ctxt E \<beta>]\<^sup>T : type_subst T 0 \<tau>"
    by clarsimp 
qed

lemma struct_subst_type_command:  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : U \<Leftarrow> T1 
    \<Longrightarrow> \<Delta>1 = \<Delta>\<langle>\<alpha>:T1\<rangle> \<Longrightarrow>  \<Gamma>, \<Delta>\<langle>\<beta>:U\<rangle> \<turnstile>\<^sub>C c[(Some \<alpha>)=(Some \<beta>) (liftM_ctxt E \<beta>)]\<^sup>C"
  by(auto simp add: struct_subst_type)

lemma struct_subst_type_top:
  "\<Gamma> , \<Delta>1 \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Delta>\<langle>\<alpha>:\<bottom>\<rangle> = \<Delta>1 \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T dropM_trm (t[Some \<alpha>=None \<diamond>]\<^sup>T) \<alpha> : T"
  "\<Gamma> , \<Delta>1 \<turnstile>\<^sub>C c \<Longrightarrow> 
    (\<And>\<Delta> \<alpha>. \<Delta>\<langle>\<alpha>:\<bottom>\<rangle> = \<Delta>1 \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>C dropM_cmd (c[Some \<alpha>=None \<diamond>]\<^sup>C) \<alpha>)"
  by(induct arbitrary: \<Delta> \<alpha> rule: typing_trm_typing_cmd.inducts) (fastforce)+

subsection\<open>Type preservation\<close>

lemma  dropM_env:
  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>T t[(Some k)=(Some x) \<diamond>]\<^sup>T : T \<Longrightarrow> \<Delta>1 = \<Delta>\<langle>x:(\<Delta> x)\<rangle> 
    \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T dropM_trm (t[(Some k)=(Some x) \<diamond>]\<^sup>T) x : T" 
  "\<Gamma>, \<Delta>1 \<turnstile>\<^sub>C c[(Some k)=(Some x) \<diamond>]\<^sup>C \<Longrightarrow> \<Delta>1 = \<Delta>\<langle>x:(\<Delta> x)\<rangle> \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>C dropM_cmd (c[(Some k)=(Some x) \<diamond>]\<^sup>C) x"
   apply(induct t and c arbitrary: \<Gamma> T \<Delta>1 x k \<Delta> and \<Gamma> T \<Delta>1 x k \<Delta>)
                     apply fastforce
                    apply fastforce
                   apply fastforce
                  apply fastforce
  using env_type_shift_def apply force
                apply fastforce+
   defer
   apply clarsimp
   apply (metis cmd.distinct(1) cmd.inject(2) typing_cmd.cases)
  apply (erule typing_cmd.cases)
    defer
   apply (clarsimp split: if_splits)
  apply (clarsimp split: if_splits)
     apply blast
    apply fastforce
   apply fastforce
  apply fastforce
  done


lemma type_subst_liftT_type_fix [simp]:
  shows "type_subst (liftT_type T m) m \<tau> = T"
  by(induction T arbitrary: m \<tau>, auto)
  
definition env_type_subst :: "(nat \<Rightarrow> type) \<Rightarrow> nat \<Rightarrow> type \<Rightarrow> (nat \<Rightarrow> type)" where
  "env_type_subst \<Gamma> k \<tau> x \<equiv> type_subst (\<Gamma> x) k \<tau>"

lemma env_type_subst_distr_shift:
  "(env_type_subst \<Gamma> m \<tau>)\<langle>k:type_subst T1 m \<tau>\<rangle> = env_type_subst (\<Gamma>\<langle>k:T1\<rangle>) m \<tau>"
  by(auto simp add: env_type_subst_def shift_def)

lemma type_subst_liftL_comm:
  "n\<le>m \<Longrightarrow> liftT_type (type_subst T m \<tau>) n = type_subst (liftT_type T n) (Suc m) (liftT_type \<tau> n)"
  by(induct T arbitrary: n m \<tau>) (auto simp add: liftT_type_commute)
    
lemma env_type_shift_env_type_subst_comm:
  "env_type_shift (env_type_subst \<Delta> m \<tau>) 0 = env_type_subst (env_type_shift \<Delta> 0) (Suc m) (liftT_type \<tau> 0)"
  by(rule ext) (auto simp add: env_type_shift_def env_type_subst_def type_subst_liftL_comm)

lemma type_subst_comm:
  "n\<le>m \<Longrightarrow> type_subst (type_subst T n \<tau>) m \<tau>' = type_subst (type_subst T (Suc m) (liftT_type \<tau>' n)) n (type_subst \<tau> m \<tau>')"
  by(induct T arbitrary: n m \<tau> \<tau>') (auto simp add: liftT_type_commute type_subst_liftL_comm)  

lemma env_type_subst_env_type_shift_fix:
  "\<Gamma> = env_type_subst (env_type_shift \<Gamma> m) m \<tau>"
  by(rule ext) (auto simp add: env_type_subst_def env_type_shift_def)
    
lemma subst_typing_stronger:
  "\<Gamma>', \<Delta>' \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma> = env_type_subst \<Gamma>' m \<tau> \<Longrightarrow> \<Delta> = env_type_subst \<Delta>' m \<tau> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T subst_type_trm t m \<tau> : type_subst T m \<tau>"
  "\<Gamma>', \<Delta>' \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma> = env_type_subst \<Gamma>' m \<tau> \<Longrightarrow> \<Delta> = env_type_subst \<Delta>' m \<tau> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C subst_type_cmd c m \<tau>"
   apply(induct arbitrary: \<Gamma> \<Delta> m \<tau> and \<Gamma> \<Delta> m \<tau> rule: typing_trm_typing_cmd.inducts)
                     apply auto
         apply(auto simp add: env_type_subst_def env_type_subst_distr_shift)
   apply(erule_tac x="env_type_subst (env_type_shift \<Gamma> 0) (Suc m) (liftT_type \<tau> 0)" in meta_allE)
   apply(erule_tac x="env_type_subst (env_type_shift \<Delta> 0)(Suc m) (liftT_type \<tau> 0)" in meta_allE)
   apply(erule_tac x="Suc m" in meta_allE)
   apply(erule_tac x="liftT_type \<tau> 0" in meta_allE)
   apply(clarsimp simp add: env_type_shift_env_type_subst_comm)
  apply(simp add: type_subst_comm)
    apply(clarsimp)
done   
     
lemma subst_typing:
  shows "\<Gamma>' , \<Delta>' \<turnstile>\<^sub>T t : T \<Longrightarrow> \<Gamma>' = env_type_shift \<Gamma> m \<Longrightarrow> \<Delta>' = env_type_shift \<Delta> m \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T subst_type_trm t m \<tau> : type_subst T m \<tau>"
    and "\<Gamma>' , \<Delta>' \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma>' = env_type_shift \<Gamma> m \<Longrightarrow> \<Delta>' = env_type_shift \<Delta> m \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>C subst_type_cmd c m \<tau>"
   apply(rule subst_typing_stronger[where ?\<Gamma>' = "\<Gamma>'" and ?\<Delta>' = "\<Delta>'"])
     apply(clarsimp simp add: env_type_subst_env_type_shift_fix)+
    apply(rule subst_typing_stronger[where ?\<Gamma>' = "\<Gamma>'" and ?\<Delta>' = "\<Delta>'"])
    apply(clarsimp simp add: env_type_subst_env_type_shift_fix)+
done        
    
theorem type_preservation:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T \<Longrightarrow> t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T s : T"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> \<forall>d. (c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C d)"
proof(induct arbitrary: s rule: typing_trm_typing_cmd.inducts)
  fix \<Gamma> :: "nat \<Rightarrow> type" and x T \<Delta> s
  assume "\<Gamma> x = T" 
         "LVar x \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T"
    by blast
next
  fix \<Gamma> \<Delta> s
  assume "Zero \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat"
    by blast
next
  fix \<Gamma> \<Delta> t s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : Nat" 
         "S t \<rightarrow>\<^sub>\<beta> s"
  and IH: "\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat"
    apply(safe)
    apply(frule struct_subst_type_command 
      [where ?\<Delta>1.0 = "\<Delta>\<langle>0:type.Nat\<rangle>" and ?\<Delta> = \<Delta> and ?\<alpha> = 0 and ?T1.0 = Nat 
       and ?E = "CSuc \<diamond>" and ?U = Nat])
    apply(fastforce)+
  done
next
  fix \<Gamma> \<Delta> t T1 T2 s sa
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1 \<rightarrow> T2"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 \<rightarrow> T2)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1"
         "(\<And>sa. s \<rightarrow>\<^sub>\<beta> sa \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T sa : T1)"
         "t \<degree> s \<rightarrow>\<^sub>\<beta> sa"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T sa : T2"
    apply(safe)
    apply(clarsimp simp add: subst_type)
    apply(frule struct_subst_type_command
      [where ?\<Delta>1.0 = "\<Delta>\<langle>0:T1 \<rightarrow> T2\<rangle>" and ?\<Delta> = \<Delta> and ?\<alpha> = 0 and ?T1.0 = "T1 \<rightarrow> T2" 
       and ?E = "\<diamond> \<^sup>\<bullet> s" and ?U = T2])
          apply auto
  done
next
  fix \<Gamma> T1 \<Delta> t T2 s
  assume "\<Gamma>\<langle>0:T1\<rangle> , \<Delta> \<turnstile>\<^sub>T t : T2" 
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma>\<langle>0:T1\<rangle> , \<Delta> \<turnstile>\<^sub>T s : T2)"
         "(\<lambda> T1 : t) \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 \<rightarrow> T2"
    by (auto simp add: subst_type)
next
  fix \<Gamma> \<Delta> r T s t sa
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T r : T"
         "(\<And>s. r \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat \<rightarrow> T \<rightarrow> T"
         "(\<And>sa. s \<rightarrow>\<^sub>\<beta> sa \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T sa : Nat \<rightarrow> T \<rightarrow> T)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : Nat"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Nat)"
         "Nrec T r s t \<rightarrow>\<^sub>\<beta> sa"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T sa : T"
    apply(safe)
          apply assumption
         apply assumption
      apply fastforce
   apply(frule struct_subst_type_command
      [where ?\<Delta>1.0 = "\<Delta>\<langle>0:Nat\<rangle>" and ?\<Delta> = \<Delta> and ?\<alpha> = 0 and ?T1.0 = Nat 
       and ?E = "CNrec T r s \<diamond>" and ?U = T])
   apply auto
  done
next
  fix \<Gamma> \<Delta> T c s
  assume "\<Gamma> , \<Delta>\<langle>0:T\<rangle> \<turnstile>\<^sub>C c"
         "\<forall>d. c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<longrightarrow> \<Gamma> , \<Delta>\<langle>0:T\<rangle> \<turnstile>\<^sub>C d"
         "(\<mu> T : c) \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T"
    apply(safe)
    using dropM_type(1) apply (simp add: typing_cmd.simps)
    using dropM_type(1) apply (simp add: typing_cmd.simps)
    done
next
  fix \<Gamma> \<Delta> t T x
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T)"
         "\<Delta> x = T"
  thus "\<forall>d. <x> t \<^sub>C\<rightarrow>\<^sub>\<beta> d \<longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>C d"   
    apply(safe)
   apply(rule dropM_env(2) [where ?\<Delta>1.0 = "\<Delta>\<langle>x:\<Delta> x\<rangle>"])
   apply(drule struct_subst_type_command
     [where ?\<Delta>1.0 = "\<Delta>\<langle>0:\<Delta> x\<rangle>" and ?\<Delta> = \<Delta> and ?\<alpha> = 0 and ?T1.0 = "\<Delta> x" 
      and ?E = \<diamond> and ?U = "\<Delta> x"])
   apply(fastforce)+
  done
next
  fix \<Gamma> \<Delta> t
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : \<bottom>"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : \<bottom>)"
  thus "\<forall>d. <\<top>> t \<^sub>C\<rightarrow>\<^sub>\<beta> d \<longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>C d"
    apply(safe)
    apply(clarsimp simp add: struct_subst_type_top)+
  done
next
  fix \<Gamma> \<Delta> s
  assume "true \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Bool"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> s
  assume "false \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Bool"
    by(clarsimp)
next
  fix \<Gamma> \<Delta> t1 t2 T t3 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t1 : Bool"
         "(\<And>s. t1 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : Bool)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t2 : T"
         "(\<And>s. t2 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t3 : T"
         "(\<And>s. t3 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T)"
         "(If T t1  Then t2 Else t3) \<rightarrow>\<^sub>\<beta> s "
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T"
    apply -
    apply(erule redE)
       apply force
      apply force
     apply force
    apply(clarsimp)
    apply(frule struct_subst_type_command 
      [where ?\<Delta> = \<Delta> and ?E = "CIf T \<diamond> t2 t3" and ?U = T and ?T1.0 ="Bool" and ?\<alpha> = 0 and ?\<beta> = 0])
    apply auto
  done   
next
  fix \<Gamma> \<Delta> t1 T1 t2 T2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t1 : T1"
         "(\<And>s. t1 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1)"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t2 : T2"
         "(\<And>s. t2 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T2)"
         "(\<lparr>t1,t2\<rparr>:(T1\<times>\<^sub>tT2)) \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 \<times>\<^sub>t T2"
    apply -
    apply(erule redE)
    apply fastforce+
    apply(clarsimp)
    apply(frule struct_subst_type_command 
      [where ?\<Delta> = \<Delta> and ?E = "\<lparr>\<diamond>, t2\<rparr>\<^sub>l:(T1 \<times>\<^sub>t T2)" and ?U = "T1 \<times>\<^sub>t T2" and ?T1.0 =T1 and ?\<alpha> = 0 and ?\<beta> = 0])
    apply auto
    apply(frule struct_subst_type_command 
      [where ?\<Delta> = \<Delta> and ?E = "\<lparr>t1, \<diamond>\<rparr>\<^sub>r:(T1 \<times>\<^sub>t T2)" and ?U = "T1 \<times>\<^sub>t T2" and ?T1.0 =T2 and ?\<alpha> = 0 and ?\<beta> = 0])
    apply auto
  done
next
  fix \<Gamma> \<Delta> t T1 T2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1 \<times>\<^sub>t T2"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 \<times>\<^sub>t T2)"
         "(\<pi>\<^sub>1 t) \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1"
    apply(safe)
    apply(fastforce)
    apply(frule struct_subst_type_command 
      [where ?\<Delta> = \<Delta> and ?E = "\<Pi>\<^sub>1\<diamond>" and ?U = T1 and ?T1.0 ="T1 \<times>\<^sub>t T2" and ?\<alpha> = 0 and ?\<beta> = 0])
    apply(fastforce)+
  done
next
  fix \<Gamma> \<Delta> t T1 T2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1 \<times>\<^sub>t T2"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 \<times>\<^sub>t T2)"
         "(\<pi>\<^sub>2 t) \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T2"
    apply(safe)
    apply(fastforce)
    apply(frule struct_subst_type_command 
      [where ?\<Delta> = \<Delta> and ?E = "\<Pi>\<^sub>2\<diamond>" and ?U = T2 and ?T1.0 ="T1 \<times>\<^sub>t T2" and ?\<alpha> = 0 and ?\<beta> = 0])
    apply(fastforce)+
  done
next
  fix \<Gamma> \<Delta> t T1 T2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T1"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1)"
         "Inl (T1 +\<^sub>t T2) t \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 +\<^sub>t T2"
    apply -
    apply(erule redE)
    apply(fastforce)
    apply(clarsimp)
    apply(drule struct_subst_type_command
      [where ?\<Delta> = \<Delta> and ?E = "CInl (T1 +\<^sub>t T2) \<diamond>" and ?U = "T1+\<^sub>tT2" and ?T1.0 = T1 and ?\<alpha> = 0 and ?\<beta> = 0])
    apply(fastforce)+
 done
next
  fix \<Gamma> \<Delta> t T1 T2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : T2"
         "(\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T2)"
         "Inr (T1 +\<^sub>t T2) t \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 +\<^sub>t T2"
    apply -
    apply(erule redE)
    apply(fastforce)
    apply(clarsimp)
    apply(drule struct_subst_type_command
      [where ?\<Delta> = \<Delta> and ?E = "CInr (T1 +\<^sub>t T2) \<diamond>" and ?U = "T1+\<^sub>tT2" and ?T1.0 = T2 and ?\<alpha> = 0 and ?\<beta> = 0])
    apply(fastforce)+
 done
next
  fix \<Gamma> \<Delta> t0 T1 T2 t1 T t2 s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t0 : T1 +\<^sub>t T2"
         "(\<And>s. t0 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T1 +\<^sub>t T2)"
         "\<Gamma>\<langle>0:T1\<rangle> , \<Delta> \<turnstile>\<^sub>T t1 : T"
         "(\<And>s. t1 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma>\<langle>0:T1\<rangle> , \<Delta> \<turnstile>\<^sub>T s : T)"
         "\<Gamma>\<langle>0:T2\<rangle> , \<Delta> \<turnstile>\<^sub>T t2 : T"
         "(\<And>s. t2 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma>\<langle>0:T2\<rangle> , \<Delta> \<turnstile>\<^sub>T s : T)"
  thus "(Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : T"
    apply -
    apply(erule redE)
    apply(fastforce simp add: subst_type)+
    apply(clarsimp)
    apply(drule struct_subst_type_command
       [where ?\<Delta> = \<Delta> and ?U = T and ?T1.0 = "T1 +\<^sub>t T2" and ?E = "CCase T \<diamond> Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2" and ?\<beta> = 0])
    apply auto
    done
next
  fix \<Gamma> \<Delta> t T s
  assume "env_type_shift \<Gamma> 0 , env_type_shift \<Delta> 0 \<turnstile>\<^sub>T t : T"
    and "\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> env_type_shift \<Gamma> 0 , env_type_shift \<Delta> 0 \<turnstile>\<^sub>T s : T"
    and "TAbs t \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : TAll T"
    by(auto elim: redE)
next
  fix \<Gamma> \<Delta> t T \<tau> s
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : TAll T"
    and "\<And>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<Gamma> , \<Delta> \<turnstile>\<^sub>T s : TAll T"
    and "TApp t \<tau> \<rightarrow>\<^sub>\<beta> s"
  thus "\<Gamma> , \<Delta> \<turnstile>\<^sub>T s : type_subst T 0 \<tau>"
    apply -
      apply(erule redE)
      apply(clarsimp simp add: subst_typing)
     apply clarsimp
    apply clarsimp
    apply(subgoal_tac "\<forall>x. \<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau> = liftM_ctxt (\<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau>) x")
     apply(insert struct_subst_type(2)[rule_format])
     apply(erule_tac x="\<Gamma>" in meta_allE)
     apply(erule_tac x="\<Delta>\<langle>0:TAll T\<rangle>" in meta_allE)
     apply(erule_tac x="c" in meta_allE)
     apply(erule_tac x="\<Delta>" in meta_allE)
     apply(erule_tac x="\<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau>" in meta_allE)
     apply(erule_tac x="type_subst T 0 \<tau>" in meta_allE)
     apply(erule_tac x="TAll T" in meta_allE)
     apply(erule_tac x=0 in meta_allE)
     apply(erule_tac x=0 in meta_allE)
     apply clarsimp
     apply(subgoal_tac "\<Gamma> , \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau> : type_subst T 0 \<tau> \<Leftarrow> TAll T")
      apply force
     apply(rule typing_ctxt.intros)
     apply blast
     apply simp
    done
qed

end