section\<open>Contexts\<close>

theory ContextFacts
  imports
    Types
    Reduction
begin

subsection\<open>Contextual typing judgements\<close>

inductive typing_ctxt :: 
  "(nat \<Rightarrow> type) \<Rightarrow> (nat \<Rightarrow> type) \<Rightarrow> ctxt \<Rightarrow> type \<Rightarrow> type \<Rightarrow> bool"  
     ("_ , _ \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t _ : _ \<Leftarrow> _" [50, 50, 50, 50, 50] 50)
where
  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<diamond> : T \<Leftarrow> T" |
  [intro]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : (T1\<rightarrow>T2) \<Leftarrow> U; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T1 \<rbrakk>
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (E \<^sup>\<bullet> t) : T2 \<Leftarrow> U" |

  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : Nat \<Leftarrow> U 
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (CSuc E) : Nat \<Leftarrow> U" |
  [intro]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T r : U; \<Gamma>, \<Delta> \<turnstile>\<^sub>T s : Nat\<rightarrow>U\<rightarrow>U; \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : Nat \<Leftarrow> T \<rbrakk>
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (CNrec U r s E) : U \<Leftarrow> T" |

  [intro]: "\<lbrakk>\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : TAll \<tau>' \<Leftarrow> U\<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E \<^sup>\<bullet>\<^sup>\<tau> \<tau> : type_subst \<tau>' 0 \<tau> \<Leftarrow> U" |
  
  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : Bool \<Leftarrow> U \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t2 : T \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t3 : T
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CIf T E t2 t3 : T \<Leftarrow> U" |

  [intro]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T1 \<Leftarrow> U; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t2 : T2 \<rbrakk>
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<lparr>E, t2\<rparr>\<^sub>l:(T1\<times>\<^sub>tT2) : T1\<times>\<^sub>tT2 \<Leftarrow> U" |
  [intro]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T2 \<Leftarrow> U; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t1 : T1 \<rbrakk>
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<lparr>t1, E\<rparr>\<^sub>r:(T1\<times>\<^sub>tT2) : T1\<times>\<^sub>tT2 \<Leftarrow> U" |

  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T1\<times>\<^sub>tT2 \<Leftarrow> U \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<Pi>\<^sub>1 E : T1 \<Leftarrow> U" |
  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T1\<times>\<^sub>tT2 \<Leftarrow> U \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<Pi>\<^sub>2 E : T2 \<Leftarrow> U" |

  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T1 \<Leftarrow> U \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CInl (T1+\<^sub>tT2) E : T1+\<^sub>tT2 \<Leftarrow> U" |
  [intro]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T2 \<Leftarrow> U \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CInr (T1+\<^sub>tT2) E : T1+\<^sub>tT2 \<Leftarrow> U" |
  [intro]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T1+\<^sub>tT2 \<Leftarrow> U; \<Gamma>\<langle>0:T1\<rangle>, \<Delta> \<turnstile>\<^sub>T t1 : T; \<Gamma>\<langle>0:T2\<rangle>, \<Delta> \<turnstile>\<^sub>T t2 : T \<rbrakk>
      \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2 : T \<Leftarrow> U"

inductive_cases typing_ctxt_elims [elim!]:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<diamond> : T \<Leftarrow> T1"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (E \<^sup>\<bullet> t) : T \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (CSuc E) : T \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t (CNrec T1 r s E) : T \<Leftarrow> U"
  
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E \<^sup>\<bullet>\<^sup>\<tau> \<tau> : T \<Leftarrow> U"

  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CIf T1 E t2 t3 : T \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<lparr>E, t2\<rparr>\<^sub>l:T1 : T \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<lparr>t1, E\<rparr>\<^sub>r:T1 : T \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<Pi>\<^sub>1 E : T1 \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t \<Pi>\<^sub>2 E : T2 \<Leftarrow> U"

  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CInl T2 E : T1 \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CInr T2 E : T1 \<Leftarrow> U"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2 : T1 \<Leftarrow> U"


subsection\<open>Correctness of contextual typing judgements\<close>

lemma term_to_ctxt_typing:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (ctxt_subst E r) : T"
    shows "\<exists>U. (\<Gamma>, \<Delta> \<turnstile>\<^sub>T r : U \<and> \<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T \<Leftarrow> U)"
using assms by(induction E arbitrary: \<Gamma> \<Delta> T r; force)

lemma ctxt_to_term_typing:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>c\<^sub>t\<^sub>x\<^sub>t E : T \<Leftarrow> U" "\<Gamma>, \<Delta> \<turnstile>\<^sub>T r : U"
    shows "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (ctxt_subst E r) : T"
  using assms by(induction arbitrary: r rule: typing_ctxt.induct) auto

subsection\<open>Reduction of contexts\<close>

lemma ctxt_reduction: 
  "is_nf t \<Longrightarrow> is_nf (\<mu>(T1\<rightarrow>T2) : c) \<Longrightarrow> ctxt_subst (\<diamond> \<^sup>\<bullet> t) (\<mu>(T1\<rightarrow>T2) : c) 
     \<rightarrow>\<^sub>\<beta> \<mu> T2 : (c[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet> (liftM_trm t 0))]\<^sup>C)"
  "is_nfC c \<Longrightarrow> ctxt_subst (CSuc \<diamond>) (\<mu> T : c)
     \<rightarrow>\<^sub>\<beta> \<mu> T : (c[(Some 0) = (Some 0) (CSuc \<diamond>)]\<^sup>C)"
  "is_nf s \<Longrightarrow> is_nf t \<Longrightarrow> is_nfC c \<Longrightarrow> ctxt_subst (CNrec T s t \<diamond>) (Mu T1 c)
     \<rightarrow>\<^sub>\<beta> \<mu> T : (c[(Some 0) = (Some 0) (CNrec T (liftM_trm s 0) (liftM_trm t 0) \<diamond>)]\<^sup>C)"
  by auto

end