section\<open>Syntax\<close>

theory Syntax
  imports Main
begin
  
subsection\<open>Types\<close>
  
datatype kind
  = Star ("\<star>")
  
datatype type = 
  TVar nat |
  TAll type |
  Nat |
  Bool |
  Bottom ("\<bottom>") |
  Prod type type (infix "\<times>\<^sub>t" 200) |
  Sum type type (infix "+\<^sub>t" 200) |
  Fun type type (infixr "\<rightarrow>" 200) 

subsection\<open>Terms\<close>
  
datatype trm =
  LVar nat    ("`_" [100] 100) |
  Lbd type trm ("\<lambda>_:_" [0, 60] 60) |
  App trm trm (infixl "\<degree>" 60) |
  Mu type cmd ("\<mu>_:_" [0, 60] 60) |
  
  TAbs trm |
  TApp trm type |
  
  Zero |
  S trm |
  Nrec type trm trm trm |

  TTrue ("true") | 
  TFalse ("false") |
  If type trm trm trm ("If _  _ Then _ Else _") |

  Pair trm trm type ("\<lparr>_,_\<rparr>:_") |
  Proj1 trm ("\<pi>\<^sub>1_") |
  Proj2 trm ("\<pi>\<^sub>2_") |

  Inl type trm |
  Inr type trm |
  Case type trm trm trm ("Case _  _ Of Inl\<Rightarrow> _|Inr\<Rightarrow> _")

and cmd = 
  MVar nat trm ("<_>") |
  Top trm ("<\<top>>")
      
subsection\<open>Contexts\<close>
  
datatype ctxt = 
  ContextEmpty ("\<diamond>") |
  ContextSuc ctxt ("CSuc") |
  ContextApp ctxt trm (infixl "\<^sup>\<bullet>" 75) |
  ContextTApp ctxt type (infixl "\<^sup>\<bullet>\<^sup>\<tau>" 75) |
  ContextNrec type trm trm ctxt ("CNrec") |

  ContextIf type ctxt trm trm ("CIf") |
  ContextPairL ctxt trm type ("\<lparr>_, _\<rparr>\<^sub>l:_") |
  ContextPairR trm ctxt type ("\<lparr>_, _\<rparr>\<^sub>r:_") |
  ContextProj1 ctxt ("\<Pi>\<^sub>1_") |
  ContextProj2 ctxt ("\<Pi>\<^sub>2_") |

  ContextInl type ctxt ("CInl") |
  ContextInr type ctxt ("CInr") |
  ContextCase type ctxt trm trm ("CCase _  _ Of CInl\<Rightarrow> _|CInr\<Rightarrow> _")
    
primrec ctxt_app :: "ctxt \<Rightarrow> ctxt \<Rightarrow> ctxt" (infix "." 60) where
  "\<diamond> . F = F" |
  "(CSuc E) . F = CSuc (E . F)" |
  "(E \<^sup>\<bullet> t) . F = (E . F) \<^sup>\<bullet> t" |
  "(E \<^sup>\<bullet>\<^sup>\<tau> \<tau>) . F = (E . F) \<^sup>\<bullet>\<^sup>\<tau> \<tau>" | (* ? *)
  "(CNrec T r s E) . F = CNrec T r s (E . F)" |
  "(CIf T E t2 t3) . F = CIf T (E . F) t2 t3" |
  
  "(\<lparr>E, t\<rparr>\<^sub>l:T) . F = \<lparr>(E . F), t\<rparr>\<^sub>l:T" |
  "(\<lparr>t, E\<rparr>\<^sub>r:T) . F = \<lparr>t, (E . F)\<rparr>\<^sub>r:T" |
  "(\<Pi>\<^sub>1 E) . F = (\<Pi>\<^sub>1 (E . F))" |
  "(\<Pi>\<^sub>2 E) . F = (\<Pi>\<^sub>2 (E . F))" |

  "(CInl T E) . F = CInl T (E . F)" |
  "(CInr T E) . F = CInr T (E . F)" |
  "(CCase T E Of CInl\<Rightarrow> t2|CInr\<Rightarrow> t3) . F = CCase T (E . F) Of CInl\<Rightarrow> t2|CInr\<Rightarrow> t3"

subsection\<open>Values\<close>

fun is_val :: "trm \<Rightarrow> bool" where
  "is_val (\<lambda> T : t) = True" |
  
  "is_val Zero = True" |
  "is_val (S n) = is_val n" |
  
  "is_val true = True" |
  "is_val false = True" |
  "is_val (\<lparr>t1, t2\<rparr>:T) = (is_val t1 \<and> is_val t2)" |

  "is_val (Inl T t) = is_val t" |
  "is_val (Inr T t) = is_val t" |
  
  "is_val (TAbs t) = True" |
  
  "is_val _ = False"
    
fun is_natval :: "trm \<Rightarrow> bool" where
  "is_natval Zero = True" |
  "is_natval (S n) = is_natval n" |
  "is_natval _ = False"
  
lemma is_natval: "is_natval v \<Longrightarrow> is_val v"
  by (induct rule: is_natval.induct) auto

end