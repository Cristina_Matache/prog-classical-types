subsection\<open>Examples\<close>

theory TypesExamples
imports Types
begin

schematic_goal "e\<langle>3:Nat\<rangle>, f \<turnstile>\<^sub>T (`3) : ?T"
  by force

schematic_goal "e, f \<turnstile>\<^sub>T Zero : ?T"
  by force

schematic_goal "e, f \<turnstile>\<^sub>T (S(S(S(S(Zero))))) : ?T"
  by force

schematic_goal "e, f \<turnstile>\<^sub>T (\<lambda> (T1\<rightarrow>T2) : Zero) : ?T"
  by force

schematic_goal "e, f \<turnstile>\<^sub>T (\<lambda> T1 : (`0)) : ?T"
  by force

schematic_goal "e\<langle>1:T1\<rangle>, f \<turnstile>\<^sub>T (\<lambda> T1 : (`0))\<degree>(`1) : ?T"
  by force

schematic_goal
  "e\<langle>0:(T1\<rightarrow>T2)\<rangle>\<langle>1:((T1\<rightarrow>T2)\<rightarrow>T3\<rightarrow>T1)\<rangle>, f \<turnstile>\<^sub>T 
      (\<lambda> T3 : ((`1) \<degree> ((`2) \<degree> (`1) \<degree> (`0)))) : ?T"
  by force

schematic_goal "e, f \<turnstile>\<^sub>T (\<lambda> T1 : (\<lambda> T2 : (`0))) : ?T"
  by force

schematic_goal 
  "e, f \<turnstile>\<^sub>T (\<lambda> ((T1\<rightarrow>T2)\<rightarrow>T3\<rightarrow>T1) : 
            (\<lambda> (T1\<rightarrow>T2) : 
              (\<lambda> T3 : ((`1) \<degree> ((`2) \<degree> (`1) \<degree> (`0)))))) : ?T"
  by force

schematic_goal 
  "e, f \<turnstile>\<^sub>T Nrec Nat (S Zero) (\<lambda> Nat : (\<lambda> Nat : (S(`0)))) (S(S(S Zero))) : ?T"
  by force

schematic_goal 
  "e, f \<turnstile>\<^sub>T Nrec ?T (\<lambda> T1 : (`0)) (\<lambda> Nat : (\<lambda> ?T : (`0))) (S(S(S Zero))) : ?T2"
  by force

schematic_goal "e, f\<langle>1:T\<rightarrow>T\<rangle> \<turnstile>\<^sub>C (<1> (\<lambda> T : (`0))) : ?T1"
  by force

schematic_goal "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<mu> ?T1 : (<0> (\<lambda> T : (`0))) : ?T2"
  by force

schematic_goal "e, f\<langle>0:T\<rightarrow>T\<rangle> \<turnstile>\<^sub>T \<mu> ?T1 : (<1> (\<lambda> T : (`0))) : ?T2"
  by force
    
schematic_goal "e, f \<turnstile>\<^sub>T TAbs (\<lambda> (TVar 1) : (\<lambda> (TVar 0) : (` 0))) : ?T2"
  apply(rule typing_trm_typing_cmd.intros)
  apply simp
  apply(rule typing_trm_typing_cmd.intros)
  apply(rule typing_trm_typing_cmd.intros)
  apply(rule typing_trm_typing_cmd.intros)
  apply force
  done

end