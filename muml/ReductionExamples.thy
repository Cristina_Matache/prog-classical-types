subsection\<open>Examples\<close>

theory ReductionExamples
  imports Reduction
begin

text\<open>Testing the functions and predicates defined in Reduction.thy\<close>

lemma "i\<ge>k \<Longrightarrow> flv_trm (`i) k = {i-k}"
  by simp

lemma "i<k \<Longrightarrow> flv_trm (`i) k = {}"
  by simp

lemma "flv_trm (\<lambda> T : ((`0)\<degree>(`1))) 0 = {0}"
  by simp

lemma "flv_trm (\<mu> T : (<1> (S Zero))) 0 = {}"
  by simp


lemma "i\<ge>k \<Longrightarrow> fmv_cmd (<i> t) k = {i-k} \<union> (fmv_trm t k)"
  by simp

lemma "i<k \<Longrightarrow> fmv_cmd (<i> t) k = (fmv_trm t k)"
  by simp

lemma "fmv_trm (\<mu> T : (<0> (\<mu> T1 : (<2> Zero)))) 0 = {0}"
  by simp

lemma "fmv_trm (\<lambda> T : ((`1)\<degree>(S (Nrec T1 Zero Zero Zero)))) 0 = {}"
  by simp

lemma "((\<lambda> T : (`0))\<degree>(\<lambda> T1 : (`0))\<degree>t) \<rightarrow>\<^sub>\<beta> (((`0)[(\<lambda> T1 : (`0))/0]\<^sup>T)\<degree>t)"
  by fastforce
    
schematic_goal "(\<lambda> T : (`0)) \<degree> (\<lambda> T1 : (`0)) \<rightarrow>\<^sub>\<beta> ?t"
  by auto

lemma "(\<lambda> T : (`0)) \<degree> (\<lambda> T1 : (`0)) \<rightarrow>\<^sub>\<beta> (\<lambda> T1 : (`0))"
  apply (subgoal_tac "(\<lambda> T : (`0)) \<degree> (\<lambda> T1 : (`0)) \<rightarrow>\<^sub>\<beta> (`0)[\<lambda>T1:`0/0]\<^sup>T")
  apply force+
  apply fastforce
  done

lemma "(\<lambda> T : (`0))\<degree>(\<lambda> T1 : (`0)) \<rightarrow>\<^sub>\<beta> (`0)[(\<lambda> T1 : (`0))/0]\<^sup>T"
  apply (rule beta)
  apply simp
  done

lemma "S (\<mu> T : (<0> Zero)) \<rightarrow>\<^sub>\<beta> S Zero"
  apply (rule red_trm_red_cmd.intros)
  apply (subgoal_tac "0 \<notin> fmv_trm Zero 0 \<and> is_val Zero \<and> dropM_trm Zero 0 = Zero")
  using rename apply metis
  apply force
  done

lemma "(\<mu> (T1\<rightarrow>T2) : (<1> (\<lambda> T1 : (S (`0)))))\<degree>Zero
   \<rightarrow>\<^sub>\<beta> \<mu> T2 : ((<1> (\<lambda> T1 : (S (`0))))[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet> (liftM_trm Zero 0))]\<^sup>C)"
  by fastforce

lemma "(\<mu> T : (<0> Zero)) \<rightarrow>\<^sub>\<beta> Zero"
  apply (subgoal_tac "0 \<notin> fmv_trm Zero 0 \<and> is_val Zero \<and> dropM_trm Zero 0 = Zero")
  using rename apply metis
  apply force
  done

lemma "Nrec T Zero (\<lambda> T : (\<lambda> T1 : (S (`1)))) Zero \<rightarrow>\<^sub>\<beta> Zero"
  by auto

lemma "Nrec T Zero (\<lambda> T : (\<lambda> T1 : (S (`1)))) (S (S Zero))
   \<rightarrow>\<^sub>\<beta> (\<lambda> T : (\<lambda> T1 : (S (`1))))\<degree>(S Zero)\<degree>(Nrec T Zero (\<lambda> T : (\<lambda> T1 : (S (`1)))) (S Zero))"
  by auto

lemma "is_nf r \<Longrightarrow> is_nf s \<Longrightarrow> is_nfC c \<Longrightarrow> Nrec T r s (\<mu> T1 : c)
    \<rightarrow>\<^sub>\<beta> \<mu> T : (c[(Some 0) = (Some 0) (CNrec T (liftM_trm r 0) (liftM_trm s 0) \<diamond>)]\<^sup>C)"
  by auto
    
schematic_goal
  "(\<mu> Nat : (<0> (S 
    ((\<lambda> (Nat\<rightarrow>Nat) : (\<mu> Nat : (<0> ((`0)\<degree>Zero))))\<degree>
    (\<lambda> Nat : (\<mu> Nat : (<1> (`0)))))
  ))) \<rightarrow>\<^sub>\<beta> ?t"
  by fastforce
    
schematic_goal "(\<lambda>Nat : (\<mu> Nat : <Suc (Suc 0)> (`0))) \<degree> Zero \<rightarrow>\<^sub>\<beta> ?t"
  by force
    
lemma
  "(\<mu> Nat : (<0> (S (((\<mu> Nat : 
    (<0> ((`0) \<degree> Zero)))[\<lambda> Nat : (\<mu> Nat : (<1> (`0)))/0]\<^sup>T)
  ))))
   \<rightarrow>\<^sub>\<beta> \<mu> Nat : (<0> (S 
        (\<mu> Nat : (<0> 
          (\<mu> Nat : (<2> Zero))))))"
  apply (rule red_trm_red_cmd.intros)+
  apply simp
  apply (rule red_trm_red_cmd.intros)+
  apply (subgoal_tac "(\<lambda>Nat : (\<mu> Nat : <Suc (Suc 0)> (`0))) \<degree> Zero \<rightarrow>\<^sub>\<beta> (\<mu> Nat:<Suc (Suc 0)> (`0))[Zero/0]\<^sup>T")
  apply (simp add: numeral_2_eq_2)
  apply (rule beta)
  apply simp
  done

schematic_goal
  "(\<mu> Nat : (<0> (S 
    (\<mu> Nat : (<0> 
      (\<mu> Nat : (<2> Zero))))))) \<rightarrow>\<^sub>\<beta> ?t"
  by fastforce

schematic_goal
  "(\<mu> Nat : (<0> (\<mu> Nat : ((<0> 
    (\<mu> Nat : (<2> Zero)))[(Some 0)=(Some 0) CSuc \<diamond>]\<^sup>C))))
  \<rightarrow>\<^sub>\<beta> ?t"
  by fastforce

lemma "(\<mu> Nat : (dropM_cmd (<1> Zero[(Some 0)=(Some 0) \<diamond>]\<^sup>C) 0)) \<rightarrow>\<^sub>\<beta> Zero"
  apply clarsimp
  apply (subgoal_tac "0 \<notin> fmv_trm Zero 0 \<and> is_val Zero \<and> dropM_trm Zero 0 = Zero")
  using rename apply metis
  apply force
  done

lemma 
  "(\<mu> Nat : (<0> (S 
    ((\<lambda> (Nat\<rightarrow>Nat) : (\<mu> Nat : (<0> ((`0)\<degree>Zero))))\<degree>(\<lambda> Nat : (\<mu> Nat : (<1> (`0)))))
  ))) \<rightarrow>\<^sub>\<beta>\<^sup>* Zero"
  apply (rule step_term, force, simp)+
done

end