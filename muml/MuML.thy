section\<open>$\mu\mathbf{ML}$ operational semantics\<close>

theory MuML
imports
    Progress
    "~~/src/HOL/Library/Monad_Syntax"
begin

fun (sequential) sem_trm :: "trm \<Rightarrow> trm option"
and sem_cmd :: "cmd \<Rightarrow> cmd option" 
where
  "sem_trm (s\<degree>t)
      = (if is_nf s then
          if is_nf t then
            (case s of
               (\<lambda> T:r)          \<Rightarrow> Some (r[t/0]\<^sup>T)
             | (\<mu> (T1\<rightarrow>T2) : c) \<Rightarrow> Some (\<mu> T2 : (c[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet> (liftM_trm t 0))]\<^sup>C))
             | _                \<Rightarrow> None)
          else do { u \<leftarrow> sem_trm t; Some (s\<degree>u) }
        else do { u \<leftarrow> sem_trm s; Some (u\<degree>t) })" |
  "sem_trm (TApp t \<tau>)
      = (if is_nf t then
            (case t of
              TAbs t \<Rightarrow> Some (subst_type_trm t 0 \<tau>)
            | \<mu> (TAll T) : c \<Rightarrow> Some (\<mu> (type_subst T 0 \<tau>) : c[(Some 0) = (Some 0) (\<diamond> \<^sup>\<bullet>\<^sup>\<tau> \<tau>)]\<^sup>C)
            | _ \<Rightarrow> None)
        else do { u \<leftarrow> sem_trm t; Some (TApp u \<tau>) })" |
  "sem_cmd (<i> t) 
      = (if is_nf t then 
          case t of 
            (\<mu> T : c) \<Rightarrow> Some (dropM_cmd (c[(Some 0) = (Some i) \<diamond>]\<^sup>C) i)
          | _ \<Rightarrow> None
        else
         do { u \<leftarrow> sem_trm t; Some (<i> u) })" |
  "sem_cmd (<\<top>> t) 
      = (if is_nf t then 
          case t of 
            (\<mu> T : c) \<Rightarrow> Some (dropM_cmd (c[(Some 0) = None \<diamond>]\<^sup>C) 0)
           | _ \<Rightarrow> None
         else
           do { u \<leftarrow> sem_trm t; Some (<\<top>> u) })" |
  "sem_trm (\<mu> T : c)
      = (case c of
           (<0> t) \<Rightarrow> (if is_val t \<and> 0 \<notin> (fmv_trm t 0) then
                        Some (dropM_trm t 0) 
                     else do { d \<leftarrow> sem_cmd c; Some (\<mu> T : d) })
         | _ \<Rightarrow> do { d \<leftarrow> sem_cmd c; Some (\<mu> T : d) })"  |
  "sem_trm (S t) 
      = (case t of
          (\<mu> T : c) \<Rightarrow> if is_nfC c then
                        Some (\<mu> T : (c[(Some 0) = (Some 0) (CSuc \<diamond>)]\<^sup>C))
                       else do { u \<leftarrow> sem_trm t; Some (S u) }
         | _ \<Rightarrow> do { u \<leftarrow> sem_trm t; Some (S u) })" |
  "sem_trm (Nrec T r s t) 
      = (if is_nf r then
            if is_nf s then
              (case t of
                   Zero \<Rightarrow> Some r
                 | (S n) \<Rightarrow> if is_natval t then Some (s\<degree>n\<degree>(Nrec T r s n))
                            else do { u \<leftarrow> sem_trm t; Some (Nrec T r s u) }
                 | (\<mu> T1 : c) \<Rightarrow> if is_nfC c then
                                    Some (\<mu> T : (c[(Some 0) = (Some 0) (CNrec T (liftM_trm r 0) (liftM_trm s 0) \<diamond>)]\<^sup>C))
                                  else do { u \<leftarrow> sem_trm t; Some (Nrec T r s u) }
                  | _ \<Rightarrow> do { u \<leftarrow> sem_trm t; Some (Nrec T r s u) })
            else do { u \<leftarrow> sem_trm s; Some (Nrec T r u t) }
          else do { u \<leftarrow> sem_trm r; Some (Nrec T u s t) })" |
  "sem_trm (If T t1 Then t2 Else t3) 
      = (case t1 of
           true \<Rightarrow> Some t2
         | false \<Rightarrow> Some t3
         | (\<mu> U: c) \<Rightarrow> if is_nfC c then
                        Some (\<mu> T:(c[(Some 0) = (Some 0) (CIf T \<diamond> (liftM_trm t2 0) (liftM_trm t3 0))]\<^sup>C))
                       else do { u \<leftarrow> sem_trm t1; Some (If T u Then t2 Else t3) }
         | _ \<Rightarrow> do { u \<leftarrow> sem_trm t1; Some (If T u Then t2 Else t3) })" |
  "sem_trm (\<pi>\<^sub>1 t) 
      = (case t of
           \<lparr>t1, t2\<rparr>:T \<Rightarrow> if is_val t1 \<and> is_val t2 then Some t1
                        else do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>1 u) }
         | (\<mu> T1\<times>\<^sub>tT2: c) \<Rightarrow> if is_nfC c then
                              Some (\<mu> T1:(c[(Some 0) = (Some 0) (\<Pi>\<^sub>1 \<diamond>)]\<^sup>C))
                            else do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>1 u) }
         | _ \<Rightarrow> do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>1 u) })" |
  "sem_trm (\<pi>\<^sub>2 t) 
      = (case t of
           \<lparr>t1, t2\<rparr>:T \<Rightarrow> if is_val t1 then 
                          if is_val t2 then Some t2
                          else do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>2 u) } 
                        else do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>2 u) }
         | (\<mu> T1\<times>\<^sub>tT2: c) \<Rightarrow> if is_nfC c then
                              Some (\<mu> T2:(c[(Some 0) = (Some 0) (\<Pi>\<^sub>2 \<diamond>)]\<^sup>C))
                            else do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>2 u) }
         | _ \<Rightarrow> do { u \<leftarrow> sem_trm t; Some (\<pi>\<^sub>2 u) })" |
  "sem_trm (\<lparr>t1, t2\<rparr>:T)
      = (if is_val t1 then
          case t2 of
            \<mu> T2: c \<Rightarrow> if is_nfC c then 
                        Some (\<mu> T:(c[(Some 0) = (Some 0) \<lparr>(liftM_trm t1 0), \<diamond>\<rparr>\<^sub>r:T]\<^sup>C))
                       else do {u \<leftarrow> sem_trm t2; Some \<lparr>t1, u\<rparr>:T}
          | _ \<Rightarrow> do { u \<leftarrow> sem_trm t2; Some \<lparr>t1, u\<rparr>:T}
        else 
          case t1 of
            \<mu> T1: c \<Rightarrow> if is_nfC c then
                        Some (\<mu> T:(c[(Some 0) = (Some 0) \<lparr>\<diamond>, (liftM_trm t2 0)\<rparr>\<^sub>l:T]\<^sup>C))
                       else do { u \<leftarrow> sem_trm t1; Some \<lparr>u, t2\<rparr>:T}
           | _ \<Rightarrow> do { u \<leftarrow> sem_trm t1; Some \<lparr>u, t2\<rparr>:T})" |
  "sem_trm (Inl T s)
    = (case s of
        \<mu> U: c \<Rightarrow> if is_nfC c then
                    Some (\<mu> T:(c[(Some 0) = (Some 0) (CInl T \<diamond>)]\<^sup>C))
                  else do { u \<leftarrow> sem_trm s; Some (Inl T u) }
       | _ \<Rightarrow> do { u \<leftarrow> sem_trm s; Some (Inl T u) })" |
  "sem_trm (Inr T s)
    = (case s of
         \<mu> U: c \<Rightarrow> if is_nfC c then
                      Some (\<mu> T:(c[(Some 0) = (Some 0) (CInr T \<diamond>)]\<^sup>C))
                   else do { u \<leftarrow> sem_trm s; Some (Inr T u) }
       | _ \<Rightarrow> do { u \<leftarrow> sem_trm s; Some (Inr T u) })" |
  "sem_trm (Case U t Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2)
    = (case t of
         (Inl T s) \<Rightarrow> if is_val s then Some (t1[s/0]\<^sup>T)
                      else do { u \<leftarrow> sem_trm t; Some (Case U u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) }
       | (Inr T s) \<Rightarrow> if is_val s then Some (t2[s/0]\<^sup>T)
                      else do { u \<leftarrow> sem_trm t; Some (Case U u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) }
       | (\<mu> T: c) \<Rightarrow> if is_nfC c then
                         Some (\<mu> U:(c[(Some 0) = (Some 0) (CCase U \<diamond> Of CInl\<Rightarrow> (liftM_trm t1 0)|CInr\<Rightarrow> (liftM_trm t2 0))]\<^sup>C))
                     else do { u \<leftarrow> sem_trm t; Some (Case U u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) }
       |  _ \<Rightarrow> do { u \<leftarrow> sem_trm t; Some (Case U u Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) })" |
  "sem_trm _ = None"

export_code open sem_trm sem_cmd type_term 
   in "OCaml" file "ocaml/reduction.ml"
   
subsection\<open>Soundness\<close>
  
lemma nf_dont_reduce:
      "is_nf t \<Longrightarrow> sem_trm t = None"
      "is_nfC c \<Longrightarrow> sem_cmd c = None"
      apply (induct t and c)
      apply force
      apply force
      apply force
                  defer
                  apply force
      apply force
      apply (drule is_nfD)
      apply clarsimp
      apply (case_tac x; clarsimp)
      apply force
      apply force
      apply force
      apply force
      apply clarsimp
      apply (subgoal_tac "is_nf x1 \<and> is_nf x2")
      apply clarsimp
      apply (subgoal_tac "\<not> (\<exists>T c. x1 = \<mu> T : c)")
      apply (subgoal_tac "\<not> (\<exists>T c. x2 = \<mu> T : c)")
      prefer 2
      apply force
      prefer 2
      apply force
      apply (case_tac x1; case_tac x2; clarsimp)
      apply force
      apply force
      apply force
      apply clarsimp
      apply (case_tac x2; clarsimp)
      apply clarsimp
      apply (case_tac x2; clarsimp)
      apply force
      apply clarsimp
      apply (rule conjI)
      apply clarsimp
      apply (case_tac x2; clarsimp)
      apply clarsimp
      apply force
      apply clarsimp
      apply (case_tac x; clarsimp)
      apply clarsimp
      apply (case_tac x2; clarsimp)
      apply (rule conjI)
      apply clarsimp
      apply (case_tac x12; clarsimp; case_tac x11; clarsimp)
      apply (case_tac x12; clarsimp; case_tac x11; clarsimp)
      done
    
lemma proj1_soundness: "(\<And>u. sem_trm x = Some u \<Longrightarrow> x \<rightarrow>\<^sub>\<beta> u) \<Longrightarrow> sem_trm (\<pi>\<^sub>1 x) = Some u \<Longrightarrow> (\<pi>\<^sub>1 x) \<rightarrow>\<^sub>\<beta> u"
  apply (case_tac x)
  apply force
  apply force
  apply (force split: bind_splits)
                defer
                apply (force split: bind_splits)
      apply (force split: bind_splits)
  apply force
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply force
  apply force
  apply (force split: bind_splits)
  defer
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply clarsimp
   apply (case_tac x41; clarsimp)
           apply (force split: bind_splits)
      apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)

  prefer 2
  apply (force split: bind_splits)
  prefer 2
  apply (force split: bind_splits)
  apply (case_tac "is_nfC x42")
  apply force
  apply (force split: bind_splits)
  apply (clarsimp simp del: sem_trm.simps)
  apply (erule sem_trm.elims; clarsimp)
  apply (case_tac "is_val x131")
  prefer 2
  apply (force split: if_split_asm bind_splits)
  apply clarsimp
  apply (case_tac "is_val x132")
  prefer 2
  apply (force split: if_split_asm bind_splits)
  apply clarsimp  
  apply (case_tac "\<exists>T c. u = \<mu> T:c")
  prefer 2
  apply force
  apply (case_tac "\<exists>T c. x132 = \<mu> T:c")
  prefer 2
  apply force
  apply clarsimp
  done

lemma proj2_soundness: "(\<And>u. sem_trm x = Some u \<Longrightarrow> x \<rightarrow>\<^sub>\<beta> u) \<Longrightarrow> sem_trm (\<pi>\<^sub>2 x) = Some u \<Longrightarrow> (\<pi>\<^sub>2 x) \<rightarrow>\<^sub>\<beta> u"
  apply (case_tac x)
  apply force
  apply force
  apply (force split: bind_splits)
  defer
                apply force
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply force
  apply force
  apply (force split: bind_splits)
  defer
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply clarsimp
   apply (case_tac x41; clarsimp)
      apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  apply (force split: bind_splits)

  prefer 2
  apply (force split: bind_splits)
      prefer 2

  apply (force split: bind_splits)
  apply (case_tac "is_nfC x42")
  apply force
  apply (force split: bind_splits)
  
  apply (clarsimp simp del: sem_trm.simps)
  apply (erule sem_trm.elims; clarsimp)
  apply (case_tac "is_nf x131")
  prefer 2
  apply (force split: if_split_asm bind_splits)
  apply (case_tac "is_nf x132")
  prefer 2
  apply (force split: if_split_asm bind_splits)
  apply (case_tac "\<exists>T c. u = \<mu> T:c")
  prefer 2
  apply force
  apply (case_tac "\<exists>T c. x132 = \<mu> T:c")
  prefer 2
  apply force
  apply clarsimp
  apply force
  done

lemma soundness:
  "\<forall>u. sem_trm t = Some u \<longrightarrow> t \<rightarrow>\<^sub>\<beta> u"
  "\<forall>d. sem_cmd c = Some d \<longrightarrow> c \<^sub>C\<rightarrow>\<^sub>\<beta> d"
  apply (induct t and c)
  (* Lvar *)
  apply force
  (* Lbd *)
  apply force
  (* App *)
  apply (case_tac "is_nf x1")
  apply (case_tac "is_nf x2")
  apply clarsimp
  apply (case_tac x1; clarsimp)
  apply force
  apply (case_tac x41; clarsimp)
  apply force
  apply (force split: bind_splits)
  apply (force split: bind_splits)
  (* Mu *)
  apply clarsimp
  apply (case_tac x2)
  apply clarsimp
  apply (case_tac x11)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
                  apply (force split: if_split_asm bind_splits)
   (* TAbs *)
                 apply (force split: if_split_asm bind_splits)
   (* TApp *)
defer

  (* Zero *)
    apply force

  (* S n *)
  apply clarsimp
  apply (case_tac x; clarsimp)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
                      apply (force split: if_split_asm bind_splits)
      apply (force split: if_split_asm bind_splits)
  apply (smt red_trm_red_cmd.intros bind_eq_Some_conv option.sel)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  (* Nrec *)
  apply clarsimp
  apply (rule conjI)
  apply clarsimp
  apply (rule conjI)
  apply clarsimp
  apply (case_tac "\<exists>T1 c. x4 = \<mu> T1:c")
  apply clarsimp
  apply (force split: if_split_asm bind_splits)
  apply (case_tac "x4 = Zero")
  apply force
  apply (case_tac "\<exists>n. x4 = S n")
  apply clarsimp
  apply (case_tac "is_natval n")
  apply force
  apply clarsimp
  apply (smt bind_eq_Some_conv red_trm_red_cmd.intros option.sel)
  apply (subgoal_tac "sem_trm x4 \<bind> (\<lambda>u. Some (Nrec x1 x2 x3 u)) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x4; clarsimp)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  
  (* true *)
  apply force
  (* false *)
  apply force
  (* if then else *)
  apply clarsimp
  apply (case_tac "\<exists>U c. x2 = \<mu> U:c")
  apply clarsimp
  apply (force split: if_split_asm bind_splits)
  apply (case_tac "x2 = true")
  apply force
  apply (case_tac "x2 = false")
  apply force
  apply (subgoal_tac "sem_trm x2 \<bind> (\<lambda>u. Some If x1  u Then x3 Else x4) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x2; clarsimp)
  (* pair *)
  apply (case_tac "is_val x1")
  apply clarsimp
  apply (case_tac "\<exists>U c. x2 = \<mu> U:c")
  apply (force split: if_split_asm bind_splits)
  apply (subgoal_tac "sem_trm x2 \<bind> (\<lambda>u. Some \<lparr>x1,u\<rparr>:x3) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x2; clarsimp)
  apply (case_tac "\<exists>U c. x1 = \<mu> U:c")
  apply (force split: if_split_asm bind_splits)
  apply clarsimp
  apply (subgoal_tac "sem_trm x1 \<bind> (\<lambda>u. Some \<lparr>u,x2\<rparr>:x3) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x1; force)
  
  (* proj1 *)
  using proj1_soundness apply force
  (* proj2 *)
  using proj2_soundness apply force
  
  (* Inl *)
  apply clarsimp
  apply (case_tac "\<exists>U c. x2 = \<mu> U:c")
  apply (force split: if_split_asm bind_splits)
  apply (subgoal_tac "sem_trm x2 \<bind> (\<lambda>u. Some (trm.Inl x1 u)) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x2; clarsimp)
  (* Inr *)
  apply clarsimp
  apply (case_tac "\<exists>U c. x2 = \<mu> U:c")
  apply (force split: if_split_asm bind_splits)
  apply (subgoal_tac "sem_trm x2 \<bind> (\<lambda>u. Some (trm.Inr x1 u)) = Some u")
  apply (force split: if_split_asm bind_splits)
  apply (case_tac x2; clarsimp)
  (* case *)
  apply clarsimp
  apply (case_tac x2; clarsimp)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
             apply (force split: if_split_asm bind_splits)
      apply (force split: if_split_asm bind_splits)
  apply (smt bind_eq_Some_conv red_trm_red_cmd.intros option.sel)
  apply (force split: if_split_asm bind_splits)
  apply (smt bind_eq_Some_conv red_trm_red_cmd.intros option.sel)
  
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  
  (* <x> t *)
  apply clarsimp
  apply (rule conjI)
  apply clarsimp
  apply (case_tac x2; force split: if_split_asm bind_splits)
  apply (force split: if_split_asm bind_splits)
  (* <T> t *)
  apply clarsimp
   apply (case_tac x; force split: if_split_asm bind_splits)
  (* TApp *)
  apply clarsimp
  apply (case_tac "is_nf x1")
   apply clarsimp
  defer
   apply (force split: if_split_asm bind_splits)
  apply (case_tac x1; clarsimp)
   defer
   apply force
  apply (case_tac x41; clarsimp)
  apply (rule red_trm_red_cmd.intros(30))
    apply force
  done

subsection\<open>Completeness\<close>
  
lemma completeness:
  "(t \<rightarrow>\<^sub>\<beta> u \<longrightarrow> sem_trm t = Some u)
  \<and> (c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<longrightarrow> sem_cmd c = Some d)"
  apply (induct rule: red_trm_red_cmd.induct)
  (* beta *)
  apply force
  (* muApp *)
  apply force
  (* muRename *)
    apply force
  (* type_beta *)
  apply force
  (* muVar *)
  apply clarsimp
  (* mVarTop *)
  apply clarsimp
  (* appL *)
  apply (frule red_not_nfD)
  apply clarsimp
  (* appR *)
  apply (frule red_not_nfD)
  apply clarsimp
  (* mu *)
  apply (case_tac c)
  prefer 2
  apply force
  apply (case_tac x11)
  prefer 2
  apply force
  apply (case_tac "is_nf x12")
  prefer 2
  apply force
  apply clarsimp
  apply (case_tac x12; clarsimp)
  (* mvar2 *)
  apply clarsimp
  apply (frule red_not_nfD)
  apply clarsimp
  (* mtop *)
  apply clarsimp
  apply (frule red_not_nfD)
  apply clarsimp
  (* nrecZero *)
  apply clarsimp
  (* nrecSuc *)
  apply clarsimp
  (* muSuc *)
  apply clarsimp
  (* nrecMu *)
  apply clarsimp
  (* suc *)
  apply (case_tac "\<exists>T c. s = \<mu> T : c")
  prefer 2
  apply (case_tac s; clarsimp)
  apply clarsimp
  using red_not_nfD2 apply force
  (* nrecL *)
  apply (frule red_not_nfD)
  apply clarsimp
  (* nrecM *)
  apply (frule red_not_nfD)
  apply clarsimp
  (* nrecR *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t = \<mu> T : c")
  using red_not_nfD2 apply force
  apply (case_tac t; clarsimp)
  using is_natval apply force
  (* if1 *)
  apply clarsimp
  (* if2 *)
  apply clarsimp
  (* if3 *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t1 = \<mu> T : c")
  using red_not_nfD2 apply force
  apply (case_tac t1; clarsimp)
  (* proj1 *)
  apply force
  (* proj2 *)
  apply force
  (* proj3 *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t = \<mu> T : c")
  prefer 2
  apply (case_tac "\<exists>t1 t2 T. t = \<lparr>t1, t2\<rparr>:T")
  prefer 2
  apply (case_tac t; clarsimp)
  apply (erule exE)+
  apply (case_tac "is_nf t1 \<and> is_nf t2")
  prefer 2
  apply force
  defer
 defer
defer    

(* TApp *)
defer
  (* pair 1*)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t1 = \<mu> T : c")
  using red_not_nfD2 apply force
  apply (case_tac t1; clarsimp)
  (* pair 2 *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t2 = \<mu> T : c")
  using red_not_nfD2 apply force
                    apply (case_tac t2; clarsimp)
    defer
  (* if_mu *)
  apply clarsimp
  (* pair1_mu *)
  apply clarsimp
  (* pair2_mu *)
  apply clarsimp
  (* proj1_mu *)
  apply clarsimp
  (* proj2_mu *)
  apply clarsimp
  (* case_inl *)
  apply clarsimp
  (* case_inr *)
  apply clarsimp
  (* case_step *)
  defer
  (* inl_step *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. s = \<mu> T : c")
  using red_not_nfD2 apply force
  apply (case_tac s; clarsimp)
  (* inr_step *)
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. s = \<mu> T : c")
  using red_not_nfD2 apply force
  apply (case_tac s; clarsimp)     
  (* inl_mu *)
  apply clarsimp
  (* inr_mu *)
  apply clarsimp
  (* case_mu *)
  apply clarsimp
  
  apply (frule red_not_nfD)
  defer
  apply (erule exE)+
  apply clarify
  apply (case_tac "\<exists>T1 T2. T = T1\<times>\<^sub>tT2")
  apply (erule exE)+
  apply clarify
  apply force
  apply (case_tac T; clarsimp)
  defer
      defer
    defer
  apply clarsimp
  apply (case_tac "\<exists>T c. s = \<mu> T : c")
  apply clarsimp
  apply (case_tac c)
  apply clarsimp
  apply (case_tac x11)
  apply clarsimp
  apply (subgoal_tac "is_nf x12")
  prefer 2
  apply force
  apply clarsimp
  apply (case_tac x12; clarsimp)
  apply clarsimp
  apply (subgoal_tac "is_nf x12")
  prefer 2
  apply force
  apply clarsimp
  apply (case_tac x12; clarsimp)
  apply clarsimp
  apply (subgoal_tac "is_nf x2")
  prefer 2
  apply force
  apply clarsimp
  apply (case_tac x2; clarsimp)
  
  apply (frule red_not_nfD)
  apply (case_tac "\<exists> T sa. s = Inl T sa")
  apply force
  apply (case_tac "\<exists> T sa. s = Inr T sa")
  apply force   
  apply (case_tac s; clarsimp)
  apply force
  
  apply (frule red_not_nfD)
  apply (case_tac "\<exists>T c. t = \<mu> T : c")
  prefer 2
  apply (case_tac "\<exists>t1 t2 T. t = \<lparr>t1, t2\<rparr>:T")
  prefer 2
  apply (case_tac t; clarsimp)
  apply (erule exE)+
  apply (case_tac "is_nf t1 \<and> is_nf t2")
  prefer 2
  apply force
  apply (frule red_not_nfD)
  defer
  apply (erule exE)+
   apply clarify
  apply (case_tac "\<exists>T1 T2. T = T1\<times>\<^sub>tT2")
  apply (erule exE)+
  apply clarify
  apply force
     apply (case_tac T; clarsimp)
    prefer 3
    apply force
   defer
   apply clarsimp
  apply (frule red_not_nfD)
  apply (case_tac t; clarsimp)
  done

    
end