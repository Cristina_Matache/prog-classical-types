{

open Lexing
open Parser
open Printf

exception SyntaxError of string

let file = "tests/lexer_output.txt"
let tokens = ref []

let print_string_list ls file =
     let output = open_out file in
          List.iter (fprintf output "%s ") ls;
          close_out output 

(* From "Real World Ocaml", Chapter 16*)
let next_line lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_bol = lexbuf.lex_curr_pos;
               pos_lnum = pos.pos_lnum + 1
    }

}

(*let var_regex = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']* 
Only allow one-letter variable names for now*)
let var_regex = ['a'-'o' 'u'-'z' 'A'-'O' 'U'-'Z']
let tvar_regex = ['p'-'t' 'P'-'T']
let int_regex = ['0'-'9']+
let newline = '\r' | '\n' | "\r\n"

rule token verbose = parse
     |[' ' '\t']    {token verbose lexbuf}
     |';'           {tokens := "SEMICOLON"::(!tokens) ; SEMICOLON}
     |"(*"          {comment verbose lexbuf}
     |newline       {tokens := "\n"::(!tokens) ; next_line lexbuf; token verbose lexbuf}
     |int_regex     {tokens := "NAT"::(!tokens) ; NAT (int_of_string (Lexing.lexeme lexbuf))}

     |"abort"       {tokens := "ABORT"::(!tokens) ; ABORT}
     |"fun"         {tokens := "FUN"::(!tokens) ; FUN}
     |"nrec"        {tokens := "NREC"::(!tokens) ; NREC}
     |"bind"        {tokens := "BIND"::(!tokens) ; BIND}
     |"tabs"        {tokens := "TABS"::(!tokens) ; TABS}
     |"suc"         {tokens := "SUC"::(!tokens); SUC}
     |"end"         {tokens := "END"::(!tokens) ; END}

     |"true"        {tokens := "TRUE"::(!tokens) ; TRUE}
     |"false"       {tokens := "FALSE"::(!tokens) ; FALSE}
     |"if"          {tokens := "IF"::(!tokens) ; IF}
     |"then"        {tokens := "THEN"::(!tokens) ; THEN}
     |"else"        {tokens := "ELSE"::(!tokens) ; ELSE}    

     |"{"           {tokens := "LCBR"::(!tokens) ; LCBR}
     |"}"           {tokens := "RCBR"::(!tokens) ; RCBR}
     |"proj1"       {tokens := "PROJ1"::(!tokens) ; PROJ1}
     |"proj2"       {tokens := "PROJ2"::(!tokens) ; PROJ2}
     |"*"           {tokens := "TIMES"::(!tokens) ; TIMES}

     |"inl"         {tokens := "INL"::(!tokens) ; INL}
     |"inr"         {tokens := "INR"::(!tokens) ; INR}
     |"case"        {tokens := "CASE"::(!tokens) ; CASE}
     |"of"          {tokens := "OF"::(!tokens) ; OF}
     |"|"           {tokens := "BAR"::(!tokens) ; BAR}
     |"+"           {tokens := "PLUS"::(!tokens) ; PLUS}

     |'['           {tokens := "LBRACK"::(!tokens) ; LBRACK}
     |']'           {tokens := "RBRACK"::(!tokens) ; RBRACK}
     |'('           {tokens := "LPARAN"::(!tokens) ; LPARAN}
     |')'           {tokens := "RPARAN"::(!tokens) ; RPARAN}
     |':'           {tokens := "COLON"::(!tokens) ; COLON}
     |"->"          {tokens := "ARROW"::(!tokens) ; ARROW}
     |'.'           {tokens := "DOT"::(!tokens) ; DOT}
     |','           {tokens := "COMMA"::(!tokens) ; COMMA}

     |"nat"         {tokens := "NATTYPE"::(!tokens) ; NATTYPE}

     |"bot"         {tokens := "BOTTYPE"::(!tokens) ; BOTTYPE}
     |"bool"        {tokens := "BOOLTYPE"::(!tokens) ; BOOLTYPE}
     |"forall"      {tokens := "FORALL"::(!tokens) ; FORALL}

     |var_regex     {tokens := "VAR"::(!tokens) ; VAR (Lexing.lexeme lexbuf)}
     |tvar_regex    {tokens := "TVAR"::(!tokens) ; TVAR (Lexing.lexeme lexbuf)}
     |eof           {tokens := "EOF"::(!tokens); if verbose then print_string_list (List.rev !tokens) file else (); EOF}
     |_             {raise (SyntaxError ("Unexpected character: "^Lexing.lexeme lexbuf))}     

(* Handle comments (not nested)*)
and comment verbose = parse
     |"*)"          {token verbose lexbuf}
     |newline       {next_line lexbuf; comment verbose lexbuf}
     |_             {comment verbose lexbuf}
