open Interpreter
open Testing


(* the name of the file which contains the expressions *)
let repl = ref true
let filename = ref ""
let verbose_frontend = ref false
let verbose_translation = ref false
let verbose_typing = ref false
let evaluated_output = ref "tests/evaluated_output.txt"

let parser_output = "tests/parser_output.txt"
let typing_output = "tests/typing_output.txt" 

let option_spec = [
     ("-tfront", Arg.Set verbose_frontend, "test the lexer and parser; the output is witten to tests/lexer_output.txt and parser_output.txt respectively");
     ("-ttrans", Arg.Set verbose_translation, "test the De Bruijn and to-Isabelle-AST translations; the output is witten to test/dbast_output.txt and isabelleast_output.txt  respectively");
     ("-ttypes", Arg.Set verbose_typing, "test the typing function; the output is written to typing_output.txt");
     ("-out", Arg.Set_string evaluated_output, "<file>.txt the file where the evaluated output is to be written; only works when no other options are used; otherwise, the default location tests/evaluated_output.txt is used")
]

let usage_msg = "Usage: main.native [options] [<file>.txt]\nIf no arguments are provided the REPL starts\nOptions are:"

let set_input f = 
     filename := f; repl := false 

let _ = Arg.parse option_spec set_input usage_msg


let rec add_newline ls = match ls with
     |[] -> []
     |x::xs -> x::"\n"::(add_newline xs)

(* Check if any tests need to be run, and run them *)

let _ = if !verbose_frontend then
          let input = open_in !filename in
          Lexer.print_string_list (add_newline (test_frontend_ounit input)) parser_output;
          Printf.printf "Lexer and parser were tested\n\n";
          close_in input
        else ()

let _ = if !verbose_translation then
          let input = open_in !filename in
          test_translation input;
          Printf.printf "Translation was tested\n\n";
          close_in input
        else ()

let _ = if !verbose_typing then
          let input = open_in !filename in
          Lexer.print_string_list (add_newline (test_typing_ounit input)) typing_output;
          Printf.printf "Typing was tested\n\n";
          close_in input
        else ()


(* Call the main function of the interpreter *)

let _ = if not (!repl || !verbose_frontend || !verbose_translation || !verbose_typing) then     
          let input = open_in !filename in          
          process input !evaluated_output;
          Printf.printf "Input was interpreted\n\n";
          close_in input
        else ();;


(* The repl *)

let read_input instr =
     if instr <> "" then 
          let lexbuf = Lexing.from_string instr in 
          Parser.start (Lexer.token false) lexbuf
     else []

let _= 
     if !repl then try
          while true do
               try print_string "> ";
                   flush stdout;
                   let input = List.map Parsedast_to_isast.translate_expr 
                         (List.map Parsedast_to_isast.dbexpr (read_input (read_line ()))) in
                   match input with
                        |[] -> ()
                        |l::ls -> try let ty = Reduction.Types.type_term empty_env empty_env l in
                                      try let e = reduce l in
                                          Printf.printf "%s : %s\n" (Parsedast_to_isast.pp_dBT e) 
                                            (Parsedast_to_isast.pp_typea ty [] (ref "p") (ref []) 0)
                                      with Match_failure _ -> print_endline "Reduction error"
                                          |Failure msg -> print_endline ("Reduction error: "^msg)
                                  with  _ -> print_endline "Type error"
               with Parser.Error -> print_endline "Parse error"
                   |Lexer.SyntaxError msg -> print_endline msg       
          done
     with End_of_file -> print_endline ""
     else ()
