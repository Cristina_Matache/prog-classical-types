datatype nat = Zero 
             | Suc of nat;
fun add Zero n = n 
  | add (Suc m) n = Suc (add m n);

add (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc (Suc Zero)))))))))))))))))))))))))))))))))))))))) (Suc (Suc (Suc (Suc (Suc Zero)))));

