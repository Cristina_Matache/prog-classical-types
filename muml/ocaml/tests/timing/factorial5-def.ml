datatype nat = Zero 
             | Suc of nat;

fun add Zero n = n 
  | add (Suc m) n = Suc (add m n);

fun times Zero n = Zero
  | times m Zero = Zero
  | times (Suc m) n = add n (times m n);

fun fact Zero = (Suc Zero)
  | fact (Suc n) = times (Suc n) (fact n);

fact (Suc (Suc (Suc (Suc (Suc Zero)))));
