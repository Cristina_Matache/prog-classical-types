open OUnit2
open Testing
open Interpreter

let parser_basicsyntax = "tests/ounit/parser_basicsyntax.lmt"
let parser_application = "tests/ounit/parser_application.lmt"
let parser_successor = "tests/ounit/parser_successor.lmt"
let parser_abort = "tests/ounit/parser_abort.lmt"
let parser_if = "tests/ounit/parser_if.lmt"
let parser_pairs = "tests/ounit/parser_pairs.lmt"
let parser_sums = "tests/ounit/parser_sums.lmt"

let parser_tests = "tests for parser" >:::[
     "basic syntax" >:: (fun _ -> assert_equal [
                            "4";
                            "111";
                            "(((((x b) f) e) w) g)";
                            "(x y)";
                            "bind(a:bot) -> [a].x end";
                            "nrec:(nat) -> (x, y, z) end";
                            "fun(x:(nat->bot)) -> x end"]
                           (test_frontend_ounit (open_in parser_basicsyntax)));
     "application" >:: (fun _ -> assert_equal [
                            "(fun(x:nat) -> x end 3)";
                            "(fun(x:(nat->nat)) -> x end fun(x:nat) -> x end)"]
                           (test_frontend_ounit (open_in parser_application)));
     "successor" >:: (fun _ -> assert_equal [
                            "suc (x)";
                            "suc (fun(x:nat) -> x end)";
                            "suc ((x y))";
                            "suc (bind(a:bot) -> [a].x end)";
                            "suc (nrec:(nat) -> (x, y, z) end)"]
                           (test_frontend_ounit (open_in parser_successor)));
     "abort" >:: (fun _ -> assert_equal [
                            "bind(a:nat) -> [abort].3 end";
                            "fun(y:((nat->bot)->bot)) -> bind(a:nat) -> [abort].(y fun(x:nat) -> bind(b:bot) -> [a].x end end) end end"]
                           (test_frontend_ounit (open_in parser_abort)));
     "if" >:: (fun _ -> assert_equal [
                            "true";
                            "false";
                            "if:(bool) true then false else true end";
                            "if:(nat) suc (3) then fun(x:(bot->bot)) -> 6 end else 3 end"]
                           (test_frontend_ounit (open_in parser_if)));
     "pairs" >:: (fun _ -> assert_equal [
                            "proj1(true)";
                            "proj2(3)";
                            "{5, 6}:(nat*nat)";
                            "proj1({fun(x:bot) -> 5 end, fun(y:bot) -> 1 end}:((bot->nat)*(bot->nat)))"]
                           (test_frontend_ounit (open_in parser_pairs)));
     "sums" >:: (fun _ -> assert_equal [
                            "inl:(nat+bot)(3)";
                            "inr:((nat->nat)+bool)(true)";
                            "case:(nat) inl:(nat+nat)(5) of inl x -> suc (x)|inr y -> y end";
                            "case:((bot->nat)) inr:(nat+(nat->nat))(fun(x:nat) -> suc (x) end) of inl x -> fun(y:bot) -> x end|inr y -> fun(z:bot) -> (y 1) end end"]
                           (test_frontend_ounit (open_in parser_sums)))
]


let _ = run_test_tt_main parser_tests
