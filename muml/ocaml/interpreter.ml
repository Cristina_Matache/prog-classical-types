module Stdlib_list = List
open Parsedast
open Parsedast_to_isast
open Reduction

exception TypeError of string

(* Print a list of expressions *)
 
let print_expr_list print_func ls file = 
     let rec assemble_string_list ls result =
          match ls with
               |[] -> "\n"::result; 
               |x::xs -> (print_func x)::"\n"::(assemble_string_list xs result);
     in Lexer.print_string_list (assemble_string_list ls []) file 


(* Define a multistep reduction function from the one step reduction in reduction.ml *)
let rec reduce t = 
     match MuML.sem_trm t with
          |Some s -> reduce s
          |None -> t


let empty_env = fun x -> raise (TypeError ("Variable " ^ (string_of_int (isNatToInt x 0)) 
     ^ "is not in the typing environment"))

let rec interpret terms = 
     let findtype e = 
          try Some (Types.type_term empty_env empty_env e)
          with 
           |_ -> None
     in match terms with
          |[] -> []
          |e::es ->
              match findtype e with
               |Some ty -> (Printf.printf "INTERPRET -- The term %s typechecked\n" (pp_dBT e));
                   (try let e1 = reduce e 
                       in (Printf.sprintf "%s : %s\n" (pp_dBT e1) (pp_typea ty [] (ref "p") (ref []) 0))::(interpret es)
                   with 
                    |Match_failure _ -> (Printf.eprintf "INTERPRET -- Reduction error: in term %s
                         \n" (pp_dBT e)); (interpret es)
                    |Failure msg -> (Printf.eprintf "INTERPRET -- Failure %s : in term %s\n" msg 
                         (pp_dBT e)); (interpret es))
               |None -> (Printf.eprintf "INTERPRET -- Type error in expression: %s\n" (pp_dBT e);
                   (interpret es))


(* The main loop of the interpreter *)

let process_ounit input = 
     let filebuf = Lexing.from_channel input in
          try interpret (Stdlib_list.map translate_expr 
               (Stdlib_list.map dbexpr (Parser.start (Lexer.token false) filebuf)))
          with
          |Lexer.SyntaxError msg -> Printf.eprintf "%s%!" msg; raise (Failure "Lexer error")
          |Parser.Error -> 
               Printf.eprintf "At offset %d: syntax error.\n%!" (Lexing.lexeme_start filebuf); 
               raise (Failure "Parser error")

let process input output_file = 
     let output = process_ounit input
     in Lexer.print_string_list output output_file
     
