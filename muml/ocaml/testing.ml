module Stdlib_list = List
open Parsedast
open Parsedast_to_isast
open Reduction
open Interpreter

let dbast_output = "tests/dbast_output.txt"
let isabelleast_output = "tests/isabelleast_output.txt"


(* Test the output of the lexer and the parser *)

let test_frontend_ounit input =
     let filebuf = Lexing.from_channel input in
     let parsedast = 
          try Parser.start (Lexer.token true) filebuf
          with
          |Lexer.SyntaxError msg -> Printf.eprintf "%s%!" msg; raise (Failure "Lexer error")
          |Parser.Error -> 
               Printf.eprintf "At offset %d: syntax error.\n%!" (Lexing.lexeme_start filebuf);
               raise (Failure "Parser error")
     in Stdlib_list.map print_expr parsedast
     

(* Test the output of the De Bruijn translation and translation to the Isabelle ast *) 


let test_translate_ounit input = print_dBT (translate_expr (dbexpr input))


let test_translation input =
     let filebuf = Lexing.from_channel input in
     let dbterms = 
          try Stdlib_list.map dbexpr (Parser.start (Lexer.token false) filebuf)
          with
          |Lexer.SyntaxError msg -> Printf.eprintf "%s%!" msg; raise (Failure "Lexer error")
          |Parser.Error -> 
               Printf.eprintf "At offset %d: syntax error.\n%!" (Lexing.lexeme_start filebuf);
               raise (Failure "Parser error")
     in let isast = Stdlib_list.map translate_expr dbterms in
     print_expr_list print_expr dbterms dbast_output;
     print_expr_list print_dBT isast isabelleast_output
     


(* Test the typing *)

let rec infertype es = match es with
     |[] -> []
     |x::xs -> try let t = (Types.type_term empty_env empty_env x) in t::(infertype xs)
               with 
                    |Match_failure f -> ((Printf.eprintf "The term %s is not well-typed.\n%!" (pp_dBT x));
                     (infertype xs))
(* Shouldn't catch all exceptions! *)

let test_typing_ounit input = 
     let filebuf = Lexing.from_channel input in
     let typings = 
          try infertype (Stdlib_list.map translate_expr 
               (Stdlib_list.map dbexpr (Parser.start (Lexer.token false) filebuf)))
          with
          |Lexer.SyntaxError msg -> Printf.eprintf "%s%!" msg; raise (Failure "Lexer error")
          |Parser.Error -> 
               Printf.eprintf "At offset %d: syntax error.\n%!" (Lexing.lexeme_start filebuf);
               raise (Failure "Parser error")
     in Stdlib_list.map print_typea typings
     

