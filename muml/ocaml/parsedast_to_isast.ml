(* Transform AST produced by parser to Isabelle AST *)

module Stdlib_string = String
open Parsedast
open Reduction
open Printf

exception Not_a_number
exception Var_not_in_list


(* Transform variables to De Bruijn notation *)

(* Keep a list of de Bruijn  indices for variables, both free and bound *) 

(* Function to retrieve the index of a variable. 
   Free variables are associated their ascii code. *)
let rec getindex indexlist count x = match indexlist with
     |[] -> count + (Char.code (Stdlib_string.get x 0)) 
     (* When the variable is free return its ascii code + the number of abstractions it's under *)
     |(y, n)::ls -> if y = x then n else getindex ls count x

(* Function to update the index of all variables when a new abstraction is encountered.
   If the variable x is a new bound variable, just reset its index to 0. *)
let rec updateindex indexlist x = 
     let newvar = ref true in
     match indexlist with
          |[] -> if !newvar then [(x,0)] else []
          |(y, n)::ls -> if y=x then (newvar := false; (y,0)::(updateindex ls x))
                         else (y, (n+1))::(updateindex ls x)

(* count = number of abstraction entered *)
let dbexpr expr =
     let rec convertlvars expr indexlist count = match expr with
          |Var x -> DbVar (x, (getindex indexlist count x))
          |Suc e -> Suc (convertlvars e indexlist count)
          |App (e1, e2) -> App (convertlvars e1 indexlist count, convertlvars e2 indexlist count)
          |Fun (x, t, e) -> Fun (x, t, (convertlvars e (updateindex indexlist x) (count+1)))
          |Nrec (t, e1, e2, e3) -> Nrec (t, convertlvars e1 indexlist count, 
                                            convertlvars e2 indexlist count, 
                                            convertlvars e3 indexlist count)
          |Mu (x, t, e) -> Mu (x, t, convertlvars e indexlist count)
          |Command (x, e) -> Command (x, convertlvars e indexlist count)
          |Abort e -> Abort (convertlvars e indexlist count)
     
          |If(t, e1, e2, e3) -> If(t, convertlvars e1 indexlist count, 
                                      convertlvars e2 indexlist count, 
                                      convertlvars e3 indexlist count)

          |Proj1 e -> Proj1 (convertlvars e indexlist count)
          |Proj2 e -> Proj2 (convertlvars e indexlist count)
          |Pair (e1, e2, t) -> Pair(convertlvars e1 indexlist count,
                                    convertlvars e2 indexlist count, t)

          |Inl (t, e) -> Inl (t, convertlvars e indexlist count)
          |Inr (t, e) -> Inr (t, convertlvars e indexlist count)
          |Case (t, e1, x, e2, y, e3) -> 
               Case (t, convertlvars e1 indexlist count,
                     x, convertlvars e2 (updateindex indexlist x) (count+1),
                     y, convertlvars e3 (updateindex indexlist y) (count+1))
 
          |TAbs (p, e) -> TAbs (p, convertlvars e indexlist count)
          |TApp (e, t) -> TApp (convertlvars e indexlist count, t)
   
          |e -> e
     in let rec convertmvars expr indexlist count = match expr with
          |Suc e -> Suc (convertmvars e indexlist count)
          |App (e1, e2) -> App (convertmvars e1 indexlist count, convertmvars e2 indexlist count)
          |Fun (x, t, e) -> Fun (x, t, convertmvars e indexlist count)
          |Nrec (t, e1, e2, e3) -> Nrec (t, convertmvars e1 indexlist count, 
                                            convertmvars e2 indexlist count, 
                                            convertmvars e3 indexlist count)
          |Mu (x, t, e) -> Mu (x, t, convertmvars e (updateindex indexlist x) (count+1))
          |Command (x, e) -> DbCommand(x, (getindex indexlist count x), 
                                       convertmvars e indexlist count)
          |Abort e -> Abort (convertmvars e indexlist count)

          |If(t, e1, e2, e3) -> If(t, convertmvars e1 indexlist count, 
                                      convertmvars e2 indexlist count, 
                                      convertmvars e3 indexlist count)     

          |Proj1 e -> Proj1 (convertmvars e indexlist count)
          |Proj2 e -> Proj2 (convertmvars e indexlist count)
          |Pair (e1, e2, t) -> Pair (convertmvars e1 indexlist count,
                                     convertmvars e2 indexlist count, t)

          |Inl (t, e) -> Inl (t, convertmvars e indexlist count)
          |Inr (t, e) -> Inr (t, convertmvars e indexlist count)
          |Case (t, e1, x, e2, y, e3) -> 
               Case (t, convertmvars e1 indexlist count,
                     x, convertmvars e2 indexlist count,
                     y, convertmvars e3 indexlist count)

          |TAbs (p, e) -> TAbs (p, convertmvars e indexlist count)
          |TApp (e, t) -> TApp (convertmvars e indexlist count, t)

          |e -> e
     in let rec converttypevars_type type_expr indexlist count = match type_expr with
          |TArrow (t1, t2) -> TArrow(converttypevars_type t1 indexlist count,
                                     converttypevars_type t2 indexlist count)
          |TProd (t1, t2) -> TProd(converttypevars_type t1 indexlist count,
                                   converttypevars_type t2 indexlist count)
          |TSum (t1, t2) -> TSum(converttypevars_type t1 indexlist count,
                                 converttypevars_type t2 indexlist count)
          |TVar x -> DbTVar (x, (getindex indexlist count x))
          |TForall (x, t) -> TForall(x,
                                     converttypevars_type t (updateindex indexlist x) (count+1))
          |t -> t 
     in let rec converttypevars_expr expr indexlist count = match expr with
          |Suc e -> Suc (converttypevars_expr e indexlist count)
          |App (e1, e2) -> App (converttypevars_expr e1 indexlist count, 
                                converttypevars_expr e2 indexlist count)
          |Fun (x, t, e) -> Fun (x, converttypevars_type t indexlist count,
                                 converttypevars_expr e indexlist count)
          |Nrec (t, e1, e2, e3) -> Nrec (converttypevars_type t indexlist count,
                                         converttypevars_expr e1 indexlist count,
                                         converttypevars_expr e2 indexlist count,
                                         converttypevars_expr e3 indexlist count)
          |Mu (x, t, e) -> Mu (x, converttypevars_type t indexlist count,
                               converttypevars_expr e indexlist count)
          |Command (v, e) -> Command (v, converttypevars_expr e indexlist count)
          |Abort e -> Abort (converttypevars_expr e indexlist count)
          |DbCommand (v, n, e) -> DbCommand (v, n, converttypevars_expr e indexlist count)

          |If(t, e1, e2, e3) -> If(converttypevars_type t indexlist count,
                                   converttypevars_expr e1 indexlist count,
                                   converttypevars_expr e2 indexlist count,
                                   converttypevars_expr e3 indexlist count)

          |Proj1 e -> Proj1 (converttypevars_expr e indexlist count)
          |Proj2 e -> Proj2 (converttypevars_expr e indexlist count)
          |Pair(e1, e2, t) -> Pair(converttypevars_expr e1 indexlist count,
                                   converttypevars_expr e2 indexlist count,
                                   converttypevars_type t indexlist count)

          |Inl(t, e) -> Inl (converttypevars_type t indexlist count,
                             converttypevars_expr e indexlist count)
          |Inr(t, e) -> Inr (converttypevars_type t indexlist count,
                             converttypevars_expr e indexlist count)
          |Case(t, e1, x, e2, y, e3) -> Case(converttypevars_type t indexlist count,
                                             converttypevars_expr e1 indexlist count, x,
                                             converttypevars_expr e2 indexlist count, y,
                                             converttypevars_expr e3 indexlist count)

          |TAbs (x, e) -> TAbs(x, converttypevars_expr e (updateindex indexlist x) (count+1))
          |TApp (e, t) -> TApp(converttypevars_expr e indexlist count,
                               converttypevars_type t indexlist count)   
          |e -> e

     in converttypevars_expr (convertmvars (convertlvars expr [] 0) [] 0) [] 0     


(* Transform to Isabelle AST *)

(* Transform the int representation of a natural number to the lambdamuT S representation *)
let rec toS n acc = match n with
     |0 -> acc
     |n -> if n>0 then toS (n-1) (Syntax.S acc)
           else raise (Invalid_argument "Cannot convert negative int to nat")

(* Convert an int de Bruijn index to the Isabelle representation of natural numbers *)
let rec toIsNat n acc = match n with
     |0 -> acc
     |n -> if n>0 then toIsNat (n-1) (Arith.Suc acc)
           else raise (Invalid_argument "Cannot convert negative int to nat")

(* Convert the types in the parsed ast to the types in the Isabelle AST*)
let rec translate_type t = match t with
     |TNat -> Syntax.Nat

     |TBot -> Syntax.Bottom
     |(TArrow (t1, t2)) -> (Syntax.Fun ((translate_type t1), (translate_type t2)))

     |TBool -> Syntax.Bool

     |TProd (t1, t2) -> Syntax.Prod (translate_type t1, translate_type t2)

     |TSum (t1, t2) -> Syntax.Sum (translate_type t1, translate_type t2)

     |DbTVar (x, n) -> Syntax.TVar (toIsNat n Arith.Zero_nat)
     |TForall (x, t) -> Syntax.TAll (translate_type t)
     |_ -> raise (MalformedAST "the parsed AST is malformed")

(* Convert parsed ast expressions to Isabelle expressions *)
let rec translate_expr e = match e with
     |DbVar (v, n) -> Syntax.LVar (toIsNat n Arith.Zero_nat)
     |Suc e -> Syntax.S (translate_expr e)
     |Fun (x, t, e) -> Syntax.Lbd (translate_type t, translate_expr e)
     |App (e1, e2) -> Syntax.App (translate_expr e1, translate_expr e2)
     |Mu (x, t, c) -> Syntax.Mu (translate_type t, translate_comm c)
     |Nrec (t, e1, e2, e3) -> 
          Syntax.Nrec (translate_type t, translate_expr e1, translate_expr e2, translate_expr e3)
     |Nat n -> toS n Syntax.Zero

     |True -> Syntax.TTrue
     |False -> Syntax.TFalse
     |If (t, e1, e2, e3) -> 
          Syntax.If (translate_type t, translate_expr e1, translate_expr e2, translate_expr e3)

     |Proj1 e -> Syntax.Proj1 (translate_expr e)
     |Proj2 e -> Syntax.Proj2 (translate_expr e)
     |Pair (e1, e2, t) -> Syntax.Pair (translate_expr e1, translate_expr e2, translate_type t)

     |Inl (t, e) -> Syntax.Inl (translate_type t, translate_expr e)
     |Inr (t, e) -> Syntax.Inr (translate_type t, translate_expr e)
     |Case (t, e1, x, e2, y, e3) -> 
          Syntax.Case (translate_type t, translate_expr e1, translate_expr e2, translate_expr e3)

     |TAbs(x, e) -> Syntax.TAbs(translate_expr e)
     |TApp(e, t) -> Syntax.TApp(translate_expr e, translate_type t)

     |_ -> raise (MalformedAST "the parsed AST is malformed")
and translate_comm c = match c with
     |DbCommand (v, n, e) -> Syntax.MVar (toIsNat n Arith.Zero_nat, translate_expr e)
     |Abort e -> Syntax.Top (translate_expr e)
     |_ -> raise (MalformedAST "the parsed AST is malformed!")


(* Print Isabelle AST as it is*)

(* Transform an Isabelle natural number to an int *)
let rec isNatToInt n acc = match n with
     |Arith.Zero_nat -> acc
     |Arith.Suc n1 -> isNatToInt n1 (acc+1)

(* Print types *)
let rec print_typea t = match t with
     |Syntax.Nat -> "Nat"

     |Syntax.Bottom -> "Bottom"
     |Syntax.Fun (t1, t2) -> sprintf "%s->%s" (print_typea t1) (print_typea t2)

     |Syntax.Bool -> "Bool"

     |Syntax.Prod (t1, t2) -> sprintf "%s*%s" (print_typea t1) (print_typea t2)

     |Syntax.Sum (t1, t2) -> sprintf "%s+%s" (print_typea t1) (print_typea t2)

     |Syntax.TVar n -> sprintf "(TVar %s)" (string_of_int (isNatToInt n 0))
     |Syntax.TAll t -> sprintf "(TAll %s)" (print_typea t)
(* Print expressions *)
let rec print_dBT e = match e with
     |Syntax.LVar n -> sprintf "(LVar %s)" (string_of_int (isNatToInt n 0))
     |Syntax.Lbd (t, e1) -> sprintf "(Lbd %s %s)" (print_typea t) (print_dBT e1)
     |Syntax.App (e1, e2) -> sprintf "(App %s %s)" (print_dBT e1) (print_dBT e2)
     |Syntax.Mu (t, c) -> sprintf "(Mu %s %s)" (print_typea t) (print_dBC c)
     |Syntax.Zero -> "Zero"
     |Syntax.S n -> sprintf "(S %s)" (print_dBT n)
     |Syntax.Nrec (t, e1, e2, e3) -> sprintf "(Nrec %s %s %s %s)" 
          (print_typea t) (print_dBT e1) (print_dBT e2) (print_dBT e3)

     |Syntax.TTrue -> "True"
     |Syntax.TFalse -> "False"
     |Syntax.If(t, e1, e2, e3) -> sprintf "(If %s %s %s %s)" 
          (print_typea t) (print_dBT e1) (print_dBT e2) (print_dBT e3)

     |Syntax.Proj1 e -> sprintf "(Proj1 %s)" (print_dBT e)
     |Syntax.Proj2 e -> sprintf "(Proj2 %s)" (print_dBT e)
     |Syntax.Pair (e1, e2, t) -> sprintf "(|%s, %s|):%s" 
          (print_dBT e1) (print_dBT e2) (print_typea t)

     |Syntax.Inl (t, e) -> sprintf "(Inl %s %s)" (print_typea t) (print_dBT e)
     |Syntax.Inr (t, e) -> sprintf "(Inr %s %s)" (print_typea t) (print_dBT e)
     |Syntax.Case (t, e1, e2, e3) -> sprintf "(Case %s %s %s %s)"
          (print_typea t) (print_dBT e1) (print_dBT e2) (print_dBT e3)

     |Syntax.TAbs e -> sprintf "(TAbs %s)" (print_dBT e)
     |Syntax.TApp (e, t) -> sprintf "(TAbs %s %s)" (print_dBT e) (print_typea t)
and print_dBC c = match c with
     |Syntax.MVar (n, e) -> sprintf "(MVar %s %s)" (string_of_int (isNatToInt n 0)) (print_dBT e)
     |Syntax.Top e -> sprintf "(Top %s)" (print_dBT e)


(* Pretty-print Isabelle AST (in the form that the interpreter receives)*)

(* Transform numbers from suc notation to ints *)
let rec sToInt e acc = match e with
     |Syntax.S e1 -> sToInt e1 (acc+1)
     |Syntax.Zero -> acc
     |_ -> raise Not_a_number

(* Update mapping between variables and de Bruijn indices every time an abstraction is entered *)
let rec update indexlist x = match indexlist with
     |[] -> [(x, 0)]
     |(y, n)::ls -> (y, n+1)::(update ls x)

let rec getname indexlist i = match indexlist with  (*Remove lastvar argument*)
     |[] -> raise Var_not_in_list
(*let varname = !lastvar in
            lastvar := !lastvar ^ "'";
            varname    *)      (*The variable searched is in fact free. Choose p because only type variables can be free in a typeable muML term.
TODO: change this!!!*)
     |(y,n)::ls -> if n=i then y else getname ls i

(* Print type *)
let rec pp_typea t typeindexlist lasttypevar freevarindexlist count = match t with
     |Syntax.Nat -> "nat"

     |Syntax.Bottom -> "bot"
     |Syntax.Fun (t1, t2) -> 
          (match t1 with
               |Syntax.Fun(_, _) -> sprintf "(%s)->%s" (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
               | _ -> sprintf "%s->%s" (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
          )

     |Syntax.Bool -> "bool"

     |Syntax.Prod (t1, t2) ->
          (match t2 with
               |Syntax.Prod(_, _) -> 
                    (match t1 with Syntax.Sum(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |Syntax.Fun(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s*(%s)" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))
               |Syntax.Sum(_, _) ->
                    (match t1 with Syntax.Sum(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |Syntax.Fun(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))
               |Syntax.Fun(_, _) ->
                    (match t1 with Syntax.Sum(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |Syntax.Fun(_, _) -> sprintf "(%s)*(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s*(%s)" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))     
               | _ -> (match t1 with Syntax.Sum(_, _) -> sprintf "(%s)*%s" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |Syntax.Fun(_, _) -> sprintf "(%s)*%s" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s*%s" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)) 
          )

     |Syntax.Sum (t1, t2) ->
          (match t2 with
               |Syntax.Sum(_, _) -> 
                    (match t1 with Syntax.Fun(_, _) -> sprintf "(%s)+(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s+(%s)" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))
               |Syntax.Fun(_, _) -> 
                    (match t1 with Syntax.Fun(_, _) -> sprintf "(%s)+(%s)" 
                                                       (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                                       (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s+(%s)" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))
               | _ -> (match t1 with Syntax.Fun(_, _) -> sprintf "(%s)+%s" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count)
                                  |_ -> sprintf "%s+%s" 
                                        (pp_typea t1 typeindexlist lasttypevar freevarindexlist count) 
                                        (pp_typea t2 typeindexlist lasttypevar freevarindexlist count))
          )

     |Syntax.TVar n -> (try getname typeindexlist (isNatToInt n 0)
                        with Var_not_in_list ->
                         (try getname !freevarindexlist ((isNatToInt n 0)-count)
                          with Var_not_in_list ->
                              (let varname = !lasttypevar in
                              lasttypevar := !lasttypevar ^ "'";
                              freevarindexlist := (varname, ((isNatToInt n 0)-count))
                                                       ::(!freevarindexlist);
                              varname
                              )
                         )
                       ) 
     |Syntax.TAll t ->
          let varname = !lasttypevar in
          lasttypevar := !lasttypevar ^ "'";
          sprintf "forall(%s)(%s)"
               varname
               (pp_typea t (update typeindexlist varname) lasttypevar freevarindexlist (count+1))

let rec pp_dBT e = 
     let lastlvar = ref "x" in
     let lastmvar = ref "a" in
     let lasttypevar = ref "p" in
     let rec pp_dBT_aux e lindexlist mindexlist typeindexlist  freevarindexlist count =
     match e with
          |Syntax.LVar n -> getname lindexlist (isNatToInt n 0)    
          |Syntax.Lbd (t, e1) ->
               let varname = (!lastlvar) in 
               lastlvar := !lastlvar ^ "'";
               sprintf "fun(%s:%s)->%s end" 
                    (varname) (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
                    (pp_dBT_aux e1 (update lindexlist varname) mindexlist typeindexlist freevarindexlist count)
          |Syntax.App (e1, e2) -> sprintf "(%s %s)" 
                    (pp_dBT_aux e1 lindexlist mindexlist typeindexlist freevarindexlist count) 
                    (pp_dBT_aux e2 lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Mu (t, c) -> 
               let varname = (!lastmvar) in
               lastmvar := !lastmvar ^ "'";
               sprintf "bind(%s:%s)->%s end" 
                    (varname) (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
                    (pp_dBC c lindexlist (update mindexlist varname) typeindexlist freevarindexlist count) 
          |Syntax.Nrec (t, e1, e2, e3) -> sprintf "nrec:(%s)->(%s, %s, %s) end" 
               (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
               (pp_dBT_aux e1 lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_dBT_aux e2 lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_dBT_aux e3 lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Zero -> "0"
          |Syntax.S e -> (try string_of_int ((sToInt e 0) + 1)
               with Not_a_number -> sprintf "suc (%s)" 
                    (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count))

          |Syntax.TTrue -> "true"
          |Syntax.TFalse -> "false"
          |Syntax.If (t, e1, e2, e3) -> sprintf "if:(%s) %s then %s else %s end" 
               (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
               (pp_dBT_aux e1 lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_dBT_aux e2 lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_dBT_aux e3 lindexlist mindexlist typeindexlist freevarindexlist count)

          |Syntax.Proj1 e -> sprintf "proj1(%s)" 
               (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Proj2 e -> sprintf "proj2(%s)" 
               (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Pair (e1, e2, t) -> sprintf "{%s, %s}:%s" 
               (pp_dBT_aux e1 lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_dBT_aux e2 lindexlist mindexlist typeindexlist freevarindexlist count)
               (pp_typea t typeindexlist lasttypevar freevarindexlist count)

          |Syntax.Inl (t, e) -> sprintf "inl:(%s)(%s)" 
               (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
               (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Inr (t, e) -> sprintf "inr:(%s)(%s)" 
               (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
               (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Case (t, e1, e2, e3) ->
               let varname1 = (!lastlvar) in
               let varname2 = (!lastlvar) ^ "'" in
               lastlvar := !lastlvar ^ "''";
               sprintf "case:(%s) %s of inl %s->%s|inr %s->%s end" 
                    (pp_typea t typeindexlist lasttypevar freevarindexlist count) 
                    (pp_dBT_aux e1 lindexlist mindexlist typeindexlist freevarindexlist count)
                    varname1
                    (pp_dBT_aux e2 (update lindexlist varname1) mindexlist typeindexlist freevarindexlist count)
                    varname2
                    (pp_dBT_aux e3 (update lindexlist varname2) mindexlist typeindexlist freevarindexlist count)

          |Syntax.TAbs e -> 
                let varname = !lasttypevar in
                lasttypevar := !lasttypevar ^ "'";
                sprintf "tabs(%s)-> %s end"
                    varname
                    (pp_dBT_aux e lindexlist mindexlist (update typeindexlist varname) freevarindexlist (count+1))
          |Syntax.TApp(e, t) -> sprintf "(%s %s)"
               (pp_dBT_aux e lindexlist mindexlist typeindexlist freevarindexlist count) 
               (pp_typea t typeindexlist lasttypevar freevarindexlist count)

     and pp_dBC c lindexlist mindexlist typeindexlist freevarindexlist count = match c with
          |Syntax.MVar (n, t) -> sprintf "[%s].(%s)" 
               (getname mindexlist (isNatToInt n 0)) 
               (pp_dBT_aux t lindexlist mindexlist typeindexlist freevarindexlist count)
          |Syntax.Top t -> sprintf "[abort].%s" (pp_dBT_aux t lindexlist mindexlist typeindexlist freevarindexlist count)
     in pp_dBT_aux e [] [] [] (ref []) 0
     
