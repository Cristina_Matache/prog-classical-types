%token <int> NAT
%token <string> VAR
%token <string> TVAR
%token FUN NREC BIND SUC LBRACK RBRACK ABORT TABS
%token LPARAN RPARAN COLON ARROW DOT EOF COMMA SEMICOLON END

%token TRUE FALSE IF THEN ELSE
%token LCBR RCBR PROJ1 PROJ2 TIMES
%token INL INR CASE OF BAR PLUS

%token NATTYPE BOTTYPE BOOLTYPE FORALL

/*%left APPLY*/
%right ARROW
%left PLUS     /*sum type is left associative and has higher priorirty than the func type */
%left TIMES    /*product type left associative and give it higher priority than the sum type */


%start <Parsedast.expr list> start

%%

start:
  |EOF    {[]}
  |e = stmt; s = start  {e::s}

stmt:
  |e = expr; SEMICOLON  {e}

base_expr:
  |n = NAT  {Parsedast.Nat n}
  |x = VAR  {Parsedast.Var x}
  |TRUE {Parsedast.True}
  |FALSE   {Parsedast.False}
  |LPARAN; e = expr; RPARAN  {e}

/*Make applicaton left associative*/
app_expr:
  |e = base_expr  {e}
  |a = app_expr; e = base_expr  {Parsedast.App (a, e)}  

  |e = app_expr; t = type_expr    {Parsedast.TApp(e, t)}
  
expr:
  |a = app_expr   {a}
/*  |e1 = expr; e2 = expr      {Parsedast.App (e1, e2)}   %prec APPLY*/
  |SUC; e = expr    {Parsedast.Suc e}    
  |FUN; LPARAN; x = VAR; COLON; t = type_expr; RPARAN; ARROW; e = expr; END
    {Parsedast.Fun (x, t, e) }
  |NREC; COLON; LPARAN; t = type_expr; RPARAN; ARROW; 
    LPARAN; e1 = expr; COMMA; e2 = expr; COMMA; e3 = expr; RPARAN; END
    {Parsedast.Nrec (t, e1, e2, e3)}
  |BIND; LPARAN; v = VAR; COLON; t = type_expr; RPARAN; ARROW; c = comm_expr; END
    {Parsedast.Mu (v, t, c)}

  |TABS; LPARAN; p = TVAR; RPARAN; ARROW; e = expr; END   {Parsedast.TAbs(p, e)}


  |IF; COLON; t = type_expr; e1 = expr; THEN; e2 = expr; ELSE e3 = expr; END {Parsedast.If(t, e1, e2, e3)}

  |PROJ1; LPARAN; e = expr; RPARAN      {Parsedast.Proj1 e}
  |PROJ2; LPARAN; e = expr; RPARAN      {Parsedast.Proj2 e}
  |LCBR; e1 = expr; COMMA; e2 = expr; RCBR; COLON; t = type_expr   {Parsedast.Pair (e1, e2, t)}

  |INL; COLON; t = type_expr; LPARAN; e = expr; RPARAN {Parsedast.Inl (t, e)}
  |INR; COLON; t = type_expr; LPARAN; e = expr; RPARAN {Parsedast.Inr (t, e)}
  |CASE; COLON; t = type_expr; e1 = expr; OF; INL; x = VAR; ARROW; e2 = expr; BAR; INR; y = VAR; ARROW; e3 = expr; END
     {Parsedast.Case(t, e1, x, e2, y, e3)}

comm_expr:
  |LBRACK; v = VAR; RBRACK; DOT; e = expr  {Parsedast.Command (v, e)}
  |LBRACK; ABORT; RBRACK; DOT; e = expr  {Parsedast.Abort e}

type_expr:
  |NATTYPE  {Parsedast.TNat}

  |BOTTYPE  {Parsedast.TBot}
  |t1 = type_expr; ARROW; t2 = type_expr  {Parsedast.TArrow(t1, t2)}

  |BOOLTYPE    {Parsedast.TBool}
   
  |t1 = type_expr; TIMES; t2 = type_expr     {Parsedast.TProd (t1, t2)}

  |t1 = type_expr; PLUS; t2 = type_expr     {Parsedast.TSum (t1, t2)}

  |p = TVAR    {Parsedast.TVar p}
  |FORALL; LPARAN; p = TVAR; RPARAN; LPARAN; t = type_expr; RPARAN     {Parsedast.TForall(p, t)}

  |LPARAN; t = type_expr; RPARAN  {t}

