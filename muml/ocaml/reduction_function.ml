module HOL : sig
  type 'a equal = {equal : 'a -> 'a -> bool}
  val equal : 'a equal -> 'a -> 'a -> bool
  val eq : 'a equal -> 'a -> 'a -> bool
end = struct

type 'a equal = {equal : 'a -> 'a -> bool};;
let equal _A = _A.equal;;

let rec eq _A a b = equal _A a b;;

end;; (*struct HOL*)

module List : sig
  val fold : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
  val filter : ('a -> bool) -> 'a list -> 'a list
  val member : 'a HOL.equal -> 'a list -> 'a -> bool
  val insert : 'a HOL.equal -> 'a -> 'a list -> 'a list
  val removeAll : 'a HOL.equal -> 'a -> 'a list -> 'a list
end = struct

let rec fold f x1 s = match f, x1, s with f, x :: xs, s -> fold f xs (f x s)
               | f, [], s -> s;;

let rec filter
  p x1 = match p, x1 with p, [] -> []
    | p, x :: xs -> (if p x then x :: filter p xs else filter p xs);;

let rec member _A x0 y = match x0, y with [], y -> false
                    | x :: xs, y -> HOL.eq _A x y || member _A xs y;;

let rec insert _A x xs = (if member _A xs x then xs else x :: xs);;

let rec removeAll _A
  x xa1 = match x, xa1 with x, [] -> []
    | x, y :: xs ->
        (if HOL.eq _A x y then removeAll _A x xs else y :: removeAll _A x xs);;

end;; (*struct List*)

module Set : sig
  type 'a set = Set of 'a list | Coset of 'a list
  val insert : 'a HOL.equal -> 'a -> 'a set -> 'a set
  val member : 'a HOL.equal -> 'a -> 'a set -> bool
  val bot_set : 'a set
  val sup_set : 'a HOL.equal -> 'a set -> 'a set -> 'a set
end = struct

type 'a set = Set of 'a list | Coset of 'a list;;

let rec insert _A
  x xa1 = match x, xa1 with x, Coset xs -> Coset (List.removeAll _A x xs)
    | x, Set xs -> Set (List.insert _A x xs);;

let rec member _A
  x xa1 = match x, xa1 with x, Coset xs -> not (List.member _A xs x)
    | x, Set xs -> List.member _A xs x;;

let bot_set : 'a set = Set [];;

let rec sup_set _A
  x0 a = match x0, a with
    Coset xs, a -> Coset (List.filter (fun x -> not (member _A x a)) xs)
    | Set xs, a -> List.fold (insert _A) xs a;;

end;; (*struct Set*)

module Arith : sig
  type nat = Zero_nat | Suc of nat
  val equal_nata : nat -> nat -> bool
  val equal_nat : nat HOL.equal
  type num = One | Bit0 of num | Bit1 of num
  val one_nat : nat
  val less_nat : nat -> nat -> bool
  val less_eq_nat : nat -> nat -> bool
  val plus_nat : nat -> nat -> nat
  val minus_nat : nat -> nat -> nat
end = struct

type nat = Zero_nat | Suc of nat;;

let rec equal_nata x0 x1 = match x0, x1 with Zero_nat, Suc x2 -> false
                     | Suc x2, Zero_nat -> false
                     | Suc x2, Suc y2 -> equal_nata x2 y2
                     | Zero_nat, Zero_nat -> true;;

let equal_nat = ({HOL.equal = equal_nata} : nat HOL.equal);;

type num = One | Bit0 of num | Bit1 of num;;

let one_nat : nat = Suc Zero_nat;;

let rec less_nat m x1 = match m, x1 with m, Suc n -> less_eq_nat m n
                   | n, Zero_nat -> false
and less_eq_nat x0 n = match x0, n with Suc m, n -> less_nat m n
                  | Zero_nat, n -> true;;

let rec plus_nat x0 n = match x0, n with Suc m, n -> plus_nat m (Suc n)
                   | Zero_nat, n -> n;;

let rec minus_nat m n = match m, n with Suc m, Suc n -> minus_nat m n
                    | Zero_nat, n -> Zero_nat
                    | m, Zero_nat -> m;;

end;; (*struct Arith*)

module Product_Type : sig
  val equal_unita : unit -> unit -> bool
  val equal_unit : unit HOL.equal
end = struct

let rec equal_unita u v = true;;

let equal_unit = ({HOL.equal = equal_unita} : unit HOL.equal);;

end;; (*struct Product_Type*)

module String : sig
  type char = Zero_char | Char of Arith.num
end = struct

type char = Zero_char | Char of Arith.num;;

end;; (*struct String*)

module Predicate : sig
  type 'a seq = Empty | Insert of 'a * 'a pred | Join of 'a pred * 'a seq
  and 'a pred = Seq of (unit -> 'a seq)
  val is_empty : 'a pred -> bool
  val null : 'a seq -> bool
  val singleton : 'a HOL.equal -> (unit -> 'a) -> 'a pred -> 'a
  val the_only : 'a HOL.equal -> (unit -> 'a) -> 'a seq -> 'a
  val the : 'a HOL.equal -> 'a pred -> 'a
  val bind : 'a pred -> ('a -> 'b pred) -> 'b pred
  val apply : ('a -> 'b pred) -> 'a seq -> 'b seq
  val eval : 'a HOL.equal -> 'a pred -> 'a -> bool
  val member : 'a HOL.equal -> 'a seq -> 'a -> bool
  val holds : unit pred -> bool
  val bot_pred : 'a pred
  val single : 'a -> 'a pred
  val adjunct : 'a pred -> 'a seq -> 'a seq
  val sup_pred : 'a pred -> 'a pred -> 'a pred
end = struct

type 'a seq = Empty | Insert of 'a * 'a pred | Join of 'a pred * 'a seq
and 'a pred = Seq of (unit -> 'a seq);;

let rec is_empty (Seq f) = null (f ())
and null = function Empty -> true
           | Insert (x, p) -> false
           | Join (p, xq) -> is_empty p && null xq;;

let rec singleton _A
  default (Seq f) =
    (match f () with Empty -> default ()
      | Insert (x, p) ->
        (if is_empty p then x
          else (let y = singleton _A default p in
                 (if HOL.eq _A x y then x else default ())))
      | Join (p, xq) ->
        (if is_empty p then the_only _A default xq
          else (if null xq then singleton _A default p
                 else (let x = singleton _A default p in
                       let y = the_only _A default xq in
                        (if HOL.eq _A x y then x else default ())))))
and the_only _A
  default x1 = match default, x1 with
    default, Insert (x, p) ->
      (if is_empty p then x
        else (let y = singleton _A default p in
               (if HOL.eq _A x y then x else default ())))
    | default, Join (p, xq) ->
        (if is_empty p then the_only _A default xq
          else (if null xq then singleton _A default p
                 else (let x = singleton _A default p in
                       let y = the_only _A default xq in
                        (if HOL.eq _A x y then x else default ()))));;

let rec the _A
  a = singleton _A (fun _ -> failwith "not_unique" (fun _ -> the _A a)) a;;

let rec bind (Seq g) f = Seq (fun _ -> apply f (g ()))
and apply f x1 = match f, x1 with f, Empty -> Empty
            | f, Insert (x, p) -> Join (f x, Join (bind p f, Empty))
            | f, Join (p, xq) -> Join (bind p f, apply f xq);;

let rec eval _A (Seq f) = member _A (f ())
and member _A xa0 x = match xa0, x with Empty, x -> false
                | Insert (y, p), x -> HOL.eq _A x y || eval _A p x
                | Join (p, xq), x -> eval _A p x || member _A xq x;;

let rec holds p = eval Product_Type.equal_unit p ();;

let bot_pred : 'a pred = Seq (fun _ -> Empty);;

let rec single x = Seq (fun _ -> Insert (x, bot_pred));;

let rec adjunct p x1 = match p, x1 with p, Empty -> Join (p, Empty)
                  | p, Insert (x, q) -> Insert (x, sup_pred q p)
                  | p, Join (q, xq) -> Join (q, adjunct p xq)
and sup_pred
  (Seq f) (Seq g) =
    Seq (fun _ ->
          (match f () with Empty -> g ()
            | Insert (x, p) -> Insert (x, sup_pred p (Seq g))
            | Join (p, xq) -> adjunct (Seq g) (Join (p, xq))));;

end;; (*struct Predicate*)

module Types : sig
  type typea = Nat | Command | Fun of typea * typea | Bottom | Bool |
    Prod of typea * typea | Sum of typea * typea
  val equal_typea : typea -> typea -> bool
  val equal_type : typea HOL.equal
  type dBC = MVar of Arith.nat * dBT | Top of dBT
  and dBT = LVar of Arith.nat | Lbd of typea * dBT | App of dBT * dBT |
    Mu of typea * dBC | Zero | S of dBT | Nrec of typea * dBT * dBT * dBT | True
    | False | If of typea * dBT * dBT * dBT | Pair of dBT * dBT * typea |
    Proj1 of dBT | Proj2 of dBT | Inl of typea * dBT | Inr of typea * dBT |
    Case of typea * dBT * dBT * dBT
  val shift : (Arith.nat -> 'a) -> Arith.nat -> 'a -> Arith.nat -> 'a
  val eq_i_i : 'a HOL.equal -> 'a -> 'a -> unit Predicate.pred
  val eq_i_o : 'a -> 'a Predicate.pred
  val typing_dBT_i_i_i_o :
    (Arith.nat -> typea) -> (Arith.nat -> typea) -> dBT -> typea Predicate.pred
  val typing_dBC_i_i_i_i :
    (Arith.nat -> typea) ->
      (Arith.nat -> typea) -> dBC -> typea -> unit Predicate.pred
  val type_term : (Arith.nat -> typea) -> (Arith.nat -> typea) -> dBT -> typea
  val typing_dBC_i_i_i_o :
    (Arith.nat -> typea) -> (Arith.nat -> typea) -> dBC -> typea Predicate.pred
  val type_command :
    (Arith.nat -> typea) -> (Arith.nat -> typea) -> dBC -> typea
end = struct

type typea = Nat | Command | Fun of typea * typea | Bottom | Bool |
  Prod of typea * typea | Sum of typea * typea;;

let rec equal_typea
  x0 x1 = match x0, x1 with Prod (x61, x62), Sum (x71, x72) -> false
    | Sum (x71, x72), Prod (x61, x62) -> false
    | Bool, Sum (x71, x72) -> false
    | Sum (x71, x72), Bool -> false
    | Bool, Prod (x61, x62) -> false
    | Prod (x61, x62), Bool -> false
    | Bottom, Sum (x71, x72) -> false
    | Sum (x71, x72), Bottom -> false
    | Bottom, Prod (x61, x62) -> false
    | Prod (x61, x62), Bottom -> false
    | Bottom, Bool -> false
    | Bool, Bottom -> false
    | Fun (x31, x32), Sum (x71, x72) -> false
    | Sum (x71, x72), Fun (x31, x32) -> false
    | Fun (x31, x32), Prod (x61, x62) -> false
    | Prod (x61, x62), Fun (x31, x32) -> false
    | Fun (x31, x32), Bool -> false
    | Bool, Fun (x31, x32) -> false
    | Fun (x31, x32), Bottom -> false
    | Bottom, Fun (x31, x32) -> false
    | Command, Sum (x71, x72) -> false
    | Sum (x71, x72), Command -> false
    | Command, Prod (x61, x62) -> false
    | Prod (x61, x62), Command -> false
    | Command, Bool -> false
    | Bool, Command -> false
    | Command, Bottom -> false
    | Bottom, Command -> false
    | Command, Fun (x31, x32) -> false
    | Fun (x31, x32), Command -> false
    | Nat, Sum (x71, x72) -> false
    | Sum (x71, x72), Nat -> false
    | Nat, Prod (x61, x62) -> false
    | Prod (x61, x62), Nat -> false
    | Nat, Bool -> false
    | Bool, Nat -> false
    | Nat, Bottom -> false
    | Bottom, Nat -> false
    | Nat, Fun (x31, x32) -> false
    | Fun (x31, x32), Nat -> false
    | Nat, Command -> false
    | Command, Nat -> false
    | Sum (x71, x72), Sum (y71, y72) ->
        equal_typea x71 y71 && equal_typea x72 y72
    | Prod (x61, x62), Prod (y61, y62) ->
        equal_typea x61 y61 && equal_typea x62 y62
    | Fun (x31, x32), Fun (y31, y32) ->
        equal_typea x31 y31 && equal_typea x32 y32
    | Bool, Bool -> true
    | Bottom, Bottom -> true
    | Command, Command -> true
    | Nat, Nat -> true;;

let equal_type = ({HOL.equal = equal_typea} : typea HOL.equal);;

type dBC = MVar of Arith.nat * dBT | Top of dBT
and dBT = LVar of Arith.nat | Lbd of typea * dBT | App of dBT * dBT |
  Mu of typea * dBC | Zero | S of dBT | Nrec of typea * dBT * dBT * dBT | True |
  False | If of typea * dBT * dBT * dBT | Pair of dBT * dBT * typea |
  Proj1 of dBT | Proj2 of dBT | Inl of typea * dBT | Inr of typea * dBT |
  Case of typea * dBT * dBT * dBT;;

let rec shift
  e i a =
    (fun j ->
      (if Arith.less_nat j i then e j
        else (if Arith.equal_nata j i then a
               else e (Arith.minus_nat j Arith.one_nat))));;

let rec eq_i_i _A
  xa xb =
    Predicate.bind (Predicate.single (xa, xb))
      (fun (x, xaa) ->
        (if HOL.eq _A x xaa then Predicate.single () else Predicate.bot_pred));;

let rec eq_i_o xa = Predicate.bind (Predicate.single xa) Predicate.single;;

let rec typing_dBT_i_i_i_o
  xa xb xc =
    Predicate.sup_pred
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a
            with (gamma, (_, LVar x)) ->
              Predicate.bind (eq_i_o (gamma x)) Predicate.single
            | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
            | (_, (_, App (_, _))) -> Predicate.bot_pred
            | (_, (_, Mu (_, _))) -> Predicate.bot_pred
            | (_, (_, Zero)) -> Predicate.bot_pred
            | (_, (_, S _)) -> Predicate.bot_pred
            | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
            | (_, (_, True)) -> Predicate.bot_pred
            | (_, (_, False)) -> Predicate.bot_pred
            | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
            | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
            | (_, (_, Proj1 _)) -> Predicate.bot_pred
            | (_, (_, Proj2 _)) -> Predicate.bot_pred
            | (_, (_, Inl (_, _))) -> Predicate.bot_pred
            | (_, (_, Inr (_, _))) -> Predicate.bot_pred
            | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
      (Predicate.sup_pred
        (Predicate.bind (Predicate.single (xa, (xb, xc)))
          (fun a ->
            (match a with (_, (_, LVar _)) -> Predicate.bot_pred
              | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
              | (_, (_, App (_, _))) -> Predicate.bot_pred
              | (_, (_, Mu (_, _))) -> Predicate.bot_pred
              | (_, (_, Zero)) -> Predicate.single Nat
              | (_, (_, S _)) -> Predicate.bot_pred
              | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
              | (_, (_, True)) -> Predicate.bot_pred
              | (_, (_, False)) -> Predicate.bot_pred
              | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
              | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
              | (_, (_, Proj1 _)) -> Predicate.bot_pred
              | (_, (_, Proj2 _)) -> Predicate.bot_pred
              | (_, (_, Inl (_, _))) -> Predicate.bot_pred
              | (_, (_, Inr (_, _))) -> Predicate.bot_pred
              | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
        (Predicate.sup_pred
          (Predicate.bind (Predicate.single (xa, (xb, xc)))
            (fun a ->
              (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                | (_, (_, App (_, _))) -> Predicate.bot_pred
                | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                | (_, (_, Zero)) -> Predicate.bot_pred
                | (gamma, (delta, S t)) ->
                  Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                    (fun aa ->
                      (match aa with Nat -> Predicate.single Nat
                        | Command -> Predicate.bot_pred
                        | Fun (_, _) -> Predicate.bot_pred
                        | Bottom -> Predicate.bot_pred
                        | Bool -> Predicate.bot_pred
                        | Prod (_, _) -> Predicate.bot_pred
                        | Sum (_, _) -> Predicate.bot_pred))
                | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                | (_, (_, True)) -> Predicate.bot_pred
                | (_, (_, False)) -> Predicate.bot_pred
                | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                | (_, (_, Proj1 _)) -> Predicate.bot_pred
                | (_, (_, Proj2 _)) -> Predicate.bot_pred
                | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
          (Predicate.sup_pred
            (Predicate.bind (Predicate.single (xa, (xb, xc)))
              (fun a ->
                (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                  | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                  | (gamma, (delta, App (t, s))) ->
                    Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                      (fun aa ->
                        (match aa with Nat -> Predicate.bot_pred
                          | Command -> Predicate.bot_pred
                          | Fun (t1, t2) ->
                            Predicate.bind (typing_dBT_i_i_i_o gamma delta s)
                              (fun x ->
                                (if equal_typea t1 x then Predicate.single t2
                                  else Predicate.bot_pred))
                          | Bottom -> Predicate.bot_pred
                          | Bool -> Predicate.bot_pred
                          | Prod (_, _) -> Predicate.bot_pred
                          | Sum (_, _) -> Predicate.bot_pred))
                  | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                  | (_, (_, Zero)) -> Predicate.bot_pred
                  | (_, (_, S _)) -> Predicate.bot_pred
                  | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                  | (_, (_, True)) -> Predicate.bot_pred
                  | (_, (_, False)) -> Predicate.bot_pred
                  | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                  | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                  | (_, (_, Proj1 _)) -> Predicate.bot_pred
                  | (_, (_, Proj2 _)) -> Predicate.bot_pred
                  | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                  | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                  | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
            (Predicate.sup_pred
              (Predicate.bind (Predicate.single (xa, (xb, xc)))
                (fun a ->
                  (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                    | (gamma, (delta, Lbd (t1, t))) ->
                      Predicate.bind
                        (typing_dBT_i_i_i_o (shift gamma Arith.Zero_nat t1)
                          delta t)
                        (fun x -> Predicate.single (Fun (t1, x)))
                    | (_, (_, App (_, _))) -> Predicate.bot_pred
                    | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                    | (_, (_, Zero)) -> Predicate.bot_pred
                    | (_, (_, S _)) -> Predicate.bot_pred
                    | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                    | (_, (_, True)) -> Predicate.bot_pred
                    | (_, (_, False)) -> Predicate.bot_pred
                    | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                    | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                    | (_, (_, Proj1 _)) -> Predicate.bot_pred
                    | (_, (_, Proj2 _)) -> Predicate.bot_pred
                    | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                    | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                    | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
              (Predicate.sup_pred
                (Predicate.bind (Predicate.single (xa, (xb, xc)))
                  (fun a ->
                    (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                      | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                      | (_, (_, App (_, _))) -> Predicate.bot_pred
                      | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                      | (_, (_, Zero)) -> Predicate.bot_pred
                      | (_, (_, S _)) -> Predicate.bot_pred
                      | (gamma, (delta, Nrec (t, r, s, ta))) ->
                        Predicate.bind (typing_dBT_i_i_i_o gamma delta r)
                          (fun x ->
                            (if equal_typea t x
                              then Predicate.bind
                                     (typing_dBT_i_i_i_o gamma delta s)
                                     (fun aa ->
                                       (match aa with Nat -> Predicate.bot_pred
 | Command -> Predicate.bot_pred | Fun (Nat, Nat) -> Predicate.bot_pred
 | Fun (Nat, Command) -> Predicate.bot_pred
 | Fun (Nat, Fun (tb, tc)) ->
   (if equal_typea t tc && equal_typea t tb
     then Predicate.bind (typing_dBT_i_i_i_o gamma delta ta)
            (fun ab ->
              (match ab with Nat -> Predicate.single t
                | Command -> Predicate.bot_pred
                | Fun (_, _) -> Predicate.bot_pred
                | Bottom -> Predicate.bot_pred | Bool -> Predicate.bot_pred
                | Prod (_, _) -> Predicate.bot_pred
                | Sum (_, _) -> Predicate.bot_pred))
     else Predicate.bot_pred)
 | Fun (Nat, Bottom) -> Predicate.bot_pred
 | Fun (Nat, Bool) -> Predicate.bot_pred
 | Fun (Nat, Prod (_, _)) -> Predicate.bot_pred
 | Fun (Nat, Sum (_, _)) -> Predicate.bot_pred
 | Fun (Command, _) -> Predicate.bot_pred
 | Fun (Fun (_, _), _) -> Predicate.bot_pred
 | Fun (Bottom, _) -> Predicate.bot_pred | Fun (Bool, _) -> Predicate.bot_pred
 | Fun (Prod (_, _), _) -> Predicate.bot_pred
 | Fun (Sum (_, _), _) -> Predicate.bot_pred | Bottom -> Predicate.bot_pred
 | Bool -> Predicate.bot_pred | Prod (_, _) -> Predicate.bot_pred
 | Sum (_, _) -> Predicate.bot_pred))
                              else Predicate.bot_pred))
                      | (_, (_, True)) -> Predicate.bot_pred
                      | (_, (_, False)) -> Predicate.bot_pred
                      | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                      | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                      | (_, (_, Proj1 _)) -> Predicate.bot_pred
                      | (_, (_, Proj2 _)) -> Predicate.bot_pred
                      | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                      | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                      | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
                (Predicate.sup_pred
                  (Predicate.bind (Predicate.single (xa, (xb, xc)))
                    (fun a ->
                      (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                        | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                        | (_, (_, App (_, _))) -> Predicate.bot_pred
                        | (gamma, (delta, Mu (t, c))) ->
                          Predicate.bind
                            (typing_dBC_i_i_i_i gamma
                              (shift delta Arith.Zero_nat t) c Command)
                            (fun () -> Predicate.single t)
                        | (_, (_, Zero)) -> Predicate.bot_pred
                        | (_, (_, S _)) -> Predicate.bot_pred
                        | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                        | (_, (_, True)) -> Predicate.bot_pred
                        | (_, (_, False)) -> Predicate.bot_pred
                        | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                        | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                        | (_, (_, Proj1 _)) -> Predicate.bot_pred
                        | (_, (_, Proj2 _)) -> Predicate.bot_pred
                        | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                        | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                        | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
                  (Predicate.sup_pred
                    (Predicate.bind (Predicate.single (xa, (xb, xc)))
                      (fun a ->
                        (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                          | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                          | (_, (_, App (_, _))) -> Predicate.bot_pred
                          | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                          | (_, (_, Zero)) -> Predicate.bot_pred
                          | (_, (_, S _)) -> Predicate.bot_pred
                          | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                          | (_, (_, True)) -> Predicate.single Bool
                          | (_, (_, False)) -> Predicate.bot_pred
                          | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                          | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                          | (_, (_, Proj1 _)) -> Predicate.bot_pred
                          | (_, (_, Proj2 _)) -> Predicate.bot_pred
                          | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                          | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                          | (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
                    (Predicate.sup_pred
                      (Predicate.bind (Predicate.single (xa, (xb, xc)))
                        (fun a ->
                          (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                            | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                            | (_, (_, App (_, _))) -> Predicate.bot_pred
                            | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                            | (_, (_, Zero)) -> Predicate.bot_pred
                            | (_, (_, S _)) -> Predicate.bot_pred
                            | (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
                            | (_, (_, True)) -> Predicate.bot_pred
                            | (_, (_, False)) -> Predicate.single Bool
                            | (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
                            | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                            | (_, (_, Proj1 _)) -> Predicate.bot_pred
                            | (_, (_, Proj2 _)) -> Predicate.bot_pred
                            | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                            | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                            | (_, (_, Case (_, _, _, _))) ->
                              Predicate.bot_pred)))
                      (Predicate.sup_pred
                        (Predicate.bind (Predicate.single (xa, (xb, xc)))
                          (fun a ->
                            (match a with (_, (_, LVar _)) -> Predicate.bot_pred
                              | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                              | (_, (_, App (_, _))) -> Predicate.bot_pred
                              | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                              | (_, (_, Zero)) -> Predicate.bot_pred
                              | (_, (_, S _)) -> Predicate.bot_pred
                              | (_, (_, Nrec (_, _, _, _))) ->
                                Predicate.bot_pred
                              | (_, (_, True)) -> Predicate.bot_pred
                              | (_, (_, False)) -> Predicate.bot_pred
                              | (gamma, (delta, If (t, t1, t2, t3))) ->
                                Predicate.bind
                                  (typing_dBT_i_i_i_o gamma delta t1)
                                  (fun aa ->
                                    (match aa with Nat -> Predicate.bot_pred
                                      | Command -> Predicate.bot_pred
                                      | Fun (_, _) -> Predicate.bot_pred
                                      | Bottom -> Predicate.bot_pred
                                      | Bool ->
Predicate.bind (typing_dBT_i_i_i_o gamma delta t2)
  (fun x ->
    (if equal_typea t x
      then Predicate.bind (typing_dBT_i_i_i_o gamma delta t3)
             (fun xd ->
               (if equal_typea t xd then Predicate.single t
                 else Predicate.bot_pred))
      else Predicate.bot_pred))
                                      | Prod (_, _) -> Predicate.bot_pred
                                      | Sum (_, _) -> Predicate.bot_pred))
                              | (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
                              | (_, (_, Proj1 _)) -> Predicate.bot_pred
                              | (_, (_, Proj2 _)) -> Predicate.bot_pred
                              | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                              | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                              | (_, (_, Case (_, _, _, _))) ->
                                Predicate.bot_pred)))
                        (Predicate.sup_pred
                          (Predicate.bind (Predicate.single (xa, (xb, xc)))
                            (fun a ->
                              (match a
                                with (_, (_, LVar _)) -> Predicate.bot_pred
                                | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                                | (_, (_, App (_, _))) -> Predicate.bot_pred
                                | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                                | (_, (_, Zero)) -> Predicate.bot_pred
                                | (_, (_, S _)) -> Predicate.bot_pred
                                | (_, (_, Nrec (_, _, _, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, True)) -> Predicate.bot_pred
                                | (_, (_, False)) -> Predicate.bot_pred
                                | (_, (_, If (_, _, _, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Pair (_, _, Nat))) ->
                                  Predicate.bot_pred
                                | (_, (_, Pair (_, _, Command))) ->
                                  Predicate.bot_pred
                                | (_, (_, Pair (_, _, Fun (_, _)))) ->
                                  Predicate.bot_pred
                                | (_, (_, Pair (_, _, Bottom))) ->
                                  Predicate.bot_pred
                                | (_, (_, Pair (_, _, Bool))) ->
                                  Predicate.bot_pred
                                | (gamma,
                                    (delta, Pair (t1, t2, Prod (t1a, t2a))))
                                  -> Predicate.bind
                                       (typing_dBT_i_i_i_o gamma delta t1)
                                       (fun x ->
 (if equal_typea t1a x
   then Predicate.bind (typing_dBT_i_i_i_o gamma delta t2)
          (fun xd ->
            (if equal_typea t2a xd then Predicate.single (Prod (t1a, t2a))
              else Predicate.bot_pred))
   else Predicate.bot_pred))
                                | (_, (_, Pair (_, _, Sum (_, _)))) ->
                                  Predicate.bot_pred
                                | (_, (_, Proj1 _)) -> Predicate.bot_pred
                                | (_, (_, Proj2 _)) -> Predicate.bot_pred
                                | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                                | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                                | (_, (_, Case (_, _, _, _))) ->
                                  Predicate.bot_pred)))
                          (Predicate.sup_pred
                            (Predicate.bind (Predicate.single (xa, (xb, xc)))
                              (fun a ->
                                (match a
                                  with (_, (_, LVar _)) -> Predicate.bot_pred
                                  | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                                  | (_, (_, App (_, _))) -> Predicate.bot_pred
                                  | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                                  | (_, (_, Zero)) -> Predicate.bot_pred
                                  | (_, (_, S _)) -> Predicate.bot_pred
                                  | (_, (_, Nrec (_, _, _, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, True)) -> Predicate.bot_pred
                                  | (_, (_, False)) -> Predicate.bot_pred
                                  | (_, (_, If (_, _, _, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Pair (_, _, _))) ->
                                    Predicate.bot_pred
                                  | (gamma, (delta, Proj1 t)) ->
                                    Predicate.bind
                                      (typing_dBT_i_i_i_o gamma delta t)
                                      (fun aa ->
(match aa with Nat -> Predicate.bot_pred | Command -> Predicate.bot_pred
  | Fun (_, _) -> Predicate.bot_pred | Bottom -> Predicate.bot_pred
  | Bool -> Predicate.bot_pred | Prod (t1, _) -> Predicate.single t1
  | Sum (_, _) -> Predicate.bot_pred))
                                  | (_, (_, Proj2 _)) -> Predicate.bot_pred
                                  | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                                  | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                                  | (_, (_, Case (_, _, _, _))) ->
                                    Predicate.bot_pred)))
                            (Predicate.sup_pred
                              (Predicate.bind (Predicate.single (xa, (xb, xc)))
                                (fun a ->
                                  (match a
                                    with (_, (_, LVar _)) -> Predicate.bot_pred
                                    | (_, (_, Lbd (_, _))) -> Predicate.bot_pred
                                    | (_, (_, App (_, _))) -> Predicate.bot_pred
                                    | (_, (_, Mu (_, _))) -> Predicate.bot_pred
                                    | (_, (_, Zero)) -> Predicate.bot_pred
                                    | (_, (_, S _)) -> Predicate.bot_pred
                                    | (_, (_, Nrec (_, _, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, True)) -> Predicate.bot_pred
                                    | (_, (_, False)) -> Predicate.bot_pred
                                    | (_, (_, If (_, _, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Pair (_, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Proj1 _)) -> Predicate.bot_pred
                                    | (gamma, (delta, Proj2 t)) ->
                                      Predicate.bind
(typing_dBT_i_i_i_o gamma delta t)
(fun aa ->
  (match aa with Nat -> Predicate.bot_pred | Command -> Predicate.bot_pred
    | Fun (_, _) -> Predicate.bot_pred | Bottom -> Predicate.bot_pred
    | Bool -> Predicate.bot_pred | Prod (_, ab) -> Predicate.single ab
    | Sum (_, _) -> Predicate.bot_pred))
                                    | (_, (_, Inl (_, _))) -> Predicate.bot_pred
                                    | (_, (_, Inr (_, _))) -> Predicate.bot_pred
                                    | (_, (_, Case (_, _, _, _))) ->
                                      Predicate.bot_pred)))
                              (Predicate.sup_pred
                                (Predicate.bind
                                  (Predicate.single (xa, (xb, xc)))
                                  (fun a ->
                                    (match a
                                      with (_, (_, LVar _)) ->
Predicate.bot_pred
                                      | (_, (_, Lbd (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, App (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Mu (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Zero)) -> Predicate.bot_pred
                                      | (_, (_, S _)) -> Predicate.bot_pred
                                      | (_, (_, Nrec (_, _, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, True)) -> Predicate.bot_pred
                                      | (_, (_, False)) -> Predicate.bot_pred
                                      | (_, (_, If (_, _, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, Pair (_, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, Proj1 _)) -> Predicate.bot_pred
                                      | (_, (_, Proj2 _)) -> Predicate.bot_pred
                                      | (_, (_, Inl (Nat, _))) ->
Predicate.bot_pred
                                      | (_, (_, Inl (Command, _))) ->
Predicate.bot_pred
                                      | (_, (_, Inl (Fun (_, _), _))) ->
Predicate.bot_pred
                                      | (_, (_, Inl (Bottom, _))) ->
Predicate.bot_pred
                                      | (_, (_, Inl (Bool, _))) ->
Predicate.bot_pred
                                      | (_, (_, Inl (Prod (_, _), _))) ->
Predicate.bot_pred
                                      | (gamma, (delta, Inl (Sum (t1, t2), t)))
-> Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
     (fun x ->
       (if equal_typea t1 x then Predicate.single (Sum (t1, t2))
         else Predicate.bot_pred))
                                      | (_, (_, Inr (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Case (_, _, _, _))) ->
Predicate.bot_pred)))
                                (Predicate.sup_pred
                                  (Predicate.bind
                                    (Predicate.single (xa, (xb, xc)))
                                    (fun a ->
                                      (match a
with (_, (_, LVar _)) -> Predicate.bot_pred
| (_, (_, Lbd (_, _))) -> Predicate.bot_pred
| (_, (_, App (_, _))) -> Predicate.bot_pred
| (_, (_, Mu (_, _))) -> Predicate.bot_pred
| (_, (_, Zero)) -> Predicate.bot_pred | (_, (_, S _)) -> Predicate.bot_pred
| (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, True)) -> Predicate.bot_pred | (_, (_, False)) -> Predicate.bot_pred
| (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
| (_, (_, Proj1 _)) -> Predicate.bot_pred
| (_, (_, Proj2 _)) -> Predicate.bot_pred
| (_, (_, Inl (_, _))) -> Predicate.bot_pred
| (_, (_, Inr (Nat, _))) -> Predicate.bot_pred
| (_, (_, Inr (Command, _))) -> Predicate.bot_pred
| (_, (_, Inr (Fun (_, _), _))) -> Predicate.bot_pred
| (_, (_, Inr (Bottom, _))) -> Predicate.bot_pred
| (_, (_, Inr (Bool, _))) -> Predicate.bot_pred
| (_, (_, Inr (Prod (_, _), _))) -> Predicate.bot_pred
| (gamma, (delta, Inr (Sum (t1, t2), t))) ->
  Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
    (fun x ->
      (if equal_typea t2 x then Predicate.single (Sum (t1, t2))
        else Predicate.bot_pred))
| (_, (_, Case (_, _, _, _))) -> Predicate.bot_pred)))
                                  (Predicate.bind
                                    (Predicate.single (xa, (xb, xc)))
                                    (fun a ->
                                      (match a
with (_, (_, LVar _)) -> Predicate.bot_pred
| (_, (_, Lbd (_, _))) -> Predicate.bot_pred
| (_, (_, App (_, _))) -> Predicate.bot_pred
| (_, (_, Mu (_, _))) -> Predicate.bot_pred
| (_, (_, Zero)) -> Predicate.bot_pred | (_, (_, S _)) -> Predicate.bot_pred
| (_, (_, Nrec (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, True)) -> Predicate.bot_pred | (_, (_, False)) -> Predicate.bot_pred
| (_, (_, If (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, Pair (_, _, _))) -> Predicate.bot_pred
| (_, (_, Proj1 _)) -> Predicate.bot_pred
| (_, (_, Proj2 _)) -> Predicate.bot_pred
| (_, (_, Inl (_, _))) -> Predicate.bot_pred
| (_, (_, Inr (_, _))) -> Predicate.bot_pred
| (gamma, (delta, Case (t, t0, t1, t2))) ->
  Predicate.bind (typing_dBT_i_i_i_o gamma delta t0)
    (fun aa ->
      (match aa with Nat -> Predicate.bot_pred | Command -> Predicate.bot_pred
        | Fun (_, _) -> Predicate.bot_pred | Bottom -> Predicate.bot_pred
        | Bool -> Predicate.bot_pred | Prod (_, _) -> Predicate.bot_pred
        | Sum (t1a, t2a) ->
          Predicate.bind
            (typing_dBT_i_i_i_o (shift gamma Arith.Zero_nat t1a) delta t1)
            (fun x ->
              (if equal_typea t x
                then Predicate.bind
                       (typing_dBT_i_i_i_o (shift gamma Arith.Zero_nat t2a)
                         delta t2)
                       (fun xd ->
                         (if equal_typea t xd then Predicate.single t
                           else Predicate.bot_pred))
                else Predicate.bot_pred)))))))))))))))))))))
and typing_dBC_i_i_i_i
  xa xb xc xd =
    Predicate.sup_pred
      (Predicate.bind (Predicate.single (xa, (xb, (xc, xd))))
        (fun a ->
          (match a with (_, (_, (MVar (_, _), Nat))) -> Predicate.bot_pred
            | (gamma, (delta, (MVar (x, t), Command))) ->
              Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                (fun xaa ->
                  Predicate.bind (eq_i_i equal_type (delta x) xaa)
                    (fun () -> Predicate.single ()))
            | (_, (_, (MVar (_, _), Fun (_, _)))) -> Predicate.bot_pred
            | (_, (_, (MVar (_, _), Bottom))) -> Predicate.bot_pred
            | (_, (_, (MVar (_, _), Bool))) -> Predicate.bot_pred
            | (_, (_, (MVar (_, _), Prod (_, _)))) -> Predicate.bot_pred
            | (_, (_, (MVar (_, _), Sum (_, _)))) -> Predicate.bot_pred
            | (_, (_, (Top _, _))) -> Predicate.bot_pred)))
      (Predicate.bind (Predicate.single (xa, (xb, (xc, xd))))
        (fun a ->
          (match a with (_, (_, (MVar (_, _), _))) -> Predicate.bot_pred
            | (_, (_, (Top _, Nat))) -> Predicate.bot_pred
            | (gamma, (delta, (Top t, Command))) ->
              Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                (fun aa ->
                  (match aa with Nat -> Predicate.bot_pred
                    | Command -> Predicate.bot_pred
                    | Fun (_, _) -> Predicate.bot_pred
                    | Bottom -> Predicate.single () | Bool -> Predicate.bot_pred
                    | Prod (_, _) -> Predicate.bot_pred
                    | Sum (_, _) -> Predicate.bot_pred))
            | (_, (_, (Top _, Fun (_, _)))) -> Predicate.bot_pred
            | (_, (_, (Top _, Bottom))) -> Predicate.bot_pred
            | (_, (_, (Top _, Bool))) -> Predicate.bot_pred
            | (_, (_, (Top _, Prod (_, _)))) -> Predicate.bot_pred
            | (_, (_, (Top _, Sum (_, _)))) -> Predicate.bot_pred)));;

let rec type_term
  lenv menv t = Predicate.the equal_type (typing_dBT_i_i_i_o lenv menv t);;

let rec typing_dBC_i_i_i_o
  xa xb xc =
    Predicate.sup_pred
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a
            with (gamma, (delta, MVar (x, t))) ->
              Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                (fun xaa ->
                  Predicate.bind (eq_i_i equal_type (delta x) xaa)
                    (fun () -> Predicate.single Command))
            | (_, (_, Top _)) -> Predicate.bot_pred)))
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a with (_, (_, MVar (_, _))) -> Predicate.bot_pred
            | (gamma, (delta, Top t)) ->
              Predicate.bind (typing_dBT_i_i_i_o gamma delta t)
                (fun aa ->
                  (match aa with Nat -> Predicate.bot_pred
                    | Command -> Predicate.bot_pred
                    | Fun (_, _) -> Predicate.bot_pred
                    | Bottom -> Predicate.single Command
                    | Bool -> Predicate.bot_pred
                    | Prod (_, _) -> Predicate.bot_pred
                    | Sum (_, _) -> Predicate.bot_pred)))));;

let rec type_command
  lenv menv t = Predicate.the equal_type (typing_dBC_i_i_i_o lenv menv t);;

end;; (*struct Types*)

module Reduction : sig
  val is_natval_i : Types.dBT -> unit Predicate.pred
  val is_natval : Types.dBT -> bool
end = struct

let rec is_natval_i
  x = Predicate.sup_pred
        (Predicate.bind (Predicate.single x)
          (fun a ->
            (match a with Types.LVar _ -> Predicate.bot_pred
              | Types.Lbd (_, _) -> Predicate.bot_pred
              | Types.App (_, _) -> Predicate.bot_pred
              | Types.Mu (_, _) -> Predicate.bot_pred
              | Types.Zero -> Predicate.single ()
              | Types.S _ -> Predicate.bot_pred
              | Types.Nrec (_, _, _, _) -> Predicate.bot_pred
              | Types.True -> Predicate.bot_pred
              | Types.False -> Predicate.bot_pred
              | Types.If (_, _, _, _) -> Predicate.bot_pred
              | Types.Pair (_, _, _) -> Predicate.bot_pred
              | Types.Proj1 _ -> Predicate.bot_pred
              | Types.Proj2 _ -> Predicate.bot_pred
              | Types.Inl (_, _) -> Predicate.bot_pred
              | Types.Inr (_, _) -> Predicate.bot_pred
              | Types.Case (_, _, _, _) -> Predicate.bot_pred)))
        (Predicate.bind (Predicate.single x)
          (fun a ->
            (match a with Types.LVar _ -> Predicate.bot_pred
              | Types.Lbd (_, _) -> Predicate.bot_pred
              | Types.App (_, _) -> Predicate.bot_pred
              | Types.Mu (_, _) -> Predicate.bot_pred
              | Types.Zero -> Predicate.bot_pred
              | Types.S n ->
                Predicate.bind (is_natval_i n) (fun () -> Predicate.single ())
              | Types.Nrec (_, _, _, _) -> Predicate.bot_pred
              | Types.True -> Predicate.bot_pred
              | Types.False -> Predicate.bot_pred
              | Types.If (_, _, _, _) -> Predicate.bot_pred
              | Types.Pair (_, _, _) -> Predicate.bot_pred
              | Types.Proj1 _ -> Predicate.bot_pred
              | Types.Proj2 _ -> Predicate.bot_pred
              | Types.Inl (_, _) -> Predicate.bot_pred
              | Types.Inr (_, _) -> Predicate.bot_pred
              | Types.Case (_, _, _, _) -> Predicate.bot_pred)));;

let rec is_natval x1 = Predicate.holds (is_natval_i x1);;

end;; (*struct Reduction*)

module Substitution : sig
  type ctxt = ContextEmpty | ContextSuc of ctxt | ContextApp of ctxt * Types.dBT
    | ContextNrec of Types.typea * Types.dBT * Types.dBT * ctxt |
    ContextIf of Types.typea * ctxt * Types.dBT * Types.dBT |
    ContextPairL of ctxt * Types.dBT * Types.typea |
    ContextPairR of Types.dBT * ctxt * Types.typea | ContextProj1 of ctxt |
    ContextProj2 of ctxt | ContextInl of Types.typea * ctxt |
    ContextInr of Types.typea * ctxt |
    ContextCase of Types.typea * ctxt * Types.dBT * Types.dBT
  val liftL_dBC : Types.dBC -> Arith.nat -> Types.dBC
  val liftL_dBT : Types.dBT -> Arith.nat -> Types.dBT
  val liftM_dBC : Types.dBC -> Arith.nat -> Types.dBC
  val liftM_dBT : Types.dBT -> Arith.nat -> Types.dBT
  val subst_dBC : Types.dBC -> Types.dBT -> Arith.nat -> Types.dBC
  val subst_dBT : Types.dBT -> Types.dBT -> Arith.nat -> Types.dBT
  val ctxt_subst : ctxt -> Types.dBT -> Types.dBT
  val liftL_ctxt : ctxt -> Arith.nat -> ctxt
  val liftM_ctxt : ctxt -> Arith.nat -> ctxt
  val struct_subst_dBC :
    Types.dBC -> Arith.nat option -> Arith.nat option -> ctxt -> Types.dBC
  val struct_subst_dBT :
    Types.dBT -> Arith.nat option -> Arith.nat option -> ctxt -> Types.dBT
end = struct

type ctxt = ContextEmpty | ContextSuc of ctxt | ContextApp of ctxt * Types.dBT |
  ContextNrec of Types.typea * Types.dBT * Types.dBT * ctxt |
  ContextIf of Types.typea * ctxt * Types.dBT * Types.dBT |
  ContextPairL of ctxt * Types.dBT * Types.typea |
  ContextPairR of Types.dBT * ctxt * Types.typea | ContextProj1 of ctxt |
  ContextProj2 of ctxt | ContextInl of Types.typea * ctxt |
  ContextInr of Types.typea * ctxt |
  ContextCase of Types.typea * ctxt * Types.dBT * Types.dBT;;

let rec liftL_dBC
  x0 k = match x0, k with Types.MVar (i, t), k -> Types.MVar (i, liftL_dBT t k)
    | Types.Top t, k -> Types.Top (liftL_dBT t k)
and liftL_dBT
  x0 k = match x0, k with
    Types.LVar i, k ->
      (if Arith.less_nat i k then Types.LVar i
        else Types.LVar (Arith.plus_nat i Arith.one_nat))
    | Types.Lbd (ta, t), k ->
        Types.Lbd (ta, liftL_dBT t (Arith.plus_nat k Arith.one_nat))
    | Types.App (s, t), k -> Types.App (liftL_dBT s k, liftL_dBT t k)
    | Types.Mu (t, c), k -> Types.Mu (t, liftL_dBC c k)
    | Types.Zero, k -> Types.Zero
    | Types.S t, k -> Types.S (liftL_dBT t k)
    | Types.Nrec (ta, t, u, v), k ->
        Types.Nrec (ta, liftL_dBT t k, liftL_dBT u k, liftL_dBT v k)
    | Types.True, k -> Types.True
    | Types.False, k -> Types.False
    | Types.If (t, t1, t2, t3), k ->
        Types.If (t, liftL_dBT t1 k, liftL_dBT t2 k, liftL_dBT t3 k)
    | Types.Pair (t1, t2, t), k ->
        Types.Pair (liftL_dBT t1 k, liftL_dBT t2 k, t)
    | Types.Proj1 t, k -> Types.Proj1 (liftL_dBT t k)
    | Types.Proj2 t, k -> Types.Proj2 (liftL_dBT t k)
    | Types.Inl (ta, t), k -> Types.Inl (ta, liftL_dBT t k)
    | Types.Inr (ta, t), k -> Types.Inr (ta, liftL_dBT t k)
    | Types.Case (t, t0, t1, t2), k ->
        Types.Case
          (t, liftL_dBT t0 k, liftL_dBT t1 (Arith.plus_nat k Arith.one_nat),
            liftL_dBT t2 (Arith.plus_nat k Arith.one_nat));;

let rec liftM_dBC
  x0 k = match x0, k with
    Types.MVar (i, t), k ->
      (if Arith.less_nat i k then Types.MVar (i, liftM_dBT t k)
        else Types.MVar (Arith.plus_nat i Arith.one_nat, liftM_dBT t k))
    | Types.Top t, k -> Types.Top (liftM_dBT t k)
and liftM_dBT
  x0 k = match x0, k with Types.LVar i, k -> Types.LVar i
    | Types.Lbd (ta, t), k -> Types.Lbd (ta, liftM_dBT t k)
    | Types.App (s, t), k -> Types.App (liftM_dBT s k, liftM_dBT t k)
    | Types.Mu (t, c), k ->
        Types.Mu (t, liftM_dBC c (Arith.plus_nat k Arith.one_nat))
    | Types.Zero, k -> Types.Zero
    | Types.S t, k -> Types.S (liftM_dBT t k)
    | Types.Nrec (ta, t, u, v), k ->
        Types.Nrec (ta, liftM_dBT t k, liftM_dBT u k, liftM_dBT v k)
    | Types.True, k -> Types.True
    | Types.False, k -> Types.False
    | Types.If (t, t1, t2, t3), k ->
        Types.If (t, liftM_dBT t1 k, liftM_dBT t2 k, liftM_dBT t3 k)
    | Types.Pair (t1, t2, t), k ->
        Types.Pair (liftM_dBT t1 k, liftM_dBT t2 k, t)
    | Types.Proj1 t, k -> Types.Proj1 (liftM_dBT t k)
    | Types.Proj2 t, k -> Types.Proj2 (liftM_dBT t k)
    | Types.Inl (ta, t), k -> Types.Inl (ta, liftM_dBT t k)
    | Types.Inr (ta, t), k -> Types.Inr (ta, liftM_dBT t k)
    | Types.Case (t, t0, t1, t2), k ->
        Types.Case (t, liftM_dBT t0 k, liftM_dBT t1 k, liftM_dBT t2 k);;

let rec subst_dBC
  x0 s k = match x0, s, k with
    Types.MVar (i, t), s, k -> Types.MVar (i, subst_dBT t s k)
    | Types.Top t, s, k -> Types.Top (subst_dBT t s k)
and subst_dBT
  x0 s k = match x0, s, k with
    Types.LVar i, s, k ->
      (if Arith.less_nat k i then Types.LVar (Arith.minus_nat i Arith.one_nat)
        else (if Arith.equal_nata k i then s else Types.LVar i))
    | Types.Lbd (ta, t), s, k ->
        Types.Lbd
          (ta, subst_dBT t (liftL_dBT s Arith.Zero_nat)
                 (Arith.plus_nat k Arith.one_nat))
    | Types.App (t, u), s, k -> Types.App (subst_dBT t s k, subst_dBT u s k)
    | Types.Mu (t, c), s, k ->
        Types.Mu (t, subst_dBC c (liftM_dBT s Arith.Zero_nat) k)
    | Types.Zero, s, k -> Types.Zero
    | Types.S t, s, k -> Types.S (subst_dBT t s k)
    | Types.Nrec (ta, t, u, v), s, k ->
        Types.Nrec (ta, subst_dBT t s k, subst_dBT u s k, subst_dBT v s k)
    | Types.True, s, k -> Types.True
    | Types.False, s, k -> Types.False
    | Types.If (t, t1, t2, t3), s, k ->
        Types.If (t, subst_dBT t1 s k, subst_dBT t2 s k, subst_dBT t3 s k)
    | Types.Pair (t1, t2, t), s, k ->
        Types.Pair (subst_dBT t1 s k, subst_dBT t2 s k, t)
    | Types.Proj1 t, s, k -> Types.Proj1 (subst_dBT t s k)
    | Types.Proj2 t, s, k -> Types.Proj2 (subst_dBT t s k)
    | Types.Inl (ta, t), s, k -> Types.Inl (ta, subst_dBT t s k)
    | Types.Inr (ta, t), s, k -> Types.Inr (ta, subst_dBT t s k)
    | Types.Case (t, t0, t1, t2), s, k ->
        Types.Case
          (t, subst_dBT t0 s k,
            subst_dBT t1 (liftL_dBT s Arith.Zero_nat)
              (Arith.plus_nat k Arith.one_nat),
            subst_dBT t2 (liftL_dBT s Arith.Zero_nat)
              (Arith.plus_nat k Arith.one_nat));;

let rec ctxt_subst
  x0 s = match x0, s with ContextEmpty, s -> s
    | ContextSuc e, s -> Types.S (ctxt_subst e s)
    | ContextApp (e, t), s -> Types.App (ctxt_subst e s, t)
    | ContextNrec (ta, r, t, e), s -> Types.Nrec (ta, r, t, ctxt_subst e s)
    | ContextIf (t, e, t2, t3), s -> Types.If (t, ctxt_subst e s, t2, t3)
    | ContextPairL (e, ta, t), s -> Types.Pair (ctxt_subst e s, ta, t)
    | ContextPairR (ta, e, t), s -> Types.Pair (ta, ctxt_subst e s, t)
    | ContextProj1 e, s -> Types.Proj1 (ctxt_subst e s)
    | ContextProj2 e, s -> Types.Proj2 (ctxt_subst e s)
    | ContextInl (t, e), s -> Types.Inl (t, ctxt_subst e s)
    | ContextInr (t, e), s -> Types.Inr (t, ctxt_subst e s)
    | ContextCase (t, e, t2, t3), s -> Types.Case (t, ctxt_subst e s, t2, t3);;

let rec liftL_ctxt
  x0 n = match x0, n with ContextEmpty, n -> ContextEmpty
    | ContextSuc e, n -> ContextSuc (liftL_ctxt e n)
    | ContextApp (e, t), n -> ContextApp (liftL_ctxt e n, liftL_dBT t n)
    | ContextNrec (t, r, s, e), n ->
        ContextNrec (t, liftL_dBT r n, liftL_dBT s n, liftL_ctxt e n)
    | ContextIf (t, e, t2, t3), n ->
        ContextIf (t, liftL_ctxt e n, liftL_dBT t2 n, liftL_dBT t3 n)
    | ContextPairL (e, ta, t), n ->
        ContextPairL (liftL_ctxt e n, liftL_dBT ta n, t)
    | ContextPairR (ta, e, t), n ->
        ContextPairR (liftL_dBT ta n, liftL_ctxt e n, t)
    | ContextProj1 e, n -> ContextProj1 (liftL_ctxt e n)
    | ContextProj2 e, n -> ContextProj2 (liftL_ctxt e n)
    | ContextInl (t, e), k -> ContextInl (t, liftL_ctxt e k)
    | ContextInr (t, e), k -> ContextInr (t, liftL_ctxt e k)
    | ContextCase (t, e, t1, t2), k ->
        ContextCase
          (t, liftL_ctxt e k, liftL_dBT t1 (Arith.plus_nat k Arith.one_nat),
            liftL_dBT t2 (Arith.plus_nat k Arith.one_nat));;

let rec liftM_ctxt
  x0 n = match x0, n with ContextEmpty, n -> ContextEmpty
    | ContextSuc e, n -> ContextSuc (liftM_ctxt e n)
    | ContextApp (e, t), n -> ContextApp (liftM_ctxt e n, liftM_dBT t n)
    | ContextNrec (t, r, s, e), n ->
        ContextNrec (t, liftM_dBT r n, liftM_dBT s n, liftM_ctxt e n)
    | ContextIf (t, e, t2, t3), n ->
        ContextIf (t, liftM_ctxt e n, liftM_dBT t2 n, liftM_dBT t3 n)
    | ContextPairL (e, ta, t), n ->
        ContextPairL (liftM_ctxt e n, liftM_dBT ta n, t)
    | ContextPairR (ta, e, t), n ->
        ContextPairR (liftM_dBT ta n, liftM_ctxt e n, t)
    | ContextProj1 e, n -> ContextProj1 (liftM_ctxt e n)
    | ContextProj2 e, n -> ContextProj2 (liftM_ctxt e n)
    | ContextInl (t, e), k -> ContextInl (t, liftM_ctxt e k)
    | ContextInr (t, e), k -> ContextInr (t, liftM_ctxt e k)
    | ContextCase (t, e, t1, t2), k ->
        ContextCase (t, liftM_ctxt e k, liftM_dBT t1 k, liftM_dBT t2 k);;

let rec struct_subst_dBC
  x0 x1 k e = match x0, x1, k, e with
    Types.MVar (i, t), Some j, Some k, e ->
      (if Arith.equal_nata i j
        then Types.MVar
               (k, ctxt_subst e (struct_subst_dBT t (Some j) (Some k) e))
        else (if Arith.less_nat j i && Arith.less_eq_nat i k
               then Types.MVar
                      (Arith.minus_nat i Arith.one_nat,
                        struct_subst_dBT t (Some j) (Some k) e)
               else (if Arith.less_eq_nat k i && Arith.less_nat i j
                      then Types.MVar
                             (Arith.plus_nat i Arith.one_nat,
                               struct_subst_dBT t (Some j) (Some k) e)
                      else Types.MVar
                             (i, struct_subst_dBT t (Some j) (Some k) e))))
    | Types.MVar (i, t), None, k, e ->
        Types.MVar (i, struct_subst_dBT t None k e)
    | Types.MVar (i, t), Some j, None, e ->
        (if Arith.equal_nata i j
          then Types.Top (ctxt_subst e (struct_subst_dBT t (Some j) None e))
          else Types.MVar (i, struct_subst_dBT t (Some j) None e))
    | Types.Top t, Some j, k, e -> Types.Top (struct_subst_dBT t (Some j) k e)
    | Types.Top t, None, Some k, e ->
        Types.MVar (k, ctxt_subst e (struct_subst_dBT t None (Some k) e))
    | Types.Top t, None, None, e ->
        Types.Top (ctxt_subst e (struct_subst_dBT t None None e))
and struct_subst_dBT
  x0 j k e = match x0, j, k, e with Types.LVar i, j, k, e -> Types.LVar i
    | Types.Lbd (ta, t), j, k, e ->
        Types.Lbd (ta, struct_subst_dBT t j k (liftL_ctxt e Arith.Zero_nat))
    | Types.App (t, s), j, k, e ->
        Types.App (struct_subst_dBT t j k e, struct_subst_dBT s j k e)
    | Types.Zero, j, k, e -> Types.Zero
    | Types.S t, j, k, e -> Types.S (struct_subst_dBT t j k e)
    | Types.Nrec (ta, r, s, t), j, k, e ->
        Types.Nrec
          (ta, struct_subst_dBT r j k e, struct_subst_dBT s j k e,
            struct_subst_dBT t j k e)
    | Types.Mu (t, c), Some j, Some k, e ->
        Types.Mu
          (t, struct_subst_dBC c (Some (Arith.plus_nat j Arith.one_nat))
                (Some (Arith.plus_nat k Arith.one_nat))
                (liftM_ctxt e Arith.Zero_nat))
    | Types.Mu (t, c), None, Some k, e ->
        Types.Mu
          (t, struct_subst_dBC c None (Some (Arith.plus_nat k Arith.one_nat))
                (liftM_ctxt e Arith.Zero_nat))
    | Types.Mu (t, c), Some j, None, e ->
        Types.Mu
          (t, struct_subst_dBC c (Some (Arith.plus_nat j Arith.one_nat)) None
                (liftM_ctxt e Arith.Zero_nat))
    | Types.Mu (t, c), None, None, e ->
        Types.Mu (t, struct_subst_dBC c None None (liftM_ctxt e Arith.Zero_nat))
    | Types.True, j, k, e -> Types.True
    | Types.False, j, k, e -> Types.False
    | Types.If (t, t1, t2, t3), j, k, e ->
        Types.If
          (t, struct_subst_dBT t1 j k e, struct_subst_dBT t2 j k e,
            struct_subst_dBT t3 j k e)
    | Types.Pair (t1, t2, t), j, k, e ->
        Types.Pair (struct_subst_dBT t1 j k e, struct_subst_dBT t2 j k e, t)
    | Types.Proj1 t, j, k, e -> Types.Proj1 (struct_subst_dBT t j k e)
    | Types.Proj2 t, j, k, e -> Types.Proj2 (struct_subst_dBT t j k e)
    | Types.Inl (ta, t), j, k, e -> Types.Inl (ta, struct_subst_dBT t j k e)
    | Types.Inr (ta, t), j, k, e -> Types.Inr (ta, struct_subst_dBT t j k e)
    | Types.Case (t, t1, t2, t3), j, k, e ->
        Types.Case
          (t, struct_subst_dBT t1 j k e,
            struct_subst_dBT t2 j k (liftL_ctxt e Arith.Zero_nat),
            struct_subst_dBT t3 j k (liftL_ctxt e Arith.Zero_nat));;

end;; (*struct Substitution*)

module FreeVariables : sig
  val fmv_dBC : Types.dBC -> Arith.nat -> Arith.nat Set.set
  val fmv_dBT : Types.dBT -> Arith.nat -> Arith.nat Set.set
  val dropM_dBC : Types.dBC -> Arith.nat -> Types.dBC
  val dropM_dBT : Types.dBT -> Arith.nat -> Types.dBT
end = struct

let rec fmv_dBC
  x0 k = match x0, k with
    Types.MVar (i, t), k ->
      (if Arith.less_eq_nat k i
        then Set.sup_set Arith.equal_nat
               (Set.insert Arith.equal_nat (Arith.minus_nat i k) Set.bot_set)
               (fmv_dBT t k)
        else fmv_dBT t k)
    | Types.Top t, k -> fmv_dBT t k
and fmv_dBT
  x0 k = match x0, k with Types.LVar i, k -> Set.bot_set
    | Types.Lbd (ta, t), k -> fmv_dBT t k
    | Types.App (s, t), k ->
        Set.sup_set Arith.equal_nat (fmv_dBT s k) (fmv_dBT t k)
    | Types.Mu (t, c), k -> fmv_dBC c (Arith.plus_nat k Arith.one_nat)
    | Types.Zero, k -> Set.bot_set
    | Types.S t, k -> fmv_dBT t k
    | Types.Nrec (ta, r, s, t), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_dBT r k) (fmv_dBT s k))
          (fmv_dBT t k)
    | Types.True, k -> Set.bot_set
    | Types.False, k -> Set.bot_set
    | Types.If (t, t1, t2, t3), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_dBT t1 k) (fmv_dBT t2 k))
          (fmv_dBT t3 k)
    | Types.Pair (t1, t2, t), k ->
        Set.sup_set Arith.equal_nat (fmv_dBT t1 k) (fmv_dBT t2 k)
    | Types.Proj1 t, k -> fmv_dBT t k
    | Types.Proj2 t, k -> fmv_dBT t k
    | Types.Inl (ta, t), k -> fmv_dBT t k
    | Types.Inr (ta, t), k -> fmv_dBT t k
    | Types.Case (t, t1, t2, t3), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_dBT t1 k) (fmv_dBT t2 k))
          (fmv_dBT t3 k);;

let rec dropM_dBC
  x0 k = match x0, k with
    Types.MVar (i, t), k ->
      (if Arith.less_nat k i
        then Types.MVar (Arith.minus_nat i Arith.one_nat, dropM_dBT t k)
        else Types.MVar (i, dropM_dBT t k))
    | Types.Top t, k -> Types.Top (dropM_dBT t k)
and dropM_dBT
  x0 k = match x0, k with Types.LVar i, k -> Types.LVar i
    | Types.Lbd (ta, t), k -> Types.Lbd (ta, dropM_dBT t k)
    | Types.App (s, t), k -> Types.App (dropM_dBT s k, dropM_dBT t k)
    | Types.Mu (t, c), k ->
        Types.Mu (t, dropM_dBC c (Arith.plus_nat k Arith.one_nat))
    | Types.Zero, k -> Types.Zero
    | Types.S t, k -> Types.S (dropM_dBT t k)
    | Types.Nrec (ta, r, s, t), k ->
        Types.Nrec (ta, dropM_dBT r k, dropM_dBT s k, dropM_dBT t k)
    | Types.True, k -> Types.True
    | Types.False, k -> Types.False
    | Types.If (t, t1, t2, t3), k ->
        Types.If (t, dropM_dBT t1 k, dropM_dBT t2 k, dropM_dBT t3 k)
    | Types.Pair (t1, t2, t), k ->
        Types.Pair (dropM_dBT t1 k, dropM_dBT t2 k, t)
    | Types.Proj1 t, k -> Types.Proj1 (dropM_dBT t k)
    | Types.Proj2 t, k -> Types.Proj2 (dropM_dBT t k)
    | Types.Inl (ta, t), k -> Types.Inl (ta, dropM_dBT t k)
    | Types.Inr (ta, t), k -> Types.Inr (ta, dropM_dBT t k)
    | Types.Case (t, t1, t2, t3), k ->
        Types.Case (t, dropM_dBT t1 k, dropM_dBT t2 k, dropM_dBT t3 k);;

end;; (*struct FreeVariables*)

module ReductionFunction : sig
  val red_term : Types.dBT -> Types.dBT option
  val red_command : Types.dBC -> Types.dBC option
end = struct

let rec red_term
  = function
    Types.App (s, t) ->
      (match s
        with Types.LVar _ ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Lbd (_, r) -> Some (Substitution.subst_dBT r t Arith.Zero_nat)
        | Types.App (_, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Nat, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Command, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Fun (_, t2), c) ->
          Some (Types.Mu
                 (t2, Substitution.struct_subst_dBC c (Some Arith.Zero_nat)
                        (Some Arith.Zero_nat)
                        (Substitution.ContextApp
                          (Substitution.ContextEmpty,
                            Substitution.liftM_dBT t Arith.Zero_nat))))
        | Types.Mu (Types.Bottom, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Bool, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Prod (_, _), _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Mu (Types.Sum (_, _), _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Zero ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.S _ ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Nrec (_, _, _, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.True ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.False ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.If (_, _, _, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Pair (_, _, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Proj1 _ ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Proj2 _ ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Inl (_, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Inr (_, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t)))
        | Types.Case (_, _, _, _) ->
          (match red_term s
            with None ->
              (match red_term t with None -> None
                | Some u -> Some (Types.App (s, u)))
            | Some u -> Some (Types.App (u, t))))
    | Types.Lbd (t, s) ->
        (match red_term s with None -> None | Some u -> Some (Types.Lbd (t, u)))
    | Types.S t ->
        (match t
          with Types.LVar _ ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Lbd (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.App (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Mu (ta, c) ->
            Some (Types.Mu
                   (ta, Substitution.struct_subst_dBC c (Some Arith.Zero_nat)
                          (Some Arith.Zero_nat)
                          (Substitution.ContextSuc Substitution.ContextEmpty)))
          | Types.Zero ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.S _ ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Nrec (_, _, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.True ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.False ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.If (_, _, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Pair (_, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Proj1 _ ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Proj2 _ ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Inl (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Inr (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u))
          | Types.Case (_, _, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.S u)))
    | Types.Mu (t, c) ->
        (match c
          with Types.MVar (Arith.Zero_nat, ta) ->
            (if not (Set.member Arith.equal_nat Arith.Zero_nat
                      (FreeVariables.fmv_dBT ta Arith.Zero_nat))
              then Some (FreeVariables.dropM_dBT ta Arith.Zero_nat)
              else (match red_command (Types.MVar (Arith.Zero_nat, ta))
                     with None -> None | Some d -> Some (Types.Mu (t, d))))
          | Types.MVar (Arith.Suc _, _) ->
            (match red_command c with None -> None
              | Some d -> Some (Types.Mu (t, d)))
          | Types.Top _ ->
            (match red_command c with None -> None
              | Some d -> Some (Types.Mu (t, d))))
    | Types.Nrec (ta, r, s, t) ->
        (match t
          with Types.LVar _ ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Lbd (_, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.App (_, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Mu (_, c) ->
            Some (Types.Mu
                   (ta, Substitution.struct_subst_dBC c (Some Arith.Zero_nat)
                          (Some Arith.Zero_nat)
                          (Substitution.ContextNrec
                            (ta, Substitution.liftM_dBT r Arith.Zero_nat,
                              Substitution.liftM_dBT s Arith.Zero_nat,
                              Substitution.ContextEmpty))))
          | Types.Zero -> Some r
          | Types.S n ->
            (if Reduction.is_natval n
              then Some (Types.App (Types.App (s, n), Types.Nrec (ta, r, s, n)))
              else (match red_term (Types.S n)
                     with None ->
                       (match red_term r
                         with None ->
                           (match red_term s with None -> None
                             | Some u ->
                               Some (Types.Nrec (ta, r, u, Types.S n)))
                         | Some u -> Some (Types.Nrec (ta, u, s, Types.S n)))
                     | Some u -> Some (Types.Nrec (ta, r, s, u))))
          | Types.Nrec (_, _, _, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.True ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.False ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.If (_, _, _, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Pair (_, _, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Proj1 _ ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Proj2 _ ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Inl (_, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Inr (_, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t)))
          | Types.Case (_, _, _, _) ->
            (match red_term r
              with None ->
                (match red_term s
                  with None ->
                    (match red_term t with None -> None
                      | Some u -> Some (Types.Nrec (ta, r, s, u)))
                  | Some u -> Some (Types.Nrec (ta, r, u, t)))
              | Some u -> Some (Types.Nrec (ta, u, s, t))))
    | Types.If (t, t1, t2, t3) ->
        (match t1
          with Types.LVar _ ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Lbd (_, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.App (_, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Mu (_, c) ->
            Some (Types.Mu
                   (t, Substitution.struct_subst_dBC c (Some Arith.Zero_nat)
                         (Some Arith.Zero_nat)
                         (Substitution.ContextIf
                           (t, Substitution.ContextEmpty,
                             Substitution.liftM_dBT t2 Arith.Zero_nat,
                             Substitution.liftM_dBT t3 Arith.Zero_nat))))
          | Types.Zero ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.S _ ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Nrec (_, _, _, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.True -> Some t2 | Types.False -> Some t3
          | Types.If (_, _, _, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Pair (_, _, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Proj1 _ ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Proj2 _ ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Inl (_, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Inr (_, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3)))
          | Types.Case (_, _, _, _) ->
            (match red_term t1 with None -> None
              | Some s1 -> Some (Types.If (t, s1, t2, t3))))
    | Types.LVar x -> None
    | Types.Zero -> None
    | Types.True -> None
    | Types.False -> None
and red_command
  = function
    Types.MVar (i, t) ->
      (match t
        with Types.LVar _ ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Lbd (_, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.App (_, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Mu (_, c) ->
          Some (FreeVariables.dropM_dBC
                 (Substitution.struct_subst_dBC c (Some Arith.Zero_nat) (Some i)
                   Substitution.ContextEmpty)
                 i)
        | Types.Zero ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.S _ ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Nrec (_, _, _, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.True ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.False ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.If (_, _, _, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Pair (_, _, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Proj1 _ ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Proj2 _ ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Inl (_, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Inr (_, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u)))
        | Types.Case (_, _, _, _) ->
          (match red_term t with None -> None
            | Some u -> Some (Types.MVar (i, u))))
    | Types.Top t ->
        (match t
          with Types.LVar _ ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Lbd (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.App (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Mu (_, c) ->
            Some (FreeVariables.dropM_dBC
                   (Substitution.struct_subst_dBC c (Some Arith.Zero_nat) None
                     Substitution.ContextEmpty)
                   Arith.Zero_nat)
          | Types.Zero ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.S _ ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Nrec (_, _, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.True ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.False ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.If (_, _, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Pair (_, _, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Proj1 _ ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Proj2 _ ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Inl (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Inr (_, _) ->
            (match red_term t with None -> None | Some u -> Some (Types.Top u))
          | Types.Case (_, _, _, _) ->
            (match red_term t with None -> None
              | Some u -> Some (Types.Top u)));;

end;; (*struct ReductionFunction*)
