open OUnit2
open Testing
open Interpreter

let typing_simple = "tests/ounit/typing_simple.lmt"
let typing_abort = "tests/ounit/typing_abort.lmt"

let evaluation_irreducible = "tests/ounit/evaluation_irreducible.lmt"
let evaluation_application = "tests/ounit/evaluation_application.lmt"
let evaluation_example = "tests/ounit/evaluation_example3_15.lmt"
let evaluation_abort = "tests/ounit/evaluation_abort.lmt"

let evaluation_booleans = "tests/ounit/evaluation_booleans.lmt"
let evaluation_pairs1 = "tests/ounit/evaluation_pairs1.lmt"
let evaluation_pairs2 = "tests/ounit/evaluation_pairs2.lmt"
let evaluation_sums_inl = "tests/ounit/evaluation_sums_inl.lmt"
let evaluation_sums_case = "tests/ounit/evaluation_sums_case.lmt"

let typing_tests = "tests for typing" >:::[
     "basic expressions" >:: (fun _ -> assert_equal [
                            "10 : nat\n";
                            "fun(x:nat)->x end : nat->nat\n";
                            "5 : nat\n";
                            "9 : nat\n";
                            "0 : nat\n"; 
                            "fun(x:nat->bot)->(x 8) end : (nat->bot)->bot\n"]
                         (process_ounit (open_in typing_simple)));
     "abort" >:: (fun _ -> assert_equal [
                            "fun(x:(nat->bot)->bot)->bind(a:nat)->[abort].(x fun(x':nat)->bind(a':bot)->[a].(x') end end) end end : ((nat->bot)->bot)->nat\n";
                            "fun(x:bot)->bind(a:nat)->[abort].x end end : bot->nat\n"]
                         (process_ounit (open_in typing_abort)))
]

let evaluation_tests = "tests for evaluation" >:::[
     "shouldn't reduce" >:: (fun _ -> assert_equal [
                            "6 : nat\n";
                            "fun(x:nat->nat)->suc (suc ((x 0))) end : (nat->nat)->nat\n";
                            "fun(x:nat->nat->nat)->fun(x':nat)->fun(x'':nat)->((x x') x'') end end end : (nat->nat->nat)->nat->nat->nat\n"]
                         (process_ounit (open_in evaluation_irreducible)));
     "application" >:: (fun _ -> assert_equal [
                            "0 : nat\n";
                            "5 : nat\n";
                            "7 : nat\n";
                            "2 : nat\n";
                            "2 : nat\n";
                            "4 : nat\n"]
                         (process_ounit (open_in evaluation_application)));
     "paper example" >:: (fun _ -> assert_equal [
                            "0 : nat\n"]
                         (process_ounit (open_in evaluation_example)));
     "abort" >:: (fun _ -> assert_equal [
                            "fun(x:(nat->bot)->bot)->bind(a:nat)->[abort].(x fun(x':nat)->bind(a':bot)->[a].(x') end end) end end : ((nat->bot)->bot)->nat\n";
                            "fun(x:bot)->bind(a:nat)->[abort].x end end : bot->nat\n";
                            "fun(x:bot)->bind(a:nat)->[abort].bind(a':bot)->[a'].(x) end end end : bot->nat\n";
                            "fun(x:bot)->bind(a:nat)->[abort].bind(a':bot)->[a].(3) end end end : bot->nat\n"]
                         (process_ounit (open_in evaluation_abort)));
]


let booleans_tests = "tests for booleans" >:::[
     "basic expressions" >:: (fun _ -> assert_equal [
                            "true : bool\n";
                            "false : bool\n";
                            "3 : nat\n";
                            "false : bool\n";
                            "7 : nat\n"; 
                            "9 : nat\n";
                            "fun(x:bot)->if:(nat) bind(a:bool)->[abort].x end then 9 else 10 end end : bot->nat\n"]
                         (process_ounit (open_in evaluation_booleans)))
]


let pairs_tests = "tests for pairs" >:::[
     "basic expressions" >:: (fun _ -> assert_equal [
                            "{3, 4}:nat*nat : nat*nat\n";
                            "3 : nat\n";
                            "4 : nat\n";
                            "{4, 4}:nat*nat : nat*nat\n";
                            "{3, 5}:nat*nat : nat*nat\n"; 
                            "5 : nat\n"]
                         (process_ounit (open_in evaluation_pairs1)));
     "bind expressions" >:: (fun _ -> assert_equal [
                            "{3, 4}:nat*nat : nat*nat\n";
                            "{3, 4}:nat*nat : nat*nat\n";
                            "3 : nat\n";
                            "4 : nat\n";
                            "fun(x:bot)->proj1(bind(a:nat*nat)->[abort].x end) end : bot->nat\n"; 
                            "fun(x:bot)->4 end : bot->nat\n"]
                         (process_ounit (open_in evaluation_pairs2)))
]

let sums_tests = "tests for sums" >:::[
     "inl/inr" >:: (fun _ -> assert_equal [
                            "inl:(nat+bot)(3) : nat+bot\n";
                            "inr:(bot+nat)(3) : bot+nat\n";
                            "inl:(nat+bot)(4) : nat+bot\n";
                            "inr:(bot+nat)(4) : bot+nat\n";
                            "inl:(nat+bot)(5) : nat+bot\n"; 
                            "inr:(bot+nat)(5) : bot+nat\n";
                            "fun(x:bot)->inl:(nat+bot)(bind(a:nat)->[abort].x end) end : bot->nat+bot\n";
                            "inl:(nat+(bot->bot))(3) : nat+(bot->bot)\n"]
                         (process_ounit (open_in evaluation_sums_inl)));
     "case" >:: (fun _ -> assert_equal [
                            "3 : nat\n";
                            "4 : nat\n";
                            "6 : nat\n";
                            "fun(x:bot)->case:(nat) bind(a:nat+nat)->[abort].x end of inl x'->x'|inr x''->suc (x'') end end : bot->nat\n";
                            "8 : nat\n"]
                         (process_ounit (open_in evaluation_sums_case)))
]

let _ = run_test_tt_main typing_tests
let _ = run_test_tt_main evaluation_tests
let _ = run_test_tt_main booleans_tests
let _ = run_test_tt_main pairs_tests
let _ = run_test_tt_main sums_tests
