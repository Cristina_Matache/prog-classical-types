module HOL : sig
  type 'a equal = {equal : 'a -> 'a -> bool}
  val equal : 'a equal -> 'a -> 'a -> bool
  val eq : 'a equal -> 'a -> 'a -> bool
end = struct

type 'a equal = {equal : 'a -> 'a -> bool};;
let equal _A = _A.equal;;

let rec eq _A a b = equal _A a b;;

end;; (*struct HOL*)

module List : sig
  val fold : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
  val filter : ('a -> bool) -> 'a list -> 'a list
  val member : 'a HOL.equal -> 'a list -> 'a -> bool
  val insert : 'a HOL.equal -> 'a -> 'a list -> 'a list
  val removeAll : 'a HOL.equal -> 'a -> 'a list -> 'a list
end = struct

let rec fold f x1 s = match f, x1, s with f, x :: xs, s -> fold f xs (f x s)
               | f, [], s -> s;;

let rec filter
  p x1 = match p, x1 with p, [] -> []
    | p, x :: xs -> (if p x then x :: filter p xs else filter p xs);;

let rec member _A x0 y = match x0, y with [], y -> false
                    | x :: xs, y -> HOL.eq _A x y || member _A xs y;;

let rec insert _A x xs = (if member _A xs x then xs else x :: xs);;

let rec removeAll _A
  x xa1 = match x, xa1 with x, [] -> []
    | x, y :: xs ->
        (if HOL.eq _A x y then removeAll _A x xs else y :: removeAll _A x xs);;

end;; (*struct List*)

module Set : sig
  type 'a set = Set of 'a list | Coset of 'a list
  val insert : 'a HOL.equal -> 'a -> 'a set -> 'a set
  val member : 'a HOL.equal -> 'a -> 'a set -> bool
  val bot_set : 'a set
  val sup_set : 'a HOL.equal -> 'a set -> 'a set -> 'a set
end = struct

type 'a set = Set of 'a list | Coset of 'a list;;

let rec insert _A
  x xa1 = match x, xa1 with x, Coset xs -> Coset (List.removeAll _A x xs)
    | x, Set xs -> Set (List.insert _A x xs);;

let rec member _A
  x xa1 = match x, xa1 with x, Coset xs -> not (List.member _A xs x)
    | x, Set xs -> List.member _A xs x;;

let bot_set : 'a set = Set [];;

let rec sup_set _A
  x0 a = match x0, a with
    Coset xs, a -> Coset (List.filter (fun x -> not (member _A x a)) xs)
    | Set xs, a -> List.fold (insert _A) xs a;;

end;; (*struct Set*)

module Arith : sig
  type nat = Zero_nat | Suc of nat
  val equal_nata : nat -> nat -> bool
  val equal_nat : nat HOL.equal
  type num = One | Bit0 of num | Bit1 of num
  val one_nat : nat
  val less_nat : nat -> nat -> bool
  val less_eq_nat : nat -> nat -> bool
  val plus_nat : nat -> nat -> nat
  val minus_nat : nat -> nat -> nat
end = struct

type nat = Zero_nat | Suc of nat;;

let rec equal_nata x0 x1 = match x0, x1 with Zero_nat, Suc x2 -> false
                     | Suc x2, Zero_nat -> false
                     | Suc x2, Suc y2 -> equal_nata x2 y2
                     | Zero_nat, Zero_nat -> true;;

let equal_nat = ({HOL.equal = equal_nata} : nat HOL.equal);;

type num = One | Bit0 of num | Bit1 of num;;

let one_nat : nat = Suc Zero_nat;;

let rec less_nat m x1 = match m, x1 with m, Suc n -> less_eq_nat m n
                   | n, Zero_nat -> false
and less_eq_nat x0 n = match x0, n with Suc m, n -> less_nat m n
                  | Zero_nat, n -> true;;

let rec plus_nat x0 n = match x0, n with Suc m, n -> plus_nat m (Suc n)
                   | Zero_nat, n -> n;;

let rec minus_nat m n = match m, n with Suc m, Suc n -> minus_nat m n
                    | Zero_nat, n -> Zero_nat
                    | m, Zero_nat -> m;;

end;; (*struct Arith*)

module Syntax : sig
  type typea = TVar of Arith.nat | TAll of typea | Nat | Bool | Bottom |
    Prod of typea * typea | Sum of typea * typea | Fun of typea * typea
  val equal_typea : typea -> typea -> bool
  val equal_type : typea HOL.equal
  type cmd = MVar of Arith.nat * trm | Top of trm
  and trm = LVar of Arith.nat | Lbd of typea * trm | App of trm * trm |
    Mu of typea * cmd | TAbs of trm | TApp of trm * typea | Zero | S of trm |
    Nrec of typea * trm * trm * trm | TTrue | TFalse |
    If of typea * trm * trm * trm | Pair of trm * trm * typea | Proj1 of trm |
    Proj2 of trm | Inl of typea * trm | Inr of typea * trm |
    Case of typea * trm * trm * trm
  type ctxt = ContextEmpty | ContextSuc of ctxt | ContextApp of ctxt * trm |
    ContextTApp of ctxt * typea | ContextNrec of typea * trm * trm * ctxt |
    ContextIf of typea * ctxt * trm * trm | ContextPairL of ctxt * trm * typea |
    ContextPairR of trm * ctxt * typea | ContextProj1 of ctxt |
    ContextProj2 of ctxt | ContextInl of typea * ctxt |
    ContextInr of typea * ctxt | ContextCase of typea * ctxt * trm * trm
  val is_val : trm -> bool
  val is_natval : trm -> bool
end = struct

type typea = TVar of Arith.nat | TAll of typea | Nat | Bool | Bottom |
  Prod of typea * typea | Sum of typea * typea | Fun of typea * typea;;

let rec equal_typea
  x0 x1 = match x0, x1 with Sum (x71, x72), Fun (x81, x82) -> false
    | Fun (x81, x82), Sum (x71, x72) -> false
    | Prod (x61, x62), Fun (x81, x82) -> false
    | Fun (x81, x82), Prod (x61, x62) -> false
    | Prod (x61, x62), Sum (x71, x72) -> false
    | Sum (x71, x72), Prod (x61, x62) -> false
    | Bottom, Fun (x81, x82) -> false
    | Fun (x81, x82), Bottom -> false
    | Bottom, Sum (x71, x72) -> false
    | Sum (x71, x72), Bottom -> false
    | Bottom, Prod (x61, x62) -> false
    | Prod (x61, x62), Bottom -> false
    | Bool, Fun (x81, x82) -> false
    | Fun (x81, x82), Bool -> false
    | Bool, Sum (x71, x72) -> false
    | Sum (x71, x72), Bool -> false
    | Bool, Prod (x61, x62) -> false
    | Prod (x61, x62), Bool -> false
    | Bool, Bottom -> false
    | Bottom, Bool -> false
    | Nat, Fun (x81, x82) -> false
    | Fun (x81, x82), Nat -> false
    | Nat, Sum (x71, x72) -> false
    | Sum (x71, x72), Nat -> false
    | Nat, Prod (x61, x62) -> false
    | Prod (x61, x62), Nat -> false
    | Nat, Bottom -> false
    | Bottom, Nat -> false
    | Nat, Bool -> false
    | Bool, Nat -> false
    | TAll x2, Fun (x81, x82) -> false
    | Fun (x81, x82), TAll x2 -> false
    | TAll x2, Sum (x71, x72) -> false
    | Sum (x71, x72), TAll x2 -> false
    | TAll x2, Prod (x61, x62) -> false
    | Prod (x61, x62), TAll x2 -> false
    | TAll x2, Bottom -> false
    | Bottom, TAll x2 -> false
    | TAll x2, Bool -> false
    | Bool, TAll x2 -> false
    | TAll x2, Nat -> false
    | Nat, TAll x2 -> false
    | TVar x1, Fun (x81, x82) -> false
    | Fun (x81, x82), TVar x1 -> false
    | TVar x1, Sum (x71, x72) -> false
    | Sum (x71, x72), TVar x1 -> false
    | TVar x1, Prod (x61, x62) -> false
    | Prod (x61, x62), TVar x1 -> false
    | TVar x1, Bottom -> false
    | Bottom, TVar x1 -> false
    | TVar x1, Bool -> false
    | Bool, TVar x1 -> false
    | TVar x1, Nat -> false
    | Nat, TVar x1 -> false
    | TVar x1, TAll x2 -> false
    | TAll x2, TVar x1 -> false
    | Fun (x81, x82), Fun (y81, y82) ->
        equal_typea x81 y81 && equal_typea x82 y82
    | Sum (x71, x72), Sum (y71, y72) ->
        equal_typea x71 y71 && equal_typea x72 y72
    | Prod (x61, x62), Prod (y61, y62) ->
        equal_typea x61 y61 && equal_typea x62 y62
    | TAll x2, TAll y2 -> equal_typea x2 y2
    | TVar x1, TVar y1 -> Arith.equal_nata x1 y1
    | Bottom, Bottom -> true
    | Bool, Bool -> true
    | Nat, Nat -> true;;

let equal_type = ({HOL.equal = equal_typea} : typea HOL.equal);;

type cmd = MVar of Arith.nat * trm | Top of trm
and trm = LVar of Arith.nat | Lbd of typea * trm | App of trm * trm |
  Mu of typea * cmd | TAbs of trm | TApp of trm * typea | Zero | S of trm |
  Nrec of typea * trm * trm * trm | TTrue | TFalse |
  If of typea * trm * trm * trm | Pair of trm * trm * typea | Proj1 of trm |
  Proj2 of trm | Inl of typea * trm | Inr of typea * trm |
  Case of typea * trm * trm * trm;;

type ctxt = ContextEmpty | ContextSuc of ctxt | ContextApp of ctxt * trm |
  ContextTApp of ctxt * typea | ContextNrec of typea * trm * trm * ctxt |
  ContextIf of typea * ctxt * trm * trm | ContextPairL of ctxt * trm * typea |
  ContextPairR of trm * ctxt * typea | ContextProj1 of ctxt |
  ContextProj2 of ctxt | ContextInl of typea * ctxt | ContextInr of typea * ctxt
  | ContextCase of typea * ctxt * trm * trm;;

let rec is_val = function Lbd (ta, t) -> true
                 | Zero -> true
                 | S n -> is_val n
                 | TTrue -> true
                 | TFalse -> true
                 | Pair (t1, t2, t) -> is_val t1 && is_val t2
                 | Inl (ta, t) -> is_val t
                 | Inr (ta, t) -> is_val t
                 | TAbs t -> true
                 | LVar v -> false
                 | App (v, va) -> false
                 | Mu (v, va) -> false
                 | TApp (v, va) -> false
                 | Nrec (v, va, vb, vc) -> false
                 | If (v, va, vb, vc) -> false
                 | Proj1 v -> false
                 | Proj2 v -> false
                 | Case (v, va, vb, vc) -> false;;

let rec is_natval = function Zero -> true
                    | S n -> is_natval n
                    | LVar v -> false
                    | Lbd (v, va) -> false
                    | App (v, va) -> false
                    | Mu (v, va) -> false
                    | TAbs v -> false
                    | TApp (v, va) -> false
                    | Nrec (v, va, vb, vc) -> false
                    | TTrue -> false
                    | TFalse -> false
                    | If (v, va, vb, vc) -> false
                    | Pair (v, va, vb) -> false
                    | Proj1 v -> false
                    | Proj2 v -> false
                    | Inl (v, va) -> false
                    | Inr (v, va) -> false
                    | Case (v, va, vb, vc) -> false;;

end;; (*struct Syntax*)

module DeBruijn : sig
  val fmv_cmd : Syntax.cmd -> Arith.nat -> Arith.nat Set.set
  val fmv_trm : Syntax.trm -> Arith.nat -> Arith.nat Set.set
  val dropM_cmd : Syntax.cmd -> Arith.nat -> Syntax.cmd
  val dropM_trm : Syntax.trm -> Arith.nat -> Syntax.trm
  val liftL_cmd : Syntax.cmd -> Arith.nat -> Syntax.cmd
  val liftL_trm : Syntax.trm -> Arith.nat -> Syntax.trm
  val liftM_cmd : Syntax.cmd -> Arith.nat -> Syntax.cmd
  val liftM_trm : Syntax.trm -> Arith.nat -> Syntax.trm
  val liftT_type : Syntax.typea -> Arith.nat -> Syntax.typea
  val liftT_cmd : Syntax.cmd -> Arith.nat -> Syntax.cmd
  val liftT_trm : Syntax.trm -> Arith.nat -> Syntax.trm
  val liftL_ctxt : Syntax.ctxt -> Arith.nat -> Syntax.ctxt
  val liftM_ctxt : Syntax.ctxt -> Arith.nat -> Syntax.ctxt
  val liftT_ctxt : Syntax.ctxt -> Arith.nat -> Syntax.ctxt
  val type_subst : Syntax.typea -> Arith.nat -> Syntax.typea -> Syntax.typea
end = struct

let rec fmv_cmd
  x0 k = match x0, k with
    Syntax.MVar (i, t), k ->
      (if Arith.less_eq_nat k i
        then Set.sup_set Arith.equal_nat
               (Set.insert Arith.equal_nat (Arith.minus_nat i k) Set.bot_set)
               (fmv_trm t k)
        else fmv_trm t k)
    | Syntax.Top t, k -> fmv_trm t k
and fmv_trm
  x0 k = match x0, k with Syntax.LVar i, k -> Set.bot_set
    | Syntax.Lbd (ta, t), k -> fmv_trm t k
    | Syntax.App (s, t), k ->
        Set.sup_set Arith.equal_nat (fmv_trm s k) (fmv_trm t k)
    | Syntax.Mu (t, c), k -> fmv_cmd c (Arith.plus_nat k Arith.one_nat)
    | Syntax.Zero, k -> Set.bot_set
    | Syntax.S t, k -> fmv_trm t k
    | Syntax.Nrec (ta, r, s, t), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_trm r k) (fmv_trm s k))
          (fmv_trm t k)
    | Syntax.TAbs t, k -> fmv_trm t k
    | Syntax.TApp (ta, t), k -> fmv_trm ta k
    | Syntax.TTrue, k -> Set.bot_set
    | Syntax.TFalse, k -> Set.bot_set
    | Syntax.If (t, t1, t2, t3), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_trm t1 k) (fmv_trm t2 k))
          (fmv_trm t3 k)
    | Syntax.Pair (t1, t2, t), k ->
        Set.sup_set Arith.equal_nat (fmv_trm t1 k) (fmv_trm t2 k)
    | Syntax.Proj1 t, k -> fmv_trm t k
    | Syntax.Proj2 t, k -> fmv_trm t k
    | Syntax.Inl (ta, t), k -> fmv_trm t k
    | Syntax.Inr (ta, t), k -> fmv_trm t k
    | Syntax.Case (t, t1, t2, t3), k ->
        Set.sup_set Arith.equal_nat
          (Set.sup_set Arith.equal_nat (fmv_trm t1 k) (fmv_trm t2 k))
          (fmv_trm t3 k);;

let rec dropM_cmd
  x0 k = match x0, k with
    Syntax.MVar (i, t), k ->
      (if Arith.less_nat k i
        then Syntax.MVar (Arith.minus_nat i Arith.one_nat, dropM_trm t k)
        else Syntax.MVar (i, dropM_trm t k))
    | Syntax.Top t, k -> Syntax.Top (dropM_trm t k)
and dropM_trm
  x0 k = match x0, k with Syntax.LVar i, k -> Syntax.LVar i
    | Syntax.Lbd (ta, t), k -> Syntax.Lbd (ta, dropM_trm t k)
    | Syntax.App (s, t), k -> Syntax.App (dropM_trm s k, dropM_trm t k)
    | Syntax.Mu (t, c), k ->
        Syntax.Mu (t, dropM_cmd c (Arith.plus_nat k Arith.one_nat))
    | Syntax.Zero, k -> Syntax.Zero
    | Syntax.S t, k -> Syntax.S (dropM_trm t k)
    | Syntax.Nrec (ta, r, s, t), k ->
        Syntax.Nrec (ta, dropM_trm r k, dropM_trm s k, dropM_trm t k)
    | Syntax.TAbs t, k -> Syntax.TAbs (dropM_trm t k)
    | Syntax.TApp (t, tau), k -> Syntax.TApp (dropM_trm t k, tau)
    | Syntax.TTrue, k -> Syntax.TTrue
    | Syntax.TFalse, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), k ->
        Syntax.If (t, dropM_trm t1 k, dropM_trm t2 k, dropM_trm t3 k)
    | Syntax.Pair (t1, t2, t), k ->
        Syntax.Pair (dropM_trm t1 k, dropM_trm t2 k, t)
    | Syntax.Proj1 t, k -> Syntax.Proj1 (dropM_trm t k)
    | Syntax.Proj2 t, k -> Syntax.Proj2 (dropM_trm t k)
    | Syntax.Inl (ta, t), k -> Syntax.Inl (ta, dropM_trm t k)
    | Syntax.Inr (ta, t), k -> Syntax.Inr (ta, dropM_trm t k)
    | Syntax.Case (t, t1, t2, t3), k ->
        Syntax.Case (t, dropM_trm t1 k, dropM_trm t2 k, dropM_trm t3 k);;

let rec liftL_cmd
  x0 k = match x0, k with
    Syntax.MVar (i, t), k -> Syntax.MVar (i, liftL_trm t k)
    | Syntax.Top t, k -> Syntax.Top (liftL_trm t k)
and liftL_trm
  x0 k = match x0, k with
    Syntax.LVar i, k ->
      (if Arith.less_nat i k then Syntax.LVar i
        else Syntax.LVar (Arith.plus_nat i Arith.one_nat))
    | Syntax.Lbd (ta, t), k ->
        Syntax.Lbd (ta, liftL_trm t (Arith.plus_nat k Arith.one_nat))
    | Syntax.App (s, t), k -> Syntax.App (liftL_trm s k, liftL_trm t k)
    | Syntax.Mu (t, c), k -> Syntax.Mu (t, liftL_cmd c k)
    | Syntax.Zero, k -> Syntax.Zero
    | Syntax.S t, k -> Syntax.S (liftL_trm t k)
    | Syntax.Nrec (ta, t, u, v), k ->
        Syntax.Nrec (ta, liftL_trm t k, liftL_trm u k, liftL_trm v k)
    | Syntax.TAbs t, k -> Syntax.TAbs (liftL_trm t k)
    | Syntax.TApp (t, tau), k -> Syntax.TApp (liftL_trm t k, tau)
    | Syntax.TTrue, k -> Syntax.TTrue
    | Syntax.TFalse, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), k ->
        Syntax.If (t, liftL_trm t1 k, liftL_trm t2 k, liftL_trm t3 k)
    | Syntax.Pair (t1, t2, t), k ->
        Syntax.Pair (liftL_trm t1 k, liftL_trm t2 k, t)
    | Syntax.Proj1 t, k -> Syntax.Proj1 (liftL_trm t k)
    | Syntax.Proj2 t, k -> Syntax.Proj2 (liftL_trm t k)
    | Syntax.Inl (ta, t), k -> Syntax.Inl (ta, liftL_trm t k)
    | Syntax.Inr (ta, t), k -> Syntax.Inr (ta, liftL_trm t k)
    | Syntax.Case (t, t0, t1, t2), k ->
        Syntax.Case
          (t, liftL_trm t0 k, liftL_trm t1 (Arith.plus_nat k Arith.one_nat),
            liftL_trm t2 (Arith.plus_nat k Arith.one_nat));;

let rec liftM_cmd
  x0 k = match x0, k with
    Syntax.MVar (i, t), k ->
      (if Arith.less_nat i k then Syntax.MVar (i, liftM_trm t k)
        else Syntax.MVar (Arith.plus_nat i Arith.one_nat, liftM_trm t k))
    | Syntax.Top t, k -> Syntax.Top (liftM_trm t k)
and liftM_trm
  x0 k = match x0, k with Syntax.LVar i, k -> Syntax.LVar i
    | Syntax.Lbd (ta, t), k -> Syntax.Lbd (ta, liftM_trm t k)
    | Syntax.App (s, t), k -> Syntax.App (liftM_trm s k, liftM_trm t k)
    | Syntax.Mu (t, c), k ->
        Syntax.Mu (t, liftM_cmd c (Arith.plus_nat k Arith.one_nat))
    | Syntax.Zero, k -> Syntax.Zero
    | Syntax.S t, k -> Syntax.S (liftM_trm t k)
    | Syntax.Nrec (ta, t, u, v), k ->
        Syntax.Nrec (ta, liftM_trm t k, liftM_trm u k, liftM_trm v k)
    | Syntax.TAbs t, k -> Syntax.TAbs (liftM_trm t k)
    | Syntax.TApp (t, tau), k -> Syntax.TApp (liftM_trm t k, tau)
    | Syntax.TTrue, k -> Syntax.TTrue
    | Syntax.TFalse, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), k ->
        Syntax.If (t, liftM_trm t1 k, liftM_trm t2 k, liftM_trm t3 k)
    | Syntax.Pair (t1, t2, t), k ->
        Syntax.Pair (liftM_trm t1 k, liftM_trm t2 k, t)
    | Syntax.Proj1 t, k -> Syntax.Proj1 (liftM_trm t k)
    | Syntax.Proj2 t, k -> Syntax.Proj2 (liftM_trm t k)
    | Syntax.Inl (ta, t), k -> Syntax.Inl (ta, liftM_trm t k)
    | Syntax.Inr (ta, t), k -> Syntax.Inr (ta, liftM_trm t k)
    | Syntax.Case (t, t0, t1, t2), k ->
        Syntax.Case (t, liftM_trm t0 k, liftM_trm t1 k, liftM_trm t2 k);;

let rec liftT_type
  x0 k = match x0, k with
    Syntax.TVar i, k ->
      (if Arith.less_nat i k then Syntax.TVar i
        else Syntax.TVar (Arith.plus_nat i Arith.one_nat))
    | Syntax.TAll t, k ->
        Syntax.TAll (liftT_type t (Arith.plus_nat k Arith.one_nat))
    | Syntax.Prod (r, t), k -> Syntax.Prod (liftT_type r k, liftT_type t k)
    | Syntax.Sum (r, t), k -> Syntax.Sum (liftT_type r k, liftT_type t k)
    | Syntax.Fun (r, t), k -> Syntax.Fun (liftT_type r k, liftT_type t k)
    | Syntax.Nat, k -> Syntax.Nat
    | Syntax.Bool, k -> Syntax.Bool
    | Syntax.Bottom, k -> Syntax.Bottom;;

let rec liftT_cmd
  x0 k = match x0, k with
    Syntax.MVar (i, t), k -> Syntax.MVar (i, liftT_trm t k)
    | Syntax.Top t, k -> Syntax.Top (liftT_trm t k)
and liftT_trm
  x0 k = match x0, k with Syntax.LVar i, k -> Syntax.LVar i
    | Syntax.Lbd (ta, t), k -> Syntax.Lbd (liftT_type ta k, liftT_trm t k)
    | Syntax.App (s, t), k -> Syntax.App (liftT_trm s k, liftT_trm t k)
    | Syntax.Mu (t, c), k -> Syntax.Mu (liftT_type t k, liftT_cmd c k)
    | Syntax.Zero, k -> Syntax.Zero
    | Syntax.S t, k -> Syntax.S (liftT_trm t k)
    | Syntax.Nrec (ta, t, u, v), k ->
        Syntax.Nrec
          (liftT_type ta k, liftT_trm t k, liftT_trm u k, liftT_trm v k)
    | Syntax.TAbs t, k -> Syntax.TAbs (liftT_trm t (Arith.Suc k))
    | Syntax.TApp (t, tau), k -> Syntax.TApp (liftT_trm t k, liftT_type tau k)
    | Syntax.TTrue, k -> Syntax.TTrue
    | Syntax.TFalse, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), k ->
        Syntax.If
          (liftT_type t k, liftT_trm t1 k, liftT_trm t2 k, liftT_trm t3 k)
    | Syntax.Pair (t1, t2, t), k ->
        Syntax.Pair (liftT_trm t1 k, liftT_trm t2 k, liftT_type t k)
    | Syntax.Proj1 t, k -> Syntax.Proj1 (liftT_trm t k)
    | Syntax.Proj2 t, k -> Syntax.Proj2 (liftT_trm t k)
    | Syntax.Inl (ta, t), k -> Syntax.Inl (liftT_type ta k, liftT_trm t k)
    | Syntax.Inr (ta, t), k -> Syntax.Inr (liftT_type ta k, liftT_trm t k)
    | Syntax.Case (t, t0, t1, t2), k ->
        Syntax.Case
          (liftT_type t k, liftT_trm t0 k, liftT_trm t1 k, liftT_trm t2 k);;

let rec liftL_ctxt
  x0 n = match x0, n with Syntax.ContextEmpty, n -> Syntax.ContextEmpty
    | Syntax.ContextSuc e, n -> Syntax.ContextSuc (liftL_ctxt e n)
    | Syntax.ContextApp (e, t), n ->
        Syntax.ContextApp (liftL_ctxt e n, liftL_trm t n)
    | Syntax.ContextNrec (t, r, s, e), n ->
        Syntax.ContextNrec (t, liftL_trm r n, liftL_trm s n, liftL_ctxt e n)
    | Syntax.ContextTApp (t, tau), k -> Syntax.ContextTApp (liftL_ctxt t k, tau)
    | Syntax.ContextIf (t, e, t2, t3), n ->
        Syntax.ContextIf (t, liftL_ctxt e n, liftL_trm t2 n, liftL_trm t3 n)
    | Syntax.ContextPairL (e, ta, t), n ->
        Syntax.ContextPairL (liftL_ctxt e n, liftL_trm ta n, t)
    | Syntax.ContextPairR (ta, e, t), n ->
        Syntax.ContextPairR (liftL_trm ta n, liftL_ctxt e n, t)
    | Syntax.ContextProj1 e, n -> Syntax.ContextProj1 (liftL_ctxt e n)
    | Syntax.ContextProj2 e, n -> Syntax.ContextProj2 (liftL_ctxt e n)
    | Syntax.ContextInl (t, e), k -> Syntax.ContextInl (t, liftL_ctxt e k)
    | Syntax.ContextInr (t, e), k -> Syntax.ContextInr (t, liftL_ctxt e k)
    | Syntax.ContextCase (t, e, t1, t2), k ->
        Syntax.ContextCase
          (t, liftL_ctxt e k, liftL_trm t1 (Arith.plus_nat k Arith.one_nat),
            liftL_trm t2 (Arith.plus_nat k Arith.one_nat));;

let rec liftM_ctxt
  x0 n = match x0, n with Syntax.ContextEmpty, n -> Syntax.ContextEmpty
    | Syntax.ContextSuc e, n -> Syntax.ContextSuc (liftM_ctxt e n)
    | Syntax.ContextApp (e, t), n ->
        Syntax.ContextApp (liftM_ctxt e n, liftM_trm t n)
    | Syntax.ContextNrec (t, r, s, e), n ->
        Syntax.ContextNrec (t, liftM_trm r n, liftM_trm s n, liftM_ctxt e n)
    | Syntax.ContextTApp (t, tau), k -> Syntax.ContextTApp (liftM_ctxt t k, tau)
    | Syntax.ContextIf (t, e, t2, t3), n ->
        Syntax.ContextIf (t, liftM_ctxt e n, liftM_trm t2 n, liftM_trm t3 n)
    | Syntax.ContextPairL (e, ta, t), n ->
        Syntax.ContextPairL (liftM_ctxt e n, liftM_trm ta n, t)
    | Syntax.ContextPairR (ta, e, t), n ->
        Syntax.ContextPairR (liftM_trm ta n, liftM_ctxt e n, t)
    | Syntax.ContextProj1 e, n -> Syntax.ContextProj1 (liftM_ctxt e n)
    | Syntax.ContextProj2 e, n -> Syntax.ContextProj2 (liftM_ctxt e n)
    | Syntax.ContextInl (t, e), k -> Syntax.ContextInl (t, liftM_ctxt e k)
    | Syntax.ContextInr (t, e), k -> Syntax.ContextInr (t, liftM_ctxt e k)
    | Syntax.ContextCase (t, e, t1, t2), k ->
        Syntax.ContextCase (t, liftM_ctxt e k, liftM_trm t1 k, liftM_trm t2 k);;

let rec liftT_ctxt
  x0 n = match x0, n with Syntax.ContextEmpty, n -> Syntax.ContextEmpty
    | Syntax.ContextSuc e, n -> Syntax.ContextSuc (liftT_ctxt e n)
    | Syntax.ContextApp (e, t), n ->
        Syntax.ContextApp (liftT_ctxt e n, liftT_trm t n)
    | Syntax.ContextNrec (t, r, s, e), n ->
        Syntax.ContextNrec
          (liftT_type t n, liftT_trm r n, liftT_trm s n, liftT_ctxt e n)
    | Syntax.ContextTApp (t, tau), k ->
        Syntax.ContextTApp (liftT_ctxt t k, liftT_type tau k)
    | Syntax.ContextIf (t, e, t2, t3), n ->
        Syntax.ContextIf
          (liftT_type t n, liftT_ctxt e n, liftT_trm t2 n, liftT_trm t3 n)
    | Syntax.ContextPairL (e, ta, t), n ->
        Syntax.ContextPairL (liftT_ctxt e n, liftT_trm ta n, liftT_type t n)
    | Syntax.ContextPairR (ta, e, t), n ->
        Syntax.ContextPairR (liftT_trm ta n, liftT_ctxt e n, liftT_type t n)
    | Syntax.ContextProj1 e, n -> Syntax.ContextProj1 (liftT_ctxt e n)
    | Syntax.ContextProj2 e, n -> Syntax.ContextProj2 (liftT_ctxt e n)
    | Syntax.ContextInl (t, e), k ->
        Syntax.ContextInl (liftT_type t k, liftT_ctxt e k)
    | Syntax.ContextInr (t, e), k ->
        Syntax.ContextInr (liftT_type t k, liftT_ctxt e k)
    | Syntax.ContextCase (t, e, t1, t2), k ->
        Syntax.ContextCase
          (liftT_type t k, liftT_ctxt e k, liftT_trm t1 k, liftT_trm t2 k);;

let rec type_subst
  x0 n t = match x0, n, t with
    Syntax.TVar m, n, t ->
      (if Arith.less_nat n m then Syntax.TVar (Arith.minus_nat m Arith.one_nat)
        else (if Arith.equal_nata n m then t else Syntax.TVar m))
    | Syntax.TAll t, n, u ->
        Syntax.TAll (type_subst t (Arith.Suc n) (liftT_type u Arith.Zero_nat))
    | Syntax.Nat, n, u -> Syntax.Nat
    | Syntax.Bool, n, u -> Syntax.Bool
    | Syntax.Bottom, n, u -> Syntax.Bottom
    | Syntax.Prod (r, t), n, u ->
        Syntax.Prod (type_subst r n u, type_subst t n u)
    | Syntax.Sum (r, t), n, u -> Syntax.Sum (type_subst r n u, type_subst t n u)
    | Syntax.Fun (r, t), n, u ->
        Syntax.Fun (type_subst r n u, type_subst t n u);;

end;; (*struct DeBruijn*)

module Substitution : sig
  val subst_cmd : Syntax.cmd -> Syntax.trm -> Arith.nat -> Syntax.cmd
  val subst_trm : Syntax.trm -> Syntax.trm -> Arith.nat -> Syntax.trm
  val ctxt_subst : Syntax.ctxt -> Syntax.trm -> Syntax.trm
  val subst_type_cmd : Syntax.cmd -> Arith.nat -> Syntax.typea -> Syntax.cmd
  val subst_type_trm : Syntax.trm -> Arith.nat -> Syntax.typea -> Syntax.trm
  val struct_subst_cmd :
    Syntax.cmd ->
      Arith.nat option -> Arith.nat option -> Syntax.ctxt -> Syntax.cmd
  val struct_subst_trm :
    Syntax.trm ->
      Arith.nat option -> Arith.nat option -> Syntax.ctxt -> Syntax.trm
end = struct

let rec subst_cmd
  x0 s k = match x0, s, k with
    Syntax.MVar (i, t), s, k -> Syntax.MVar (i, subst_trm t s k)
    | Syntax.Top t, s, k -> Syntax.Top (subst_trm t s k)
and subst_trm
  x0 s k = match x0, s, k with
    Syntax.LVar i, s, k ->
      (if Arith.less_nat k i then Syntax.LVar (Arith.minus_nat i Arith.one_nat)
        else (if Arith.equal_nata k i then s else Syntax.LVar i))
    | Syntax.Lbd (ta, t), s, k ->
        Syntax.Lbd
          (ta, subst_trm t (DeBruijn.liftL_trm s Arith.Zero_nat)
                 (Arith.plus_nat k Arith.one_nat))
    | Syntax.App (t, u), s, k -> Syntax.App (subst_trm t s k, subst_trm u s k)
    | Syntax.Mu (t, c), s, k ->
        Syntax.Mu (t, subst_cmd c (DeBruijn.liftM_trm s Arith.Zero_nat) k)
    | Syntax.TAbs t, s, k ->
        Syntax.TAbs (subst_trm t (DeBruijn.liftT_trm s Arith.Zero_nat) k)
    | Syntax.TApp (t, tau), s, k -> Syntax.TApp (subst_trm t s k, tau)
    | Syntax.Zero, s, k -> Syntax.Zero
    | Syntax.S t, s, k -> Syntax.S (subst_trm t s k)
    | Syntax.Nrec (ta, t, u, v), s, k ->
        Syntax.Nrec (ta, subst_trm t s k, subst_trm u s k, subst_trm v s k)
    | Syntax.TTrue, s, k -> Syntax.TTrue
    | Syntax.TFalse, s, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), s, k ->
        Syntax.If (t, subst_trm t1 s k, subst_trm t2 s k, subst_trm t3 s k)
    | Syntax.Pair (t1, t2, t), s, k ->
        Syntax.Pair (subst_trm t1 s k, subst_trm t2 s k, t)
    | Syntax.Proj1 t, s, k -> Syntax.Proj1 (subst_trm t s k)
    | Syntax.Proj2 t, s, k -> Syntax.Proj2 (subst_trm t s k)
    | Syntax.Inl (ta, t), s, k -> Syntax.Inl (ta, subst_trm t s k)
    | Syntax.Inr (ta, t), s, k -> Syntax.Inr (ta, subst_trm t s k)
    | Syntax.Case (t, t0, t1, t2), s, k ->
        Syntax.Case
          (t, subst_trm t0 s k,
            subst_trm t1 (DeBruijn.liftL_trm s Arith.Zero_nat)
              (Arith.plus_nat k Arith.one_nat),
            subst_trm t2 (DeBruijn.liftL_trm s Arith.Zero_nat)
              (Arith.plus_nat k Arith.one_nat));;

let rec ctxt_subst
  x0 s = match x0, s with Syntax.ContextEmpty, s -> s
    | Syntax.ContextSuc e, s -> Syntax.S (ctxt_subst e s)
    | Syntax.ContextApp (e, t), s -> Syntax.App (ctxt_subst e s, t)
    | Syntax.ContextNrec (ta, r, t, e), s ->
        Syntax.Nrec (ta, r, t, ctxt_subst e s)
    | Syntax.ContextIf (t, e, t2, t3), s ->
        Syntax.If (t, ctxt_subst e s, t2, t3)
    | Syntax.ContextTApp (t, tau), s -> Syntax.TApp (ctxt_subst t s, tau)
    | Syntax.ContextPairL (e, ta, t), s -> Syntax.Pair (ctxt_subst e s, ta, t)
    | Syntax.ContextPairR (ta, e, t), s -> Syntax.Pair (ta, ctxt_subst e s, t)
    | Syntax.ContextProj1 e, s -> Syntax.Proj1 (ctxt_subst e s)
    | Syntax.ContextProj2 e, s -> Syntax.Proj2 (ctxt_subst e s)
    | Syntax.ContextInl (t, e), s -> Syntax.Inl (t, ctxt_subst e s)
    | Syntax.ContextInr (t, e), s -> Syntax.Inr (t, ctxt_subst e s)
    | Syntax.ContextCase (t, e, t2, t3), s ->
        Syntax.Case (t, ctxt_subst e s, t2, t3);;

let rec subst_type_cmd
  x0 s k = match x0, s, k with
    Syntax.MVar (i, t), s, k -> Syntax.MVar (i, subst_type_trm t s k)
    | Syntax.Top t, s, k -> Syntax.Top (subst_type_trm t s k)
and subst_type_trm
  x0 s k = match x0, s, k with Syntax.LVar i, s, k -> Syntax.LVar i
    | Syntax.Lbd (ta, t), s, k ->
        Syntax.Lbd (DeBruijn.type_subst ta s k, subst_type_trm t s k)
    | Syntax.App (t, u), s, k ->
        Syntax.App (subst_type_trm t s k, subst_type_trm u s k)
    | Syntax.Mu (t, c), s, k ->
        Syntax.Mu (DeBruijn.type_subst t s k, subst_type_cmd c s k)
    | Syntax.TAbs t, s, k ->
        Syntax.TAbs
          (subst_type_trm t (Arith.Suc s)
            (DeBruijn.liftT_type k Arith.Zero_nat))
    | Syntax.TApp (t, tau), s, k ->
        Syntax.TApp (subst_type_trm t s k, DeBruijn.type_subst tau s k)
    | Syntax.Zero, s, k -> Syntax.Zero
    | Syntax.S t, s, k -> Syntax.S (subst_type_trm t s k)
    | Syntax.Nrec (ta, t, u, v), s, k ->
        Syntax.Nrec
          (DeBruijn.type_subst ta s k, subst_type_trm t s k,
            subst_type_trm u s k, subst_type_trm v s k)
    | Syntax.TTrue, s, k -> Syntax.TTrue
    | Syntax.TFalse, s, k -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), s, k ->
        Syntax.If
          (DeBruijn.type_subst t s k, subst_type_trm t1 s k,
            subst_type_trm t2 s k, subst_type_trm t3 s k)
    | Syntax.Pair (t1, t2, t), s, k ->
        Syntax.Pair
          (subst_type_trm t1 s k, subst_type_trm t2 s k,
            DeBruijn.type_subst t s k)
    | Syntax.Proj1 t, s, k -> Syntax.Proj1 (subst_type_trm t s k)
    | Syntax.Proj2 t, s, k -> Syntax.Proj2 (subst_type_trm t s k)
    | Syntax.Inl (ta, t), s, k ->
        Syntax.Inl (DeBruijn.type_subst ta s k, subst_type_trm t s k)
    | Syntax.Inr (ta, t), s, k ->
        Syntax.Inr (DeBruijn.type_subst ta s k, subst_type_trm t s k)
    | Syntax.Case (t, t0, t1, t2), s, k ->
        Syntax.Case
          (DeBruijn.type_subst t s k, subst_type_trm t0 s k,
            subst_type_trm t1 s k, subst_type_trm t2 s k);;

let rec struct_subst_cmd
  x0 x1 k e = match x0, x1, k, e with
    Syntax.MVar (i, t), Some j, Some k, e ->
      (if Arith.equal_nata i j
        then Syntax.MVar
               (k, ctxt_subst e (struct_subst_trm t (Some j) (Some k) e))
        else (if Arith.less_nat j i && Arith.less_eq_nat i k
               then Syntax.MVar
                      (Arith.minus_nat i Arith.one_nat,
                        struct_subst_trm t (Some j) (Some k) e)
               else (if Arith.less_eq_nat k i && Arith.less_nat i j
                      then Syntax.MVar
                             (Arith.plus_nat i Arith.one_nat,
                               struct_subst_trm t (Some j) (Some k) e)
                      else Syntax.MVar
                             (i, struct_subst_trm t (Some j) (Some k) e))))
    | Syntax.MVar (i, t), None, k, e ->
        Syntax.MVar (i, struct_subst_trm t None k e)
    | Syntax.MVar (i, t), Some j, None, e ->
        (if Arith.equal_nata i j
          then Syntax.Top (ctxt_subst e (struct_subst_trm t (Some j) None e))
          else Syntax.MVar (i, struct_subst_trm t (Some j) None e))
    | Syntax.Top t, Some j, k, e -> Syntax.Top (struct_subst_trm t (Some j) k e)
    | Syntax.Top t, None, Some k, e ->
        Syntax.MVar (k, ctxt_subst e (struct_subst_trm t None (Some k) e))
    | Syntax.Top t, None, None, e ->
        Syntax.Top (ctxt_subst e (struct_subst_trm t None None e))
and struct_subst_trm
  x0 j k e = match x0, j, k, e with Syntax.LVar i, j, k, e -> Syntax.LVar i
    | Syntax.Lbd (ta, t), j, k, e ->
        Syntax.Lbd
          (ta, struct_subst_trm t j k (DeBruijn.liftL_ctxt e Arith.Zero_nat))
    | Syntax.App (t, s), j, k, e ->
        Syntax.App (struct_subst_trm t j k e, struct_subst_trm s j k e)
    | Syntax.TAbs t, j, k, e ->
        Syntax.TAbs
          (struct_subst_trm t j k (DeBruijn.liftT_ctxt e Arith.Zero_nat))
    | Syntax.TApp (t, tau), j, k, e ->
        Syntax.TApp (struct_subst_trm t j k e, tau)
    | Syntax.Mu (t, c), Some j, Some k, e ->
        Syntax.Mu
          (t, struct_subst_cmd c (Some (Arith.plus_nat j Arith.one_nat))
                (Some (Arith.plus_nat k Arith.one_nat))
                (DeBruijn.liftM_ctxt e Arith.Zero_nat))
    | Syntax.Mu (t, c), None, Some k, e ->
        Syntax.Mu
          (t, struct_subst_cmd c None (Some (Arith.plus_nat k Arith.one_nat))
                (DeBruijn.liftM_ctxt e Arith.Zero_nat))
    | Syntax.Mu (t, c), Some j, None, e ->
        Syntax.Mu
          (t, struct_subst_cmd c (Some (Arith.plus_nat j Arith.one_nat)) None
                (DeBruijn.liftM_ctxt e Arith.Zero_nat))
    | Syntax.Mu (t, c), None, None, e ->
        Syntax.Mu
          (t, struct_subst_cmd c None None
                (DeBruijn.liftM_ctxt e Arith.Zero_nat))
    | Syntax.Zero, j, k, e -> Syntax.Zero
    | Syntax.S t, j, k, e -> Syntax.S (struct_subst_trm t j k e)
    | Syntax.Nrec (ta, r, s, t), j, k, e ->
        Syntax.Nrec
          (ta, struct_subst_trm r j k e, struct_subst_trm s j k e,
            struct_subst_trm t j k e)
    | Syntax.TTrue, j, k, e -> Syntax.TTrue
    | Syntax.TFalse, j, k, e -> Syntax.TFalse
    | Syntax.If (t, t1, t2, t3), j, k, e ->
        Syntax.If
          (t, struct_subst_trm t1 j k e, struct_subst_trm t2 j k e,
            struct_subst_trm t3 j k e)
    | Syntax.Pair (t1, t2, t), j, k, e ->
        Syntax.Pair (struct_subst_trm t1 j k e, struct_subst_trm t2 j k e, t)
    | Syntax.Proj1 t, j, k, e -> Syntax.Proj1 (struct_subst_trm t j k e)
    | Syntax.Proj2 t, j, k, e -> Syntax.Proj2 (struct_subst_trm t j k e)
    | Syntax.Inl (ta, t), j, k, e -> Syntax.Inl (ta, struct_subst_trm t j k e)
    | Syntax.Inr (ta, t), j, k, e -> Syntax.Inr (ta, struct_subst_trm t j k e)
    | Syntax.Case (t, t1, t2, t3), j, k, e ->
        Syntax.Case
          (t, struct_subst_trm t1 j k e,
            struct_subst_trm t2 j k (DeBruijn.liftL_ctxt e Arith.Zero_nat),
            struct_subst_trm t3 j k (DeBruijn.liftL_ctxt e Arith.Zero_nat));;

end;; (*struct Substitution*)

module Reduction : sig
  val is_nfC : Syntax.cmd -> bool
  val is_nf : Syntax.trm -> bool
end = struct

let rec is_nfC
  = function
    Syntax.MVar (c, v) ->
      Syntax.is_val v &&
        (if Arith.equal_nata c Arith.Zero_nat
          then Set.member Arith.equal_nat Arith.Zero_nat
                 (DeBruijn.fmv_trm v Arith.Zero_nat)
          else true)
    | Syntax.Top v -> Syntax.is_val v;;

let rec is_nf
  = function Syntax.Mu (t, c) -> is_nfC c
    | Syntax.LVar va -> Syntax.is_val (Syntax.LVar va)
    | Syntax.Lbd (va, vb) -> Syntax.is_val (Syntax.Lbd (va, vb))
    | Syntax.App (va, vb) -> Syntax.is_val (Syntax.App (va, vb))
    | Syntax.TAbs va -> Syntax.is_val (Syntax.TAbs va)
    | Syntax.TApp (va, vb) -> Syntax.is_val (Syntax.TApp (va, vb))
    | Syntax.Zero -> Syntax.is_val Syntax.Zero
    | Syntax.S va -> Syntax.is_val (Syntax.S va)
    | Syntax.Nrec (va, vb, vc, vd) ->
        Syntax.is_val (Syntax.Nrec (va, vb, vc, vd))
    | Syntax.TTrue -> Syntax.is_val Syntax.TTrue
    | Syntax.TFalse -> Syntax.is_val Syntax.TFalse
    | Syntax.If (va, vb, vc, vd) -> Syntax.is_val (Syntax.If (va, vb, vc, vd))
    | Syntax.Pair (va, vb, vc) -> Syntax.is_val (Syntax.Pair (va, vb, vc))
    | Syntax.Proj1 va -> Syntax.is_val (Syntax.Proj1 va)
    | Syntax.Proj2 va -> Syntax.is_val (Syntax.Proj2 va)
    | Syntax.Inl (va, vb) -> Syntax.is_val (Syntax.Inl (va, vb))
    | Syntax.Inr (va, vb) -> Syntax.is_val (Syntax.Inr (va, vb))
    | Syntax.Case (va, vb, vc, vd) ->
        Syntax.is_val (Syntax.Case (va, vb, vc, vd));;

end;; (*struct Reduction*)

module Option : sig
  val bind : 'a option -> ('a -> 'b option) -> 'b option
end = struct

let rec bind x0 f = match x0, f with None, f -> None
               | Some x, f -> f x;;

end;; (*struct Option*)

module MuML : sig
  val sem_cmd : Syntax.cmd -> Syntax.cmd option
  val sem_trm : Syntax.trm -> Syntax.trm option
end = struct

let rec sem_cmd
  = function
    Syntax.MVar (i, t) ->
      (if Reduction.is_nf t
        then (match t with Syntax.LVar _ -> None | Syntax.Lbd (_, _) -> None
               | Syntax.App (_, _) -> None
               | Syntax.Mu (_, c) ->
                 Some (DeBruijn.dropM_cmd
                        (Substitution.struct_subst_cmd c (Some Arith.Zero_nat)
                          (Some i) Syntax.ContextEmpty)
                        i)
               | Syntax.TAbs _ -> None | Syntax.TApp (_, _) -> None
               | Syntax.Zero -> None | Syntax.S _ -> None
               | Syntax.Nrec (_, _, _, _) -> None | Syntax.TTrue -> None
               | Syntax.TFalse -> None | Syntax.If (_, _, _, _) -> None
               | Syntax.Pair (_, _, _) -> None | Syntax.Proj1 _ -> None
               | Syntax.Proj2 _ -> None | Syntax.Inl (_, _) -> None
               | Syntax.Inr (_, _) -> None | Syntax.Case (_, _, _, _) -> None)
        else Option.bind (sem_trm t) (fun u -> Some (Syntax.MVar (i, u))))
    | Syntax.Top t ->
        (if Reduction.is_nf t
          then (match t with Syntax.LVar _ -> None | Syntax.Lbd (_, _) -> None
                 | Syntax.App (_, _) -> None
                 | Syntax.Mu (_, c) ->
                   Some (DeBruijn.dropM_cmd
                          (Substitution.struct_subst_cmd c (Some Arith.Zero_nat)
                            None Syntax.ContextEmpty)
                          Arith.Zero_nat)
                 | Syntax.TAbs _ -> None | Syntax.TApp (_, _) -> None
                 | Syntax.Zero -> None | Syntax.S _ -> None
                 | Syntax.Nrec (_, _, _, _) -> None | Syntax.TTrue -> None
                 | Syntax.TFalse -> None | Syntax.If (_, _, _, _) -> None
                 | Syntax.Pair (_, _, _) -> None | Syntax.Proj1 _ -> None
                 | Syntax.Proj2 _ -> None | Syntax.Inl (_, _) -> None
                 | Syntax.Inr (_, _) -> None | Syntax.Case (_, _, _, _) -> None)
          else Option.bind (sem_trm t) (fun u -> Some (Syntax.Top u)))
and sem_trm
  = function
    Syntax.App (s, t) ->
      (if Reduction.is_nf s
        then (if Reduction.is_nf t
               then (match s with Syntax.LVar _ -> None
                      | Syntax.Lbd (_, r) ->
                        Some (Substitution.subst_trm r t Arith.Zero_nat)
                      | Syntax.App (_, _) -> None
                      | Syntax.Mu (Syntax.TVar _, _) -> None
                      | Syntax.Mu (Syntax.TAll _, _) -> None
                      | Syntax.Mu (Syntax.Nat, _) -> None
                      | Syntax.Mu (Syntax.Bool, _) -> None
                      | Syntax.Mu (Syntax.Bottom, _) -> None
                      | Syntax.Mu (Syntax.Prod (_, _), _) -> None
                      | Syntax.Mu (Syntax.Sum (_, _), _) -> None
                      | Syntax.Mu (Syntax.Fun (_, t2), c) ->
                        Some (Syntax.Mu
                               (t2, Substitution.struct_subst_cmd c
                                      (Some Arith.Zero_nat)
                                      (Some Arith.Zero_nat)
                                      (Syntax.ContextApp
(Syntax.ContextEmpty, DeBruijn.liftM_trm t Arith.Zero_nat))))
                      | Syntax.TAbs _ -> None | Syntax.TApp (_, _) -> None
                      | Syntax.Zero -> None | Syntax.S _ -> None
                      | Syntax.Nrec (_, _, _, _) -> None | Syntax.TTrue -> None
                      | Syntax.TFalse -> None | Syntax.If (_, _, _, _) -> None
                      | Syntax.Pair (_, _, _) -> None | Syntax.Proj1 _ -> None
                      | Syntax.Proj2 _ -> None | Syntax.Inl (_, _) -> None
                      | Syntax.Inr (_, _) -> None
                      | Syntax.Case (_, _, _, _) -> None)
               else Option.bind (sem_trm t) (fun u -> Some (Syntax.App (s, u))))
        else Option.bind (sem_trm s) (fun u -> Some (Syntax.App (u, t))))
    | Syntax.TApp (t, tau) ->
        (if Reduction.is_nf t
          then (match t with Syntax.LVar _ -> None | Syntax.Lbd (_, _) -> None
                 | Syntax.App (_, _) -> None
                 | Syntax.Mu (Syntax.TVar _, _) -> None
                 | Syntax.Mu (Syntax.TAll ta, c) ->
                   Some (Syntax.Mu
                          (DeBruijn.type_subst ta Arith.Zero_nat tau,
                            Substitution.struct_subst_cmd c
                              (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                              (Syntax.ContextTApp (Syntax.ContextEmpty, tau))))
                 | Syntax.Mu (Syntax.Nat, _) -> None
                 | Syntax.Mu (Syntax.Bool, _) -> None
                 | Syntax.Mu (Syntax.Bottom, _) -> None
                 | Syntax.Mu (Syntax.Prod (_, _), _) -> None
                 | Syntax.Mu (Syntax.Sum (_, _), _) -> None
                 | Syntax.Mu (Syntax.Fun (_, _), _) -> None
                 | Syntax.TAbs ta ->
                   Some (Substitution.subst_type_trm ta Arith.Zero_nat tau)
                 | Syntax.TApp (_, _) -> None | Syntax.Zero -> None
                 | Syntax.S _ -> None | Syntax.Nrec (_, _, _, _) -> None
                 | Syntax.TTrue -> None | Syntax.TFalse -> None
                 | Syntax.If (_, _, _, _) -> None
                 | Syntax.Pair (_, _, _) -> None | Syntax.Proj1 _ -> None
                 | Syntax.Proj2 _ -> None | Syntax.Inl (_, _) -> None
                 | Syntax.Inr (_, _) -> None | Syntax.Case (_, _, _, _) -> None)
          else Option.bind (sem_trm t) (fun u -> Some (Syntax.TApp (u, tau))))
    | Syntax.Mu (t, c) ->
        (match c
          with Syntax.MVar (Arith.Zero_nat, ta) ->
            (if Syntax.is_val ta &&
                  not (Set.member Arith.equal_nat Arith.Zero_nat
                        (DeBruijn.fmv_trm ta Arith.Zero_nat))
              then Some (DeBruijn.dropM_trm ta Arith.Zero_nat)
              else Option.bind (sem_cmd c) (fun d -> Some (Syntax.Mu (t, d))))
          | Syntax.MVar (Arith.Suc _, _) ->
            Option.bind (sem_cmd c) (fun d -> Some (Syntax.Mu (t, d)))
          | Syntax.Top _ ->
            Option.bind (sem_cmd c) (fun d -> Some (Syntax.Mu (t, d))))
    | Syntax.S t ->
        (match t
          with Syntax.LVar _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Mu (ta, c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (ta, Substitution.struct_subst_cmd c
                                 (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                 (Syntax.ContextSuc Syntax.ContextEmpty)))
              else Option.bind (sem_trm t) (fun u -> Some (Syntax.S u)))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Zero -> Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.S _ -> Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.TTrue -> Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.TFalse ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Pair (_, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.S u)))
    | Syntax.Nrec (ta, r, s, t) ->
        (if Reduction.is_nf r
          then (if Reduction.is_nf s
                 then (match t
                        with Syntax.LVar _ ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Lbd (_, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.App (_, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Mu (_, c) ->
                          (if Reduction.is_nfC c
                            then Some (Syntax.Mu
(ta, Substitution.struct_subst_cmd c (Some Arith.Zero_nat) (Some Arith.Zero_nat)
       (Syntax.ContextNrec
         (ta, DeBruijn.liftM_trm r Arith.Zero_nat,
           DeBruijn.liftM_trm s Arith.Zero_nat, Syntax.ContextEmpty))))
                            else Option.bind (sem_trm t)
                                   (fun u -> Some (Syntax.Nrec (ta, r, s, u))))
                        | Syntax.TAbs _ ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.TApp (_, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Zero -> Some r
                        | Syntax.S n ->
                          (if Syntax.is_natval t
                            then Some (Syntax.App
(Syntax.App (s, n), Syntax.Nrec (ta, r, s, n)))
                            else Option.bind (sem_trm t)
                                   (fun u -> Some (Syntax.Nrec (ta, r, s, u))))
                        | Syntax.Nrec (_, _, _, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.TTrue ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.TFalse ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.If (_, _, _, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Pair (_, _, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Proj1 _ ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Proj2 _ ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Inl (_, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Inr (_, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u)))
                        | Syntax.Case (_, _, _, _) ->
                          Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Nrec (ta, r, s, u))))
                 else Option.bind (sem_trm s)
                        (fun u -> Some (Syntax.Nrec (ta, r, u, t))))
          else Option.bind (sem_trm r)
                 (fun u -> Some (Syntax.Nrec (ta, u, s, t))))
    | Syntax.If (t, t1, t2, t3) ->
        (match t1
          with Syntax.LVar _ ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Mu (_, c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (t, Substitution.struct_subst_cmd c
                                (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                (Syntax.ContextIf
                                  (t, Syntax.ContextEmpty,
                                    DeBruijn.liftM_trm t2 Arith.Zero_nat,
                                    DeBruijn.liftM_trm t3 Arith.Zero_nat))))
              else Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.If (t, u, t2, t3))))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Zero ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.S _ ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.TTrue -> Some t2 | Syntax.TFalse -> Some t3
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Pair (_, _, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3)))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm t1) (fun u -> Some (Syntax.If (t, u, t2, t3))))
    | Syntax.Proj1 t ->
        (match t
          with Syntax.LVar _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.TVar _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.TAll _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.Nat, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.Bool, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.Bottom, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.Prod (t1, _), c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (t1, Substitution.struct_subst_cmd c
                                 (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                 (Syntax.ContextProj1 Syntax.ContextEmpty)))
              else Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u)))
          | Syntax.Mu (Syntax.Sum (_, _), _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Mu (Syntax.Fun (_, _), _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Zero ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.S _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.TTrue ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.TFalse ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Pair (t1, t2, _) ->
            (if Syntax.is_val t1 && Syntax.is_val t2 then Some t1
              else Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj1 u)))
    | Syntax.Proj2 t ->
        (match t
          with Syntax.LVar _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.TVar _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.TAll _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.Nat, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.Bool, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.Bottom, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.Prod (_, t2), c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (t2, Substitution.struct_subst_cmd c
                                 (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                 (Syntax.ContextProj2 Syntax.ContextEmpty)))
              else Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u)))
          | Syntax.Mu (Syntax.Sum (_, _), _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Mu (Syntax.Fun (_, _), _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Zero ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.S _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.TTrue ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.TFalse ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Pair (t1, t2, _) ->
            (if Syntax.is_val t1
              then (if Syntax.is_val t2 then Some t2
                     else Option.bind (sem_trm t)
                            (fun u -> Some (Syntax.Proj2 u)))
              else Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm t) (fun u -> Some (Syntax.Proj2 u)))
    | Syntax.Pair (t1, t2, t) ->
        (if Syntax.is_val t1
          then (match t2
                 with Syntax.LVar _ ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Lbd (_, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.App (_, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Mu (_, c) ->
                   (if Reduction.is_nfC c
                     then Some (Syntax.Mu
                                 (t, Substitution.struct_subst_cmd c
                                       (Some Arith.Zero_nat)
                                       (Some Arith.Zero_nat)
                                       (Syntax.ContextPairR
 (DeBruijn.liftM_trm t1 Arith.Zero_nat, Syntax.ContextEmpty, t))))
                     else Option.bind (sem_trm t2)
                            (fun u -> Some (Syntax.Pair (t1, u, t))))
                 | Syntax.TAbs _ ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.TApp (_, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Zero ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.S _ ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Nrec (_, _, _, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.TTrue ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.TFalse ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.If (_, _, _, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Pair (_, _, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Proj1 _ ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Proj2 _ ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Inl (_, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Inr (_, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t)))
                 | Syntax.Case (_, _, _, _) ->
                   Option.bind (sem_trm t2)
                     (fun u -> Some (Syntax.Pair (t1, u, t))))
          else (match t1
                 with Syntax.LVar _ ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Lbd (_, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.App (_, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Mu (_, c) ->
                   (if Reduction.is_nfC c
                     then Some (Syntax.Mu
                                 (t, Substitution.struct_subst_cmd c
                                       (Some Arith.Zero_nat)
                                       (Some Arith.Zero_nat)
                                       (Syntax.ContextPairL
 (Syntax.ContextEmpty, DeBruijn.liftM_trm t2 Arith.Zero_nat, t))))
                     else Option.bind (sem_trm t1)
                            (fun u -> Some (Syntax.Pair (u, t2, t))))
                 | Syntax.TAbs _ ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.TApp (_, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Zero ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.S _ ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Nrec (_, _, _, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.TTrue ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.TFalse ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.If (_, _, _, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Pair (_, _, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Proj1 _ ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Proj2 _ ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Inl (_, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Inr (_, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))
                 | Syntax.Case (_, _, _, _) ->
                   Option.bind (sem_trm t1)
                     (fun u -> Some (Syntax.Pair (u, t2, t)))))
    | Syntax.Inl (t, s) ->
        (match s
          with Syntax.LVar _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Mu (_, c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (t, Substitution.struct_subst_cmd c
                                (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                (Syntax.ContextInl (t, Syntax.ContextEmpty))))
              else Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u))))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Zero ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.S _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.TTrue ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.TFalse ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Pair (_, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u)))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inl (t, u))))
    | Syntax.Inr (t, s) ->
        (match s
          with Syntax.LVar _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Mu (_, c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (t, Substitution.struct_subst_cmd c
                                (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                (Syntax.ContextInr (t, Syntax.ContextEmpty))))
              else Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u))))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Zero ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.S _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.TTrue ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.TFalse ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Pair (_, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Inl (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Inr (_, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u)))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm s) (fun u -> Some (Syntax.Inr (t, u))))
    | Syntax.Case (u, t, t1, t2) ->
        (match t
          with Syntax.LVar _ ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Lbd (_, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.App (_, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Mu (_, c) ->
            (if Reduction.is_nfC c
              then Some (Syntax.Mu
                          (u, Substitution.struct_subst_cmd c
                                (Some Arith.Zero_nat) (Some Arith.Zero_nat)
                                (Syntax.ContextCase
                                  (u, Syntax.ContextEmpty,
                                    DeBruijn.liftM_trm t1 Arith.Zero_nat,
                                    DeBruijn.liftM_trm t2 Arith.Zero_nat))))
              else Option.bind (sem_trm t)
                     (fun ua -> Some (Syntax.Case (u, ua, t1, t2))))
          | Syntax.TAbs _ ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.TApp (_, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Zero ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.S _ ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Nrec (_, _, _, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.TTrue ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.TFalse ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.If (_, _, _, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Pair (_, _, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Proj1 _ ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Proj2 _ ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2)))
          | Syntax.Inl (_, s) ->
            (if Syntax.is_val s
              then Some (Substitution.subst_trm t1 s Arith.Zero_nat)
              else Option.bind (sem_trm t)
                     (fun ua -> Some (Syntax.Case (u, ua, t1, t2))))
          | Syntax.Inr (_, s) ->
            (if Syntax.is_val s
              then Some (Substitution.subst_trm t2 s Arith.Zero_nat)
              else Option.bind (sem_trm t)
                     (fun ua -> Some (Syntax.Case (u, ua, t1, t2))))
          | Syntax.Case (_, _, _, _) ->
            Option.bind (sem_trm t)
              (fun ua -> Some (Syntax.Case (u, ua, t1, t2))))
    | Syntax.LVar v -> None
    | Syntax.Lbd (v, va) -> None
    | Syntax.TAbs v -> None
    | Syntax.Zero -> None
    | Syntax.TTrue -> None
    | Syntax.TFalse -> None;;

end;; (*struct MuML*)

module String : sig
  type char = Zero_char | Char of Arith.num
end = struct

type char = Zero_char | Char of Arith.num;;

end;; (*struct String*)

module Predicate : sig
  type 'a seq = Empty | Insert of 'a * 'a pred | Join of 'a pred * 'a seq
  and 'a pred = Seq of (unit -> 'a seq)
  val is_empty : 'a pred -> bool
  val null : 'a seq -> bool
  val singleton : 'a HOL.equal -> (unit -> 'a) -> 'a pred -> 'a
  val the_only : 'a HOL.equal -> (unit -> 'a) -> 'a seq -> 'a
  val the : 'a HOL.equal -> 'a pred -> 'a
  val bind : 'a pred -> ('a -> 'b pred) -> 'b pred
  val apply : ('a -> 'b pred) -> 'a seq -> 'b seq
  val bot_pred : 'a pred
  val single : 'a -> 'a pred
  val adjunct : 'a pred -> 'a seq -> 'a seq
  val sup_pred : 'a pred -> 'a pred -> 'a pred
end = struct

type 'a seq = Empty | Insert of 'a * 'a pred | Join of 'a pred * 'a seq
and 'a pred = Seq of (unit -> 'a seq);;

let rec is_empty (Seq f) = null (f ())
and null = function Empty -> true
           | Insert (x, p) -> false
           | Join (p, xq) -> is_empty p && null xq;;

let rec singleton _A
  default (Seq f) =
    (match f () with Empty -> default ()
      | Insert (x, p) ->
        (if is_empty p then x
          else (let y = singleton _A default p in
                 (if HOL.eq _A x y then x else default ())))
      | Join (p, xq) ->
        (if is_empty p then the_only _A default xq
          else (if null xq then singleton _A default p
                 else (let x = singleton _A default p in
                       let y = the_only _A default xq in
                        (if HOL.eq _A x y then x else default ())))))
and the_only _A
  default x1 = match default, x1 with
    default, Insert (x, p) ->
      (if is_empty p then x
        else (let y = singleton _A default p in
               (if HOL.eq _A x y then x else default ())))
    | default, Join (p, xq) ->
        (if is_empty p then the_only _A default xq
          else (if null xq then singleton _A default p
                 else (let x = singleton _A default p in
                       let y = the_only _A default xq in
                        (if HOL.eq _A x y then x else default ()))));;

let rec the _A
  a = singleton _A (fun _ -> failwith "not_unique" (fun _ -> the _A a)) a;;

let rec bind (Seq g) f = Seq (fun _ -> apply f (g ()))
and apply f x1 = match f, x1 with f, Empty -> Empty
            | f, Insert (x, p) -> Join (f x, Join (bind p f, Empty))
            | f, Join (p, xq) -> Join (bind p f, apply f xq);;

let bot_pred : 'a pred = Seq (fun _ -> Empty);;

let rec single x = Seq (fun _ -> Insert (x, bot_pred));;

let rec adjunct p x1 = match p, x1 with p, Empty -> Join (p, Empty)
                  | p, Insert (x, q) -> Insert (x, sup_pred q p)
                  | p, Join (q, xq) -> Join (q, adjunct p xq)
and sup_pred
  (Seq f) (Seq g) =
    Seq (fun _ ->
          (match f () with Empty -> g ()
            | Insert (x, p) -> Insert (x, sup_pred p (Seq g))
            | Join (p, xq) -> adjunct (Seq g) (Join (p, xq))));;

end;; (*struct Predicate*)

module Types : sig
  val shift : (Arith.nat -> 'a) -> Arith.nat -> 'a -> Arith.nat -> 'a
  val eq_i_i : 'a HOL.equal -> 'a -> 'a -> unit Predicate.pred
  val eq_i_o : 'a -> 'a Predicate.pred
  val env_type_shift :
    (Arith.nat -> Syntax.typea) -> Arith.nat -> Arith.nat -> Syntax.typea
  val typing_trm_i_i_i_o :
    (Arith.nat -> Syntax.typea) ->
      (Arith.nat -> Syntax.typea) -> Syntax.trm -> Syntax.typea Predicate.pred
  val typing_cmd_i_i_i :
    (Arith.nat -> Syntax.typea) ->
      (Arith.nat -> Syntax.typea) -> Syntax.cmd -> unit Predicate.pred
  val type_term :
    (Arith.nat -> Syntax.typea) ->
      (Arith.nat -> Syntax.typea) -> Syntax.trm -> Syntax.typea
end = struct

let rec shift
  e i a =
    (fun j ->
      (if Arith.less_nat j i then e j
        else (if Arith.equal_nata j i then a
               else e (Arith.minus_nat j Arith.one_nat))));;

let rec eq_i_i _A
  xa xb =
    Predicate.bind (Predicate.single (xa, xb))
      (fun (x, xaa) ->
        (if HOL.eq _A x xaa then Predicate.single () else Predicate.bot_pred));;

let rec eq_i_o xa = Predicate.bind (Predicate.single xa) Predicate.single;;

let rec env_type_shift gamma k x = DeBruijn.liftT_type (gamma x) k;;

let rec typing_trm_i_i_i_o
  xa xb xc =
    Predicate.sup_pred
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a
            with (gamma, (_, Syntax.LVar x)) ->
              Predicate.bind (eq_i_o (gamma x)) Predicate.single
            | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
            | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
            | (_, (_, Syntax.S _)) -> Predicate.bot_pred
            | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
            | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
            | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
            | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
            | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
            | (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
      (Predicate.sup_pred
        (Predicate.bind (Predicate.single (xa, (xb, xc)))
          (fun a ->
            (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
              | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
              | (gamma, (delta, Syntax.App (t, s))) ->
                Predicate.bind (typing_trm_i_i_i_o gamma delta t)
                  (fun aa ->
                    (match aa with Syntax.TVar _ -> Predicate.bot_pred
                      | Syntax.TAll _ -> Predicate.bot_pred
                      | Syntax.Nat -> Predicate.bot_pred
                      | Syntax.Bool -> Predicate.bot_pred
                      | Syntax.Bottom -> Predicate.bot_pred
                      | Syntax.Prod (_, _) -> Predicate.bot_pred
                      | Syntax.Sum (_, _) -> Predicate.bot_pred
                      | Syntax.Fun (t1, t2) ->
                        Predicate.bind (typing_trm_i_i_i_o gamma delta s)
                          (fun x ->
                            (if Syntax.equal_typea t1 x then Predicate.single t2
                              else Predicate.bot_pred))))
              | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
              | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
              | (_, (_, Syntax.S _)) -> Predicate.bot_pred
              | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
              | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
              | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
              | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
              | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
              | (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
        (Predicate.sup_pred
          (Predicate.bind (Predicate.single (xa, (xb, xc)))
            (fun a ->
              (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                | (gamma, (delta, Syntax.Lbd (t1, t))) ->
                  Predicate.bind
                    (typing_trm_i_i_i_o (shift gamma Arith.Zero_nat t1) delta t)
                    (fun x -> Predicate.single (Syntax.Fun (t1, x)))
                | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                | (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
          (Predicate.sup_pred
            (Predicate.bind (Predicate.single (xa, (xb, xc)))
              (fun a ->
                (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                  | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                  | (gamma, (delta, Syntax.Mu (t, c))) ->
                    Predicate.bind
                      (typing_cmd_i_i_i gamma (shift delta Arith.Zero_nat t) c)
                      (fun () -> Predicate.single t)
                  | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                  | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                  | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                  | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                  | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                  | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                  | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                  | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                  | (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
            (Predicate.sup_pred
              (Predicate.bind (Predicate.single (xa, (xb, xc)))
                (fun a ->
                  (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                    | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                    | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Zero)) -> Predicate.single Syntax.Nat
                    | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                    | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                    | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                    | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                    | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                    | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                    | (_, (_, Syntax.Case (_, _, _, _))) ->
                      Predicate.bot_pred)))
              (Predicate.sup_pred
                (Predicate.bind (Predicate.single (xa, (xb, xc)))
                  (fun a ->
                    (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                      | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                      | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                      | (gamma, (delta, Syntax.S t)) ->
                        Predicate.bind (typing_trm_i_i_i_o gamma delta t)
                          (fun aa ->
                            (match aa with Syntax.TVar _ -> Predicate.bot_pred
                              | Syntax.TAll _ -> Predicate.bot_pred
                              | Syntax.Nat -> Predicate.single Syntax.Nat
                              | Syntax.Bool -> Predicate.bot_pred
                              | Syntax.Bottom -> Predicate.bot_pred
                              | Syntax.Prod (_, _) -> Predicate.bot_pred
                              | Syntax.Sum (_, _) -> Predicate.bot_pred
                              | Syntax.Fun (_, _) -> Predicate.bot_pred))
                      | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                      | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                      | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                      | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                      | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                      | (_, (_, Syntax.Case (_, _, _, _))) ->
                        Predicate.bot_pred)))
                (Predicate.sup_pred
                  (Predicate.bind (Predicate.single (xa, (xb, xc)))
                    (fun a ->
                      (match a
                        with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                        | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                        | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                        | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                        | (gamma, (delta, Syntax.Nrec (t, r, s, ta))) ->
                          Predicate.bind (typing_trm_i_i_i_o gamma delta r)
                            (fun x ->
                              (if Syntax.equal_typea t x
                                then Predicate.bind
                                       (typing_trm_i_i_i_o gamma delta s)
                                       (fun aa ->
 (match aa with Syntax.TVar _ -> Predicate.bot_pred
   | Syntax.TAll _ -> Predicate.bot_pred | Syntax.Nat -> Predicate.bot_pred
   | Syntax.Bool -> Predicate.bot_pred | Syntax.Bottom -> Predicate.bot_pred
   | Syntax.Prod (_, _) -> Predicate.bot_pred
   | Syntax.Sum (_, _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.TVar _, _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.TAll _, _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.TVar _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.TAll _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Nat) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Bool) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Bottom) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Prod (_, _)) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Sum (_, _)) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Nat, Syntax.Fun (tb, tc)) ->
     (if Syntax.equal_typea t tc && Syntax.equal_typea t tb
       then Predicate.bind (typing_trm_i_i_i_o gamma delta ta)
              (fun ab ->
                (match ab with Syntax.TVar _ -> Predicate.bot_pred
                  | Syntax.TAll _ -> Predicate.bot_pred
                  | Syntax.Nat -> Predicate.single t
                  | Syntax.Bool -> Predicate.bot_pred
                  | Syntax.Bottom -> Predicate.bot_pred
                  | Syntax.Prod (_, _) -> Predicate.bot_pred
                  | Syntax.Sum (_, _) -> Predicate.bot_pred
                  | Syntax.Fun (_, _) -> Predicate.bot_pred))
       else Predicate.bot_pred)
   | Syntax.Fun (Syntax.Bool, _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Bottom, _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Prod (_, _), _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Sum (_, _), _) -> Predicate.bot_pred
   | Syntax.Fun (Syntax.Fun (_, _), _) -> Predicate.bot_pred))
                                else Predicate.bot_pred))
                        | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                        | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                        | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                        | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                        | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                        | (_, (_, Syntax.Case (_, _, _, _))) ->
                          Predicate.bot_pred)))
                  (Predicate.sup_pred
                    (Predicate.bind (Predicate.single (xa, (xb, xc)))
                      (fun a ->
                        (match a
                          with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                          | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                          | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                          | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                          | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                            Predicate.bot_pred
                          | (_, (_, Syntax.TTrue)) ->
                            Predicate.single Syntax.Bool
                          | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                          | (_, (_, Syntax.If (_, _, _, _))) ->
                            Predicate.bot_pred
                          | (_, (_, Syntax.Pair (_, _, _))) ->
                            Predicate.bot_pred
                          | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                          | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                          | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                          | (_, (_, Syntax.Case (_, _, _, _))) ->
                            Predicate.bot_pred)))
                    (Predicate.sup_pred
                      (Predicate.bind (Predicate.single (xa, (xb, xc)))
                        (fun a ->
                          (match a
                            with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                            | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                            | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                            | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                            | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                              Predicate.bot_pred
                            | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                            | (_, (_, Syntax.TFalse)) ->
                              Predicate.single Syntax.Bool
                            | (_, (_, Syntax.If (_, _, _, _))) ->
                              Predicate.bot_pred
                            | (_, (_, Syntax.Pair (_, _, _))) ->
                              Predicate.bot_pred
                            | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                            | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                            | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
                            | (_, (_, Syntax.Case (_, _, _, _))) ->
                              Predicate.bot_pred)))
                      (Predicate.sup_pred
                        (Predicate.bind (Predicate.single (xa, (xb, xc)))
                          (fun a ->
                            (match a
                              with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
                              | (_, (_, Syntax.Lbd (_, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.App (_, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
                              | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                              | (_, (_, Syntax.TApp (_, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                              | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                              | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                              | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                              | (gamma, (delta, Syntax.If (t, t1, t2, t3))) ->
                                Predicate.bind
                                  (typing_trm_i_i_i_o gamma delta t1)
                                  (fun aa ->
                                    (match aa
                                      with Syntax.TVar _ -> Predicate.bot_pred
                                      | Syntax.TAll _ -> Predicate.bot_pred
                                      | Syntax.Nat -> Predicate.bot_pred
                                      | Syntax.Bool ->
Predicate.bind (typing_trm_i_i_i_o gamma delta t2)
  (fun x ->
    (if Syntax.equal_typea t x
      then Predicate.bind (typing_trm_i_i_i_o gamma delta t3)
             (fun xd ->
               (if Syntax.equal_typea t xd then Predicate.single t
                 else Predicate.bot_pred))
      else Predicate.bot_pred))
                                      | Syntax.Bottom -> Predicate.bot_pred
                                      | Syntax.Prod (_, _) -> Predicate.bot_pred
                                      | Syntax.Sum (_, _) -> Predicate.bot_pred
                                      | Syntax.Fun (_, _) ->
Predicate.bot_pred))
                              | (_, (_, Syntax.Pair (_, _, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                              | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                              | (_, (_, Syntax.Inl (_, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.Inr (_, _))) ->
                                Predicate.bot_pred
                              | (_, (_, Syntax.Case (_, _, _, _))) ->
                                Predicate.bot_pred)))
                        (Predicate.sup_pred
                          (Predicate.bind (Predicate.single (xa, (xb, xc)))
                            (fun a ->
                              (match a
                                with (_, (_, Syntax.LVar _)) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Lbd (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.App (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Mu (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
                                | (_, (_, Syntax.TApp (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                                | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                                | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                                | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
                                | (_, (_, Syntax.If (_, _, _, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Pair (_, _, Syntax.TVar _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Pair (_, _, Syntax.TAll _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Pair (_, _, Syntax.Nat))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Pair (_, _, Syntax.Bool))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Pair (_, _, Syntax.Bottom))) ->
                                  Predicate.bot_pred
                                | (gamma,
                                    (delta,
                                      Syntax.Pair
(t1, t2, Syntax.Prod (t1a, t2a))))
                                  -> Predicate.bind
                                       (typing_trm_i_i_i_o gamma delta t1)
                                       (fun x ->
 (if Syntax.equal_typea t1a x
   then Predicate.bind (typing_trm_i_i_i_o gamma delta t2)
          (fun xd ->
            (if Syntax.equal_typea t2a xd
              then Predicate.single (Syntax.Prod (t1a, t2a))
              else Predicate.bot_pred))
   else Predicate.bot_pred))
                                | (_, (_,
Syntax.Pair (_, _, Syntax.Sum (_, _))))
                                  -> Predicate.bot_pred
                                | (_, (_,
Syntax.Pair (_, _, Syntax.Fun (_, _))))
                                  -> Predicate.bot_pred
                                | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
                                | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
                                | (_, (_, Syntax.Inl (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Inr (_, _))) ->
                                  Predicate.bot_pred
                                | (_, (_, Syntax.Case (_, _, _, _))) ->
                                  Predicate.bot_pred)))
                          (Predicate.sup_pred
                            (Predicate.bind (Predicate.single (xa, (xb, xc)))
                              (fun a ->
                                (match a
                                  with (_, (_, Syntax.LVar _)) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Lbd (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.App (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Mu (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.TAbs _)) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.TApp (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
                                  | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                                  | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
                                  | (_, (_, Syntax.TFalse)) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.If (_, _, _, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Pair (_, _, _))) ->
                                    Predicate.bot_pred
                                  | (gamma, (delta, Syntax.Proj1 t)) ->
                                    Predicate.bind
                                      (typing_trm_i_i_i_o gamma delta t)
                                      (fun aa ->
(match aa with Syntax.TVar _ -> Predicate.bot_pred
  | Syntax.TAll _ -> Predicate.bot_pred | Syntax.Nat -> Predicate.bot_pred
  | Syntax.Bool -> Predicate.bot_pred | Syntax.Bottom -> Predicate.bot_pred
  | Syntax.Prod (t1, _) -> Predicate.single t1
  | Syntax.Sum (_, _) -> Predicate.bot_pred
  | Syntax.Fun (_, _) -> Predicate.bot_pred))
                                  | (_, (_, Syntax.Proj2 _)) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Inl (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Inr (_, _))) ->
                                    Predicate.bot_pred
                                  | (_, (_, Syntax.Case (_, _, _, _))) ->
                                    Predicate.bot_pred)))
                            (Predicate.sup_pred
                              (Predicate.bind (Predicate.single (xa, (xb, xc)))
                                (fun a ->
                                  (match a
                                    with (_, (_, Syntax.LVar _)) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Lbd (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.App (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Mu (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.TAbs _)) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.TApp (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Zero)) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.S _)) -> Predicate.bot_pred
                                    | (_, (_, Syntax.Nrec (_, _, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.TTrue)) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.TFalse)) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.If (_, _, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Pair (_, _, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Proj1 _)) ->
                                      Predicate.bot_pred
                                    | (gamma, (delta, Syntax.Proj2 t)) ->
                                      Predicate.bind
(typing_trm_i_i_i_o gamma delta t)
(fun aa ->
  (match aa with Syntax.TVar _ -> Predicate.bot_pred
    | Syntax.TAll _ -> Predicate.bot_pred | Syntax.Nat -> Predicate.bot_pred
    | Syntax.Bool -> Predicate.bot_pred | Syntax.Bottom -> Predicate.bot_pred
    | Syntax.Prod (_, ab) -> Predicate.single ab
    | Syntax.Sum (_, _) -> Predicate.bot_pred
    | Syntax.Fun (_, _) -> Predicate.bot_pred))
                                    | (_, (_, Syntax.Inl (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Inr (_, _))) ->
                                      Predicate.bot_pred
                                    | (_, (_, Syntax.Case (_, _, _, _))) ->
                                      Predicate.bot_pred)))
                              (Predicate.sup_pred
                                (Predicate.bind
                                  (Predicate.single (xa, (xb, xc)))
                                  (fun a ->
                                    (match a
                                      with (_, (_, Syntax.LVar _)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Lbd (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.App (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Mu (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.TAbs _)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.TApp (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Zero)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.S _)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Nrec (_, _, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.TTrue)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.TFalse)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.If (_, _, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Pair (_, _, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Proj1 _)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Proj2 _)) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Inl (Syntax.TVar _, _)))
-> Predicate.bot_pred
                                      | (_, (_, Syntax.Inl (Syntax.TAll _, _)))
-> Predicate.bot_pred
                                      | (_, (_, Syntax.Inl (Syntax.Nat, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Inl (Syntax.Bool, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Inl (Syntax.Bottom, _)))
-> Predicate.bot_pred
                                      |
(_, (_, Syntax.Inl (Syntax.Prod (_, _), _))) -> Predicate.bot_pred
                                      |
(gamma, (delta, Syntax.Inl (Syntax.Sum (t1, t2), t))) ->
Predicate.bind (typing_trm_i_i_i_o gamma delta t)
  (fun x ->
    (if Syntax.equal_typea t1 x then Predicate.single (Syntax.Sum (t1, t2))
      else Predicate.bot_pred))
                                      |
(_, (_, Syntax.Inl (Syntax.Fun (_, _), _))) -> Predicate.bot_pred
                                      | (_, (_, Syntax.Inr (_, _))) ->
Predicate.bot_pred
                                      | (_, (_, Syntax.Case (_, _, _, _))) ->
Predicate.bot_pred)))
                                (Predicate.sup_pred
                                  (Predicate.bind
                                    (Predicate.single (xa, (xb, xc)))
                                    (fun a ->
                                      (match a
with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
| (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
| (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
| (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
| (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Zero)) -> Predicate.bot_pred
| (_, (_, Syntax.S _)) -> Predicate.bot_pred
| (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
| (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
| (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
| (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
| (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.TVar _, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.TAll _, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.Nat, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.Bool, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.Bottom, _))) -> Predicate.bot_pred
| (_, (_, Syntax.Inr (Syntax.Prod (_, _), _))) -> Predicate.bot_pred
| (gamma, (delta, Syntax.Inr (Syntax.Sum (t1, t2), t))) ->
  Predicate.bind (typing_trm_i_i_i_o gamma delta t)
    (fun x ->
      (if Syntax.equal_typea t2 x then Predicate.single (Syntax.Sum (t1, t2))
        else Predicate.bot_pred))
| (_, (_, Syntax.Inr (Syntax.Fun (_, _), _))) -> Predicate.bot_pred
| (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
                                  (Predicate.sup_pred
                                    (Predicate.bind
                                      (Predicate.single (xa, (xb, xc)))
                                      (fun a ->
(match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
  | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
  | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
  | (_, (_, Syntax.S _)) -> Predicate.bot_pred
  | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
  | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
  | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
  | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
  | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
  | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
  | (gamma, (delta, Syntax.Case (t, t0, t1, t2))) ->
    Predicate.bind (typing_trm_i_i_i_o gamma delta t0)
      (fun aa ->
        (match aa with Syntax.TVar _ -> Predicate.bot_pred
          | Syntax.TAll _ -> Predicate.bot_pred
          | Syntax.Nat -> Predicate.bot_pred | Syntax.Bool -> Predicate.bot_pred
          | Syntax.Bottom -> Predicate.bot_pred
          | Syntax.Prod (_, _) -> Predicate.bot_pred
          | Syntax.Sum (t1a, t2a) ->
            Predicate.bind
              (typing_trm_i_i_i_o (shift gamma Arith.Zero_nat t1a) delta t1)
              (fun x ->
                (if Syntax.equal_typea t x
                  then Predicate.bind
                         (typing_trm_i_i_i_o (shift gamma Arith.Zero_nat t2a)
                           delta t2)
                         (fun xd ->
                           (if Syntax.equal_typea t xd then Predicate.single t
                             else Predicate.bot_pred))
                  else Predicate.bot_pred))
          | Syntax.Fun (_, _) -> Predicate.bot_pred)))))
                                    (Predicate.sup_pred
                                      (Predicate.bind
(Predicate.single (xa, (xb, xc)))
(fun a ->
  (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
    | (gamma, (delta, Syntax.TAbs t)) ->
      Predicate.bind
        (typing_trm_i_i_i_o (env_type_shift gamma Arith.Zero_nat)
          (env_type_shift delta Arith.Zero_nat) t)
        (fun x -> Predicate.single (Syntax.TAll x))
    | (_, (_, Syntax.TApp (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
    | (_, (_, Syntax.S _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
    | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
    | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Case (_, _, _, _))) -> Predicate.bot_pred)))
                                      (Predicate.bind
(Predicate.single (xa, (xb, xc)))
(fun a ->
  (match a with (_, (_, Syntax.LVar _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Lbd (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.App (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Mu (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.TAbs _)) -> Predicate.bot_pred
    | (gamma, (delta, Syntax.TApp (t, tau))) ->
      Predicate.bind (typing_trm_i_i_i_o gamma delta t)
        (fun aa ->
          (match aa with Syntax.TVar _ -> Predicate.bot_pred
            | Syntax.TAll ta ->
              Predicate.single (DeBruijn.type_subst ta Arith.Zero_nat tau)
            | Syntax.Nat -> Predicate.bot_pred
            | Syntax.Bool -> Predicate.bot_pred
            | Syntax.Bottom -> Predicate.bot_pred
            | Syntax.Prod (_, _) -> Predicate.bot_pred
            | Syntax.Sum (_, _) -> Predicate.bot_pred
            | Syntax.Fun (_, _) -> Predicate.bot_pred))
    | (_, (_, Syntax.Zero)) -> Predicate.bot_pred
    | (_, (_, Syntax.S _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Nrec (_, _, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.TTrue)) -> Predicate.bot_pred
    | (_, (_, Syntax.TFalse)) -> Predicate.bot_pred
    | (_, (_, Syntax.If (_, _, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Pair (_, _, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Proj1 _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Proj2 _)) -> Predicate.bot_pred
    | (_, (_, Syntax.Inl (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Inr (_, _))) -> Predicate.bot_pred
    | (_, (_, Syntax.Case (_, _, _, _))) ->
      Predicate.bot_pred)))))))))))))))))))
and typing_cmd_i_i_i
  xa xb xc =
    Predicate.sup_pred
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a
            with (gamma, (delta, Syntax.MVar (x, t))) ->
              Predicate.bind (typing_trm_i_i_i_o gamma delta t)
                (fun xaa ->
                  Predicate.bind (eq_i_i Syntax.equal_type (delta x) xaa)
                    (fun () -> Predicate.single ()))
            | (_, (_, Syntax.Top _)) -> Predicate.bot_pred)))
      (Predicate.bind (Predicate.single (xa, (xb, xc)))
        (fun a ->
          (match a with (_, (_, Syntax.MVar (_, _))) -> Predicate.bot_pred
            | (gamma, (delta, Syntax.Top t)) ->
              Predicate.bind (typing_trm_i_i_i_o gamma delta t)
                (fun aa ->
                  (match aa with Syntax.TVar _ -> Predicate.bot_pred
                    | Syntax.TAll _ -> Predicate.bot_pred
                    | Syntax.Nat -> Predicate.bot_pred
                    | Syntax.Bool -> Predicate.bot_pred
                    | Syntax.Bottom -> Predicate.single ()
                    | Syntax.Prod (_, _) -> Predicate.bot_pred
                    | Syntax.Sum (_, _) -> Predicate.bot_pred
                    | Syntax.Fun (_, _) -> Predicate.bot_pred)))));;

let rec type_term
  lenv menv t =
    Predicate.the Syntax.equal_type (typing_trm_i_i_i_o lenv menv t);;

end;; (*struct Types*)
