(* AST produced by parser *)

open Printf

exception MalformedAST of string

type type_expr = 
     |TNat

     |TBot
     |TArrow of type_expr * type_expr

     |TBool

     |TProd of type_expr*type_expr

     |TSum of type_expr*type_expr

     |TVar of string
     |TForall of string*type_expr
     |DbTVar of string*int

(* DbVar and DbCommand are used when the conversion to de Bruijn indices is made *)
type expr = 
     |Nat of int
     |Var of string
     |Suc of expr
     |App of expr * expr
     |Fun of string * type_expr * expr
     |Nrec of type_expr * expr * expr * expr
     |Mu of string * type_expr * expr
     |Command of string * expr
     |Abort of expr
     |DbVar of string*int
     |DbCommand of string*int*expr

     |True
     |False
     |If of type_expr*expr*expr*expr

     |Proj1 of expr
     |Proj2 of expr     
     |Pair of expr*expr*type_expr

     |Inl of type_expr*expr
     |Inr of type_expr*expr
     |Case of type_expr*expr*string*expr*string*expr

     |TAbs of string*expr
     |TApp of expr*type_expr

(*Pretty printing for this AST*)


let rec print_type t = match t with
     |TNat -> "nat"

     |TBot -> "bot"
     |TArrow (t1, t2) -> sprintf "(%s->%s)" (print_type t1) (print_type t2)

     |TBool -> "bool"

     |TProd (t1, t2) -> sprintf "%s*%s" (print_type t1) (print_type t2)

     |TSum (t1, t2) -> sprintf "%s+%s" (print_type t1) (print_type t2)

     |TVar v -> v
     |DbTVar (v, n) -> sprintf "(%s, %s)" v (string_of_int n)
     |TForall (v, t) -> sprintf "forall(%s)(%s)" v (print_type t)

let rec print_expr expr = match expr with
     |Nat n -> string_of_int n
     |Var v -> v
     |Suc e -> sprintf "suc (%s)" (print_expr e)
     |App (e1, e2) -> sprintf "(%s %s)" (print_expr e1) (print_expr e2)
     |Fun (x, t, e) -> sprintf "fun(%s:%s) -> %s end" x (print_type t) (print_expr e)
     |Nrec (t, e1, e2, e3) -> sprintf "nrec:(%s) -> (%s, %s, %s) end" 
          (print_type t) (print_expr e1) (print_expr e2) (print_expr e3)
     |Mu (x, t, e) -> sprintf "bind(%s:%s) -> %s end" x (print_type t) (print_expr e)
     |Command (x, e) -> sprintf "[%s].%s" x (print_expr e)
     |Abort e -> sprintf "[abort].%s" (print_expr e)
     |DbVar (x, n) -> sprintf "(%s, %s)" x (string_of_int n)
     |DbCommand (x, n, e) -> sprintf "[(%s, %s)].%s" x (string_of_int n) (print_expr e)

     |True -> "true"
     |False -> "false"
     |If (t, e1, e2, e3) -> sprintf "if:(%s) %s then %s else %s end" 
          (print_type t) (print_expr e1) (print_expr e2) (print_expr e3)

     |Proj1 e -> sprintf "proj1(%s)"  (print_expr e)
     |Proj2 e -> sprintf "proj2(%s)"  (print_expr e)
     |Pair (e1, e2, t) -> sprintf "{%s, %s}:(%s)" (print_expr e1) (print_expr e2) (print_type t) 

     |Inl (t, e) -> sprintf "inl:(%s)(%s)" (print_type t) (print_expr e)
     |Inr (t, e) -> sprintf "inr:(%s)(%s)" (print_type t) (print_expr e)
     |Case (t, e1, x, e2, y, e3) -> sprintf "case:(%s) %s of inl %s -> %s|inr %s -> %s end"
          (print_type t) (print_expr e1) x (print_expr e2) y (print_expr e3)

     |TAbs (v, e) -> sprintf "tabs(%s)-> %s end" v (print_expr e)
     |TApp (e, t) -> sprintf "(%s %s)" (print_expr e) (print_type t)
