open OUnit2
open Testing
open Parsedast


let translation_tests = "tests for de Bruin indices and translation">:::[
     "application" >:: (fun _ -> assert_equal 
                            "(App (App (LVar 120) (LVar 121)) (LVar 122))"
                            (test_translate_ounit (App ((App ((Var "x"), (Var "y"))), (Var "z"))) ));
     "fun1" >:: (fun _ -> assert_equal 
                            "(Lbd Nat->Nat->Bottom->Nat (LVar 0))"
                            (test_translate_ounit (Fun ("x", (TArrow (TNat, (TArrow (TNat, (TArrow (TBot, TNat)))))), (Var "x"))) ));
     "fun2" >:: (fun _ -> assert_equal 
                            "(Lbd Nat (App (LVar 121) (LVar 122)))"
                            (test_translate_ounit (Fun ("z", TNat, (App ((Var "x"), (Var "y"))))) ));
     "bind1" >:: (fun _ -> assert_equal 
                            "(Mu Bottom (MVar 0 (App (LVar 120) (LVar 121))))"
                            (test_translate_ounit (Mu ("a", TBot, (Command ("a", (App ((Var "x"), (Var "y"))))))) ));
     "nrec1" >:: (fun _ -> assert_equal 
                            "(Nrec Nat (LVar 120) (App (LVar 121) (LVar 122)) (LVar 119))"
                            (test_translate_ounit (Nrec (TNat, (Var "x"), (App ((Var "y"), (Var "z"))), (Var "w"))) ));
     "fun3" >:: (fun _ -> assert_equal 
                            "(Lbd Nat (Lbd Nat (App (App (LVar 1) (LVar 0)) (LVar 124))))"
                            (test_translate_ounit (Fun ("x", TNat, (Fun ("y", TNat, (App ((App ((Var "x"), (Var "y"))), (Var "z"))))))) ));
     "bind2" >:: (fun _ -> assert_equal 
                            "(Mu Bottom (MVar 99 (Mu Bottom (MVar 1 (Mu Bottom (MVar 1 (LVar 120)))))))"
                            (test_translate_ounit (Mu ("a", TBot, (Command ("b", (Mu ("b", TBot, (Command ("a", (Mu ("c", TBot, (Command ("b", (Var "x"))))))))))))) ));
     "fun4" >:: (fun _ -> assert_equal 
                            "(Lbd Nat (Lbd Nat (App (Lbd Nat (S (LVar 0))) (LVar 1))))"
                            (test_translate_ounit (Fun ("x", TNat, (Fun ("y", TNat, (App ((Fun ("x", TNat, (Suc (Var "x")))), (Var "x"))))))) ));



     "suc1" >:: (fun _ -> assert_equal 
                            "(S (Lbd Nat (LVar 0)))"
                            (test_translate_ounit (Suc (Fun("x", TNat, (Var "x")))) ));
     "suc2" >:: (fun _ -> assert_equal 
                            "(S (S (S (S Zero))))"
                            (test_translate_ounit (Suc (Nat 3)) ));


     "abort1" >:: (fun _ -> assert_equal 
                            "(Mu Nat (Top (S (S (S Zero)))))"
                            (test_translate_ounit (Mu ("a", TNat, (Abort (Nat 3)))) ));
     "abort2" >:: (fun _ -> assert_equal 
                            "(Mu Nat (Top (Mu Bottom (MVar 1 (S (S (S Zero)))))))"
                            (test_translate_ounit (Mu ("a", TNat, (Abort (Mu ("b", TBot, (Command ("a", (Nat 3)))))))) ));



     "true" >:: (fun _ -> assert_equal 
                            "True"
                            (test_translate_ounit (True) ));
     "false" >:: (fun _ -> assert_equal 
                            "False"
                            (test_translate_ounit (False) ));
     "if1" >:: (fun _ -> assert_equal 
                            "(If Nat True (LVar 120) (LVar 121))"
                            (test_translate_ounit (If (TNat, True, (Var "x"), (Var "y"))) ));
     "if2" >:: (fun _ -> assert_equal 
                            "(If Nat->Nat (App (Lbd Nat False) (S Zero)) (Lbd Nat (S (LVar 0))) True)"
                            (test_translate_ounit (If ((TArrow (TNat, TNat)), (App ((Fun ("x", TNat, False)), (Nat 1))), (Fun ("x", TNat, (Suc (Var "x")))), True)) ));



     "proj1" >:: (fun _ -> assert_equal 
                            "(Proj1 (LVar 120))"
                            (test_translate_ounit (Proj1 (Var "x")) ));
     "proj2" >:: (fun _ -> assert_equal 
                            "(Proj2 (LVar 121))"
                            (test_translate_ounit (Proj2 (Var "y")) ));
     "pair" >:: (fun _ -> assert_equal 
                            "(|(Lbd Nat (LVar 0)), (Lbd Bottom (LVar 121))|):Nat*Bottom"
                            (test_translate_ounit (Pair ((Fun ("x", TNat, (Var "x"))), (Fun ("y", TBot, (Var "x"))), (TProd (TNat, TBot)))) ));
     "proj1 and pair" >:: (fun _ -> assert_equal 
                            "(Proj1 (|(LVar 120), (LVar 121)|):Nat*Bottom)"
                            (test_translate_ounit (Proj1 (Pair ((Var "x"), (Var "y"), (TProd (TNat, TBot))))) )); 



     "inl1" >:: (fun _ -> assert_equal 
                            "(Inl Nat+Bottom (LVar 120))"
                            (test_translate_ounit (Inl ((TSum (TNat, TBot)), (Var "x"))) ));
     "inr1" >:: (fun _ -> assert_equal 
                            "(Inr Nat+Bottom (LVar 120))"
                            (test_translate_ounit (Inr ((TSum (TNat, TBot)), (Var "x"))) ));         
     "inl2" >:: (fun _ -> assert_equal 
                            "(Inl Nat+Bottom (App (Lbd Nat (LVar 0)) (S Zero)))"
                            (test_translate_ounit (Inl ((TSum (TNat, TBot)), (App ((Fun("x", TNat, (Var "x"))), (Nat 1))))) ));

     "case1" >:: (fun _ -> assert_equal 
                            "(Case Nat True (LVar 0) (LVar 0))"
                            (test_translate_ounit (Case (TNat, True, "x", (Var "x"), "y", (Var "y"))) ));
     "case2" >:: (fun _ -> assert_equal 
                            "(Case Nat (Inr Nat+Nat (S (S Zero))) (App (Lbd Nat (S (LVar 0))) (LVar 0)) (App (Lbd Nat (S (LVar 0))) (LVar 121)))"
                            (test_translate_ounit (Case (TNat, (Inr ((TSum (TNat, TNat)), (Nat 2))), "x", (App ((Fun ("y", TNat, (Suc (Var "y")))), (Var "x"))), "y", (App ((Fun ("y", TNat, (Suc (Var "y")))), (Var "x"))))) ));
     "case3" >:: (fun _ -> assert_equal 
                            "(Case Nat (Inl Nat+Nat (LVar 120)) (Lbd Nat (LVar 1)) (App (LVar 121) (LVar 0)))"
                            (test_translate_ounit (Case (TNat, (Inl ((TSum (TNat, TNat)), (Var "x"))), "x", (Fun ("y", TNat, (Var "x"))), "y", (App ((Var "x"), (Var "y"))))) ));     
]

let _ = run_test_tt_main translation_tests

