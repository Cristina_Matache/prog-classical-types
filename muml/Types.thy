section \<open>Typing Judgements\<close>

theory Types
  imports Syntax DeBruijn
begin

subsection \<open>Environments\<close>

text\<open>From src/HOL/Proofs/LambdaType.thy\<close>
definition
  shift :: "(nat \<Rightarrow> 'a) \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> nat \<Rightarrow> 'a"  ("_\<langle>_:_\<rangle>" [90, 0, 0] 91) where
  "e\<langle>i:a\<rangle> = (\<lambda>j. if j < i then e j else if j = i then a else e (j-1))"

lemma shift_eq [simp]: "i = j \<Longrightarrow> (e\<langle>i:T\<rangle>) j = T"
  by (simp add: shift_def)

lemma shift_gt [simp]: "j < i \<Longrightarrow> (e\<langle>i:T\<rangle>) j = e j"
  by (simp add: shift_def)

lemma shift_lt [simp]: "i < j \<Longrightarrow> (e\<langle>i:T\<rangle>) j = e (j - 1)"
  by (simp add: shift_def)

lemma shift_commute [simp]: "e\<langle>i:U\<rangle>\<langle>0:T\<rangle> = e\<langle>0:T\<rangle>\<langle>Suc i:U\<rangle>"
  by (rule ext) (simp_all add: shift_def split: nat.split, force)

lemma shift_pres_eq:
  "\<forall>x. (x \<noteq> \<alpha> \<longrightarrow> \<Delta> x = \<Delta>' x) \<Longrightarrow> \<forall>x. (x \<noteq> Suc \<alpha> \<longrightarrow> (\<Delta>\<langle>0:T\<rangle>) x = (\<Delta>'\<langle>0:T\<rangle>) x)"
  by(auto simp add: shift_def)
    
definition env_type_shift :: "(nat \<Rightarrow> type) \<Rightarrow> nat \<Rightarrow> nat \<Rightarrow> type" where
  "env_type_shift \<Gamma> k x \<equiv> liftT_type (\<Gamma> x) k"
  
lemma env_type_shift_eq [simp]:
  shows "env_type_shift (\<Gamma>\<langle>k : t\<rangle>) m = (env_type_shift \<Gamma> m)\<langle>k : liftT_type t m\<rangle>"
by(rule ext) (auto simp add: env_type_shift_def shift_def)

lemma liftT_type_commute:
  shows "n \<le> k \<Longrightarrow> liftT_type (liftT_type T n) (Suc k) = liftT_type (liftT_type T k) n"
  by(induction T arbitrary: k n, auto)
  
lemma env_type_shift_commute:
  shows "env_type_shift (env_type_shift \<Gamma> 0) (Suc k) = env_type_shift (env_type_shift \<Gamma> k) 0"
  apply(rule ext)
  apply(auto simp add: env_type_shift_def liftT_type_commute)
  done
  
subsection \<open>Typing rules\<close>
   
inductive typing_trm :: "(nat \<Rightarrow> type) \<Rightarrow> (nat \<Rightarrow> type) \<Rightarrow> trm \<Rightarrow> type \<Rightarrow> bool"  
     ("_ , _ \<turnstile>\<^sub>T _ : _" [50, 50, 50, 50] 50)
and typing_cmd :: "(nat \<Rightarrow> type) \<Rightarrow> (nat \<Rightarrow> type) \<Rightarrow> cmd \<Rightarrow> bool"
     ("_ , _ \<turnstile>\<^sub>C _" [50, 50, 50] 50)
where 
  [intro!]: "\<Gamma> x = T \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T `x : T" |
  [intro!]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t : (T1\<rightarrow>T2); \<Gamma>, \<Delta> \<turnstile>\<^sub>T s : T1 \<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (t\<degree>s) : T2" |
  [intro!]: "\<Gamma>\<langle>0:T1\<rangle>, \<Delta> \<turnstile>\<^sub>T t : T2 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (\<lambda> T1 : t) : (T1\<rightarrow>T2)" |
  [intro!]: "\<Gamma>, \<Delta>\<langle>0:T\<rangle> \<turnstile>\<^sub>C c \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (\<mu> T : c) : T" |
  [intro!]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T; \<Delta> x = T \<rbrakk>  \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C (<x> t)" |
  
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T Zero : Nat" |
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : Nat \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (S t) : Nat" |
  [intro!]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T r : T; \<Gamma>, \<Delta> \<turnstile>\<^sub>T s : (Nat\<rightarrow>T\<rightarrow>T); \<Gamma>, \<Delta> \<turnstile>\<^sub>T t : Nat \<rbrakk> \<Longrightarrow>
                    \<Gamma>, \<Delta> \<turnstile>\<^sub>T (Nrec T r s t) : T" |

  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : \<bottom> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>C (<\<top>> t)" | 

  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T true : Bool" |
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T false : Bool" |
  [intro!]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t1 : Bool; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t2 : T; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t3 : T \<rbrakk> \<Longrightarrow>
                      \<Gamma>, \<Delta> \<turnstile>\<^sub>T (If T t1 Then t2 Else t3) : T" |

  [intro!]: "\<lbrakk>\<Gamma>, \<Delta> \<turnstile>\<^sub>T t1 : T1; \<Gamma>, \<Delta> \<turnstile>\<^sub>T t2 : T2\<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lparr>t1, t2\<rparr>:(T1\<times>\<^sub>tT2) : T1\<times>\<^sub>tT2" |
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T1\<times>\<^sub>tT2 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T \<pi>\<^sub>1 t : T1" |
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T1\<times>\<^sub>tT2 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T \<pi>\<^sub>2 t : T2" | 

  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T1 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (Inl (T1+\<^sub>tT2) t) : T1+\<^sub>tT2" |
  [intro!]: "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T2 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T (Inr (T1+\<^sub>tT2) t) : T1+\<^sub>tT2" |
  [intro!]: "\<lbrakk> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t0 : T1+\<^sub>tT2; \<Gamma>\<langle>0:T1\<rangle>, \<Delta> \<turnstile>\<^sub>T t1 : T; \<Gamma>\<langle>0:T2\<rangle>, \<Delta> \<turnstile>\<^sub>T t2 : T \<rbrakk> \<Longrightarrow>
                        \<Gamma>, \<Delta> \<turnstile>\<^sub>T (Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) : T" |
  (* polymorphism... *)
  [intro!]: "\<lbrakk>env_type_shift \<Gamma> 0, env_type_shift \<Delta> 0 \<turnstile>\<^sub>T t : T\<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T TAbs t : TAll T" |
  [intro!]: "\<lbrakk>\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : TAll T\<rbrakk> \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T TApp t \<tau> : type_subst T 0 \<tau>"

inductive_cases typing_elims [elim!]:
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T `x : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T Zero : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (S t) : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t\<degree>s : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lambda> T1 : t : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T Nrec T1 r s t : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<mu> T1 : t : T"

    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T true : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T false : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (If U t1 Then t2 Else t3) : T"

    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<lparr>t1, t2\<rparr>:U : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<pi>\<^sub>1 t : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T \<pi>\<^sub>2 t : T"

    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (Inl U t) : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T (Inr U t) : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T Case U t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2 : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T TAbs t : T"
    "\<Gamma>, \<Delta> \<turnstile>\<^sub>T TApp t T : U"

code_pred(modes: i \<Rightarrow> i \<Rightarrow> i \<Rightarrow> o \<Rightarrow> bool) typing_trm .
code_pred(modes: i \<Rightarrow> i \<Rightarrow> i \<Rightarrow> o \<Rightarrow> bool) typing_cmd .

definition type_term :: "(nat \<Rightarrow> type) \<Rightarrow> (nat \<Rightarrow> type) \<Rightarrow> trm \<Rightarrow> type" where
  "type_term lenv menv t = Predicate.the (typing_trm_i_i_i_o lenv menv t)"


lemma uniqueness:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T1 \<Longrightarrow> \<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T2 \<Longrightarrow> T1 = T2"
  apply(induct arbitrary: T2 rule: typing_trm_typing_cmd.inducts(1))
                     apply blast+
                  apply force+
    done
    
lemma typed_values:
  assumes "is_val v" "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : T"
  shows "T = Nat \<Longrightarrow> v = Zero \<or> (\<exists> v1. v = (S v1) \<and> is_natval v1)"
        "T = T1\<rightarrow>T2 \<Longrightarrow> \<exists>U r. v = \<lambda> U : r"
        "T = Bool \<Longrightarrow> v = true \<or> v = false"
        "T = (T1 \<times>\<^sub>t T2) \<Longrightarrow> \<exists>v1 v2. v = \<lparr>v1, v2\<rparr>:(T1 \<times>\<^sub>t T2) \<and> is_val v1 \<and> is_val v2"
        "T = (T1 +\<^sub>t T2) \<Longrightarrow> (\<exists>v1. v = Inl (T1+\<^sub>tT2) v1 \<and> is_val v1) \<or> (\<exists> v2. v = Inr (T1+\<^sub>tT2) v2 \<and> is_val v2)"
        "T = TAll U \<Longrightarrow> (\<exists>t. v = TAbs t)"
  using assms by safe (induct v; fastforce)+
    
lemma val_nat[dest]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : Nat" "is_val v"
  shows "v = Zero \<or> (\<exists> v1. v = (S v1) \<and> is_natval v1)"
  using assms typed_values by fastforce
    
lemma val_fun[dest]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : T1\<rightarrow>T2" "is_val v"
  shows "\<exists>U r. v = \<lambda> U : r"
  using assms typed_values by fastforce
    
lemma val_bool[dest]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : Bool" "is_val v"
  shows "v = true \<or> v = false"
  using assms typed_values by fastforce
  
lemma val_pair[dest]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : (T1 \<times>\<^sub>t T2)" "is_val v"
  shows "\<exists>v1 v2. v = \<lparr>v1, v2\<rparr>:(T1 \<times>\<^sub>t T2) \<and> is_val v1 \<and> is_val v2"
  using assms typed_values by fastforce
    
lemma val_sum[dest]:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : (T1 +\<^sub>t T2)" "is_val v"
  shows "(\<exists>v1. v = Inl (T1+\<^sub>tT2) v1 \<and> is_val v1) \<or> (\<exists> v2. v = Inr (T1+\<^sub>tT2) v2 \<and> is_val v2)"
  using assms typed_values(5) by blast
    
lemma val_bot [dest]:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T v : \<bottom> \<Longrightarrow> \<not> is_val v"
  by(induct rule: is_val.induct) fastforce+
    
end
