section\<open>De Bruijn Nameless Representation\<close>

theory DeBruijn
  imports Syntax
begin
  
subsection\<open>Type substitution\<close>
  
fun liftT_type :: "type \<Rightarrow> nat \<Rightarrow> type" where
  "liftT_type (TVar i) k = (if i<k then TVar i else TVar (i+1))" |
  "liftT_type (TAll T) k = TAll (liftT_type T (k+1))" |
  "liftT_type (Prod R T) k = Prod (liftT_type R k) (liftT_type T k)" |
  "liftT_type (Sum R T) k = Sum (liftT_type R k) (liftT_type T k)" |
  "liftT_type (Fun R T) k = Fun (liftT_type R k) (liftT_type T k)" |
  "liftT_type T k = T"
  
fun type_subst :: "type \<Rightarrow> nat \<Rightarrow> type \<Rightarrow> type" where
  "type_subst (TVar m) n T = (if n < m then TVar (m-1) else if n = m then T else (TVar m))" | 
  "type_subst (TAll T) n U = TAll (type_subst T (Suc n) (liftT_type U 0))" |
  "type_subst Nat n U = Nat" |
  "type_subst Bool n U = Bool" |
  "type_subst Bottom n U = Bottom" |
  "type_subst (Prod R T) n U = Prod (type_subst R n U) (type_subst T n U)" |
  "type_subst (Sum R T) n U = Sum (type_subst R n U) (type_subst T n U)" |
  "type_subst (Fun R T) n U = Fun (type_subst R n U) (type_subst T n U)"
  
subsection\<open>Free variables\<close>

primrec flv_trm :: "trm \<Rightarrow> nat \<Rightarrow> nat set"
    and flv_cmd :: "cmd \<Rightarrow> nat \<Rightarrow> nat set"
where
    "flv_trm (`i) k = (if i\<ge>k then {i-k} else {})"
  | "flv_trm (\<lambda> T : t) k = flv_trm t (k+1)"
  | "flv_trm (s\<degree>t) k = (flv_trm s k) \<union> (flv_trm t k)"
  | "flv_trm (\<mu> T : c) k = flv_cmd c k"
  | "flv_trm Zero k = {}"
  | "flv_trm (S t) k = flv_trm t k"
  | "flv_trm (Nrec T r s t) k = (flv_trm r k) \<union> (flv_trm s k) \<union> (flv_trm t k)"
  | "flv_cmd (<i> t) k = flv_trm t k"
  | "flv_cmd (<\<top>> t) k = flv_trm t k"
    
  | "flv_trm (TAbs t) k = flv_trm t k"
  | "flv_trm (TApp t T) k = flv_trm t k"

  | "flv_trm true k = {}"
  | "flv_trm false k = {}"
  | "flv_trm (If T t1 Then t2 Else t3) k 
      = (flv_trm t1 k) \<union> (flv_trm t2 k) \<union> (flv_trm t3 k)"

  | "flv_trm (\<lparr>t1, t2\<rparr>:T) k = (flv_trm t1 k) \<union> (flv_trm t2 k)"
  | "flv_trm (\<pi>\<^sub>1 t) k = flv_trm t k"
  | "flv_trm (\<pi>\<^sub>2 t) k = flv_trm t k"

  | "flv_trm (Inl T t) k = flv_trm t k"
  | "flv_trm (Inr T t) k = flv_trm t k"
  | "flv_trm (Case T t1 Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3) k 
      = (flv_trm t1 k) \<union> (flv_trm t2 (k+1)) \<union> (flv_trm t3 (k+1))"
  
abbreviation lambda_closed :: "_" where
  "lambda_closed t \<equiv> flv_trm t 0 = {}"
  
abbreviation lambda_closedC :: "_" where
  "lambda_closedC c \<equiv> flv_cmd c 0 = {}"
  
fun fmv_trm :: "trm \<Rightarrow> nat \<Rightarrow> nat set"
    and fmv_cmd :: "cmd \<Rightarrow> nat \<Rightarrow> nat set"
where
    "fmv_trm (`i) k = {}"
  | "fmv_trm (\<lambda> T : t) k = fmv_trm t k"
  | "fmv_trm (s\<degree>t) k = (fmv_trm s k) \<union> (fmv_trm t k)"
  | "fmv_trm (\<mu> T : c) k = fmv_cmd c (k+1)"
  | "fmv_trm Zero k = {}"
  | "fmv_trm (S t) k = fmv_trm t k"
  | "fmv_trm (Nrec T r s t) k = (fmv_trm r k) \<union> (fmv_trm s k) \<union> (fmv_trm t k)"
  | "fmv_cmd (<i> t) k = (if i\<ge>k then {i-k} \<union> (fmv_trm t k) else (fmv_trm t k))"
  | "fmv_cmd (<\<top>> t) k = fmv_trm t k"
    
  | "fmv_trm (TAbs t) k = fmv_trm t k"
  | "fmv_trm (TApp t T) k = fmv_trm t k"

  | "fmv_trm true k = {}"
  | "fmv_trm false k = {}"
  | "fmv_trm (If T t1 Then t2 Else t3) k 
      = (fmv_trm t1 k) \<union> (fmv_trm t2 k) \<union> (fmv_trm t3 k)"

  | "fmv_trm (\<lparr>t1, t2\<rparr>:T) k = (fmv_trm t1 k) \<union> (fmv_trm t2 k)"
  | "fmv_trm (\<pi>\<^sub>1 t) k = fmv_trm t k"
  | "fmv_trm (\<pi>\<^sub>2 t) k = fmv_trm t k"

  | "fmv_trm (Inl T t) k = fmv_trm t k"
  | "fmv_trm (Inr T t) k = fmv_trm t k"
  | "fmv_trm (Case T t1 Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3) k 
      = (fmv_trm t1 k) \<union> (fmv_trm t2 k) \<union> (fmv_trm t3 k)"

text\<open>Functions that finds the free mu and lambda variables in a context\<close>

fun fmv_ctxt :: "ctxt \<Rightarrow> nat \<Rightarrow> nat set" where
    "fmv_ctxt \<diamond> k = {}"
  | "fmv_ctxt (CSuc E) k = fmv_ctxt E k"
  | "fmv_ctxt (E \<^sup>\<bullet> t) k = (fmv_ctxt E k) \<union> (fmv_trm t k)"
  | "fmv_ctxt (CNrec T r s E) k = (fmv_trm r k) \<union> (fmv_trm s k) \<union> (fmv_ctxt E k)"
  
  | "fmv_ctxt (T \<^sup>\<bullet>\<^sup>\<tau> \<tau>) k = fmv_ctxt T k"

  | "fmv_ctxt (CIf T E t2 t3) k = (fmv_ctxt E k) \<union> (fmv_trm t2 k) \<union> (fmv_trm t3 k)"
  | "fmv_ctxt (\<lparr>E, t\<rparr>\<^sub>l:T) k = (fmv_trm t k) \<union> (fmv_ctxt E k)"
  | "fmv_ctxt (\<lparr>t, E\<rparr>\<^sub>r:T) k = (fmv_trm t k) \<union> (fmv_ctxt E k)"
  | "fmv_ctxt (\<Pi>\<^sub>1 E) k = fmv_ctxt E k"
  | "fmv_ctxt (\<Pi>\<^sub>2 E) k = fmv_ctxt E k"

  | "fmv_ctxt (CInl T E) k = fmv_ctxt E k"
  | "fmv_ctxt (CInr T E) k = fmv_ctxt E k"
  | "fmv_ctxt (CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2) k = (fmv_ctxt E k) \<union> (fmv_trm t1 k) \<union> (fmv_trm t2 k)"

fun flv_ctxt :: "ctxt \<Rightarrow> nat \<Rightarrow> nat set" where
    "flv_ctxt \<diamond> k = {}"
  | "flv_ctxt (CSuc E) k = flv_ctxt E k"
  | "flv_ctxt (E \<^sup>\<bullet> t) k = (flv_ctxt E k) \<union> (flv_trm t k)"
  | "flv_ctxt (CNrec T r s E) k = (flv_trm r k) \<union> (flv_trm s k) \<union> (flv_ctxt E k)"
    
  | "flv_ctxt (T \<^sup>\<bullet>\<^sup>\<tau> \<tau>) k = flv_ctxt T k"

  | "flv_ctxt (CIf T E t2 t3) k = (flv_ctxt E k) \<union> (flv_trm t2 k) \<union> (flv_trm t3 k)"
  | "flv_ctxt (\<lparr>E, t\<rparr>\<^sub>l:T) k = (flv_trm t k) \<union> (flv_ctxt E k)"
  | "flv_ctxt (\<lparr>t, E\<rparr>\<^sub>r:T) k = (flv_trm t k) \<union> (flv_ctxt E k)"
  | "flv_ctxt (\<Pi>\<^sub>1 E) k = flv_ctxt E k"
  | "flv_ctxt (\<Pi>\<^sub>2 E) k = flv_ctxt E k"

  | "flv_ctxt (CInl T E) k = flv_ctxt E k"
  | "flv_ctxt (CInr T E) k = flv_ctxt E k"
  | "flv_ctxt (CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2) k 
      = (flv_ctxt E k) \<union> (flv_trm t1 (k+1)) \<union> (flv_trm t2 (k+1))"
    
abbreviation mu_closed :: "_" where
  "mu_closed t \<equiv> fmv_trm t 0 = {}"
  
abbreviation mu_closedC :: "_" where
  "mu_closedC c \<equiv> fmv_cmd c 0 = {}"
  
abbreviation closed :: "_" where
  "closed t \<equiv> lambda_closed t \<and> mu_closed t"
  
abbreviation closedC :: "_" where
  "closedC c \<equiv> lambda_closedC c \<and> mu_closedC c"
    
subsection\<open>Lifting functions\<close>

primrec
  liftL_trm :: "[trm, nat] \<Rightarrow> trm" and
  liftL_cmd :: "[cmd, nat] \<Rightarrow> cmd"
where
      "liftL_trm (`i) k = (if i<k then `i else `(i+1))"
    | "liftL_trm (\<lambda> T : t) k = \<lambda> T : (liftL_trm t (k+1))"
    | "liftL_trm (s \<degree> t) k = liftL_trm s k \<degree> liftL_trm t k"
    | "liftL_trm (\<mu> T : c) k = \<mu> T : (liftL_cmd c k)"
    | "liftL_trm Zero k = Zero"
    | "liftL_trm (S t) k = S (liftL_trm t k)"
    | "liftL_trm (Nrec T t u v) k = Nrec T (liftL_trm t k) (liftL_trm u k) (liftL_trm v k)"
    | "liftL_cmd (<i> t) k = <i> (liftL_trm t k)"
    | "liftL_cmd (<\<top>> t) k = <\<top>> (liftL_trm t k)"
      
    | "liftL_trm (TAbs t) k = TAbs (liftL_trm t k)"
    | "liftL_trm (TApp T \<tau>) k = TApp (liftL_trm T k) \<tau>"

      
    | "liftL_trm true k = true"
    | "liftL_trm false k = false"
    | "liftL_trm (If T t1 Then t2 Else t3) k = 
        If T (liftL_trm t1 k) Then (liftL_trm t2 k) Else (liftL_trm t3 k)"

    | "liftL_trm (\<lparr>t1, t2\<rparr>:T) k = \<lparr>(liftL_trm t1 k), (liftL_trm t2 k)\<rparr>:T"
    | "liftL_trm (\<pi>\<^sub>1 t) k = \<pi>\<^sub>1 (liftL_trm t k)"
    | "liftL_trm (\<pi>\<^sub>2 t) k = \<pi>\<^sub>2 (liftL_trm t k)"

    | "liftL_trm (Inl T t) k = Inl T (liftL_trm t k)"
    | "liftL_trm (Inr T t) k = Inr T (liftL_trm t k)"
    | "liftL_trm (Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) k 
        = Case T (liftL_trm t0 k) Of Inl\<Rightarrow> (liftL_trm t1 (k+1))|Inr\<Rightarrow> (liftL_trm t2 (k+1))"
      
fun liftT_trm :: "trm \<Rightarrow> nat \<Rightarrow> trm"
  and liftT_cmd :: "cmd \<Rightarrow> nat \<Rightarrow> cmd" where
      "liftT_trm (`i) k = (`i)"
    | "liftT_trm (\<lambda> T : t) k = \<lambda> (liftT_type T k) : (liftT_trm t k)"
    | "liftT_trm (s \<degree> t) k = liftT_trm s k \<degree> liftT_trm t k"
    | "liftT_trm (\<mu> T : c) k = \<mu> (liftT_type T k) : (liftT_cmd c k)"
    | "liftT_trm Zero k = Zero"
    | "liftT_trm (S t) k = S (liftT_trm t k)"
    | "liftT_trm (Nrec T t u v) k = Nrec (liftT_type T k) (liftT_trm t k) (liftT_trm u k) (liftT_trm v k)"
    | "liftT_cmd (<i> t) k = <i> (liftT_trm t k)"
    | "liftT_cmd (<\<top>> t) k = <\<top>> (liftT_trm t k)"
      
    | "liftT_trm (TAbs t) k = TAbs (liftT_trm t (Suc k))"
    | "liftT_trm (TApp T \<tau>) k = TApp (liftT_trm T k) (liftT_type \<tau> k)"
      
    | "liftT_trm true k = true"
    | "liftT_trm false k = false"
    | "liftT_trm (If T t1 Then t2 Else t3) k = 
        If (liftT_type T k) (liftT_trm t1 k) Then (liftT_trm t2 k) Else (liftT_trm t3 k)"

    | "liftT_trm (\<lparr>t1, t2\<rparr>:T) k = \<lparr>(liftT_trm t1 k), (liftT_trm t2 k)\<rparr>:(liftT_type T k)"
    | "liftT_trm (\<pi>\<^sub>1 t) k = \<pi>\<^sub>1 (liftT_trm t k)"
    | "liftT_trm (\<pi>\<^sub>2 t) k = \<pi>\<^sub>2 (liftT_trm t k)"

    | "liftT_trm (Inl T t) k = Inl (liftT_type T k) (liftT_trm t k)"
    | "liftT_trm (Inr T t) k = Inr (liftT_type T k) (liftT_trm t k)"
    | "liftT_trm (Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) k 
        = Case (liftT_type T k) (liftT_trm t0 k) Of Inl\<Rightarrow> (liftT_trm t1 k)|Inr\<Rightarrow> (liftT_trm t2 k)"

primrec
  liftM_trm :: "[trm, nat] \<Rightarrow> trm" and
  liftM_cmd :: "[cmd, nat] \<Rightarrow> cmd"
where
      "liftM_trm (`i) k = `i"
    | "liftM_trm (\<lambda> T : t) k = \<lambda> T : (liftM_trm t k)"
    | "liftM_trm (s \<degree> t) k = liftM_trm s k \<degree> liftM_trm t k"
    | "liftM_trm (\<mu> T : c) k = \<mu> T : (liftM_cmd c (k+1))"
    | "liftM_trm Zero k = Zero"
    | "liftM_trm (S t) k = S (liftM_trm t k)"
    | "liftM_trm (Nrec T t u v) k = Nrec T (liftM_trm t k) (liftM_trm u k) (liftM_trm v k)"
    | "liftM_cmd (<i> t) k = 
        (if i<k then (<i> (liftM_trm t k)) else (<i+1> (liftM_trm t k)))"
    | " liftM_cmd (<\<top>> t) k =  <\<top>> (liftM_trm t k)"
    | "liftM_trm (TAbs t) k = TAbs (liftM_trm t k)"
    | "liftM_trm (TApp T \<tau>) k = TApp (liftM_trm T k) \<tau>"      
      
    | "liftM_trm true k = true"
    | "liftM_trm false k = false"
    | "liftM_trm (If T t1 Then t2 Else t3) k = 
        If T (liftM_trm t1 k) Then (liftM_trm t2 k) Else (liftM_trm t3 k)"

    | "liftM_trm (\<lparr>t1, t2\<rparr>:T) k = \<lparr>(liftM_trm t1 k), (liftM_trm t2 k)\<rparr>:T"
    | "liftM_trm (\<pi>\<^sub>1 t) k = \<pi>\<^sub>1 (liftM_trm t k)"
    | "liftM_trm (\<pi>\<^sub>2 t) k = \<pi>\<^sub>2 (liftM_trm t k)"

    | "liftM_trm (Inl T t) k = Inl T (liftM_trm t k)"
    | "liftM_trm (Inr T t) k = Inr T (liftM_trm t k)"
    | "liftM_trm (Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) k 
        = Case T (liftM_trm t0 k) Of Inl\<Rightarrow> (liftM_trm t1 k)|Inr\<Rightarrow> (liftM_trm t2 k)"

text\<open>Shift free lambda and mu variables in contexts to make structural substitution capture avoiding\<close>

primrec liftL_ctxt :: "ctxt \<Rightarrow> nat \<Rightarrow> ctxt" where
    "liftL_ctxt \<diamond> n = \<diamond>"
  | "liftL_ctxt (CSuc E) n = CSuc (liftL_ctxt E n)"
  | "liftL_ctxt (E \<^sup>\<bullet> t) n = (liftL_ctxt E n) \<^sup>\<bullet> (liftL_trm t n)"
  | "liftL_ctxt (CNrec T r s E) n =
      CNrec T (liftL_trm r n) (liftL_trm s n) (liftL_ctxt E n)"
    
  | "liftL_ctxt (T \<^sup>\<bullet>\<^sup>\<tau> \<tau>) k = (liftL_ctxt T k \<^sup>\<bullet>\<^sup>\<tau> \<tau>)"

  | "liftL_ctxt (CIf T E t2 t3) n = CIf T (liftL_ctxt E n) (liftL_trm t2 n) (liftL_trm t3 n)"
  | "liftL_ctxt (\<lparr>E, t\<rparr>\<^sub>l:T) n = \<lparr>(liftL_ctxt E n), (liftL_trm t n)\<rparr>\<^sub>l:T"
  | "liftL_ctxt (\<lparr>t, E\<rparr>\<^sub>r:T) n = \<lparr>(liftL_trm t n), (liftL_ctxt E n)\<rparr>\<^sub>r:T"
  | "liftL_ctxt (\<Pi>\<^sub>1 E) n = \<Pi>\<^sub>1 (liftL_ctxt E n)"
  | "liftL_ctxt (\<Pi>\<^sub>2 E) n = \<Pi>\<^sub>2 (liftL_ctxt E n)"

  | "liftL_ctxt (CInl T E) k = CInl T (liftL_ctxt E k)"
  | "liftL_ctxt (CInr T E) k = CInr T (liftL_ctxt E k)"
  | "liftL_ctxt (CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2) k
      = CCase T (liftL_ctxt E k) Of CInl\<Rightarrow> (liftL_trm t1 (k+1))|CInr\<Rightarrow> (liftL_trm t2 (k+1))"

primrec liftM_ctxt :: "ctxt \<Rightarrow> nat \<Rightarrow> ctxt" where
    "liftM_ctxt \<diamond> n = \<diamond>"
  | "liftM_ctxt (CSuc E) n = CSuc (liftM_ctxt E n)"
  | "liftM_ctxt (E \<^sup>\<bullet> t) n = (liftM_ctxt E n) \<^sup>\<bullet> (liftM_trm t n)"
  | "liftM_ctxt (CNrec T r s E) n =
      CNrec T (liftM_trm r n) (liftM_trm s n) (liftM_ctxt E n)"

  | "liftM_ctxt (T \<^sup>\<bullet>\<^sup>\<tau> \<tau>) k = (liftM_ctxt T k \<^sup>\<bullet>\<^sup>\<tau> \<tau>)"
    
  | "liftM_ctxt (CIf T E t2 t3) n = CIf T (liftM_ctxt E n) (liftM_trm t2 n) (liftM_trm t3 n)"
  | "liftM_ctxt (\<lparr>E, t\<rparr>\<^sub>l:T) n = \<lparr>(liftM_ctxt E n), (liftM_trm t n)\<rparr>\<^sub>l:T"
  | "liftM_ctxt (\<lparr>t, E\<rparr>\<^sub>r:T) n = \<lparr>(liftM_trm t n), (liftM_ctxt E n)\<rparr>\<^sub>r:T"
  | "liftM_ctxt (\<Pi>\<^sub>1 E) n = \<Pi>\<^sub>1 (liftM_ctxt E n)"
  | "liftM_ctxt (\<Pi>\<^sub>2 E) n = \<Pi>\<^sub>2 (liftM_ctxt E n)"

  | "liftM_ctxt (CInl T E) k = CInl T (liftM_ctxt E k)"
  | "liftM_ctxt (CInr T E) k = CInr T (liftM_ctxt E k)"
  | "liftM_ctxt (CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2) k
      = CCase T (liftM_ctxt E k) Of CInl\<Rightarrow> (liftM_trm t1 k)|CInr\<Rightarrow> (liftM_trm t2 k)"

fun liftT_ctxt :: "ctxt \<Rightarrow> nat \<Rightarrow> ctxt" where
    "liftT_ctxt \<diamond> n = \<diamond>"
  | "liftT_ctxt (CSuc E) n = CSuc (liftT_ctxt E n)"
  | "liftT_ctxt (E \<^sup>\<bullet> t) n = (liftT_ctxt E n) \<^sup>\<bullet> (liftT_trm t n)"
  | "liftT_ctxt (CNrec T r s E) n =
      CNrec (liftT_type T n) (liftT_trm r n) (liftT_trm s n) (liftT_ctxt E n)"

  | "liftT_ctxt (T \<^sup>\<bullet>\<^sup>\<tau> \<tau>) k = (liftT_ctxt T k \<^sup>\<bullet>\<^sup>\<tau> (liftT_type \<tau> k))"
    
  | "liftT_ctxt (CIf T E t2 t3) n = CIf (liftT_type T n) (liftT_ctxt E n) (liftT_trm t2 n) (liftT_trm t3 n)"
  | "liftT_ctxt (\<lparr>E, t\<rparr>\<^sub>l:T) n = \<lparr>(liftT_ctxt E n), (liftT_trm t n)\<rparr>\<^sub>l:(liftT_type T n)"
  | "liftT_ctxt (\<lparr>t, E\<rparr>\<^sub>r:T) n = \<lparr>(liftT_trm t n), (liftT_ctxt E n)\<rparr>\<^sub>r:(liftT_type T n)"
  | "liftT_ctxt (\<Pi>\<^sub>1 E) n = \<Pi>\<^sub>1 (liftT_ctxt E n)"
  | "liftT_ctxt (\<Pi>\<^sub>2 E) n = \<Pi>\<^sub>2 (liftT_ctxt E n)"

  | "liftT_ctxt (CInl T E) k = CInl (liftT_type T k) (liftT_ctxt E k)"
  | "liftT_ctxt (CInr T E) k = CInr (liftT_type T k) (liftT_ctxt E k)"
  | "liftT_ctxt (CCase T E Of CInl\<Rightarrow> t1|CInr\<Rightarrow> t2) k
      = CCase (liftT_type T k) (liftT_ctxt E k) Of CInl\<Rightarrow> (liftT_trm t1 k)|CInr\<Rightarrow> (liftT_trm t2 k)"
    
primrec
  dropM_trm :: "[trm, nat] \<Rightarrow> trm" and
  dropM_cmd :: "[cmd, nat] \<Rightarrow> cmd"
where
    "dropM_trm (`i) k = `i"
  | "dropM_trm (\<lambda> T : t) k = \<lambda> T : (dropM_trm t k)"
  | "dropM_trm (s \<degree> t) k = (dropM_trm s k) \<degree> (dropM_trm t k)"
  | "dropM_trm (\<mu> T : c) k = \<mu> T : (dropM_cmd c (k+1))"
  | "dropM_trm Zero k = Zero"
  | "dropM_trm (S t) k = S (dropM_trm t k)"
  | "dropM_trm (Nrec T r s t) k = 
      Nrec T (dropM_trm r k) (dropM_trm s k) (dropM_trm t k)"
  | "dropM_cmd (<i> t) k = 
      (if i>k then (<i-1> (dropM_trm t k)) else (<i> (dropM_trm t k)))"
  | "dropM_cmd (<\<top>> t) k = <\<top>> (dropM_trm t k)"

  | "dropM_trm (TAbs t) k = TAbs (dropM_trm t k)"
  | "dropM_trm (TApp t \<tau>) k = TApp (dropM_trm t k) \<tau>"
    
  | "dropM_trm true k = true"
  | "dropM_trm false k = false"
  | "dropM_trm (If T t1 Then t2 Else t3) k = 
      If T (dropM_trm t1 k) Then (dropM_trm t2 k) Else (dropM_trm t3 k)"

  | "dropM_trm (\<lparr>t1, t2\<rparr>:T) k = \<lparr>(dropM_trm t1 k), (dropM_trm t2 k)\<rparr>:T"
  | "dropM_trm (\<pi>\<^sub>1 t) k = \<pi>\<^sub>1 (dropM_trm t k)"
  | "dropM_trm (\<pi>\<^sub>2 t) k = \<pi>\<^sub>2 (dropM_trm t k)"

  | "dropM_trm (Inl T t) k = Inl T (dropM_trm t k)"
  | "dropM_trm (Inr T t) k = Inr T (dropM_trm t k)"
  | "dropM_trm (Case T t1 Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3) k 
      = Case T (dropM_trm t1 k) Of Inl\<Rightarrow> (dropM_trm t2 k)|Inr\<Rightarrow> (dropM_trm t3 k)"

primrec
  dropL_trm :: "[trm, nat] \<Rightarrow> trm" and
  dropL_cmd :: "[cmd, nat] \<Rightarrow> cmd"
where
    "dropL_trm (`i) k = (if i>k then `(i-1) else `i)"
  | "dropL_trm (\<lambda> T : t) k = \<lambda> T : (dropL_trm t (k+1))"
  | "dropL_trm (s \<degree> t) k = (dropL_trm s k) \<degree> (dropL_trm t k)"
  | "dropL_trm (\<mu> T : c) k = \<mu> T : (dropL_cmd c k)"
  | "dropL_trm Zero k = Zero"
  | "dropL_trm (S t) k = S (dropL_trm t k)"
  | "dropL_trm (Nrec T r s t) k = 
      Nrec T (dropL_trm r k) (dropL_trm s k) (dropL_trm t k)"
  | "dropL_cmd (<i> t) k = <i> (dropL_trm t k)"
  | "dropL_cmd (<\<top>> t) k = <\<top>> (dropL_trm t k)"
    
  | "dropL_trm (TAbs t) k = TAbs (dropL_trm t k)"
  | "dropL_trm (TApp t \<tau>) k = TApp (dropL_trm t k) \<tau>"

  | "dropL_trm true k = true"
  | "dropL_trm false k = false"
  | "dropL_trm (If T t1 Then t2 Else t3) k = 
      If T (dropL_trm t1 k) Then (dropL_trm t2 k) Else (dropL_trm t3 k)"

  | "dropL_trm (\<lparr>t1, t2\<rparr>:T) k = \<lparr>(dropL_trm t1 k), (dropL_trm t2 k)\<rparr>:T"
  | "dropL_trm (\<pi>\<^sub>1 t) k = \<pi>\<^sub>1 (dropL_trm t k)"
  | "dropL_trm (\<pi>\<^sub>2 t) k = \<pi>\<^sub>2 (dropL_trm t k)"

  | "dropL_trm (Inl T t) k = Inl T (dropL_trm t k)"
  | "dropL_trm (Inr T t) k = Inr T (dropL_trm t k)"
  | "dropL_trm (Case T t1 Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3) k 
      = Case T (dropL_trm t1 k) Of Inl\<Rightarrow> (dropL_trm t2 (k+1))|Inr\<Rightarrow> (dropL_trm t3 (k+1))"
    
subsection\<open>Useful lemmas\<close>

lemma liftL_pres_flv: 
  "x \<in> flv_trm (liftL_trm s m) (Suc n) \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> flv_trm s n"
  "\<And>m n. x \<in> flv_cmd (liftL_cmd c m) (Suc n) \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> flv_cmd c n"
  by (induct s and c arbitrary: n m) (fastforce split: if_splits)+
    
lemma liftL_ctxt_pres_flv:
  "x \<in> flv_ctxt (liftL_ctxt F 0) (Suc n) \<Longrightarrow> x \<in> flv_ctxt F n"
  by (induct F arbitrary: n) (fastforce dest: liftL_pres_flv(1))+
    
lemma liftM_pres_flv: 
  "x \<in> flv_trm (liftM_trm s m) n \<Longrightarrow> x \<in> flv_trm s n"
  "\<And>n m. x \<in> flv_cmd (liftM_cmd c m) n \<Longrightarrow> x \<in> flv_cmd c n"
  by (induct s and c arbitrary: n m) (fastforce split: if_splits)+
    
lemma liftM_ctxt_pres_flv:
  "x \<in> flv_ctxt (liftM_ctxt E m) n \<Longrightarrow> x \<in> flv_ctxt E n"
  by(induct E arbitrary: n m) (fastforce simp add: liftM_pres_flv(1))+ (* XXX: slow *)

lemma liftL_pres_fmv:
  "x \<in> fmv_trm (liftL_trm s m) n \<Longrightarrow> x \<in> fmv_trm s n"
  "\<And>n m. x \<in> fmv_cmd (liftL_cmd c m) n \<Longrightarrow> x \<in> fmv_cmd c n"
  by (induct s and c arbitrary: n m) (fastforce split: if_splits)+

lemma liftL_ctxt_pres_fmv:
  "x \<in> fmv_ctxt (liftL_ctxt E m) n \<Longrightarrow> x \<in> fmv_ctxt E n"
  by(induct E arbitrary: m n) (fastforce simp add: liftL_pres_fmv)+
  
lemma liftM_pres_fmv:
  "x \<in> fmv_trm (liftM_trm s m) (Suc n) \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> fmv_trm s n"
  "\<And>n m. x \<in> fmv_cmd (liftM_cmd c m) (Suc n) \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> fmv_cmd c n"
by (induct s and c arbitrary: n m) (fastforce split: if_splits)+
  
lemma liftM_pres_fmv':
  "l\<le>k \<Longrightarrow> 0 \<notin> fmv_trm t l \<Longrightarrow> 0 \<notin> fmv_trm (liftM_trm t (Suc k)) l"
  "\<And> k l. l\<le>k \<Longrightarrow> 0 \<notin> fmv_cmd c l \<Longrightarrow> 0 \<notin> fmv_cmd (liftM_cmd c (Suc k)) l"
by (induct t and c arbitrary: k l) (fastforce split: if_splits)+
  
lemma liftM_ctxt_pres_fmv:
  "x \<in> fmv_ctxt (liftM_ctxt E m) (Suc n) \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> fmv_ctxt E n"
  by(induct E arbitrary: m n) (fastforce simp add: liftM_pres_fmv)+
    
lemma fmv_liftL: 
  "\<forall> m n. (\<beta> \<notin> fmv_trm t n \<longrightarrow> \<beta> \<notin> fmv_trm (liftL_trm t m) n)"
  "\<forall> m n. (\<beta> \<notin> fmv_cmd c n \<longrightarrow> \<beta> \<notin> fmv_cmd (liftL_cmd c m) n)"
by(induct t and c) auto

lemma fmv_liftL_ctxt:
  "\<forall> m n. (\<beta> \<notin> fmv_ctxt E m \<longrightarrow> \<beta> \<notin> fmv_ctxt (liftL_ctxt E n) m)"
  by(induct E) (fastforce simp add: fmv_liftL)+

lemma fmv_suc:
  "\<forall> n. (\<beta> \<notin> fmv_cmd c (Suc n) \<longrightarrow> (\<beta>+1) \<notin> fmv_cmd c n)"
  "\<forall> n. (\<beta> \<notin> fmv_trm t (Suc n) \<longrightarrow> (\<beta>+1) \<notin> fmv_trm t n)"
  by (induct c and t) (clarsimp | force)+
    
lemma fmv_sucD [dest]:
  assumes "\<beta> \<notin> fmv_cmd c (Suc n)"
    shows "(\<beta>+1) \<notin> fmv_cmd c n"
using assms fmv_suc by auto
    
lemma fmv_liftM:
  "\<beta> \<notin> fmv_trm t n \<Longrightarrow> Suc \<beta> \<notin> fmv_trm (liftM_trm t n) n"
  "\<forall> n. (\<beta> \<notin> fmv_cmd c n \<longrightarrow> Suc \<beta> \<notin> fmv_cmd (liftM_cmd c n) n)"
by(induct t and c arbitrary: n) auto

lemma fmv_liftM_ctxt:
  "\<beta> \<notin> fmv_ctxt E 0 \<Longrightarrow> Suc \<beta> \<notin> fmv_ctxt (liftM_ctxt E 0) 0"
  by(induct E) (fastforce simp add: fmv_liftM)+
    
lemma dropM_pres_flv:
  "x \<in> flv_trm (dropM_trm s m) n \<Longrightarrow> x \<in> flv_trm s n"
  "\<And>n m. x \<in> flv_cmd (dropM_cmd c m) n \<Longrightarrow> x \<in> flv_cmd c n"
by (induct s and c arbitrary: n m) (fastforce split: if_splits)+

lemma dropM_pres_fmv:
  "x \<in> fmv_trm (dropM_trm s m) n \<Longrightarrow> 0 \<notin> fmv_trm s m \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> fmv_trm s (Suc n)"
  "\<And>n m. x \<in> fmv_cmd (dropM_cmd c m) n \<Longrightarrow> 0 \<notin> fmv_cmd c m \<Longrightarrow> m\<le>n \<Longrightarrow> x \<in> fmv_cmd c (Suc n)"
by (induct s and c arbitrary: n m) (fastforce split: if_splits)+

lemma liftM_pres_dropM:
  "dropM_trm (liftM_trm t (Suc \<alpha>)) \<alpha> = t"
  "\<And>\<alpha>. dropM_cmd (liftM_cmd c (Suc \<alpha>)) \<alpha> = c"
by(induct t and c arbitrary: \<alpha>) fastforce+

lemma liftM_pres_dropM':
  "dropM_trm (liftM_trm t \<alpha>) \<alpha> = t"
  "\<And>\<alpha>. dropM_cmd (liftM_cmd c \<alpha>) \<alpha> = c"
by(induct t and c arbitrary: \<alpha>) fastforce+

lemma liftL_dropM_comm:
  "liftL_trm (dropM_trm s l) k = dropM_trm (liftL_trm s k) l"
  "\<And> k l. liftL_cmd (dropM_cmd c l) k = dropM_cmd (liftL_cmd c k) l"
by(induct s and c arbitrary: k l) fastforce+

lemma liftM_dropM_comm:
  "0 \<notin> fmv_trm t l \<Longrightarrow> l\<le>k \<Longrightarrow>  liftM_trm (dropM_trm t l) k = dropM_trm (liftM_trm t (Suc k)) l"
  "\<And> k l. 0 \<notin> fmv_cmd c l \<Longrightarrow> l\<le>k \<Longrightarrow> liftM_cmd (dropM_cmd c l) k = dropM_cmd (liftM_cmd c (Suc k)) l"  
by (induct t and c arbitrary: k l) (fastforce split: if_splits)+

lemma liftLM_comm: 
  "\<forall>n.\<forall> m. liftL_trm (liftM_trm t n) m = liftM_trm (liftL_trm t m) n"
  "\<forall>n. \<forall>m. liftL_cmd (liftM_cmd c n) m = liftM_cmd (liftL_cmd c m) n"
by(induct t and c) auto

lemma liftLL_comm:
  "l\<le>k \<Longrightarrow> liftL_trm (liftL_trm s k) l = liftL_trm (liftL_trm s l) (Suc k)"
  "\<And> k l. l\<le>k \<Longrightarrow> liftL_cmd (liftL_cmd c k) l = liftL_cmd (liftL_cmd c l) (Suc k)"
by(induct s and c arbitrary: k l) fastforce+

lemma liftLM_comm_ctxt:
  "liftL_ctxt (liftM_ctxt E n) m = liftM_ctxt (liftL_ctxt E m) n"
  by(induct E arbitrary: n m, auto simp add: liftLM_comm)

lemma liftLL_comm_ctxt:
  "l\<le>k \<Longrightarrow> liftL_ctxt (liftL_ctxt E k) l = liftL_ctxt (liftL_ctxt E l) (Suc k)"
  by(induct E) (fastforce simp add: liftLL_comm)+

lemma liftMM_comm:
  "\<forall>n. \<forall>m. (n\<ge>m \<longrightarrow> liftM_trm (liftM_trm t n) m = liftM_trm (liftM_trm t m) (Suc n))"
  "\<forall>n. \<forall>m. (n\<ge>m \<longrightarrow> liftM_cmd (liftM_cmd c n) m = liftM_cmd (liftM_cmd c m) (Suc n))"
by(induct t and c) auto

lemma liftMM_comm_ctxt:
  "liftM_ctxt (liftM_ctxt E n) 0 = liftM_ctxt (liftM_ctxt E 0) (n+1)"
  by(induct E arbitrary: n, auto simp add: liftMM_comm)

end