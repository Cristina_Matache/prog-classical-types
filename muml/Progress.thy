section \<open>Progress\<close>

theory Progress
  imports TypePreservation
begin

declare is_nfD2 [dest]
  
lemma red_not_nf:
  "\<exists>s. t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<not> is_nf t"
  "\<exists>d. c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<Longrightarrow> \<not> is_nfC c"
  by (induct t and c) force+

lemma red_not_nfD: "t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> \<not> is_nf t"
  using red_not_nf by blast
    
lemma red_not_nfD2: "c \<^sub>C\<rightarrow>\<^sub>\<beta> d \<Longrightarrow> \<not> is_nfC c"
  using red_not_nf by metis
    
lemma normal_forms:
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T \<Longrightarrow> lambda_closed t  \<Longrightarrow> (\<forall> s. \<not>(t \<rightarrow>\<^sub>\<beta> s)) \<Longrightarrow> is_nf t"
  "\<Gamma>, \<Delta> \<turnstile>\<^sub>C c \<Longrightarrow> lambda_closedC c \<Longrightarrow> (\<forall> \<beta> t. c = (<\<beta>> t) \<longrightarrow> (\<forall> d. \<not>(t \<rightarrow>\<^sub>\<beta> d)) \<longrightarrow> is_nf t)
                                         \<and> (\<forall> t. c = (<\<top>> t) \<longrightarrow> (\<forall> d. \<not>(t \<rightarrow>\<^sub>\<beta> d)) \<longrightarrow> is_nf t)"
proof(induct rule: typing_trm_typing_cmd.inducts)
  fix \<Gamma> \<Delta> T c
  assume "\<Gamma> , \<Delta>\<langle>0:T\<rangle> \<turnstile>\<^sub>C c" "lambda_closed (\<mu> T:c)" "\<forall>s. \<not> \<mu> T:c \<rightarrow>\<^sub>\<beta> s"
         "lambda_closedC c \<Longrightarrow> (\<forall>\<beta> t. c = <\<beta>> t \<longrightarrow> (\<forall>d. \<not> t \<rightarrow>\<^sub>\<beta> d) \<longrightarrow> is_nf t)
                              \<and> (\<forall>t. c = <\<top>> t \<longrightarrow> (\<forall>d. \<not> t \<rightarrow>\<^sub>\<beta> d) \<longrightarrow> is_nf t)"
  thus "is_nf (\<mu> T:c)"
    by (case_tac c) (clarsimp, blast?)+
next
  fix \<Gamma> \<Delta> t1 T1 t2 T2
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t1 : T1"
         "lambda_closed t1 \<Longrightarrow> \<forall>s. \<not> t1 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> is_nf t1"
         "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t2 : T2"
         "lambda_closed t2 \<Longrightarrow> \<forall>s. \<not> t2 \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> is_nf t2"
         "lambda_closed (\<lparr>t1,t2\<rparr>:(T1 \<times>\<^sub>t T2))"
         "\<forall>s. \<not> (\<lparr>t1,t2\<rparr>:(T1 \<times>\<^sub>t T2)) \<rightarrow>\<^sub>\<beta> s"
  thus "is_nf (\<lparr>t1,t2\<rparr>:(T1 \<times>\<^sub>t T2))"
    by (clarsimp, blast?)
next
  fix \<Gamma> \<Delta> t T
  assume "env_type_shift \<Gamma> 0 , env_type_shift \<Delta> 0 \<turnstile>\<^sub>T t : T"
    and "lambda_closed t \<Longrightarrow> \<forall>s. \<not> t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> is_nf t"
    and "lambda_closed (TAbs t)"
    and "\<forall>s. \<not> TAbs t \<rightarrow>\<^sub>\<beta> s"
  thus "is_nf (TAbs t)"
    by simp
next
  case 20
  fix \<Gamma> \<Delta> t T \<tau>
  assume "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : TAll T"
    and "lambda_closed t \<Longrightarrow> \<forall>s. \<not> t \<rightarrow>\<^sub>\<beta> s \<Longrightarrow> is_nf t"
    and "lambda_closed (TApp t \<tau>)"
    and "\<forall>s. \<not> TApp t \<tau> \<rightarrow>\<^sub>\<beta> s"
  thus "is_nf (TApp t \<tau>)"
    apply -
    apply(ind_cases "\<Gamma> , \<Delta> \<turnstile>\<^sub>T t : TAll T")
             apply auto
     apply(meson red_trm_red_cmd.intros(27))
    apply(subgoal_tac "is_nfC c")
     apply force
    apply auto
      done  
qed (clarsimp | blast)+

lemma is_nfDI [dest,intro]:
  assumes "\<forall>s. \<not>(t \<rightarrow>\<^sub>\<beta> s)" "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T" "lambda_closed t"
    shows "is_nf t"
using assms normal_forms by blast
    
lemma fmv_muD [dest]: "t = (\<mu> U:(<\<beta>> v)) \<Longrightarrow> mu_closed t \<Longrightarrow> \<beta> = 0"
  by(cases "Suc 0 \<le> \<beta>"; fastforce)

lemma fmv_natval [dest]: "0 \<in> fmv_trm v 0 \<Longrightarrow> \<not> is_natval v"
  by(induction rule: is_natval.induct) (fastforce)+
    
theorem is_natvalI:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : Nat" "closed t" "\<forall> s. \<not>(t \<rightarrow>\<^sub>\<beta> s)"
   shows  "is_natval t"
  using assms apply -
  apply(drule is_nfDI, fastforce, fastforce)
  apply (drule is_nfD2, erule disjE)
   apply force
  apply (induct rule: typing_trm_typing_cmd.inducts(1))
                     apply auto
  apply (erule typing_cmd.cases)
  defer
   apply force


    oops

    (*
    sledgehammer
  apply clarsimp
  apply (case_tac c)
   apply clarsimp
   defer
   apply clarsimp
   apply (metis cmd.distinct(1) cmd.inject(2) typing_cmd.cases val_bot)
  apply (clarsimp split: if_splits)
  apply (erule typing_cmd.cases)
   apply clarsimp

  sorry
    (*
  apply (clarsimp, case_tac c; force split: if_splits)
  done
*)
*)
theorem progress:
  assumes "\<Gamma>, \<Delta> \<turnstile>\<^sub>T t : T" "lambda_closed t"
    shows "is_nf t \<or> (\<exists> s. t \<rightarrow>\<^sub>\<beta> s)"
using assms by blast

end