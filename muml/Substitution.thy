section\<open>Substitutions\<close>

theory Substitution
  imports DeBruijn
begin 

subsection\<open>Logical substitution\<close>

primrec
  subst_trm :: "[trm, trm, nat] \<Rightarrow> trm"  ("_[_'/_]\<^sup>T" [300, 0, 0] 300) and
  subst_cmd ::  "[cmd, trm, nat] \<Rightarrow> cmd" ("_[_'/_]\<^sup>C" [300, 0, 0] 300)
where
  "(`i)[s/k]\<^sup>T = (if k < i then `(i-1) else if k = i then s else (`i))" |
  "(\<lambda> T : t)[s/k]\<^sup>T = \<lambda> T : (t[(liftL_trm s 0) / k+1]\<^sup>T)" |
  "(t \<degree> u)[s/k]\<^sup>T = t[s/k]\<^sup>T \<degree> u[s/k]\<^sup>T" |
  "(\<mu> T : c)[s/k]\<^sup>T  = \<mu> T : (c[(liftM_trm s 0) / k]\<^sup>C)" |
  "(<i> t)[s/k]\<^sup>C = <i> (t[s/k]\<^sup>T)" |
  "(<\<top>> t)[s/k]\<^sup>C = <\<top>> (t[s/k]\<^sup>T)" |
  "(TAbs t)[s/k]\<^sup>T = TAbs (t[liftT_trm s 0/k]\<^sup>T)" |
  "(TApp t \<tau>)[s/k]\<^sup>T = TApp (t[s/k]\<^sup>T) \<tau>" |
  
  "Zero[s/k]\<^sup>T = Zero" |
  "(S t)[s/k]\<^sup>T = (S (t[s/k]\<^sup>T))" |
  "(Nrec T t u v)[s/k]\<^sup>T = Nrec T (t[s/k]\<^sup>T) (u[s/k]\<^sup>T) (v[s/k]\<^sup>T)" |

  "true[s/k]\<^sup>T = true" |
  "false[s/k]\<^sup>T = false" |
  "(If T t1 Then t2 Else t3)[s/k]\<^sup>T = (If T (t1[s/k]\<^sup>T) Then (t2[s/k]\<^sup>T) Else (t3[s/k]\<^sup>T))" |

  "(\<lparr>t1, t2\<rparr>:T)[s/k]\<^sup>T = \<lparr>t1[s/k]\<^sup>T, t2[s/k]\<^sup>T\<rparr>:T" |
  "(\<pi>\<^sub>1 t)[s/k]\<^sup>T = \<pi>\<^sub>1 (t[s/k]\<^sup>T)" |
  "(\<pi>\<^sub>2 t)[s/k]\<^sup>T = \<pi>\<^sub>2 (t[s/k]\<^sup>T)" |
  
  "(Inl T t)[s/k]\<^sup>T = Inl T (t[s/k]\<^sup>T)" |
  "(Inr T t)[s/k]\<^sup>T = Inr T (t[s/k]\<^sup>T)" |
  "(Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2)[s/k]\<^sup>T 
        = Case T t0[s/k]\<^sup>T Of Inl\<Rightarrow> t1[(liftL_trm s 0) / k+1]\<^sup>T|Inr\<Rightarrow> (t2[(liftL_trm s 0) / k+1]\<^sup>T)"
  
fun subst_type_trm :: "trm \<Rightarrow> nat \<Rightarrow> type \<Rightarrow> trm"
    and subst_type_cmd :: "cmd \<Rightarrow> nat \<Rightarrow> type \<Rightarrow> cmd" where
  "subst_type_trm (`i) s k = (`i)" |
  "subst_type_trm (\<lambda> T : t) s k = \<lambda> (type_subst T s k) : (subst_type_trm t s k)" |
  "subst_type_trm (t \<degree> u) s k = subst_type_trm t s k \<degree> subst_type_trm u s k" |
  "subst_type_trm (\<mu> T : c) s k  = \<mu> (type_subst T s k) : (subst_type_cmd c s k)" |
  "subst_type_cmd (<i> t) s k = <i> (subst_type_trm t s k)" |
  "subst_type_cmd (<\<top>> t) s k = <\<top>> (subst_type_trm t s k)" |
  "subst_type_trm (TAbs t) s k = TAbs (subst_type_trm t (Suc s) (liftT_type k 0))" |
  "subst_type_trm (TApp t \<tau>) s k = TApp (subst_type_trm t s k) (type_subst \<tau> s k)" |
  
  "subst_type_trm Zero s k = Zero" |
  "subst_type_trm (S t) s k = (S (subst_type_trm t s k))" |
  "subst_type_trm (Nrec T t u v) s k = Nrec (type_subst T s k) (subst_type_trm t s k) (subst_type_trm u s k) (subst_type_trm v s k)" |

  "subst_type_trm true s k = true" |
  "subst_type_trm false s k = false" |
  "subst_type_trm (If T t1 Then t2 Else t3) s k = (If (type_subst T s k) (subst_type_trm t1 s k) Then (subst_type_trm t2 s k) Else (subst_type_trm t3 s k))" |

  "subst_type_trm (\<lparr>t1, t2\<rparr>:T) s k = \<lparr>subst_type_trm t1 s k, subst_type_trm t2 s k\<rparr>:(type_subst T s k)" |
  "subst_type_trm (\<pi>\<^sub>1 t) s k = \<pi>\<^sub>1 (subst_type_trm t s k)" |
  "subst_type_trm (\<pi>\<^sub>2 t) s k = \<pi>\<^sub>2 (subst_type_trm t s k)" |
  
  "subst_type_trm (Inl T t) s k = Inl (type_subst T s k) (subst_type_trm t s k)" |
  "subst_type_trm (Inr T t) s k = Inr (type_subst T s k) (subst_type_trm t s k)" |
  "subst_type_trm (Case T t0 Of Inl\<Rightarrow> t1|Inr\<Rightarrow> t2) s k 
        = Case (type_subst T s k) (subst_type_trm t0 s k) Of Inl\<Rightarrow> subst_type_trm t1 s k |Inr\<Rightarrow> subst_type_trm t2 s k"

subsection\<open>Context substitution\<close>

primrec ctxt_subst :: "ctxt \<Rightarrow> trm \<Rightarrow> trm" where
  "ctxt_subst \<diamond> s = s" |
  "ctxt_subst (CSuc E) s = S (ctxt_subst E s)" |
  "ctxt_subst (E \<^sup>\<bullet> t) s = (ctxt_subst E s)\<degree> t" |
  "ctxt_subst (CNrec T r t E) s = (Nrec T r t (ctxt_subst E s))" |
  "ctxt_subst (CIf T E t2 t3) s = If T (ctxt_subst E s) Then t2 Else t3" |
  
  "ctxt_subst (t \<^sup>\<bullet>\<^sup>\<tau> \<tau>) s = TApp (ctxt_subst t s) \<tau>" |
  
  "ctxt_subst (\<lparr>E, t\<rparr>\<^sub>l:T) s = \<lparr>(ctxt_subst E s), t\<rparr>:T" |
  "ctxt_subst (\<lparr>t, E\<rparr>\<^sub>r:T) s = \<lparr>t, (ctxt_subst E s)\<rparr>:T" |
  "ctxt_subst (\<Pi>\<^sub>1 E) s = \<pi>\<^sub>1 (ctxt_subst E s)" |
  "ctxt_subst (\<Pi>\<^sub>2 E) s = \<pi>\<^sub>2 (ctxt_subst E s)" |

  "ctxt_subst (CInl T E) s = Inl T (ctxt_subst E s)" |
  "ctxt_subst (CInr T E) s = Inr T (ctxt_subst E s)" |
  "ctxt_subst (CCase T E Of CInl\<Rightarrow> t2|CInr\<Rightarrow> t3) s = Case T (ctxt_subst E s) Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3 "

lemma ctxt_subst_ctxt_app: "ctxt_subst E (ctxt_subst F t) = ctxt_subst (E . F) t"
  by (induction E, auto)

subsection\<open>Structural substitution\<close>

fun
  struct_subst_trm :: "[trm, nat option, nat option, ctxt] \<Rightarrow> trm"  ("_[_=_ _]\<^sup>T" [300, 0, 0, 0] 300) and
  struct_subst_cmd ::  "[cmd, nat option, nat option, ctxt] \<Rightarrow> cmd" ("_[_=_ _]\<^sup>C" [300, 0, 0, 0] 300)
where
  "(`i)[j=k E]\<^sup>T = (`i)" |
  "(\<lambda> T : t)[j=k E]\<^sup>T = (\<lambda> T : (t[j=k (liftL_ctxt E 0)]\<^sup>T))" |
  "(t\<degree>s)[j=k E]\<^sup>T = (t[j=k E]\<^sup>T)\<degree>(s[j=k E]\<^sup>T)" |
  
  "(TAbs t)[j = k E]\<^sup>T = TAbs (t[j = k (liftT_ctxt E 0)]\<^sup>T)" |
  "(TApp t \<tau>)[j = k E]\<^sup>T = TApp (t[j = k E]\<^sup>T) \<tau>" |
  
  "(\<mu> T : c)[(Some j)=(Some k) E]\<^sup>T =  \<mu> T : (c[(Some (j+1))=(Some (k+1)) (liftM_ctxt E 0)]\<^sup>C)" |
  "(\<mu> T : c)[None=(Some k) E]\<^sup>T = \<mu> T : (c[None=(Some (k+1)) (liftM_ctxt E 0)]\<^sup>C)" |
  "(\<mu> T : c)[(Some j)=None E]\<^sup>T = \<mu> T : (c[(Some (j+1))=None (liftM_ctxt E 0)]\<^sup>C)" |
  "(\<mu> T : c)[None=None E]\<^sup>T = \<mu> T : (c[None=None (liftM_ctxt E 0)]\<^sup>C)" |
  
  "(<i> t)[(Some j)=(Some k) E]\<^sup>C = 
      (if i=j then (<k> (ctxt_subst E (t[(Some j)=(Some k) E]\<^sup>T))) 
       else (if j<i \<and> i\<le>k then (<i-1> (t[(Some j)=(Some k) E]\<^sup>T))
         else (if k\<le>i \<and> i<j then (<i+1> (t[(Some j)=(Some k) E]\<^sup>T))
               else (<i> (t[(Some j)=(Some k) E]\<^sup>T)))))" |
  "(<i> t)[None=k E]\<^sup>C = <i> (t[None=k E]\<^sup>T)" |
  "(<i> t)[(Some j)=None E]\<^sup>C = 
      (if i=j then (<\<top>> (ctxt_subst E (t[(Some j)=None E]\<^sup>T)))
         else (<i> (t[(Some j)=None E]\<^sup>T)))" |
  
  "Zero[j=k E]\<^sup>T = Zero" |
  "(S t)[j=k E]\<^sup>T = S (t[j=k E]\<^sup>T)" |
  "(Nrec T r s t)[j=k E]\<^sup>T = Nrec T (r[j=k E]\<^sup>T) (s[j=k E]\<^sup>T) (t[j=k E]\<^sup>T)" |

  "(<\<top>> t)[(Some j)=k E]\<^sup>C = <\<top>> (t[(Some j)=k E]\<^sup>T)" |
  "(<\<top>> t)[None=(Some k) E]\<^sup>C = <k> (ctxt_subst E (t[None=(Some k) E]\<^sup>T))" |
  "(<\<top>> t)[None=None E]\<^sup>C = <\<top>> (ctxt_subst E (t[None=None E]\<^sup>T))" |

  "true[j=k E]\<^sup>T = true" |
  "false[j=k E]\<^sup>T = false" |
  "(If T t1 Then t2 Else t3)[j=k E]\<^sup>T = (If T (t1[j=k E]\<^sup>T) Then (t2[j=k E]\<^sup>T) Else (t3[j=k E]\<^sup>T))" |

  "(\<lparr>t1, t2\<rparr>:T)[j=k E]\<^sup>T = \<lparr>t1[j=k E]\<^sup>T, t2[j=k E]\<^sup>T\<rparr>:T" |
  "(\<pi>\<^sub>1 t)[j=k E]\<^sup>T = \<pi>\<^sub>1 (t[j=k E]\<^sup>T)" |
  "(\<pi>\<^sub>2 t)[j=k E]\<^sup>T = \<pi>\<^sub>2 (t[j=k E]\<^sup>T)" |

  "(Inl T t)[j=k E]\<^sup>T = Inl T (t[j=k E]\<^sup>T)" |
  "(Inr T t)[j=k E]\<^sup>T = Inr T (t[j=k E]\<^sup>T)" |
  "(Case T t1 Of Inl\<Rightarrow> t2|Inr\<Rightarrow> t3)[j=k E]\<^sup>T 
              = Case T t1[j=k E]\<^sup>T Of Inl\<Rightarrow> t2[j=k (liftL_ctxt E 0)]\<^sup>T|Inr\<Rightarrow> (t3[j=k (liftL_ctxt E 0)]\<^sup>T)"

lemma liftM_struct_subst: 
  "(liftM_trm t i)[(Some i)=(Some i) F]\<^sup>T = liftM_trm t i"
  "\<forall>i. \<forall>F. liftM_cmd c i[(Some i)=(Some i) F]\<^sup>C = liftM_cmd c i"
  by(induct t and c arbitrary: i F) auto

lemma liftM_ctxt_struct_subst:
  "(ctxt_subst (liftM_ctxt E i) t)[(Some i)=(Some i) F]\<^sup>T = ctxt_subst (liftM_ctxt E i) (t[(Some i)=(Some i) F]\<^sup>T)"
  by(induct E arbitrary: i t F) (force simp add: liftM_struct_subst)+

end